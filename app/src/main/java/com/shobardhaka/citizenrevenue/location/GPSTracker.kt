package com.shobardhaka.citizenrevenue.location

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.*
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import com.shobardhaka.citizenrevenue.utils.showToast
import java.io.IOException
import java.util.*

class GPSTracker(private val mContext: Context) : LocationListener {

    override fun onLocationChanged(location: Location?) {
        mContext.showToast("Location Changed Called")
        location?.let {
            mCurrentLocation = location
        }
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

    }

    override fun onProviderEnabled(provider: String?) {
        if (provider == LocationManager.GPS_PROVIDER) {
            startUsingGPS()
        }
    }

    override fun onProviderDisabled(provider: String?) {

    }

    // flag for GPS status
    private var mCurrentLocation: Location? = null // location
    private var mLatitude: Double = 0.toDouble() // mLatitude
    private var mLongitude: Double = 0.toDouble() // mLongitude

    // Declaring a Location Manager
    private var mLocationManager: LocationManager? = null

    init {
        mLocationManager = mContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    @SuppressLint("MissingPermission")
    fun startUsingGPS() {
        try {
            mLocationManager?.let {
                if (isGPSEnabled()) {
                    mLocationManager!!.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            LOCATION_REFRESH_TIME,
                            LOCATION_REFRESH_DISTANCE, this
                    )
                    mCurrentLocation = mLocationManager!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                    if (mCurrentLocation != null) {
                        mLatitude = mCurrentLocation!!.latitude
                        mLongitude = mCurrentLocation!!.longitude
                    }

                }
            }

        } catch (e: Exception) {
        }
    }

    /**
     * Stop using GPS listener Calling this function will stop using GPS in your
     * app.
     */
    fun stopUsingGPS() {
        mLocationManager?.let {
            mLocationManager!!.removeUpdates(this@GPSTracker)
        }
    }

    fun getLatitude(): Double {
        mCurrentLocation?.let {
            mLatitude = mCurrentLocation!!.latitude
        }
        return mLatitude
    }

    fun getLongitude(): Double {
        mCurrentLocation?.let {
            mLongitude = mCurrentLocation!!.longitude
        }
        return mLongitude
    }

    fun isGPSEnabled(): Boolean {
        var isEnabled = false
        mLocationManager?.let {
            isEnabled = mLocationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
        }
        return isEnabled
    }

    fun showSettingsAlert() {
        val alertDialog = AlertDialog.Builder(mContext)
        alertDialog.setTitle("GPS is settings")
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?")
        alertDialog.setPositiveButton(
                "Settings"
        ) { _, _ ->
            val intent = Intent(
                    Settings.ACTION_LOCATION_SOURCE_SETTINGS
            )
            mContext.startActivity(intent)
        }
        alertDialog.setNegativeButton(
                "Cancel"
        ) { dialog, _ -> dialog.cancel() }
        alertDialog.create().show()
    }

    fun getCurrentAddress(latitude: Double = mLatitude, longitude: Double = mLatitude): Address? {
        val geocoder = Geocoder(mContext, Locale.getDefault())
        val addresses: List<Address>
        if(latitude != 0.0 && longitude != 0.0){
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                return addresses[0]
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return null
    }

    companion object {
        // The minimum distance to change Updates in meters
        const val LOCATION_REFRESH_DISTANCE: Float = 10f // 10 meters
        // The minimum time between updates in milliseconds
        const val LOCATION_REFRESH_TIME: Long = (1000 * 5 * 1).toLong() // 1 minute
    }

}