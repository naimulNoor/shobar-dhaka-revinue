package com.shobardhaka.citizenrevenue.location

import android.annotation.SuppressLint
import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import java.io.IOException
import java.util.*

class FusedLocationTracker(private var mContext: Context?) :
    LocationListener,
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {

    private val INTERVAL = (1000 * 10).toLong() // 10 Sec
    private val LOCATION_REFRESH_TIME = (1000 * 2).toLong() // 5 Sec

    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLocationRequest: LocationRequest? = null
    private var mFusedLocationProviderClient: FusedLocationProviderClient? = null
    private var mCurrentLocation: Location? = null
    private var mLatitude: Double = 0.toDouble() // mLatitude
    private var mLongitude: Double = 0.toDouble() // mLongitude

    private var mFusedLocationChangedListener: OnFusedLocationChangedListener? = null

    init {
        buildGoogleApiClient()
        buildLocationRequest()
        mContext?.let {
            mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mContext!!)
        }
    }

    fun setFusedLocationChangeListener(fusedLocationChangedListener: OnFusedLocationChangedListener) {
        this.mFusedLocationChangedListener = fusedLocationChangedListener
    }

    override fun onLocationChanged(location: Location?) {
        if (location != null) {
            mCurrentLocation = location
            mFusedLocationChangedListener?.let {
                val locStr = "${getCurrentAddress(getLatitude(), getLongitude())?.getAddressLine(0)}"
                it.onLocationChanged(locStr)
            }
        }
    }

    override fun onConnected(p0: Bundle?) {
        startLocationUpdates()
    }

    override fun onConnectionSuspended(p0: Int) {
        mGoogleApiClient?.let {
            mGoogleApiClient!!.connect()
        }
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }

    private fun buildGoogleApiClient() {
        mContext?.let {
            mGoogleApiClient = GoogleApiClient.Builder(mContext!!)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
        }
    }

    private fun buildLocationRequest() {
        mLocationRequest = LocationRequest()
            .setInterval(INTERVAL)
            .setFastestInterval(LOCATION_REFRESH_TIME)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        mGoogleApiClient?.let {
            if (mGoogleApiClient!!.isConnected) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
            }
        }
    }

    fun stopLocationUpdates() {
        mGoogleApiClient?.let {
            if (mGoogleApiClient!!.isConnected) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)

            }
        }
    }

    fun connect() {
        mGoogleApiClient?.let {
            mGoogleApiClient!!.connect()
        }
    }

    fun disConnect() {
        mGoogleApiClient?.let { apiClient ->
            if (apiClient.isConnected) {
                apiClient.disconnect()
            }
        }
//        mLocationRequest?.let {
//            mLocationRequest = null
//        }
//
//        mFusedLocationProviderClient?.let {
//            mFusedLocationProviderClient = null
//        }
//
//        mFusedLocationChangedListener?.let {
//            mFusedLocationChangedListener = null
//        }
//
//        mContext?.let {
//            mContext = null
//        }

    }

    fun getLatitude(): Double {
        mCurrentLocation?.let {
            mLatitude = mCurrentLocation!!.latitude
        }
        return mLatitude
    }

    fun getLongitude(): Double {
        mCurrentLocation?.let {
            mLongitude = mCurrentLocation!!.longitude
        }
        return mLongitude
    }


    fun getCurrentAddress(latitude: Double = mLatitude, longitude: Double = mLatitude): Address? {
        val geoCoder = Geocoder(mContext, Locale.getDefault())
        val addresses: List<Address>
        if (latitude != 0.0 && longitude != 0.0) {
            try {
                addresses = geoCoder.getFromLocation(
                    latitude,
                    longitude,
                    1
                ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                return addresses[0]
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return null
    }

    @SuppressLint("MissingPermission")
    fun lastLocation() {
        mFusedLocationProviderClient!!.lastLocation.addOnSuccessListener(
            mContext as AppCompatActivity
        ) { location -> mCurrentLocation = location }
    }

    interface OnFusedLocationChangedListener {
        fun onLocationChanged(location: String)
    }

}