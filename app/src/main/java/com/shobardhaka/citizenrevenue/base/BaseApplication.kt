package com.shobardhaka.citizenrevenue.base

import android.app.Activity
import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import com.shobardhaka.citizenrevenue.di.component.DaggerAppComponent
import javax.inject.Inject
import com.crashlytics.android.Crashlytics
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.data.prefs.PrefKeys
import com.shobardhaka.citizenrevenue.data.prefs.PreferenceManager
import com.shobardhaka.citizenrevenue.sms_retriver.AppSignatureHashHelper
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import io.fabric.sdk.android.Fabric


class BaseApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var mActivityInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var mPref: PreferenceManager

    var mAppLanguage: String? = null

    private val TAG = BaseApplication::class.java.simpleName

    @Inject
    lateinit var mPrefManager: PreferenceManager

    override fun onCreate() {
        super.onCreate()
        initDagger()
        loadAppLanguage()
        Fabric.with(this, Crashlytics())
        genHexStringForSmsRetrieverAPI()
    }

    private fun genHexStringForSmsRetrieverAPI(){
        val appSignatureHashHelper = AppSignatureHashHelper(this)
        mPref.putString(PrefKeys.APP_SIGNING_KEY, appSignatureHashHelper.appSignatures[0])

       // Log.e(TAG, "HashKey: " + appSignatureHashHelper.appSignatures[0])
    }

    private fun initDagger() {
        DaggerAppComponent.builder()
            .application(this)
            .build()
            .inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return mActivityInjector
    }


    private fun loadAppLanguage() {
        mAppLanguage = mPrefManager.getString(Keys.APP_LANGUAGE.name, AppLanguage.BENGALI.name)
        when (mAppLanguage) {
            AppLanguage.BENGALI.name -> AppUtils.shared.setLocale(this, AppLanguage.BENGALI)
            AppLanguage.ENGLISH.name -> AppUtils.shared.setLocale(this, AppLanguage.ENGLISH)
        }
    }
}