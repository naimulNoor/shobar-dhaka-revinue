package com.shobardhaka.citizenrevenue.base

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import butterknife.ButterKnife
import butterknife.Unbinder
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.data.prefs.PreferenceManager
import com.shobardhaka.citizenrevenue.ui.auth.login.LoginActivity
import com.shobardhaka.citizenrevenue.utils.*
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

abstract class MvpBaseActivity<P : BaseContract.Presenter> : AppCompatActivity(), BaseContract.View,
    HasSupportFragmentInjector {

    protected val TAG: String by lazy {
        this.javaClass.simpleName
    }

    private var progressDialog: ProgressDialog? = null
    private var unBinder: Unbinder? = null

    @Inject
    lateinit var mFragmentInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var mPresenter: P

    @Inject
    lateinit var mAlertService: AlertService

    @Inject
    lateinit var mNetworkUtils: NetworkUtils

    @Inject
    lateinit var mAppLogger: AppLogger

    @Inject
    lateinit var mPrefManager: PreferenceManager

    var mAppLanguage: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        loadAppLanguage()
        setContentView(getContentView())
        unBinder = ButterKnife.bind(this)
        supportActionBar?.let {
            supportActionBar!!.apply {
                setDisplayHomeAsUpEnabled(true)
                setDisplayShowHomeEnabled(true)
            }
        }
        onViewReady(savedInstanceState, intent)
    }

    abstract fun getContentView(): Int
    abstract fun onViewReady(savedInstanceState: Bundle?, intent: Intent)

    fun showProgressDialog(message: String) {
        if (null == progressDialog) {
            progressDialog = ProgressDialog(this, R.style.MyAlertDialogStyle)
            progressDialog!!.setCancelable(false)
        }
        progressDialog!!.setMessage(message)
        if (!progressDialog!!.isShowing) {
            progressDialog!!.show()
        }
    }

    fun hideProgressDialog() {
        progressDialog?.let {
            if (progressDialog!!.isShowing) {
                progressDialog!!.dismiss()
            }
            progressDialog = null
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            if (item.itemId == android.R.id.home) {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        mPresenter.detachView()
        mAlertService.onDestroy()
        progressDialog?.let {
            if (progressDialog!!.isShowing) {
                progressDialog!!.dismiss()
            }
            progressDialog = null
        }
        unBinder?.let {
            unBinder!!.unbind()
            unBinder = null
        }
        mPresenter.clearDisposable()

//        if (BuildConfig.DEBUG) {
//            AppWatcher.objectWatcher.watch(this, TAG)
//        }
        super.onDestroy()
    }

//    override fun supportFragmentInjector(): AndroidInjector<androidx.fragment.app.Fragment> {
//        return mFragmentInjector
//    }

    override fun getContext(): Context {
        return this
    }

    override fun onNetworkCallStarted(loadingMessage: String) {
        showProgressDialog(getString(R.string.please_wait))
    }

    override fun onNetworkCallEnded() {
        hideProgressDialog()
    }

    override fun onNetworkUnavailable() {
        mAlertService.showAlert(
            getContext(),
            getString(R.string.no_internet_connection),
            getString(R.string.no_internet_msg)
        )
    }

    override fun onServerError() {
        mAlertService.showToast(getContext(), getString(R.string.something_wrong))
    }

    override fun onTimeOutError() {
        mAlertService.showToast(getContext(), getString(R.string.something_wrong))
    }

    override fun onUserDidTooManyAttempts(errorMsg: String) {
        // showToast(errorMsg)
    }

    override fun onUserUnauthorized(errorMessage: String) {
        mPrefManager.clearPreference()
        Navigator.sharedInstance.back(getContext(), LoginActivity::class.java)
        finish()
    }

    override fun onUserDisabled(errorMsg: String) {
        mAlertService.showConfirmationAlert(
            getContext(),
            null,
            errorMsg,
            null,
            getString(R.string.okay),
            object : AlertService.AlertListener {
                override fun negativeBtnDidTapped() {

                }

                override fun positiveBtnDidTapped() {
                    System.exit(0)
                }
            })
    }

    override fun onSystemUpgrading() {
        mAlertService.showConfirmationAlert(
            getContext(),
            getString(R.string.maintenance_title),
            getString(R.string.maintenance_message),
            null,
            getString(R.string.okay),
            object : AlertService.AlertListener {
                override fun negativeBtnDidTapped() {

                }

                override fun positiveBtnDidTapped() {
                    System.exit(0)
                }
            })
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return mFragmentInjector
    }

    private fun loadAppLanguage() {
        mAppLanguage = mPrefManager.getString(Keys.APP_LANGUAGE.name, AppLanguage.BENGALI.name)
        when (mAppLanguage) {
            AppLanguage.BENGALI.name -> AppUtils.shared.setLocale(getContext(), AppLanguage.BENGALI)
            AppLanguage.ENGLISH.name -> AppUtils.shared.setLocale(getContext(), AppLanguage.ENGLISH)
        }
    }
}