package com.shobardhaka.citizenrevenue.base

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import butterknife.ButterKnife
import butterknife.Unbinder
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.data.prefs.PreferenceManager
import com.shobardhaka.citizenrevenue.ui.auth.login.LoginActivity
import com.shobardhaka.citizenrevenue.utils.*
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

abstract class BaseFragment<P : BaseContract.Presenter> : Fragment(), BaseContract.View {

    protected val TAG: String  by lazy {
        this.javaClass.simpleName
    }

    private var progressDialog: ProgressDialog? = null
    private var viewUnbinder: Unbinder? = null

    @Inject
    lateinit var mPresenter: P

    @Inject
    lateinit var mAlertService: AlertService

    @Inject
    lateinit var mNetworkUtils: NetworkUtils

    @Inject
    lateinit var mAppLogger: AppLogger

    @Inject
    lateinit var mPrefManager: PreferenceManager

    lateinit var rootView: View

    var mAppLanguage: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadAppLanguage()
    }

    @Nullable
    override fun onCreateView(@NonNull inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        rootView = getFragmentView(inflater, container, savedInstanceState)
        viewUnbinder = ButterKnife.bind(this, rootView)
        onViewReady(arguments)
        return rootView
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }


    protected abstract fun getFragmentView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        saveInstanceState: Bundle?
    ): View

    protected abstract fun onViewReady(getArguments: Bundle?)

    private fun showProgressDialog(message: String) {
        if (null == progressDialog) {
            progressDialog = ProgressDialog(activity)
            progressDialog!!.setCancelable(false)
        }
        progressDialog!!.setMessage(message)
        if (!progressDialog!!.isShowing) {
            progressDialog!!.show()
        }
    }

    private fun hideProgressDialog() {
        progressDialog?.let {
            if (progressDialog!!.isShowing) {
                progressDialog!!.dismiss()
            }
            progressDialog = null
        }
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
//        if (BuildConfig.DEBUG) {
//            AppWatcher.objectWatcher.watch(this, TAG)
//        }
        mPresenter.clearDisposable()
        mPresenter.detachView()
        progressDialog?.let {
            if (progressDialog!!.isShowing) {
                progressDialog!!.dismiss()
            }
        }
        viewUnbinder?.let {
            viewUnbinder!!.unbind()
            viewUnbinder = null
        }
        super.onDestroy()
    }


    override fun getContext(): Context {
        return activity!!
    }

    override fun onNetworkCallStarted(loadingMessage: String) {
        if (activity != null)
            showProgressDialog(loadingMessage)
    }

    override fun onNetworkCallEnded() {
        if (activity != null)
            hideProgressDialog()
    }

    override fun onNetworkUnavailable() {
        if (activity != null)
            mAlertService.showAlert(
                context,
                getString(R.string.no_internet_connection),
                getString(R.string.no_internet_msg)
            )
    }

    override fun onServerError() {
        if (activity != null)
            mAlertService.showToast(context, getString(R.string.something_wrong))
    }

    override fun onTimeOutError() {
        if (activity != null)
            mAlertService.showToast(context, getString(R.string.something_wrong))
    }

    override fun onUserDidTooManyAttempts(errorMsg: String) {
        mAlertService.showToast(context, errorMsg)
    }

    override fun onUserUnauthorized(errorMessage: String) {
        if (activity != null) {
            mPrefManager.clearPreference()
            Navigator.sharedInstance.back(context, LoginActivity::class.java)
        }
    }

    override fun onUserDisabled(errorMsg: String) {
        mAlertService.showConfirmationAlert(context, null, errorMsg, null, getString(R.string.okay), object : AlertService.AlertListener{
            override fun negativeBtnDidTapped() {

            }
            override fun positiveBtnDidTapped() {
                System.exit(0)
            }
        })
    }

    override fun onSystemUpgrading() {
        if (activity != null)
            mAlertService.showConfirmationAlert(getContext(), getString(R.string.maintenance_title), getString(R.string.maintenance_message), null, getString(R.string.okay), object :AlertService.AlertListener{
                override fun negativeBtnDidTapped() {

                }
                override fun positiveBtnDidTapped() {
                    System.exit(0)
                }
            })
    }

    private fun loadAppLanguage() {
        if (activity != null) {
            mAppLanguage = mPrefManager.getString(Keys.APP_LANGUAGE.name, AppLanguage.BENGALI.name)
            when (mAppLanguage) {
                AppLanguage.BENGALI.name -> AppUtils.shared.setLocale(context, AppLanguage.BENGALI)
                AppLanguage.ENGLISH.name -> AppUtils.shared.setLocale(context, AppLanguage.ENGLISH)
            }
        }
    }
}