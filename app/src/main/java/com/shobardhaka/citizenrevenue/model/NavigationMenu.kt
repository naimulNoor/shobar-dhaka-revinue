package com.shobardhaka.citizenrevenue.model

/**
 * Created on 21/08/2019.
 */

data class NavigationMenu(
        var menuName: String,
        var menuIcon: Int,
        var submenus: ArrayList<String>
)