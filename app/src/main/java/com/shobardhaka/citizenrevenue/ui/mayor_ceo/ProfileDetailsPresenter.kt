package com.shobardhaka.citizenrevenue.ui.mayor_ceo

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.MayorCeoResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import javax.inject.Inject

class ProfileDetailsPresenter @Inject constructor(view: ProfileDetailsContract.View) :
    BasePresenter<ProfileDetailsContract.View>(view),
    ProfileDetailsContract.Presenter {

    @Inject
    lateinit var userApiService: UserApiService


    override fun getProfileDetails() {
        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            userApiService.getMayorCeoInfo()
                .subscribeOn(appSchedulerProvider.io())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<MayorCeoResponse, ProfileDetailsContract.View>(mView) {
                    override fun onRequestSuccess(response: MayorCeoResponse) {
                        mView?.profileDetailsDidReceived(response)
                    }
                })
        )
    }
}