package com.shobardhaka.citizenrevenue.ui.dncc_office

import dagger.Binds
import dagger.Module

@Module
abstract class NearbyDnccViewModule {

    @Binds
    abstract fun provideNearbDnccPlaceView(activity: NearbyDnccOfficeActivity): NearbyDnccContract.View
}