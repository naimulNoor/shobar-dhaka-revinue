package com.shobardhaka.citizenrevenue.ui.auth.get_otp

import dagger.Binds
import dagger.Module

@Module
abstract class GetOtpViewModule {

    @Binds
    abstract fun bindGetOtpView(activity: GetOtpActivity): GetOtpContract.View

}