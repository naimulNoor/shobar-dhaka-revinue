package com.shobardhaka.citizenrevenue.ui.complaints.complaint_details

import dagger.Binds
import dagger.Module

@Module
abstract class ComplaintDetailsViewModule {

    @Binds
    abstract fun provideComplainDetailsView(activity: ComplaintDetailsActivity): ComplaintDetailsContract.View
}