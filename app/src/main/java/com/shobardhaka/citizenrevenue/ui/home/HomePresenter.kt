package com.shobardhaka.citizenrevenue.ui.home

import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.PostDataResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import javax.inject.Inject

class HomePresenter @Inject constructor(view: HomeContract.View) : BasePresenter<HomeContract.View>(view),
    HomeContract.Presenter {

    @Inject
    lateinit var userApiService: UserApiService

    override fun storeDeviceToken(deviceToken: String) {
        compositeDisposable?.add(
            userApiService.storeDeviceToken(deviceToken)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<PostDataResponse, HomeContract.View>(mView) {
                    override fun onRequestSuccess(response: PostDataResponse) {
                        // Nothing to do....
                    }
                })
        )
    }

}