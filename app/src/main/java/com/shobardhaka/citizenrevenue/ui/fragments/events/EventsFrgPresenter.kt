package com.shobardhaka.citizenrevenue.ui.fragments.events

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.GetEventResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import javax.inject.Inject

class EventsFrgPresenter @Inject constructor(view: EventsFrgContract.View) :
    BasePresenter<EventsFrgContract.View>(view),
    EventsFrgContract.Presenter {

    @Inject
    lateinit var userApiService: UserApiService


    override fun getEvents() {
        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            userApiService.getEvents()
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<GetEventResponse, EventsFrgContract.View>(mView) {
                    override fun onRequestSuccess(response: GetEventResponse) {
                        mView?.eventsDidReceived(response.events)
                    }

                })
        )
    }

}