package com.shobardhaka.citizenrevenue.ui.event_detials

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.JoinEventResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import javax.inject.Inject

class EventDetailsPresenter @Inject constructor(view: EventDetialsContract.View) :
    BasePresenter<EventDetialsContract.View>(view), EventDetialsContract.Presenter {

    @Inject
    lateinit var userApiService: UserApiService


    override fun joinEvent(eventData: HashMap<String, String>) {

        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            userApiService.joinEvent(eventData)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<JoinEventResponse, EventDetialsContract.View>(mView) {

                    override fun onRequestSuccess(response: JoinEventResponse) {
                        if (response.status == 200) {
                            mView?.joinDidSucceed(response.message)
                        } else if (response.status == 423) {
                            mView?.joinDidFailed(response.error)
                        }
                    }
                })
        )
    }
}