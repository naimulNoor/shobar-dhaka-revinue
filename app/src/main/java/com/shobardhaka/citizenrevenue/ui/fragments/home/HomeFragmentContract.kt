package com.shobardhaka.citizenrevenue.ui.fragments.home

import com.shobardhaka.citizenrevenue.base.BaseContract
import com.shobardhaka.citizenrevenue.data.network.api_response.*
import java.util.*

interface HomeFragmentContract{
    interface View : BaseContract.View {
        fun sendEmergencyAlertDidSucceed()
        fun sendEmergencyAlertDidFailed(failedMsg: String)

        // Complain Status
        fun complainStatusDidReceived(response: ComplainStatusResponse)

        // NOTICE
        fun noticeDidReceived(notices: ArrayList<GetNoticeResponse.SingleNotice?>?)

        // Check GeoFencing..
        fun geoFencingDidChecked(isUnderDNCC: Boolean)

        // Un read notificaiton
        fun unReadNotificationDidReceived(response: GetUnReadNotificaitonResponse)

        // Address
        fun addressDidReceived(address: String)
    }

    interface Presenter : BaseContract.Presenter {
        fun sendEmergencyAlert(dataMap: HashMap<String, String>)
        fun getComplainStatus()
        fun getNotice()
        fun checkGeoFencing(url: String)

        // Notification
        fun getUnreadNotificationCount()

        // Get Address
        fun getAddressFromBarikoiByLatLong(url: String)
    }
}