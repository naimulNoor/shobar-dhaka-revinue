package com.shobardhaka.citizenrevenue.ui.fragments.notification

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.NotificationResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import javax.inject.Inject

class NotificationFrgPresenter @Inject constructor(view: NotificationFrgContract.View) :
    BasePresenter<NotificationFrgContract.View>(view), NotificationFrgContract.Presenter {

    @Inject
    lateinit var userApiService: UserApiService

    override fun getNotifications() {
        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            userApiService.getNotifications()
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<NotificationResponse, NotificationFrgContract.View>(mView) {
                    override fun onRequestSuccess(response: NotificationResponse) {
                        mView?.notificationsDidReceived(response.notifications)
                    }
                })
        )
    }

    override fun setNotificationAsRead(url: String) {
        compositeDisposable?.add(
            userApiService.setNotificationAsSeen(url)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<NotificationResponse.SingleNotification, NotificationFrgContract.View>(
                        mView
                    ) {
                    override fun onRequestSuccess(response: NotificationResponse.SingleNotification) {
                        // No need to notify
                    }
                })
        )
    }

}