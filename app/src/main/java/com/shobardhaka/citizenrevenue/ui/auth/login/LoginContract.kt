package com.shobardhaka.citizenrevenue.ui.auth.login

import com.shobardhaka.citizenrevenue.base.BaseContract

interface LoginContract {

    interface View : BaseContract.View {

    }

    interface Presenter : BaseContract.Presenter {

    }
}