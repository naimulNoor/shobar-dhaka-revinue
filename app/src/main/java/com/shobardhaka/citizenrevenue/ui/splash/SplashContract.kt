package com.shobardhaka.citizenrevenue.ui.splash

import com.shobardhaka.citizenrevenue.base.BaseContract
import com.shobardhaka.citizenrevenue.data.network.api_response.GetAppVersionResponse

interface SplashContract {
    interface View : BaseContract.View {

        fun appVersionDidReceived(response: GetAppVersionResponse)
        fun addressDidReceived(address: String)
    }

    interface Presenter : BaseContract.Presenter {
        fun getAddressFromBarikoiByLatLong(url: String)
        fun getAppVersion()
    }
}