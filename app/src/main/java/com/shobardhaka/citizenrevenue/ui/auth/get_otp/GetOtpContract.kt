package com.shobardhaka.citizenrevenue.ui.auth.get_otp

import com.shobardhaka.citizenrevenue.base.BaseContract

interface GetOtpContract{
    interface View : BaseContract.View {

        fun requestOTPDidSucceed()

        fun requestOTPDidFailed(failedMsg: String)
    }

    interface Presenter : BaseContract.Presenter {

        fun requestOTP(phoneNumber: String, signingKey: String)
    }
}