package com.shobardhaka.citizenrevenue.ui.nearby.nearby_place

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.adapters.NearbyPlaceAdapter
import com.shobardhaka.citizenrevenue.adapters.RecyclerTouchListener
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.data.network.api_response.BariKoiResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.ui.maps.MapsActivity
import com.shobardhaka.citizenrevenue.utils.Navigator
import kotlinx.android.synthetic.main.activity_nearby_place.*
import kotlinx.android.synthetic.main.toolbar_with_text.*


class NearbyPlaceActivity : MvpBaseActivity<NearbyPlacePresenter>(), NearbyPlaceContract.View {

    private lateinit var nearbyPlaceAdapter: NearbyPlaceAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var animationController: LayoutAnimationController

    private var currentLocLat: Double = 0.0
    private var currentLocLong: Double = 0.0
    var locType: String? = null
    var locTypStr: String = ""

    override fun getContentView(): Int {
        return R.layout.activity_nearby_place
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {

        setSupportActionBar(toolbar)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }

        val bundle = intent.getBundleExtra(Keys.DATA_BUNDLE.name)
        currentLocLat = bundle.getDouble(Keys.CURRENT_LOC_LAT.name)
        currentLocLong = bundle.getDouble(Keys.CURRENT_LOC_LONG.name)
        locType = bundle.getString(Keys.LOCATION_TYPE.name)
        val placeTypeIcon = bundle.getInt(Keys.NEARBY_ICON.name)
        var title = ""

        animationController = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_slide_from_right);
        linearLayoutManager = LinearLayoutManager(
            getContext(),
            RecyclerView.VERTICAL,
            false
        )

        nearbyPlaceAdapter = NearbyPlaceAdapter(getContext(), placeTypeIcon)
        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = nearbyPlaceAdapter
            layoutAnimation = animationController
        }

        recyclerView.addOnItemTouchListener(
            RecyclerTouchListener(
                this,
                recyclerView,
                object : RecyclerTouchListener.ClickListener {
                    override fun onClick(view: View, position: Int) {
                        val singlePlace: BariKoiResponse.SinglePlace =
                            nearbyPlaceAdapter.getItem(position) as BariKoiResponse.SinglePlace
                        val dataBundle = Bundle()
                        dataBundle.putString("loc_type", locTypStr)
                        dataBundle.putSerializable(Keys.SINGLE_PLACE.name, singlePlace)
                        Navigator.sharedInstance.navigateWithBundle(
                            getContext(),
                            MapsActivity::class.java,
                            Keys.DATA_BUNDLE.name,
                            dataBundle
                        )
                    }

                    override fun onLongClick(view: View?, position: Int) {
                        // Nothing to do.......
                    }
                })
        )


        // Barikoi Map Api......................

        val apiKey = getString(R.string.B_API_KEY)
        var distance = 2.0
        val limit = 80
        var pType = ""


        when (locType) {
            Keys.NEARBY_BUS_STAND.name -> {
                title = getString(R.string.nearby) + " " + getString(R.string.bus_stand)
                pType = "Bus Stand"
                locTypStr = getString(R.string.bus_stand)
            }

            Keys.NEARBY_HOSPITAL.name -> {
                title = getString(R.string.nearby) + " " + getString(R.string.hospital)
                pType = "Hospital"
                locTypStr = getString(R.string.hospital)

            }

            Keys.NEARBY_MATERNITY_CLINIC.name -> {
                title = getString(R.string.nearby) + " " + getString(R.string.nearby_maternity_clinic)
                pType = "Matri Shadan"
                locTypStr = getString(R.string.nearby_maternity_clinic)
                distance = 5.0
            }

            Keys.NEARBY_POLICE_STATION.name -> {
                title = getString(R.string.nearby) + " " + getString(R.string.thana)
                pType = "Police Station"
                locTypStr = getString(R.string.thana)

            }

            Keys.NEARBY_FIRE_SERVICE.name -> {
                title = getString(R.string.nearby) + " " + getString(R.string.domkol_bahini)
                pType = "Fire Service"
                locTypStr = getString(R.string.domkol_bahini)
                distance = 5.0
            }

            Keys.NEARBY_PUBLIC_TOILET.name -> {
                title = getString(R.string.nearby) + " " + getString(R.string.public_toilet)
                pType = "Public Toilet"
                locTypStr = getString(R.string.public_toilet)
                distance = 5.0
            }

            Keys.DNCC_OFFICE.name -> {
                title = getString(R.string.dncc_office)
                pType = "Dncc"
                locTypStr = getString(R.string.dncc_office)
                distance = 5.0
            }

            Keys.NEARBY_ATM.name -> {
                title = getString(R.string.nearby) + " " + getString(R.string.nearby_atm)
                pType = "Atm"
                locTypStr = getString(R.string.nearby_atm)
            }

            Keys.NEARBY_PARKING.name -> {
                title = getString(R.string.nearby) + " " + getString(R.string.nearby_parking)
                pType = "Parking"
                locTypStr = getString(R.string.nearby_park)
            }

            Keys.NEARBY_PHARMACY.name -> {
                title = getString(R.string.nearby) + " " + getString(R.string.nearby_farmacy)
                pType = "Pharmacy"
                locTypStr = getString(R.string.nearby_farmacy)
            }

            Keys.NEARBY_COUNCILOR_OFC.name -> {
                title = getString(R.string.nearby) + " " + getString(R.string.nearby_councilor_ofc)
                pType = "Public Toilet"
                locTypStr = getString(R.string.nearby_councilor_ofc)

            }

            Keys.NEARBY_PARK.name -> {
                title = getString(R.string.nearby) + " " + getString(R.string.nearby_park)
                pType = "Park"
                locTypStr = getString(R.string.nearby_park)

            }

            Keys.NEARBY_COMMUNITY_CENTER.name -> {
                distance = 5.0
                title = getString(R.string.nearby) + " " + getString(R.string.nearby_community_center)
                pType = "Community Center"
                locTypStr = getString(R.string.nearby_community_center)
            }

            Keys.NEARBY_CNG_PUMP.name -> {
                title = getString(R.string.nearby) + " " + getString(R.string.nearby_cng_pumps)
                pType = "Fuel"
                locTypStr = getString(R.string.nearby_cng_pumps)
            }

            Keys.NEARBY_STS.name -> {
                distance = 4.0
                title = getString(R.string.nearby) + " " + getString(R.string.nearby_sts)
                pType = "STS"
                locTypStr = getString(R.string.nearby_sts)
            }
        }
        val nearByWithCategory =
            "https://barikoi.xyz/v1/api/search/nearby/category/$apiKey/$distance/$limit?longitude=$currentLocLong&latitude=$currentLocLat&ptype=$pType"
        mPresenter.getNearByPlace(nearByWithCategory)
        // title = "$title(${AppUtils.shared.getInBangla(distance.toString())} কি.মি.)"
        tvTitle.text = title
    }

    override fun nearByPlacesDidReceived(nearByPlaces: ArrayList<BariKoiResponse.SinglePlace?>?) {
        if (nearByPlaces != null && nearByPlaces.isNotEmpty()) {
            nearbyPlaceAdapter.setData(nearByPlaces)
            recyclerView.scheduleLayoutAnimation()
        }
    }

    override fun noNearbyPlaceFound(message: String) {
        mAlertService.showAlert(
            getContext(),
            null, getString(R.string.no_nearby_place_found)
        )
    }

    override fun onNetworkCallStarted(loadingMessage: String) {
        showProgressDialog(getString(R.string.please_wait))
    }

}
