package com.shobardhaka.citizenrevenue.ui.auth.verify_otp

import com.shobardhaka.citizenrevenue.base.BaseContract
import com.shobardhaka.citizenrevenue.data.network.api_response.VerifyOtpResponse

interface VerifyOtpContract{
    interface View : BaseContract.View {
        fun requestOTPDidSucceed()
        fun requestOTPDidFailed(failedMsg: String)

        fun verifyOtpDidSucceed(response: VerifyOtpResponse)
        fun verifyOtpDidFailed(failedMsg: String)
    }

    interface Presenter : BaseContract.Presenter {
        fun requestOTP(phoneNumber: String, signingKey: String)
        fun verifyOtp(mobileNumber: String, otp: String)
    }
}