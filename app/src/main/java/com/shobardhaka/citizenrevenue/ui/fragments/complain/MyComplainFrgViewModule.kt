package com.shobardhaka.citizenrevenue.ui.fragments.complain

import dagger.Binds
import dagger.Module

@Module
abstract class MyComplainFrgViewModule {

    @Binds
    abstract fun provideMyComplaintFrgView(fragment: MyComplaintsFragment): MyComplainFrgContract.View
}