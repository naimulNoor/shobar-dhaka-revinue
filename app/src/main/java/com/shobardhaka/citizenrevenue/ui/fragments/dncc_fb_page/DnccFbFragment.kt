package com.shobardhaka.citizenrevenue.ui.fragments.dncc_fb_page

import android.annotation.TargetApi
import android.app.AlertDialog

import android.content.ActivityNotFoundException
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import androidx.fragment.app.Fragment

import com.shobardhaka.citizenrevenue.R
import kotlinx.android.synthetic.main.fragment_dncc_fb.view.*


class DnccFbFragment : Fragment() {

    private var rootView: View? = null

    companion object {
        fun newInstance(): DnccFbFragment {
            return DnccFbFragment()
        }
    }

    val DNCC_FB_PAGE = "https://www.facebook.com/dncc.gov.bd/"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_dncc_fb, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        rootView?.dnccFbPageWebView?.settings?.javaScriptEnabled = true
        rootView?.dnccFbPageWebView?.isVerticalScrollBarEnabled = false
        rootView?.dnccFbPageWebView?.isHorizontalScrollBarEnabled = false
        rootView?.dnccFbPageWebView?.webViewClient = MyWebViewClient()
        rootView?.dnccFbPageWebView?.clearHistory()
        rootView?.dnccFbPageWebView?.loadUrl(DNCC_FB_PAGE)
    }

    private inner class MyWebViewClient : WebViewClient() {


        override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
            val alertBuilder = AlertDialog.Builder(activity!!)
            alertBuilder.setCancelable(true)
            alertBuilder.setTitle("ERROR")
            alertBuilder.setMessage("SSL error has been occurred. Do you want to continue?")
            alertBuilder.setNegativeButton("NO", null)
            alertBuilder.setPositiveButton("CONTINUE",
                DialogInterface.OnClickListener { dialog, which -> handler.proceed() })
            alertBuilder.create().show()
        }


        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            return handleUri(url)
        }

        private fun handleUri(url: String): Boolean {
            return if (URLUtil.isNetworkUrl(url)) {
                false
            } else {
                try {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(url)
                    startActivity(intent)

                } catch (e: ActivityNotFoundException) {
                }
                true
            }
        }


        @TargetApi(Build.VERSION_CODES.N)
        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
            return handleUri(request.url.toString())
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        rootView?.dnccFbPageWebView?.destroy()
    }
}
