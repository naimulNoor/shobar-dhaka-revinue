package com.shobardhaka.citizenrevenue.ui.notification

import com.shobardhaka.citizenrevenue.base.BasePresenter
import javax.inject.Inject


class NotificationPresenter @Inject constructor(view: NotificationContract.View) :
    BasePresenter<NotificationContract.View>(view), NotificationContract.Presenter {

}