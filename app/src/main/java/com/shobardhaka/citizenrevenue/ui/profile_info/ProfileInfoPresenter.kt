package com.shobardhaka.citizenrevenue.ui.profile_info

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.RetrofitHelperService
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.RegistrationResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.data.prefs.PreferenceManager
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.*
import javax.inject.Inject


class ProfileInfoPresenter @Inject constructor(view: ProfileInfoContract.View) :
    BasePresenter<ProfileInfoContract.View>(view), ProfileInfoContract.Presenter {

    @Inject
    lateinit var userApiService: UserApiService

    @Inject
    lateinit var retrofitHelperService: RetrofitHelperService

    @Inject
    lateinit var pref: PreferenceManager

    override fun updateProfileInfo(dataMap: HashMap<String, RequestBody>, attachment: MultipartBody.Part?) {
        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            userApiService.updateProfileInfo(dataMap, attachment)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<RegistrationResponse, ProfileInfoContract.View>(mView) {

                    override fun onRequestSuccess(response: RegistrationResponse) {
                        pref.putString(Keys.ACCESS_TOKEN.name, response.token)
                        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
                        retrofitHelperService.loadProfile(
                            compositeDisposable!!,
                            object : RetrofitHelperService.OnProfileLoadListener {
                                override fun onProfileLoaded() {
                                    mView?.onNetworkCallEnded()
                                    mView?.profileInfoUpdateDidSucceed()
                                }
                            })
                    }

                    override fun onFormDataNotValid(errorMap: LinkedHashMap<String, String>) {
                        for ((_, value) in errorMap) {
                            mView?.profileInfoUpdateDidFailed(value)
                            break
                        }
                    }
                })
        )
    }
}