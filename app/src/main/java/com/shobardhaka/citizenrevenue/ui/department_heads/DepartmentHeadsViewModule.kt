package com.shobardhaka.citizenrevenue.ui.department_heads

import dagger.Binds
import dagger.Module

@Module
abstract class DepartmentHeadsViewModule {
    @Binds
    abstract fun provideDepartmentHeadsViewModule(activity: DepartmentHeadsActivity): DepartmentHeadsContract.View
}