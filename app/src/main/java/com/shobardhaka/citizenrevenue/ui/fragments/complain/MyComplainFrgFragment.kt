package com.shobardhaka.citizenrevenue.ui.fragments.complain

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.adapters.MyComplaintsAdapter
import com.shobardhaka.citizenrevenue.adapters.PaginationScrollListener
import com.shobardhaka.citizenrevenue.adapters.RecyclerTouchListener
import com.shobardhaka.citizenrevenue.base.BaseFragment
import com.shobardhaka.citizenrevenue.data.network.APIs
import com.shobardhaka.citizenrevenue.data.network.api_response.GetComplainResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.ui.complaints.complaint_details.ComplaintDetailsActivity
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import com.shobardhaka.citizenrevenue.utils.Keys.ComplainType
import kotlinx.android.synthetic.main.fragment_my_complaints.view.*

class MyComplaintsFragment : BaseFragment<MyComplaintFrgPresenter>(), MyComplainFrgContract.View {

    companion object {
        fun newInstance(): MyComplaintsFragment {
            return MyComplaintsFragment()
        }
    }

    private lateinit var complaintsAdapter: MyComplaintsAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var animationController: LayoutAnimationController? = null

    private var nextPageUrl: String? = null
    private var isLoadingAdded: Boolean = false
    private var complainType: ComplainType = ComplainType.ALL
    private var GET_COMPLAINT_TOTAL: String = "${APIs.BASE_URL}${APIs.GET_COMPLAIN}/0"
    private var GET_COMPLAINT_UNSOLVED: String = "${APIs.BASE_URL}${APIs.GET_COMPLAIN}/1"
    private var GET_COMPLAINT_SOLVED: String = "${APIs.BASE_URL}${APIs.GET_COMPLAIN}/2"

    var type = 0

    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, saveInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_my_complaints, container, false)
    }

    override fun onViewReady(getArguments: Bundle?) {

        getArguments?.let {
            type = getArguments.getInt("type")
        }

        rootView.tabLayout.addTab(rootView.tabLayout.newTab().setText(getString(R.string.total)))
        rootView.tabLayout.addTab(rootView.tabLayout.newTab().setText(getString(R.string.solved)))
        rootView.tabLayout.addTab(rootView.tabLayout.newTab().setText(getString(R.string.unsolved)))

        animationController = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_slide_from_right);

        linearLayoutManager = LinearLayoutManager(
            context,
            RecyclerView.VERTICAL,
            false
        )

        complaintsAdapter = MyComplaintsAdapter(context)

        rootView.recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = complaintsAdapter
            layoutAnimation = animationController
        }

        if (getArguments != null) {
            complainType = getArguments.getSerializable(Keys.COMPLAINT_TYPE.name) as ComplainType
        }

        rootView.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                complaintsAdapter.clearAdapter()
                val position = tab?.position
                rootView.imgStatus.visibility = View.GONE
                position?.let {
                    when (position) {
                        0 -> {
                            mPresenter.getMyComplains(GET_COMPLAINT_TOTAL)
                            complainType = ComplainType.ALL
                        }
                        1 -> {
                            mPresenter.getMyComplains(GET_COMPLAINT_SOLVED)
                            complainType = ComplainType.SOLVED
                        }
                        2 -> {
                            mPresenter.getMyComplains(GET_COMPLAINT_UNSOLVED)
                            complainType = ComplainType.UNSOLVED
                        }
                    }
                }

            }

            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {

            }

        })

        rootView.recyclerView.addOnItemTouchListener(
            RecyclerTouchListener(
                context,
                rootView.recyclerView,
                object : RecyclerTouchListener.ClickListener {
                    override fun onClick(view: View, position: Int) {
                        val complain: GetComplainResponse.SingleComplain =
                            complaintsAdapter.getItem(position) as GetComplainResponse.SingleComplain
                        val intent = Intent(context, ComplaintDetailsActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        intent.putExtra(Keys.COMPLAINT_ID.name, "${complain.id}")
                        startActivity(intent)
                    }

                    override fun onLongClick(view: View?, position: Int) {
                        // Nothing to do.......
                    }
                })
        )

        rootView.recyclerView.addOnScrollListener(object : PaginationScrollListener(linearLayoutManager) {
            override val isLoading: Boolean
                get() = isLoadingAdded

            override fun loadMoreItems() {
                isLoadingAdded = true
                complaintsAdapter.addLoadingFooter()
                nextPageUrl?.let { mPresenter.getMyComplains(nextPageUrl!!) }
            }

            override fun hasNextPage(): Boolean {
                return nextPageUrl != null
            }
        })

        when (complainType) {
            ComplainType.ALL -> {
                mPresenter.getMyComplains(GET_COMPLAINT_TOTAL)//
            }
            ComplainType.SOLVED -> {
                val solvedTab = rootView.tabLayout.getTabAt(1)
                solvedTab?.select()
            }
            ComplainType.UNSOLVED -> {
                val unsolvedTab = rootView.tabLayout.getTabAt(2)
                unsolvedTab?.select()
            }
        }
    }


    override fun onNetworkCallStarted(loadingMessage: String) {
        if (!isLoadingAdded) {
            if (rootView.progressBar.visibility != View.VISIBLE) {
                rootView.progressBar.visibility = View.VISIBLE
            }
        }
    }

    override fun onNetworkCallEnded() {
        if (rootView.progressBar.visibility == View.VISIBLE) {
            rootView.progressBar.visibility = View.GONE
        }
    }

    override fun myComplainsDidReceived(response: GetComplainResponse) {
        nextPageUrl = response.nextPageUrl
        if (isLoadingAdded) {
            isLoadingAdded = false
            complaintsAdapter.removeLoadingFooter()
            complaintsAdapter.addAll(response.complaintList)
        } else {
            if (response.complaintList.isNotEmpty()) {
                rootView.imgStatus.visibility = View.GONE
                complaintsAdapter.setData(response.complaintList)
                rootView.recyclerView.scheduleLayoutAnimation()
            } else {
                rootView.imgStatus.visibility = View.VISIBLE
                when (complainType) {
                    ComplainType.ALL -> {
                        if (mAppLanguage == AppLanguage.BENGALI.name)
                            rootView.imgStatus.setImageResource(R.drawable.ic_empty_total_ben)
                        else
                            rootView.imgStatus.setImageResource(R.drawable.ic_empty_total_eng)
                    }
                    ComplainType.SOLVED -> {
                        if (mAppLanguage == AppLanguage.BENGALI.name)
                            rootView.imgStatus.setImageResource(R.drawable.ic_empty_solved_ben)
                        else
                            rootView.imgStatus.setImageResource(R.drawable.ic_empty_solved_eng)
                    }
                    ComplainType.UNSOLVED -> {
                        if (mAppLanguage == AppLanguage.BENGALI.name)
                            rootView.imgStatus.setImageResource(R.drawable.ic_empty_unsolved_ben)
                        else
                            rootView.imgStatus.setImageResource(R.drawable.ic_empty_unsolved_eng)
                    }
                }
            }
        }
    }
}

