package com.shobardhaka.citizenrevenue.ui.fragments.shotortobarta

import com.shobardhaka.citizenrevenue.base.BaseContract
import com.shobardhaka.citizenrevenue.data.network.api_response.GetDisasterResponse

interface ShotorkobartaFrgContract{

    interface View: BaseContract.View{
        fun disasterDidReceived(disasters: ArrayList<GetDisasterResponse.SingleDisaster?>)
    }

    interface Presenter: BaseContract.Presenter{

        fun getDisasters()
        fun setDisasterAsRead(url: String)
    }
}