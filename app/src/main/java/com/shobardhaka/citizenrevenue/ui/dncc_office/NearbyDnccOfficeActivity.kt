package com.shobardhaka.citizenrevenue.ui.dncc_office

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.OnClick
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.adapters.NearbyPlaceAdapter
import com.shobardhaka.citizenrevenue.adapters.RecyclerTouchListener
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.data.network.api_response.BariKoiResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.ui.maps.MapsActivity
import com.shobardhaka.citizenrevenue.utils.Navigator
import com.shobardhaka.citizenrevenue.utils.showToast
import kotlinx.android.synthetic.main.activity_nearby_dncc_office.*
import kotlinx.android.synthetic.main.toolbar_with_text.*


class NearbyDnccOfficeActivity : MvpBaseActivity<NearbyDnccPresenter>(), NearbyDnccContract.View {


    private lateinit var nearbyPlaceAdapter: NearbyPlaceAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var animationController: LayoutAnimationController

    private var currentLocLat: Double = 0.0
    private var currentLocLong: Double = 0.0
    var locType: String? = null
    var locTypStr: String = ""

    private val dnccHeadOfficeLat: String = "23.7956357"
    private val dnccHeadOfficeLng: String = "90.4148615"

    override fun getContentView(): Int {
        return R.layout.activity_nearby_dncc_office
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {

        setSupportActionBar(toolbar)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }

        val bundle = intent.getBundleExtra(Keys.DATA_BUNDLE.name)
        currentLocLat = bundle.getDouble(Keys.CURRENT_LOC_LAT.name)
        currentLocLong = bundle.getDouble(Keys.CURRENT_LOC_LONG.name)
        locType = bundle.getString(Keys.LOCATION_TYPE.name)
        val placeTypeIcon = bundle.getInt(Keys.NEARBY_ICON.name)
        var title = ""

        animationController = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_slide_from_right);
        linearLayoutManager = LinearLayoutManager(
            getContext(),
            RecyclerView.VERTICAL,
            false
        )

        nearbyPlaceAdapter = NearbyPlaceAdapter(getContext(), placeTypeIcon)
        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = nearbyPlaceAdapter
            layoutAnimation = animationController
        }

        recyclerView.addOnItemTouchListener(
            RecyclerTouchListener(
                this,
                recyclerView,
                object : RecyclerTouchListener.ClickListener {
                    override fun onClick(view: View, position: Int) {
                        val singlePlace: BariKoiResponse.SinglePlace =
                            nearbyPlaceAdapter.getItem(position) as BariKoiResponse.SinglePlace
                        val dataBundle = Bundle()
                        dataBundle.putString("loc_type", locTypStr)
                        dataBundle.putSerializable(Keys.SINGLE_PLACE.name, singlePlace)
                        Navigator.sharedInstance.navigateWithBundle(
                            getContext(),
                            MapsActivity::class.java,
                            Keys.DATA_BUNDLE.name,
                            dataBundle
                        )
                    }

                    override fun onLongClick(view: View?, position: Int) {
                        // Nothing to do.......
                    }
                })
        )

        // Barikoi Map Api......................

        val apiKey = getString(R.string.B_API_KEY)
        var distance = 2.0
        val limit = 80
        var pType = ""

        when (locType) {
            Keys.DNCC_OFFICE.name -> {
                title = getString(R.string.dncc_office)
                pType = "Ward Commissioner Office"
                locTypStr = getString(R.string.dncc_office)
                distance = 5.0
            }
        }
        val nearByWithCategory =
            "https://barikoi.xyz/v1/api/search/nearby/category/$apiKey/$distance/$limit?longitude=$currentLocLong&latitude=$currentLocLat&ptype=$pType"
        mPresenter.getNearByPlace(nearByWithCategory)
        // title = "$title(${AppUtils.shared.getInBangla(distance.toString())} কি.মি.)"
        tvTitle.text = title
    }


    @OnClick(R.id.btnMapHeadOffice)
    fun onBtnMapClicked() {
        val dataBundle = Bundle()
        dataBundle.putString("loc_type", locTypStr)
        val singlePlace = BariKoiResponse.SinglePlace(
            0,
            getString(R.string.dncc_head_office),
            0.0,
            dnccHeadOfficeLng,
            dnccHeadOfficeLat,
            getString(R.string.dncc_head_office_address),
            "",
            "",
            "",
            "",
            ""
        )
        dataBundle.putSerializable(Keys.SINGLE_PLACE.name, singlePlace)
        Navigator.sharedInstance.navigateWithBundle(
            getContext(),
            MapsActivity::class.java,
            Keys.DATA_BUNDLE.name,
            dataBundle
        )
    }

    override fun nearByPlacesDidReceived(nearByPlaces: ArrayList<BariKoiResponse.SinglePlace?>?) {
        if (nearByPlaces != null && nearByPlaces.isNotEmpty()) {
            nearbyPlaceAdapter.setData(nearByPlaces)
            recyclerView.scheduleLayoutAnimation()
        }
    }

    override fun noNearbyPlaceFound(message: String) {
        showToast(message)
    }

    override fun onNetworkCallStarted(loadingMessage: String) {
        showProgressDialog(getString(R.string.please_wait))
    }
}
