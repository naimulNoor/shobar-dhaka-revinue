package com.shobardhaka.citizenrevenue.ui.complaints

import android.Manifest
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.utils.showToast
import java.lang.Exception
import java.util.*


class DraggableGoogleMapActivity : AppCompatActivity(), OnMapReadyCallback,
    GoogleMap.OnCameraIdleListener {

    private lateinit var mMap: GoogleMap


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_draggable_google_map)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
        mMap.setOnCameraIdleListener(this)

        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            mMap.isMyLocationEnabled = true
            mMap.uiSettings.isMyLocationButtonEnabled = true
            mMap.uiSettings.isZoomControlsEnabled = true
            mMap.uiSettings.isZoomGesturesEnabled = true
            mMap.uiSettings.isMapToolbarEnabled = true
        }

        // mMap.isMyLocationEnabled = true
        mMap.uiSettings.isMyLocationButtonEnabled = true
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.uiSettings.isZoomGesturesEnabled = true
        mMap.uiSettings.isMapToolbarEnabled = true
        mMap.uiSettings.isRotateGesturesEnabled = true

        val placeLoc =
            LatLng(23.8314708, 90.4174283)
        val markerOption = MarkerOptions()
        markerOption.position(placeLoc)
            .icon(BitmapDescriptorFactory.defaultMarker())
            .draggable(true)
        // val mapMarker: Marker = mMap.addMarker(markerOption)
        mMap.moveCamera(CameraUpdateFactory.newLatLng(placeLoc))

        // mapMarker.showInfoWindow() // Showing info window automatically when map will open
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(placeLoc, 14.6.toFloat()))
        showToast(getAddress(placeLoc))


//        mMap.setOnMarkerDragListener(object: GoogleMap.OnMarkerDragListener{
//            override fun onMarkerDrag(p0: Marker?) {
//
//            }
//
//            override fun onMarkerDragEnd(arg0: Marker?) {
//                arg0?.let {
//                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(arg0.position, 14.0f))
//                    val message = arg0.position.latitude.toString() + "" + arg0.position.longitude.toString()
//                    Log.e("_SHISHIR13" + "_END", message)
//                }
//            }
//
//            override fun onMarkerDragStart(arg0: Marker?) {
//                val message = arg0!!.position.latitude.toString() + "" + arg0.position.longitude.toString()
//                Log.e("_SHISHIR_13" + "_DRAG", message)
//            }
//        })

    }

    override fun onCameraIdle() {
        val centerLatLong = mMap.cameraPosition.target
        Log.e("CURRENT LOC: >>>", "${centerLatLong.latitude}\n ${centerLatLong.longitude}")
        showToast(getAddress(centerLatLong))

    }

    private fun getAddress(latLng: LatLng): String {

        val geocoder = Geocoder(this, Locale.ENGLISH)
        val addresses: List<Address>?
        val address: Address?
        var addressText = ""

        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)

            if (addresses.isNotEmpty()) {
                address = addresses[0]
                addressText = address.getAddressLine(0)
            } else {
                addressText = "its not appear"
            }
        } catch (e: Exception) {

        }
        return addressText
    }
}