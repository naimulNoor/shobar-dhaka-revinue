package com.shobardhaka.citizenrevenue.ui.dncc_office

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.BariKoiResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.BariKoiApiService
import com.shobardhaka.citizenrevenue.ui.nearby.nearby_place.NearbyPlaceContract
import javax.inject.Inject

class NearbyDnccPresenter @Inject constructor(view: NearbyDnccContract.View) : BasePresenter<NearbyDnccContract.View>(view), NearbyPlaceContract.Presenter {

    @Inject
    lateinit var bariKoiApiService: BariKoiApiService

    override fun getNearByPlace(url: String) {
        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            bariKoiApiService.getNearByPlaces(url)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<BariKoiResponse, NearbyDnccContract.View>(mView) {
                    override fun onRequestSuccess(response: BariKoiResponse) {
                        if (response.statusCode == null) {
                            response.nearByPlaces.let {
                                mView?.nearByPlacesDidReceived(response.nearByPlaces)
                            }
                        } else {
                            mView?.noNearbyPlaceFound(response.message!!)
                        }
                    }
                })
        )
    }
}