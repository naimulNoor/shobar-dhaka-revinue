package com.shobardhaka.citizenrevenue.ui.fragments.complain

import com.shobardhaka.citizenrevenue.base.BaseContract
import com.shobardhaka.citizenrevenue.data.network.api_response.GetComplainResponse

interface MyComplainFrgContract{
    interface View : BaseContract.View {

        fun myComplainsDidReceived(response: GetComplainResponse)
    }

    interface Presenter : BaseContract.Presenter {

        fun getMyComplains(url: String)
    }
}