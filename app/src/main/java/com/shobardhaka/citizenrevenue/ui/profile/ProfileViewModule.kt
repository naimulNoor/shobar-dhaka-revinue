package com.shobardhaka.citizenrevenue.ui.profile

import dagger.Binds
import dagger.Module

/**
 * Created  on 19/08/2019.
 */

@Module
abstract class ProfileViewModule {

    @Binds
    abstract fun provideProfileViewView(activity: ProfileActivity): ProfileContract.View

}