package com.shobardhaka.citizenrevenue.ui.fragments.temperature


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BaseFragment
import com.shobardhaka.citizenrevenue.data.network.api_response.GetCurrentWeatherResponse
import com.shobardhaka.citizenrevenue.file.PicassoImageLoader
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.DateFormatUtils
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import kotlinx.android.synthetic.main.fragment_temperature.view.*
import java.text.SimpleDateFormat
import java.util.*

class TemperatureFragment : BaseFragment<TemperatureFrgPresenter>(), TemperatureFrgContract.View {

    private val celsiusSign = "\u2103"
    private val farenHeightSign = "\u2109"
    private val degreeSign = "\u00B0"

    companion object {
        fun newInstance(): TemperatureFragment {
            return TemperatureFragment()
        }
    }

    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, saveInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_temperature, container, false)

    }

    override fun onViewReady(getArguments: Bundle?) {
        val date = Date()
        val sdfDayNameToday = SimpleDateFormat("EEEE", Locale.getDefault())
        val sdfDate = SimpleDateFormat("dd MMM, yyyy", Locale.getDefault())  // 12:00:00 pm
        val sdfTime = SimpleDateFormat("hh:mm a", Locale.getDefault())  // 12:00:00 pm
        val dayNameToday = sdfDayNameToday.format(date)
        val dateToday = "$dayNameToday, ${sdfDate.format(date)}"
        var timeOfDay = sdfTime.format(date)

        if (mAppLanguage == AppLanguage.BENGALI.name) {
            timeOfDay = AppUtils.shared.getInBangla(timeOfDay)
        }

        rootView.tvDateToday.text = dateToday
        rootView.tvTimeToday.text = timeOfDay
        mPresenter.getCurrentWeatherOfDhaka()
    }

    override fun currentWeatherDidReceived(currentWeatherResponse: GetCurrentWeatherResponse) {
        val currentTempInCelsius =
            if (mAppLanguage == AppLanguage.BENGALI.name)
                "${AppUtils.shared.getInBangla(currentWeatherResponse.main.temp.toString())}$degreeSign"
            else
                "${currentWeatherResponse.main.temp}$degreeSign"

        val minTemp =
            if (mAppLanguage == AppLanguage.BENGALI.name)
                "${AppUtils.shared.getInBangla(currentWeatherResponse.main.tempMin.toString())}$degreeSign"
            else
                "${currentWeatherResponse.main.tempMin}$degreeSign"

        val maxTemp =
            if (mAppLanguage == AppLanguage.BENGALI.name)
                "${AppUtils.shared.getInBangla(currentWeatherResponse.main.tempMax.toString())}$degreeSign"
            else
                "${currentWeatherResponse.main.tempMax}$degreeSign"

        val humidity =
            if (mAppLanguage == AppLanguage.BENGALI.name)
                "${AppUtils.shared.getInBangla(currentWeatherResponse.main.humidity.toString())}%"
            else
                "${currentWeatherResponse.main.humidity}%"
        var sunrise = DateFormatUtils.shared.convertUnixToTime(currentWeatherResponse.sys.sunrise.toLong())
        var sunSet = DateFormatUtils.shared.convertUnixToTime(currentWeatherResponse.sys.sunset.toLong())
        if (mAppLanguage == AppLanguage.BENGALI.name) {
            sunrise = AppUtils.shared.getInBangla(sunrise)
            sunSet = AppUtils.shared.getInBangla(sunSet)
        }

        PicassoImageLoader.shared.loadImage(
            context,
            "http://openweathermap.org/img/w/${currentWeatherResponse.weather[0].icon}.png",
            rootView.imgWeatherType
        )
        rootView.tvWeatherType.text = currentWeatherResponse.weather[0].description

        rootView.tvMinTemperature.text = minTemp
        rootView.tvCurrentTemperature.text = currentTempInCelsius
        rootView.tvMaxTemperature.text = maxTemp

        rootView.tvSunrise.text = sunrise
        rootView.tvSunset.text = sunSet

    }
}

