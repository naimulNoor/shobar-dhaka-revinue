package com.shobardhaka.citizenrevenue.ui.nearby.nearby_place

import com.shobardhaka.citizenrevenue.base.BaseContract
import com.shobardhaka.citizenrevenue.data.network.api_response.BariKoiResponse

interface NearbyPlaceContract{

    interface View: BaseContract.View{
        fun nearByPlacesDidReceived(nearByPlaces: ArrayList<BariKoiResponse.SinglePlace?>?)
        fun noNearbyPlaceFound(message: String)
    }

    interface Presenter: BaseContract.Presenter{
        fun getNearByPlace(url: String)
    }
}