package com.shobardhaka.citizenrevenue.ui.complaints.complaint_details

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.RatingBar
import butterknife.OnClick
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.data.network.APIs
import com.shobardhaka.citizenrevenue.data.network.api_response.ComplainDetailsResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.file.PicassoImageLoader
import com.shobardhaka.citizenrevenue.ui.home.HomeActivity
import com.shobardhaka.citizenrevenue.utils.AlertService
import com.shobardhaka.citizenrevenue.utils.DateFormatUtils
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import com.shobardhaka.citizenrevenue.utils.Navigator
import com.shobardhaka.citizenrevenue.utils.showToast
import kotlinx.android.synthetic.main.activity_complaint_details.*
import kotlinx.android.synthetic.main.section_stepview_complain_details.*
import kotlinx.android.synthetic.main.toolbar_with_text.*

class ComplaintDetailsActivity : MvpBaseActivity<ComplaintDetailsPresenter>(),
    ComplaintDetailsContract.View {

    private var complainId: String? = null
    private var ratingMark = ""

    override fun getContentView(): Int {
        return R.layout.activity_complaint_details
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }
        tvTitle.text = getString(R.string.complaint_details)
        complainId = intent.getStringExtra(Keys.COMPLAINT_ID.name)
        complainId?.let { complain_id ->
            mPresenter.getComplainDetails(complain_id, true)

        }

        swipe_resfreshLayout.setColorSchemeResources(R.color.colorPrimary)
        swipe_resfreshLayout.setOnRefreshListener {
            refreshContent()
        }
        ratingBar.onRatingBarChangeListener = ratingBarChangeListener
    }

    private var ratingBarChangeListener: RatingBar.OnRatingBarChangeListener? =
        RatingBar.OnRatingBarChangeListener { _, rating, _ ->
            ratingMark = "$rating"
        }

    override fun onDestroy() {
        ratingBarChangeListener = null
        super.onDestroy()

    }

    private fun refreshContent() {
        complainId?.let { complain_id ->
            swipe_resfreshLayout.isRefreshing = true
            mPresenter.getComplainDetails(complain_id, true)
        }
    }

    @OnClick(R.id.floatingActionBtn, R.id.btnSubmitRating)
    fun onSubmit(v: View) {
        when (v.id) {
            R.id.floatingActionBtn -> {
                mPresenter.recomplain("${APIs.RE_COMPLAIN}/$complainId")
            }

            R.id.btnSubmitRating -> {
                val dataMap = HashMap<String, String>()
                if (ratingMark.isEmpty()) {
                    showToast(getString(R.string.rating_empty_msg))
                } else {
                    dataMap["rating_mark"] = ratingMark
                    dataMap["complain_id"] = "$complainId"
                    mPresenter.submitRating(dataMap)
                }
            }
        }
    }


    override fun complainDetailsDidReceived(complainDetailsResponse: ComplainDetailsResponse) {

        sectionDetails.visibility = View.VISIBLE

        if (swipe_resfreshLayout.isRefreshing)
            swipe_resfreshLayout.isRefreshing = false


        // Check Rating >>>
        if (complainDetailsResponse.complain.canRating == 1){
            llRating.visibility = View.VISIBLE
        }

        // Re Complain >>>>
        if (complainDetailsResponse.complain.canRecomplain == 1) {
            llReComplain.visibility = View.VISIBLE
        }


        val profilePic = mPrefManager.getString(Keys.ProfileKey.PROFILE_PIC.name)
        if (profilePic.isNotEmpty()) {
            PicassoImageLoader.shared.loadImage(
                getContext(),
                profilePic,
                imgUser,
                R.drawable.profile
            )
        }

        complainDetailsResponse.complain.category?.let {
            tvCategory.text = if(mAppLanguage == AppLanguage.BENGALI.name) it.nameBn else it.name
        }

        complainDetailsResponse.complain.subCategory?.let {
            tvIssueType.text = if(mAppLanguage == AppLanguage.BENGALI.name) it.nameBn else it.name

            tvHeadingIssueType.visibility = View.VISIBLE
            tvIssueType.visibility = View.VISIBLE
        }

        complainDetailsResponse.complain.issue?.let {
            tvIssue.text = if(mAppLanguage == AppLanguage.BENGALI.name) it.nameBn else it.name

            tvHeadingIssue.visibility = View.VISIBLE
            tvIssue.visibility = View.VISIBLE
        }

        complainDetailsResponse.complain.getRating?.let {
            myRating.rating = it.ratingMark.toFloat()
            headingMyRating.visibility = View.VISIBLE
            myRating.visibility = View.VISIBLE
        }

        val complain = complainDetailsResponse.complain
        tvComplainID.text = complain.complainId
        tvLocation.text = complain.location
        tvDate.text = DateFormatUtils.shared.getDate(complain.createdAt)
        tvTime.text = DateFormatUtils.shared.getTime(complain.createdAt)
        tvDetails.text = complain.description
        val imageUrl = "${APIs.ASSET_PREFIX}${complain.image}"
        PicassoImageLoader.shared.loadImage(getContext(), imageUrl, imgComplaint)


        if(complain.complainType != null && complain.complainType == 1){
            sectionStep.visibility = View.GONE
        }else{
            sectionStep.visibility = View.VISIBLE
        }

        when (complain.complainStatus) {
            0 -> {
                imgStep1.isSelected = true
                tvSentDate.text = DateFormatUtils.shared.getDate(complain.createdAt)
            }
            1 -> {
                imgStep1.isSelected = true
                imgStep2.isSelected = true
                tvSentDate.text = DateFormatUtils.shared.getDate(complain.createdAt)
                tvAcceptedDate.text = DateFormatUtils.shared.getDate(complain.actions[0].createdAt)

            }
            2 -> {
                imgStep1.isSelected = true
                imgStep2.isSelected = true
                imgStep3.isSelected = true
                tvSentDate.text = DateFormatUtils.shared.getDate(complain.createdAt)
                tvAcceptedDate.text = DateFormatUtils.shared.getDate(complain.actions[0].createdAt)
                tvInvestigatedDate.text =
                    DateFormatUtils.shared.getDate(complain.actions[1].createdAt)

            }
            -1, 3 -> {    // -1 = Rejected, 3= Solved
                imgStep1.isSelected = true
                imgStep2.isSelected = true
                imgStep3.isSelected = true
                imgStep4.isSelected = true

                tvSentDate.text = DateFormatUtils.shared.getDate(complain.createdAt)
                tvAcceptedDate.text = DateFormatUtils.shared.getDate(complain.actions[0].createdAt)
                tvInvestigatedDate.text =
                    DateFormatUtils.shared.getDate(complain.actions[1].createdAt)
                tvSolutionDate.text = DateFormatUtils.shared.getDate(complain.actions[2].createdAt)

                val msg = complain.solution.message
                msg?.let {
                    tvReplyFromDncc.text = msg
                    sectionReplyFromDncc.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun submitRatingDidSucceed() {
        llRating.visibility = View.GONE
        mAlertService.showAlert(getContext(), null, getString(R.string.rating_accepted))
        mPresenter.getComplainDetails("$complainId", false)
    }

    override fun submitRatingDidFailed(failedMsg: String) {
        showToast(failedMsg)
    }

    override fun recomplaintDidsSucceed() {
        llReComplain.visibility = View.GONE
        mAlertService.showConfirmationAlert(
            getContext(),
            null,
            getString(R.string.recomplaint_success_msg),
            null,
            getString(R.string.okay),
            object : AlertService.AlertListener {
                override fun negativeBtnDidTapped() {

                }

                override fun positiveBtnDidTapped() {
                    onBackPressed()
                }
            }
        )
    }

    override fun recomplaintDidFailed() {
        // showToast("Failed Msg")
    }

    override fun onNetworkCallStarted(loadingMessage: String) {
        if (!swipe_resfreshLayout.isRefreshing) showProgressDialog(getString(R.string.please_wait))
    }

    override fun onNetworkCallEnded() {
        if (swipe_resfreshLayout.isRefreshing) swipe_resfreshLayout.isRefreshing = false
        else {
            hideProgressDialog()
        }
    }


    override fun onBackPressed() {
        Navigator.sharedInstance.back(getContext(), HomeActivity::class.java)
        finish()
    }
}
