package com.shobardhaka.citizenrevenue.ui.fragments.dncc


import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.location.SSFusedLocationProviderClient
import com.shobardhaka.citizenrevenue.ui.DnccFormsActivity
import com.shobardhaka.citizenrevenue.ui.mayor_ceo.ProfileDetailsActivity
import com.shobardhaka.citizenrevenue.ui.councilors.CouncilorsActivity
import com.shobardhaka.citizenrevenue.ui.department_heads.DepartmentHeadsActivity
import com.shobardhaka.citizenrevenue.ui.dncc_office.NearbyDnccOfficeActivity
import com.shobardhaka.citizenrevenue.utils.AlertService
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.Navigator
import com.shobardhaka.citizenrevenue.utils.NetworkUtils


class DnccFragment : Fragment() {

    companion object {
        fun newInstance(): DnccFragment {
            return DnccFragment()
        }
    }

    private var viewUnbinder: Unbinder? = null
    private lateinit var mNetworkUtils: NetworkUtils
    private lateinit var alertService: AlertService

    private val DNCC_WEBSITE = "http://www.dncc.gov.bd"
    private val DNCC_FACEBOOK_PAGE = "https://www.facebook.com/dncc.gov.bd/"
    private var ssFusedLocationApiClient: SSFusedLocationProviderClient? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_dncc, container, false)
        viewUnbinder = ButterKnife.bind(this, view)
        mNetworkUtils = NetworkUtils()
        alertService = AlertService()
        return view
    }

    override fun onStart() {
        super.onStart()
        if (ActivityCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            startLocationService()
        }
    }


    override fun onStop() {
        super.onStop()
        stopLocationService()
    }

    @OnClick(
        R.id.dncc_meyor,
        R.id.dncc_ceo,
        R.id.dncc_departmental_heads,
        R.id.dncc_councilors,
        R.id.dncc_forms,
        R.id.dncc_offices,
        R.id.dncc_website,
        R.id.dncc_facebook
    )
    fun onClicked(view: View) {
        val bundle = Bundle()
        when (view.id) {
            R.id.dncc_website -> {
                if (mNetworkUtils.isConnectedToNetwork(getActivityContext())) {
                    AppUtils.shared.openLinkOnExternalBrowser(getActivityContext(), DNCC_WEBSITE)
                } else {
                    alertService.showToast(context!!, getString(R.string.no_internet_connection))
                }
            }
            R.id.dncc_facebook -> {
                if (mNetworkUtils.isConnectedToNetwork(getActivityContext())) {
                    AppUtils.shared.openFacebookPage(getActivityContext(), DNCC_FACEBOOK_PAGE)
                } else {
                    alertService.showToast(context!!, getString(R.string.no_internet_connection))
                }
            }
            R.id.dncc_meyor -> {
                bundle.putString(Keys.PROFILE_TYPE.name, Keys.MEYOR.name)
                Navigator.sharedInstance.navigateWithBundle(
                    getActivityContext(),
                    ProfileDetailsActivity::class.java,
                    Keys.DATA_BUNDLE.name,
                    bundle
                )
            }
            R.id.dncc_ceo -> {
                bundle.putString(Keys.PROFILE_TYPE.name, Keys.CEO.name)
                Navigator.sharedInstance.navigateWithBundle(
                    getActivityContext(),
                    ProfileDetailsActivity::class.java,
                    Keys.DATA_BUNDLE.name,
                    bundle
                )
            }
            R.id.dncc_departmental_heads -> {
                bundle.putString(Keys.PDF_LINK.name, "departmental_heads.pdf")
                bundle.putString(Keys.TITLE.name, getString(R.string.departmental_heads))
                Navigator.sharedInstance.navigateWithBundle(
                    getActivityContext(),
                    DepartmentHeadsActivity::class.java,
                    Keys.DATA_BUNDLE.name,
                    bundle
                )
            }
            R.id.dncc_councilors -> {
                bundle.putString(Keys.PDF_LINK.name, "councilors.pdf")
                bundle.putString(Keys.TITLE.name, getString(R.string.councilors))
                Navigator.sharedInstance.navigateWithBundle(
                    getActivityContext(),
                    CouncilorsActivity::class.java,
                    Keys.DATA_BUNDLE.name,
                    bundle
                )
            }
            R.id.dncc_forms -> {
                Navigator.sharedInstance.navigate(
                    getActivityContext(),
                    DnccFormsActivity::class.java
                )
            }
            R.id.dncc_offices -> {
                bundle.putDouble(Keys.CURRENT_LOC_LAT.name, SSFusedLocationProviderClient.mLatitude)
                bundle.putDouble(
                    Keys.CURRENT_LOC_LONG.name,
                    SSFusedLocationProviderClient.mLongitude
                )
                bundle.putString(Keys.LOCATION_TYPE.name, Keys.DNCC_OFFICE.name)
                bundle.putInt(Keys.NEARBY_ICON.name, R.drawable.ic_hospital)
                Navigator.sharedInstance.navigateWithBundle(
                    getActivityContext(),
                    NearbyDnccOfficeActivity::class.java,
                    Keys.DATA_BUNDLE.name,
                    bundle
                )
            }
        }
    }


    private fun startLocationService() {
        if (ssFusedLocationApiClient == null)
            ssFusedLocationApiClient = SSFusedLocationProviderClient(activity!!)
        ssFusedLocationApiClient?.startLocationUpdates()
//        Intent(getContext().applicationContext, LocationService::class.java).also { intent ->
//            startService(intent)
//        }
    }

    private fun stopLocationService() {
        ssFusedLocationApiClient?.stopLocationUpdates()
        ssFusedLocationApiClient?.onDestroy()
        ssFusedLocationApiClient = null
//        Intent(getContext().applicationContext, LocationService::class.java).also { intent ->
//            stopService(intent)
//        }
    }

    private fun getActivityContext(): Context {
        return activity!!
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewUnbinder?.let {
            viewUnbinder!!.unbind()
        }
    }
}
