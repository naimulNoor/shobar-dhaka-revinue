package com.shobardhaka.citizenrevenue.ui.event_detials

import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.OnClick
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.data.network.api_response.GetEventResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.file.PicassoImageLoader
import com.shobardhaka.citizenrevenue.utils.AlertService
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import kotlinx.android.synthetic.main.activity_dhaka_today_details_page.*
import kotlinx.android.synthetic.main.toolbar_with_text.*

class EventDetailsActivity : MvpBaseActivity<EventDetailsPresenter>(), EventDetialsContract.View {


    lateinit var alertService: AlertService
    private var singleEvent: GetEventResponse.SingleEvent? = null
    private var clickedBtn = 0

    override fun getContentView(): Int {
        return R.layout.activity_dhaka_today_details_page
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }
        alertService = AlertService()

        singleEvent =
                intent.getBundleExtra(Keys.DATA_BUNDLE.name).getSerializable(Keys.single_event.name) as GetEventResponse.SingleEvent
        singleEvent?.let {
            tvTitle.text = singleEvent!!.category.name

            PicassoImageLoader.shared.loadImage(
                getContext(),
                singleEvent?.eventImage!!,
                imgEvent
            )

            tvEventTitle.text = singleEvent?.title
            tvEventDetials.text = singleEvent?.description

            val eventDate = if (mAppLanguage == AppLanguage.BENGALI.name) AppUtils.shared.getInBangla(singleEvent!!.eventDay) else singleEvent!!.eventDay + singleEvent!!.eventMonth
            val venue = getString(R.string.venue) + " " + singleEvent!!.venue
            val eventTime = "${getString(R.string.time)}" + if (AppLanguage.BENGALI.name == mAppLanguage) AppUtils.shared.getInBangla(singleEvent!!.eventTime) else singleEvent!!.eventTime
            tvEventDate.text = eventDate
            tvEventVenue.text = venue
            tvEventTime.text = eventTime

            // If already joined...
            singleEvent?.join?.let {
                if (singleEvent!!.join!!.joinYn == "1") {
                    btnParticipate.visibility = View.GONE
                    btnInterested.visibility = View.GONE
                    // Message
                    mAlertService.showAlert(getContext(), null, getString(R.string.msg_participate))
                } else if (singleEvent!!.join!!.interestedYn == "1") {
                    btnInterested.visibility = View.GONE
                    mAlertService.showAlert(getContext(), null, getString(R.string.msg_interested))
                } else {
                    if (singleEvent!!.joinYn != "1") {
                        btnParticipate.visibility = View.GONE
                    }
                    if (singleEvent!!.interestedYn != "1") {
                        btnInterested.visibility = View.GONE
                    }
                }
            } ?: run {
                if (singleEvent!!.joinYn != "1") {
                    btnParticipate.visibility = View.GONE
                }
                if (singleEvent!!.interestedYn != "1") {
                    btnInterested.visibility = View.GONE
                }
            }
        }
    }

    @OnClick(
        R.id.btnParticipate,
        R.id.btnInterested
    )
    fun onBtnClicked(view: View) {
        when (view.id) {
            R.id.btnParticipate -> {
                clickedBtn = 0
                val dataMap = HashMap<String, String>()
                dataMap["event_id"] = "${singleEvent?.id}"
                dataMap["join_yn"] = "1"
                dataMap["interested_yn"] = "0"
                mPresenter.joinEvent(dataMap)
            }
            R.id.btnInterested -> {
                clickedBtn = 1
                val dataMap = HashMap<String, String>()
                dataMap["event_id"] = "${singleEvent?.id}"
                dataMap["join_yn"] = "0"
                dataMap["interested_yn"] = "1"
                mPresenter.joinEvent(dataMap)
            }
        }
    }


    override fun joinDidSucceed(message: String?) {
        message?.let {
            when (clickedBtn) {
                0 -> {
                    btnParticipate.visibility = View.GONE
                    btnInterested.visibility = View.GONE
                }
                1 -> {
                    btnInterested.visibility = View.GONE
                }
            }
            mAlertService.showAlert(getContext(), null, message)
        }
    }

    override fun joinDidFailed(failedMsg: String?) {
        failedMsg?.let {
            mAlertService.showAlert(getContext(), null, failedMsg)
        }
    }
}
