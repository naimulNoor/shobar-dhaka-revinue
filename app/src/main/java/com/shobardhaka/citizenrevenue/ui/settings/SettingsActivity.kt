package com.shobardhaka.citizenrevenue.ui.settings

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import butterknife.OnClick
import com.google.firebase.iid.FirebaseInstanceId
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.ui.home.HomeActivity
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import com.shobardhaka.citizenrevenue.utils.Navigator
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.toolbar_with_text.*

class SettingsActivity : MvpBaseActivity<SettingsPresenter>(), SettingsContract.View {

    override fun getContentView(): Int {
        return R.layout.activity_settings
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }

        val appLang = mPrefManager.getString(Keys.APP_LANGUAGE.name, AppLanguage.BENGALI.name)
        when (appLang) {
            AppLanguage.BENGALI.name -> {
                tvLanguageName.text = getString(R.string.bangla)
            }

            AppLanguage.ENGLISH.name -> {
                tvLanguageName.text = getString(R.string.english)
            }
        }

        tvTitle.text = getString(R.string.settings)
        checkboxNotification.isChecked = mPrefManager.getBoolean(Keys.IS_NOTIFICATION_ENABLED.name, true)
    }


    @OnClick(R.id.settings_notification, R.id.settings_language)
    fun onSettingsClicked(view: View) {
        when (view.id) {
            R.id.settings_notification -> {
                checkboxNotification.isChecked = !checkboxNotification.isChecked
                mPrefManager.putBoolean(Keys.IS_NOTIFICATION_ENABLED.name, checkboxNotification.isChecked)
                if(checkboxNotification.isChecked){
                    FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
                        val deviceToken = it.token
                        mPresenter.storeDeviceToken(deviceToken)
                        mPrefManager.putString(Keys.DEVICE_TOKEN.name, deviceToken)
                    }
                }else{
                    mPresenter.removeDeviceToken()
                }
            }

            R.id.settings_language -> {
                val alertBuilder = AlertDialog.Builder(this)
                alertBuilder.setCancelable(true)
                alertBuilder.setTitle(getString(R.string.change_language))
                alertBuilder.setItems(
                    arrayOf<CharSequence>(
                        getString(R.string.bangla),
                        getString(R.string.english)
                    )
                ) { _, option ->
                    when (option) {
                        0 -> {
                            AppUtils.shared.setLocale(getContext(), AppLanguage.BENGALI)
                            mPrefManager.putString(Keys.APP_LANGUAGE.name, AppLanguage.BENGALI.name)
                            tvLanguageName.text = getString(R.string.bangla)
                            startActivity(intent)
                            finish()
                        }

                        1 -> {
                            AppUtils.shared.setLocale(getContext(), AppLanguage.ENGLISH)
                            mPrefManager.putString(Keys.APP_LANGUAGE.name, AppLanguage.ENGLISH.name)
                            tvLanguageName.text = getString(R.string.english)
                            startActivity(intent)
                            finish()
                        }
                    }
                }
                alertBuilder.create().show()
            }
        }
    }

    override fun onBackPressed() {
        Navigator.sharedInstance.back(getContext(), HomeActivity::class.java)
        finish()
    }
}
