package com.shobardhaka.citizenrevenue.ui.complaints.add_complaint

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import butterknife.OnClick
import com.bumptech.glide.load.resource.bitmap.TransformationUtils
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.data.network.api_response.IssueTypeResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.file.FileUploader
import com.shobardhaka.citizenrevenue.location.SSFusedLocationProviderClient
import com.shobardhaka.citizenrevenue.ui.home.HomeActivity
import com.shobardhaka.citizenrevenue.utils.*
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import kotlinx.android.synthetic.main.activity_add_complaints.*
import kotlinx.android.synthetic.main.toolbar_with_text.*
import okhttp3.RequestBody
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class AddComplaintsActivity : MvpBaseActivity<AddComplaintPresenter>(), AddComplaintContract.View {

    companion object {
        private var imageUri: Uri? = null
        private var imagePath: String? = null
        private var gpsLocation = ""
        private var mLatitude = 0.0
        private var mLongitude = 0.0
        private var complaintType = ""
        private var complainId = ""
        private var mosquitoComplainType: Int = 0 // 0 = Mosha, 1 = Patient
    }

    private val CODE_PICK_PHOTO = 1994
    private val CODE_CAMERA_PERMISSION = 13
    private var fileToUpload: File? = null

    private lateinit var fileUploader: FileUploader


    /** Capture Image with Provider Variable */
    private val REQUEST_IMAGE_CAPTURE = 101
    private val REQUEST_WRITE_PERMISSION = 20

    private lateinit var firstLayerAdapter: ArrayAdapter<String>
    private lateinit var secondLayerAdapter: ArrayAdapter<String>


    @Inject
    lateinit var permissionUtils: PermissionUtils

    private var issueTypeResponse: IssueTypeResponse? = null
    private var layer2nd: ArrayList<IssueTypeResponse.SingleSubIssueType>? = null

    private var ssFusedLocationApiClient: SSFusedLocationProviderClient? = null

    override fun getContentView(): Int {
        return R.layout.activity_add_complaints
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }

        if (mAppLanguage == AppLanguage.BENGALI.name) {
            imgComplaint.setImageResource(R.drawable.img_capture_photo_bn)
        }

        val bundle = intent.getBundleExtra(Keys.DATA_BUNDLE.name)
        complaintType = bundle.getString(Keys.COMPLAINT_TYPE.name)!!
        complainId = "${bundle.getInt(Keys.COMPLAINT_ID.name)}"

        // Toolbar text and image
        tvTitle.text = complaintType
        imgTitle.setImageResource(bundle.getInt(Keys.COMPLAINT_ICON.name))
        imgTitle.visibility = View.VISIBLE

        // Complain Title
        tvAddComplainTitle.text = bundle.getString(Keys.COMPLAINT_MSG.name)

        // File uploader instance
        fileUploader = FileUploader(getContext())

//        spinner1sttLayer.onItemSelectedListener = spinner1stItemSelectListener
//        spinner2ndLayer.onItemSelectedListener = spinner2ndItemSelectListener

//        val adapter1stLayer = ArrayAdapter.createFromResource(getContext(), R.array.diseases, R.layout.item_view_spinner)
//        spinner1sttLayer.adapter = adapter1stLayer
//        spinner2ndLayer.adapter = adapter1stLayer
        startLocationService()

        mPresenter.getSubIssueType(complainId)
    }


    @OnClick(R.id.imgComplaint, R.id.btnSubmit)
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgComplaint -> {
                if (hasCameraAndStoragePermission()) {
                    takePicture()
                } else {
                    manageCameraAndStoragePermission()
                }
            }

            R.id.btnSubmit -> {
                val description = etDescription.text.toString().trim()
                val userInputLocation = etLocation.text.toString().trim()

                when {

                    issueTypeResponse?.data != null && spinner1sttLayer.selectedItem.toString() == getString(R.string.h_select_issue_type) -> { // Has Issue Type
                        mAlertService.showAlert(
                            getContext(),
                            null,
                            getString(R.string.m_select_issue_type)
                        )
                    }

                    layer2nd != null && layer2nd!!.isNotEmpty() && spinner2ndLayer.selectedItem.toString() == getString(R.string.h_select_issue) -> { // Has Issue
                        mAlertService.showAlert(
                            getContext(),
                            null,
                            getString(R.string.m_select_issue)
                        )
                    }

                    fileToUpload == null -> {
                        mAlertService.showAlert(
                            getContext(),
                            null,
                            getString(R.string.give_photo)
                        )
                    }

                    description.isEmpty() -> mAlertService.showAlert(
                        getContext(),
                        null,
                        getString(R.string.write_detials)
                    )


                    description.length < 3 -> {
                        mAlertService.showAlert(
                            getContext(),
                            null,
                            getString(R.string.write_details_at_least_3)
                        )
                    }

                    userInputLocation.isEmpty() -> mAlertService.showAlert(
                        getContext(),
                        null,
                        getString(R.string.give_location)
                    )
                    else -> {
                        val dataMap: HashMap<String, RequestBody> = HashMap()
                        dataMap["description"] = fileUploader.createPartFromString(description)
                        dataMap["location"] = fileUploader.createPartFromString(gpsLocation)
                        dataMap["user_location"] =
                            fileUploader.createPartFromString(userInputLocation)
                        dataMap["latitude"] = fileUploader.createPartFromString("$mLatitude")
                        dataMap["longitude"] = fileUploader.createPartFromString("$mLongitude")
                        dataMap["category_id"] = fileUploader.createPartFromString(complainId)
                        mPresenter.addComplain(
                            dataMap,
                            fileUploader.getMultipartBodyPartFromFile("image", fileToUpload)
                        )
                    }
                }
            }
        }
    }

    private fun manageCameraAndStoragePermission() {
        val storagePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE
        val cameraPermission = Manifest.permission.CAMERA
        val permissions = arrayOf(cameraPermission, storagePermission)
        val listOfPermissionNeeded = ArrayList<String>()
        for (permission in permissions) {
            if (permissionUtils.shouldAskPermission(getContext(), permission)) {
                listOfPermissionNeeded.add(permission)
            }
        }
        mAlertService.showConfirmationAlert(getContext(),
            getString(R.string.permission_required),
            getString(R.string.permission_msg_for_camera),
            getString(R.string.button_not_now),
            getString(R.string.buttoin_continue),
            object : AlertService.AlertListener {

                override fun negativeBtnDidTapped() {}

                override fun positiveBtnDidTapped() {
                    ActivityCompat.requestPermissions(
                        this@AddComplaintsActivity,
                        listOfPermissionNeeded.toTypedArray(),
                        CODE_CAMERA_PERMISSION
                    )
                }
            })

    }

    /** Handling Permission Result  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            CODE_CAMERA_PERMISSION -> if (grantResults.isNotEmpty()) {
                var isBothPermissionGranted = true
                for (i in 0 until grantResults.size) {
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        isBothPermissionGranted = false
                        break
                    }
                }
                if (isBothPermissionGranted) {
                    takePicture()
                } else {
                    manageCameraAndStoragePermission()
                }
            }
        }
    }


    override fun addComplainDidSucceed() {
        fileToUpload?.let {
            if (fileToUpload!!.exists()) {
                fileToUpload!!.delete()
            }
        }
        if ((complaintType == getString(R.string.mosha)) && mosquitoComplainType == 1) {
            mAlertService.showConfirmationAlert(getContext(),
                getString(R.string.add_complain_success_title),
                getString(R.string.add_complain_success_msg_for_mosquito_report),
                null,
                getString(R.string.okay),
                object : AlertService.AlertListener {

                    override fun negativeBtnDidTapped() {
                        // Nothing to do....
                    }

                    override fun positiveBtnDidTapped() {
                        Navigator.sharedInstance.back(
                            getContext(), HomeActivity::class.java
                        )
                        finish()
                    }
                })

        } else {
            mAlertService.showConfirmationAlert(getContext(),
                getString(R.string.add_complain_success_title),
                getString(R.string.add_complain_success_msg),
                null,
                getString(R.string.okay),
                object : AlertService.AlertListener {

                    override fun negativeBtnDidTapped() {
                        // Nothing to do....
                    }

                    override fun positiveBtnDidTapped() {
                        Navigator.sharedInstance.back(
                            getContext(), HomeActivity::class.java
                        )
                        finish()
                    }
                })
        }
    }

    override fun addComplainDidFailed(failedMsg: String) {
        mAlertService.showAlert(getContext(), null, failedMsg)
    }

    override fun geoFencingDidChecked(foundInDncc: Boolean) {
        if (!foundInDncc)
            showOutOfDnccMsg()
    }

    private fun showOutOfDnccMsg() {
        mAlertService.showConfirmationAlert(
            getContext(),
            null,
            getString(R.string.service_not_available_msg),
            null,
            getString(R.string.okay),
            object : AlertService.AlertListener {
                override fun negativeBtnDidTapped() {}

                override fun positiveBtnDidTapped() {
                    onBackPressed()
                }
            }
        )
    }

    override fun onNetworkCallStarted(loadingMessage: String) {
        showProgressDialog(getString(R.string.please_wait))
    }


    /**        Capturing Image with Provider  ----------------  */

    /** Get Bitmap from File >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    private fun getBitmapFromImagePath(imagePath: String?): Bitmap? {
        imagePath?.let {
            val imageFile = File(imagePath)
            if (imageFile.exists()) {
                val myBitmap = BitmapFactory.decodeFile(imageFile.absolutePath)
                val ei = androidx.exifinterface.media.ExifInterface(imageFile.absolutePath)
                val orientation = ei.getAttributeInt(
                    androidx.exifinterface.media.ExifInterface.TAG_ORIENTATION,
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_UNDEFINED
                )
                val rotatedBitmap: Bitmap?
                rotatedBitmap = when (orientation) {
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_ROTATE_90 -> TransformationUtils.rotateImage(
                        myBitmap,
                        90
                    )
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_ROTATE_180 -> TransformationUtils.rotateImage(
                        myBitmap,
                        180
                    )
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_ROTATE_270 -> TransformationUtils.rotateImage(
                        myBitmap,
                        270
                    )
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_NORMAL -> myBitmap
                    else -> myBitmap
                }
                return rotatedBitmap
            }
            return null
        }
        return null
    }


    /** Checking Camera Permission  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    private fun hasCameraPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            getContext(),
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
    }

    /** Checking Storage Permission  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    private fun hasStoragePermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            getContext(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    /** Checking Camera and Storage Permission  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    private fun hasCameraAndStoragePermission(): Boolean {
        return hasCameraPermission() && hasStoragePermission()
    }


    /** Creating Image File  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "IMG_$timeStamp"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            imageFileName,
            /** prefix */
            ".jpg",
            /** suffix */
            storageDir
            /** directory */
        ).apply {
            imagePath = absolutePath
        }
    }


    /** Dispatching Camera Intent  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    private fun takePicture() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(packageManager) != null) {
            var imageFile: File? = null
            try {
                imageFile = createImageFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            imageFile?.let {
                val authorities: String = applicationContext.packageName + ".provider"
                /** Authorities That we provided in Manifest.xml*/
                imageUri = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                    Uri.fromFile(imageFile)
                } else {
                    FileProvider.getUriForFile(getContext(), authorities, imageFile)
                }
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }


    /** Handling image captured by Camera   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_IMAGE_CAPTURE -> {
                if (resultCode == Activity.RESULT_OK) {
                    imagePath?.let {
                        try {
                            imgComplaint.setImageBitmap(getBitmapFromImagePath(it))
                            fileToUpload = ImageCompresser.instance.getCompressedImageFile(it, getContext())
                            if (imagePath != null) {
                                val file = File(imagePath)
                                if (file.exists())
                                    file.delete()
                            }
                            mLatitude = SSFusedLocationProviderClient.mLatitude
                            mLongitude = SSFusedLocationProviderClient.mLongitude
                            val getAddressUrl =
                                "https://barikoi.xyz/v1/api/search/reverse/${getString(R.string.B_API_KEY)}/geocode?longitude=$mLongitude&latitude=$mLatitude"
                            val checkGeoFencingUrl =
                                "https://barikoi.xyz/v1/api/search/dncc/${getString(R.string.B_API_KEY)}/$mLongitude/$mLatitude"
                            mPresenter.getAddressFromBarikoiByLatLong(getAddressUrl)
                            mPresenter.checkGeoFencing(checkGeoFencingUrl)
                        } catch (e: Exception) {
                        }
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        stopLocationService()
        fileToUpload?.let {
            fileToUpload!!.delete()
        }
        spinner1stItemSelectListener = null
        spinner2ndItemSelectListener = null
        super.onDestroy()
    }

    override fun addressDidReceived(address: String) {
        if (address.isNotEmpty()) {
            gpsLocation = address
            etLocation.setText(gpsLocation)
        }
    }

    private fun startLocationService() {
        if(ssFusedLocationApiClient == null) ssFusedLocationApiClient = SSFusedLocationProviderClient(getContext())
        ssFusedLocationApiClient?.startLocationUpdates()
//        Intent(getContext().applicationContext, LocationService::class.java).also { intent ->
//            startService(intent)
//        }
    }

    private fun stopLocationService() {
        ssFusedLocationApiClient?.stopLocationUpdates()
        ssFusedLocationApiClient?.onDestroy()
        ssFusedLocationApiClient = null
//        Intent(getContext().applicationContext, LocationService::class.java).also { intent ->
//            stopService(intent)
//        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            if (it.itemId == android.R.id.home) {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        Navigator.sharedInstance.back(getContext(), HomeActivity::class.java)
        finish()
    }

    private var spinner1stItemSelectListener: AdapterView.OnItemSelectedListener? =
        object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                for (firstLayerItem in issueTypeResponse!!.data!!) {
                    if (firstLayerItem.name == parent!!.getItemAtPosition(position).toString()) {
                        complainId = "${firstLayerItem.id}"

                        Log.e(">>>>>>>>>>>>>>", "${firstLayerItem.name}: ${firstLayerItem.id}")

                        if (firstLayerItem.secondCategory.isEmpty()) {
                            headingIssue.visibility = View.GONE
                            spinner2ndLayer.visibility = View.GONE

                        } else {
                            headingIssue.visibility = View.VISIBLE
                            spinner2ndLayer.visibility = View.VISIBLE

                            val layer2ndArray =
                                arrayOfNulls<String>(firstLayerItem.secondCategory.size + 1)
                            layer2nd = firstLayerItem.secondCategory

                            layer2ndArray[0] = getString(R.string.h_select_issue)
                            for (i in firstLayerItem.secondCategory.indices) {
                                layer2ndArray[i + 1] = firstLayerItem.secondCategory[i].name
                            }
                            secondLayerAdapter = object : ArrayAdapter<String>(
                                getContext(), // Context
                                R.layout.item_view_spinner, // Layout
                                layer2ndArray // Array
                            ) {
                                override fun isEnabled(position: Int): Boolean {
                                    return position != 0
                                }

                                override fun getView(
                                    position: Int,
                                    convertView: View?,
                                    parent: ViewGroup
                                ): View {
                                    val v = super.getView(position, convertView, parent)
                                    if (position == 0) {
                                        (v as TextView).setTextColor(Color.GRAY)
                                    } else {
                                        (v as TextView).setTextColor(
                                            ContextCompat.getColor(
                                                context,
                                                R.color.colorPrimary
                                            )
                                        )
                                    }
                                    return v
                                }

                                override fun getDropDownView(
                                    position: Int,
                                    convertView: View?,
                                    parent: ViewGroup
                                ): View {
                                    val view =
                                        super.getDropDownView(position, convertView, parent)
                                    val tv: TextView = view as TextView
                                    if (position == 0) {
                                        tv.setTextColor(Color.GRAY)
                                    } else {
                                        tv.setTextColor(
                                            ContextCompat.getColor(
                                                context,
                                                R.color.colorPrimary
                                            )
                                        )
                                    }
                                    return view
                                }
                            }
                            spinner2ndLayer.adapter = secondLayerAdapter
                            spinner2ndLayer.onItemSelectedListener = spinner2ndItemSelectListener
                        }
                        break
                    }
                }
            }

        }

    private var spinner2ndItemSelectListener: AdapterView.OnItemSelectedListener? =
        object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                layer2nd?.let {
                    for (item in it) {
                        if (item.name == parent!!.getItemAtPosition(position).toString()) {
                            complainId = "${item.id}"
                            Log.e(">>>>>>>>>>>>>>", "${item.name}: ${item.id}")
                            break
                        }
                    }
                }
            }
        }

    override fun issueSubTypeDidReceived(response: IssueTypeResponse) {
        issueTypeResponse = response
        if (response.status == 200) {
            headingIssueType.visibility = View.VISIBLE
            // headingIssue.visibility = View.VISIBLE
            spinner1sttLayer.visibility = View.VISIBLE
            //  spinner2ndLayer.visibility = View.VISIBLE

            if (response.data != null) {
                val firstLayerArray = arrayOfNulls<String>(response.data.size + 1)
                complainId = "${response.data[0].id}"
                firstLayerArray[0] = getString(R.string.h_select_issue_type)
                for (i in response.data.indices) {
                    firstLayerArray[i + 1] = response.data[i].name
                }
                firstLayerAdapter = object : ArrayAdapter<String>(
                    getContext(), // Context
                    R.layout.item_view_spinner, // Layout
                    firstLayerArray // Array
                ) {
                    override fun isEnabled(position: Int): Boolean {
                        return position != 0
                    }

                    override fun getView(
                        position: Int,
                        convertView: View?,
                        parent: ViewGroup
                    ): View {
                        val v = super.getView(position, convertView, parent)
                        if (position == 0) {
                            (v as TextView).setTextColor(Color.GRAY)
                        } else {
                            (v as TextView).setTextColor(
                                ContextCompat.getColor(
                                    context,
                                    R.color.colorPrimary
                                )
                            )
                        }
                        return v
                    }

                    override fun getDropDownView(
                        position: Int,
                        convertView: View?,
                        parent: ViewGroup
                    ): View {
                        val view =
                            super.getDropDownView(position, convertView, parent)
                        val tv: TextView = view as TextView
                        if (position == 0) {
                            tv.setTextColor(Color.GRAY)
                        } else {
                            tv.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                        }
                        return view
                    }
                }
                spinner1sttLayer.adapter = firstLayerAdapter
                spinner1sttLayer.onItemSelectedListener = spinner1stItemSelectListener

//                if (response.data[0].secondCategory.isNotEmpty()) {
//                    spinner2ndLayer.visibility = View.VISIBLE
//
//                    val layer2ndArray =
//                        arrayOfNulls<String>(response.data[0].secondCategory.size + 1)
//
//                    layer2nd = response.data[0].secondCategory
//                    complainId = "${response.data[0].secondCategory[0].id}"
//
//                    layer2ndArray[0] = "--Select Issue--"
//                    for (i in response.data[0].secondCategory.indices) {
//                        layer2ndArray[i + 1] = response.data[0].secondCategory[i].name
//                    }
//                    secondLayerAdapter = object: ArrayAdapter<String>(
//                        getContext(), // Context
//                        R.layout.item_view_spinner, // Layout
//                        layer2ndArray // Array
//                    ){
//                        override fun isEnabled(position: Int): Boolean {
//                            return position != 0
//                        }
//
//                        override fun getView(
//                            position: Int,
//                            convertView: View?,
//                            parent: ViewGroup
//                        ): View {
//                            val v = super.getView(position, convertView, parent)
//                            if (position == 0) {
//                                (v as TextView).setTextColor(Color.GRAY)
//                            } else {
//                                (v as TextView).setTextColor(
//                                    ContextCompat.getColor(
//                                        context,
//                                        R.color.colorPrimary
//                                    )
//                                )
//                            }
//                            return v
//                        }
//
//                        override fun getDropDownView(
//                            position: Int,
//                            convertView: View?,
//                            parent: ViewGroup
//                        ): View {
//                            val view =
//                                super.getDropDownView(position, convertView, parent)
//                            val tv: TextView = view as TextView
//                            if (position == 0) {
//                                tv.setTextColor(Color.GRAY)
//                            } else {
//                                tv.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
//                            }
//                            return view
//                        }
//                    }
//                    spinner2ndLayer.adapter = secondLayerAdapter
//                    spinner2ndLayer.onItemSelectedListener = spinner2ndItemSelectListener
//                } else {
//                    spinner1sttLayer.visibility = View.GONE
//                }
            }
        } else {
            headingIssueType.visibility = View.GONE
            headingIssue.visibility = View.GONE
            spinner1sttLayer.visibility = View.GONE
            spinner2ndLayer.visibility = View.GONE
        }
    }
}
