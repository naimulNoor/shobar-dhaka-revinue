package com.shobardhaka.citizenrevenue.ui.fragments.shotortobarta

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.GetDisasterResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import javax.inject.Inject


class ShotorkobartaFrgPresenter @Inject constructor(view: ShotorkobartaFrgContract.View) :
    BasePresenter<ShotorkobartaFrgContract.View>(view), ShotorkobartaFrgContract.Presenter {

    @Inject
    lateinit var userApiService: UserApiService

    override fun getDisasters() {
        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            userApiService.getDisasters()
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<GetDisasterResponse, ShotorkobartaFrgContract.View>(mView) {
                    override fun onRequestSuccess(response: GetDisasterResponse) {
                        mView?.disasterDidReceived(response.disaster)
                    }
                })
        )
    }

    override fun setDisasterAsRead(url: String) {
        compositeDisposable?.add(
            userApiService.setDisasterAsSeen(url)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<GetDisasterResponse.SingleDisaster, ShotorkobartaFrgContract.View>(mView) {
                    override fun onRequestSuccess(response: GetDisasterResponse.SingleDisaster) {
                        // No need to notify
                    }
                })
        )
    }
}