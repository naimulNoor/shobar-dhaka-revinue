package com.shobardhaka.citizenrevenue.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.OnClick
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BaseActivity
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.ui.auth.login.LoginActivity
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import com.shobardhaka.citizenrevenue.utils.Navigator
import kotlinx.android.synthetic.main.activity_language_picker.*

class LanguagePickerActivity : BaseActivity() {

    private val nextEng = "Next"
    private val nextBangla = "পরবর্তী"
    private val selectLngBangla = "ভাষা নির্বাচন করুন"
    private val selectLngEng = "Select Language"
    private var selectedLanguage = -1

    override fun getContentView(): Int {
        return R.layout.activity_language_picker
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        tvChangeHeading.text = selectLngEng
        btnNext.text = nextEng
        btnNext.isEnabled = false
    }

    @OnClick(R.id.btnBangla, R.id.btnEnglish)
    fun onBtnClicked(view: View) {
        btnNext.isEnabled = true
        when (view.id) {
            R.id.btnBangla -> {
                selectedLanguage = 0
                tvChangeHeading.text = selectLngBangla
                btnNext.text = nextBangla
                btnBangla.isSelected = true
                btnEnglish.isSelected = false
                radioBangla.isChecked = true
                radioEnglish.isChecked = false
            }
            R.id.btnEnglish -> {
                selectedLanguage = 1
                tvChangeHeading.text = selectLngEng
                btnNext.text = nextEng
                btnBangla.isSelected = false
                btnEnglish.isSelected = true
                radioBangla.isChecked = false
                radioEnglish.isChecked = true
            }
        }
    }

    @OnClick(R.id.btnNext)
    fun nextBtnDidClicked() {
        when (selectedLanguage) {
            0 -> {
                AppUtils.shared.setLocale(getContext(), AppLanguage.BENGALI)
                mPrefs.putString(Keys.APP_LANGUAGE.name, AppLanguage.BENGALI.name)
            }

            1 -> {
                AppUtils.shared.setLocale(getContext(), AppLanguage.ENGLISH)
                mPrefs.putString(Keys.APP_LANGUAGE.name, AppLanguage.ENGLISH.name)
            }
        }
        Navigator.sharedInstance.navigate(this, LoginActivity::class.java)
        finish()
    }
}
