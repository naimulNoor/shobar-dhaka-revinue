package com.shobardhaka.citizenrevenue.ui.fragments.shotortobarta

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.adapters.DisasterAdapter
import com.shobardhaka.citizenrevenue.base.BaseFragment
import com.shobardhaka.citizenrevenue.callbacks.DisasterSeenListener
import com.shobardhaka.citizenrevenue.data.network.APIs
import com.shobardhaka.citizenrevenue.data.network.api_response.GetDisasterResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.ui.DisasterDetailsActivity
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import com.shobardhaka.citizenrevenue.utils.Navigator
import kotlinx.android.synthetic.main.fragment_shotorkobarta.view.*

class ShotorkobartaFragment : BaseFragment<ShotorkobartaFrgPresenter>(), ShotorkobartaFrgContract.View {

    companion object {
        fun newInstance(): ShotorkobartaFragment {
            return ShotorkobartaFragment()
        }
    }

    private lateinit var disasterAdapter: DisasterAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var animationController: LayoutAnimationController
    private var disasterSeenListener: DisasterSeenListener? = null

    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, saveInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_shotorkobarta, container, false)

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            disasterSeenListener = context as DisasterSeenListener
        } catch (e: Exception) {
        }
    }

    override fun onDetach() {
        super.onDetach()
        disasterSeenListener = null
    }

    override fun onViewReady(getArguments: Bundle?) {
        animationController = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_slide_from_right);

        linearLayoutManager = LinearLayoutManager(
            context,
            RecyclerView.VERTICAL,
            false
        )

        disasterAdapter = DisasterAdapter(context, object : DisasterAdapter.OnDetailsBtnClickListener {

            override fun detailsBtnDidClicked(position: Int) {
                val singleDisaster: GetDisasterResponse.SingleDisaster =
                    disasterAdapter.getItem(position) as GetDisasterResponse.SingleDisaster
                if (singleDisaster.isRead == 0) {
                    singleDisaster.isRead = 1
                    disasterSeenListener?.onSeenDisaster()
                    disasterAdapter.notifyItemChanged(position)
                    mPresenter.setDisasterAsRead("${APIs.GET_DISASTER_DETAILS}${singleDisaster.id}")
                }
                val dataBundle = Bundle()
                dataBundle.putSerializable(Keys.single_disaster.name, singleDisaster)
                Navigator.sharedInstance.navigateWithBundle(
                    context,
                    DisasterDetailsActivity::class.java,
                    Keys.DATA_BUNDLE.name,
                    dataBundle
                )
            }
        })

        rootView.recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = disasterAdapter
            layoutAnimation = animationController
        }

        rootView.imgStatus.visibility = View.GONE
        mPresenter.getDisasters()
    }


    override fun onNetworkCallStarted(loadingMessage: String) {
        rootView.progress_circular.visibility = View.VISIBLE
    }

    override fun onNetworkCallEnded() {
        rootView.progress_circular.visibility = View.GONE
    }

    override fun disasterDidReceived(disasters: ArrayList<GetDisasterResponse.SingleDisaster?>) {
        if (disasters.isNotEmpty()) {
            disasterAdapter.addAll(disasters)
            rootView.recyclerView.scheduleLayoutAnimation()
        } else {
            rootView.imgStatus.visibility = View.VISIBLE
            if (mAppLanguage == AppLanguage.BENGALI.name)
                rootView.imgStatus.setImageResource(R.drawable.ic_empty_disaster_ben)
            else
                rootView.imgStatus.setImageResource(R.drawable.ic_empty_disaster_eng)
        }
    }

}
