package com.shobardhaka.citizenrevenue.ui.fragments.events

import com.shobardhaka.citizenrevenue.base.BaseContract
import com.shobardhaka.citizenrevenue.data.network.api_response.GetEventResponse

interface EventsFrgContract{

    interface View: BaseContract.View{
        fun eventsDidReceived(events: ArrayList<GetEventResponse.SingleEvent?>)
    }

    interface Presenter: BaseContract.Presenter{
        fun getEvents()
    }
}