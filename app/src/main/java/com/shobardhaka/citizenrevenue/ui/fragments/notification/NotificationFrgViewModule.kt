package com.shobardhaka.citizenrevenue.ui.fragments.notification

import dagger.Binds
import dagger.Module

@Module
abstract class NotificationFrgViewModule {

    @Binds
    abstract fun provideNotificationFrgViewModule(fragment: NotificationFragment): NotificationFrgContract.View
}