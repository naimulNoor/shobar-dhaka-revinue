package com.shobardhaka.citizenrevenue.ui.profile

import com.shobardhaka.citizenrevenue.base.BaseContract
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.HashMap

/**
 * Created  on 19/08/2019.
 */
interface ProfileContract {

    interface View : BaseContract.View {
        fun profileUpdateDidSucceed()
        fun profileUpdateDidFailed(failedMsg: String)
    }

    interface Presenter : BaseContract.Presenter {
        fun updateProfile(
            dataMap: HashMap<String, RequestBody>,
            attachment: MultipartBody.Part?
        )
    }
}