package com.shobardhaka.citizenrevenue.ui.complaints.add_complaint

import dagger.Binds
import dagger.Module

@Module
abstract class AddComplaintViewModule {
    @Binds
    abstract fun provideAddComplaintView(activity: AddComplaintsActivity): AddComplaintContract.View
}