package com.shobardhaka.citizenrevenue.ui.department_heads

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.GetDepartmentalHeadsResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import javax.inject.Inject

class DepartmentHeadsPresenter @Inject constructor(view: DepartmentHeadsContract.View) :
    BasePresenter<DepartmentHeadsContract.View>(view), DepartmentHeadsContract.Presenter {

    @Inject
    lateinit var userApiService: UserApiService

    override fun getDepartmentalHeads() {
        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            userApiService.getDepartmentHeads()
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<GetDepartmentalHeadsResponse, DepartmentHeadsContract.View>(mView) {
                    override fun onRequestSuccess(response: GetDepartmentalHeadsResponse) {
                        mView?.departmentalHeadsDidReceived(response.heads)
                    }

                })
        )
    }
}