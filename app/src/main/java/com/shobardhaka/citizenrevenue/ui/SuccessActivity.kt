package com.shobardhaka.citizenrevenue.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.shobardhaka.citizenrevenue.R

class SuccessActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success)
    }
}
