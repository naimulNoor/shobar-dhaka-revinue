package com.shobardhaka.citizenrevenue.ui.fragments.temperature

import dagger.Binds
import dagger.Module

@Module
abstract class TemperatureFrgViewModule {
    @Binds
    abstract fun provideTemperatureViewModule(fragment: TemperatureFragment): TemperatureFrgContract.View
}