package com.shobardhaka.citizenrevenue.ui.emergency
import dagger.Binds
import dagger.Module

@Module
abstract class EmergencyServiceViewModule {
    @Binds
    abstract fun provideEmergencyServiceView(activity: EmergencyServiceActivity): EmergencyServiceContract.View
}