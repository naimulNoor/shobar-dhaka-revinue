package com.shobardhaka.citizenrevenue.ui.settings

import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.PostDataResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import javax.inject.Inject

class SettingsPresenter @Inject constructor(view: SettingsContract.View) :
    BasePresenter<SettingsContract.View>(view),
    SettingsContract.Presenter {

    @Inject
    lateinit var userApiService: UserApiService

    override fun storeDeviceToken(deviceToken: String) {
        compositeDisposable?.add(
            userApiService.storeDeviceToken(deviceToken)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<PostDataResponse, SettingsContract.View>(mView) {
                    override fun onRequestSuccess(response: PostDataResponse) {
                        // Nothing to do....
                    }
                })
        )
    }

    override fun removeDeviceToken() {
        compositeDisposable?.add(
            userApiService.removeDeviceToken()
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<PostDataResponse, SettingsContract.View>(mView) {
                    override fun onRequestSuccess(response: PostDataResponse) {
                        // Nothing to do....
                    }
                })
        )
    }
}