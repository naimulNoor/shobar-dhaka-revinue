package com.shobardhaka.citizenrevenue.ui.splash

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AlertDialog
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.bumptech.glide.Glide
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.data.network.api_response.GetAppVersionResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.data.prefs.PrefKeys
import com.shobardhaka.citizenrevenue.data.prefs.PreferenceManager
import com.shobardhaka.citizenrevenue.ui.LanguagePickerActivity
import com.shobardhaka.citizenrevenue.ui.auth.login.LoginActivity
import com.shobardhaka.citizenrevenue.ui.home.HomeActivity
import com.shobardhaka.citizenrevenue.utils.AlertService
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.Keys.IntentKeys
import com.shobardhaka.citizenrevenue.utils.Navigator
import com.shobardhaka.citizenrevenue.utils.showToast
import kotlinx.android.synthetic.main.activity_splash_screen.*


class SplashScreenActivity : MvpBaseActivity<SplashPresenter>(), SplashContract.View {

    override fun getContentView(): Int {
        return R.layout.activity_splash_screen
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        Glide.with(getContext()).load(R.drawable.logo_splash_screen).into(imgShobardhaka)
        val copyrightVersion =
            "${getString(R.string.copyright)}${AppUtils.shared.getVersionName(getContext())}"
        tvVersion.text = copyrightVersion
        Handler().postDelayed({ mPresenter.getAppVersion() }, 500)
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(getContext())
            .registerReceiver(mCurrentLocationReceiver, IntentFilter(IntentKeys.LOCATION_ACTION))
    }

    override fun onPause() {
        super.onPause()
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mCurrentLocationReceiver)
    }


    override fun onNetworkUnavailable() {
        val alertDialog = AlertDialog.Builder(getContext())
        alertDialog.setTitle(getString(R.string.no_internet_connection))
        alertDialog.setMessage(getString(R.string.no_internet_msg))
        alertDialog.setPositiveButton(getString(R.string.okay)) { _, _ ->
            mPresenter.getAppVersion()
        }
        alertDialog.create().show()
    }

    override fun appVersionDidReceived(response: GetAppVersionResponse) {
        val isForceUpdateEnable = if(response.appDetails.isForceUpdateEnable != null && response.appDetails.isForceUpdateEnable == 1) 1 else 0
        val playStoreAppVersion = response.appDetails.appVersion
        val installedAppVersion = AppUtils.shared.getAppVersion(getContext())
        val title = getString(R.string.update_available_title)
        val msg = getString(R.string.update_available_message)
        val updateNow = getString(R.string.button_update)
        val notNow = getString(R.string.button_not_now)
        if (playStoreAppVersion > installedAppVersion) {
            mAlertService.showConfirmationAlert(
                getContext(),
                title,
                msg,
                if (isForceUpdateEnable == 1) null else notNow,
                updateNow,
                object : AlertService.AlertListener {
                    override fun negativeBtnDidTapped() {
                        loginNew()
                    }

                    override fun positiveBtnDidTapped() {
                        AppUtils.shared.openAppOnPlayStore(
                            getContext(),
                            getContext().applicationContext.packageName
                        )
                    }
                }
            )
        } else {
            loginNew()
        }
    }

    private fun loginNew() {
        if (mPrefManager.getBoolean(
                PrefKeys.IS_REGISTERED,
                false
            ) && PreferenceManager(getContext()).getBoolean(
                Keys.KEEP_ME_LOGGED_IN.name,
                false
            )
        ) {
            Navigator.sharedInstance.navigate(this, HomeActivity::class.java)
        } else {
            val language = mPrefManager.getString(Keys.APP_LANGUAGE.name, "")
            if (language!!.isEmpty()) {
                Navigator.sharedInstance.navigate(this, LanguagePickerActivity::class.java)
            } else {
                Navigator.sharedInstance.navigate(this, LoginActivity::class.java)
            }
        }
        finish()
    }


    override fun onDestroy() {
        imgShobardhaka.setImageDrawable(null)
//        Intent(getContext().applicationContext, LocationService::class.java).also { intent ->
//            stopService(intent)
//        }
        super.onDestroy()
    }

    override fun addressDidReceived(address: String) {
        showToast(address)
    }

    private val mCurrentLocationReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let {
                if (intent.action == IntentKeys.LOCATION_ACTION) {
                    val lat = intent.getStringExtra(IntentKeys.LATITUDE)
                    val long = intent.getStringExtra(IntentKeys.LONGITUDE)
                    val getAddressUrl =
                        "https://barikoi.xyz/v1/api/search/reverse/MTQ5MTpPREpRUlY0QlZI/geocode?longitude=$long&latitude=$lat"
                    mPresenter.getAddressFromBarikoiByLatLong(getAddressUrl)
                }
            }
        }
    }
}
