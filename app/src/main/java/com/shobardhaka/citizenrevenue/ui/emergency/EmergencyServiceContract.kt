package com.shobardhaka.citizenrevenue.ui.emergency

import com.shobardhaka.citizenrevenue.base.BaseContract
import com.shobardhaka.citizenrevenue.data.network.api_response.GetEmergencyServiceResponse

interface EmergencyServiceContract {

    interface View : BaseContract.View {

        fun emergencyServicesDidReceived(services: ArrayList<GetEmergencyServiceResponse.SingleEmergencyService?>)
    }

    interface Presenter : BaseContract.Presenter {

        fun getEmergencyServices()
    }
}