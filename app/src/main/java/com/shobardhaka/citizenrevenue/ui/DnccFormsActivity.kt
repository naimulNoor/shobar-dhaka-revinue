package com.shobardhaka.citizenrevenue.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.OnClick
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BaseActivity
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.NetworkUtils
import com.shobardhaka.citizenrevenue.utils.showToast
import kotlinx.android.synthetic.main.toolbar_with_text.*

class DnccFormsActivity : BaseActivity() {

    override fun getContentView(): Int {
        return R.layout.activity_dncc_forms
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {

        setSupportActionBar(toolbar)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        tvTitle.text = getString(R.string.dncc_forms)
    }

    @OnClick(R.id.tvBirthForm, R.id.tvDeathForm, R.id.tvHoldingForm, R.id.tvTradeLicenseForm)
    fun onClicked(view: View) {
        if (NetworkUtils.sharedInstance.isConnectedToNetwork(getContext())) {
            when (view.id) {
                R.id.tvBirthForm -> {
                    AppUtils.shared.openLinkOnExternalBrowser(getContext(), getString(R.string.birth_form_link))
                }
                R.id.tvDeathForm -> {
                    AppUtils.shared.openLinkOnExternalBrowser(getContext(), getString(R.string.death_forms_link))
                }
                R.id.tvTradeLicenseForm -> {
                    AppUtils.shared.openLinkOnExternalBrowser(getContext(), getString(R.string.trade_license_form_link))
                }
                R.id.tvHoldingForm -> {
                    AppUtils.shared.openLinkOnExternalBrowser(getContext(), getString(R.string.holding_name_form_link))
                }
            }

        } else {
            showToast(getString(R.string.no_internet_connection))
        }
    }
}
