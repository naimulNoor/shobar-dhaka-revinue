package com.shobardhaka.citizenrevenue.ui.councilors

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.NumberPicker
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.OnClick
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.adapters.CouncilorAdapter
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.callbacks.CallButtonClickListener
import com.shobardhaka.citizenrevenue.data.network.api_response.GetCouncilorResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.GetWardsResponse
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import com.shobardhaka.citizenrevenue.utils.PermissionUtils
import com.shobardhaka.citizenrevenue.utils.showToast
import kotlinx.android.synthetic.main.toolbar_with_text.*
import kotlinx.android.synthetic.main.activity_councilors.*
import javax.inject.Inject


class CouncilorsActivity : MvpBaseActivity<CouncilorPresenter>(), CouncilorContract.View {

    // Councilor lists
    private lateinit var councilorAdapter: CouncilorAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var councilorList: ArrayList<GetCouncilorResponse.SingleCouncilor?> = ArrayList()

    @Inject
    lateinit var permissionUtils: PermissionUtils
    var currentSelectedPhone = ""
    internal val REQUEST_CALL_PHONE_PERMISSION_CODE = 15
    internal var CALL_PHONE_PERMISSION = arrayOf(Manifest.permission.CALL_PHONE)

    // Councilor Filter
    private var zoneWards: Map<String, ArrayList<String>>? = null
    private var zones: List<String>? = null
    private var wards: List<String>? = null
    private var selectedZone: String = "1"
    private var selectedWard: String = "1"

    override fun getContentView(): Int {
        return R.layout.activity_councilors
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }
        tvTitle.text = getString(R.string.councilors)

        linearLayoutManager = LinearLayoutManager(
            getContext(),
            RecyclerView.VERTICAL,
            false
        )
        councilorAdapter = CouncilorAdapter(getContext(), object : CallButtonClickListener {
            override fun onCallButtonClicked(number: String) {
                AppUtils.shared.dial(getContext(), number)

//                if (!permissionUtils.shouldAskPermission(getContext(), Manifest.permission.CALL_PHONE)) {
//                    AppUtils.shared.dial(getContext(), number)
//                } else {
//                    currentSelectedPhone = number
//                    mAlertService.showConfirmationAlert(this@CouncilorsActivity,
//                        getString(R.string.permission_required),
//                        getString(R.string.permission_call_phone_one),
//                        getString(R.string.button_not_now),
//                        getString(R.string.okay),
//                        object : AlertService.AlertListener {
//
//                            override fun negativeBtnDidTapped() {}
//
//                            override fun positiveBtnDidTapped() {
//                                ActivityCompat.requestPermissions(
//                                    this@CouncilorsActivity,
//                                    CALL_PHONE_PERMISSION,
//                                    REQUEST_CALL_PHONE_PERMISSION_CODE
//                                )
//
//                            }
//                        })
//                }

            }
        })

        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = councilorAdapter
        }

        //Populate NumberPicker values from String array values
        //Set the minimum value of NumberPicker
        pickerWard.minValue = 0

        //Gets whether the selector wheel wraps when reaching the min/max value.
        pickerZone.wrapSelectorWheel = true
        pickerWard.wrapSelectorWheel = true

        pickerZone.setOnValueChangedListener(pickerValueChangeListener)
        pickerWard.setOnValueChangedListener(pickerValueChangeListener)

        mPresenter.getZoneWards()
    }


    override fun councilorsDidReceived(councilors: ArrayList<GetCouncilorResponse.SingleCouncilor?>) {
        councilorList = councilors
        if (councilorList.isNotEmpty()) {
            councilorAdapter.setData(councilors)
        } else {
            showToast(getString(R.string.no_councilor_found))
        }
    }

    override fun onFailedToFetchData(failedMsg: String) {
        showToast(failedMsg)
    }

    @OnClick(R.id.btnSearch)
    fun searchBtnDidTapped() {
        councilorList.clear()
        councilorAdapter.setData(councilorList)
        mPresenter.getCouncilors(selectedZone, selectedWard)
    }

    override fun zoneWardsDidReceived(response: GetWardsResponse) {
        zoneWards = response.zone
        zoneWards?.let {
            zones = it.keys.toList()
            if (zones != null) {
                // Setting Zone Picker...
                pickerZone.minValue = 0 //from array first value
                pickerZone.maxValue = zones!!.size - 1
                //    val zonesWithPrefix = zones!!.map { zone -> "${getString(R.string.zone)}-${AppUtils.shared.getInBangla(zone)}" }
                val zonesWithPrefix = zones!!.map { zone ->
                    "${getString(R.string.zone)}-${if (mAppLanguage == AppLanguage.BENGALI.name) AppUtils.shared.getInBangla(
                        zone
                    ) else zone}"
                }
                pickerZone.displayedValues = zonesWithPrefix.toTypedArray()
                selectedZone = zones!![pickerZone.value]

                // Setting Ward Picker...
                wards = it[zones!![0]]?.toList()
                if (wards != null) {
                    pickerWard.minValue = 0
                    pickerWard.maxValue = wards!!.size - 1
                    val wardWithPrefix =
                        wards!!.map { ward ->
                            "${getString(R.string.ward)}-${if (mAppLanguage == AppLanguage.BENGALI.name) AppUtils.shared.getInBangla(
                                ward
                            ) else ward}"
                        }
                    pickerWard.displayedValues = wardWithPrefix.toTypedArray()
                    selectedWard = wards!![pickerWard.value]
                }
            }
        }
    }

    private val pickerValueChangeListener: NumberPicker.OnValueChangeListener =
        NumberPicker.OnValueChangeListener { picker, _, newPosition ->
            when (picker) {
                pickerZone -> {
                    zoneWards?.let {
                        val zones: List<String> = it.keys.toList()
                        selectedZone = zones[newPosition]
                        wards = it[selectedZone]?.toList()

                        if (wards != null) {
                            pickerWard.displayedValues = null
                            pickerWard.maxValue = wards!!.size - 1
                            val wardWithPrefix =
                                wards!!.map { ward ->
                                    "${getString(R.string.ward)}-${if (mAppLanguage == AppLanguage.BENGALI.name) AppUtils.shared.getInBangla(
                                        ward
                                    ) else ward}"
                                }
                            pickerWard.displayedValues = wardWithPrefix.toTypedArray()
                            selectedWard = wards!![pickerWard.value]
                        }
                    }
                }
                pickerWard -> {
                    wards?.let {
                        selectedWard = wards!![newPosition]
                    }
                }
            }
        }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CALL_PHONE_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                AppUtils.shared.dial(getContext(), currentSelectedPhone)
            }
        }
    }

    override fun onNetworkCallStarted(loadingMessage: String) {
        progressBar.visibility = View.VISIBLE
    }

    override fun onNetworkCallEnded() {
        progressBar.visibility = View.GONE
    }
}
