package com.shobardhaka.citizenrevenue.ui

import android.content.Intent
import android.os.Bundle
import android.text.util.Linkify
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BaseActivity
import com.shobardhaka.citizenrevenue.data.network.api_response.GetDisasterResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.file.PicassoImageLoader
import com.shobardhaka.citizenrevenue.utils.DateFormatUtils
import kotlinx.android.synthetic.main.activity_disaster_details.*
import kotlinx.android.synthetic.main.toolbar_with_text.*

class DisasterDetailsActivity : BaseActivity() {

    override fun getContentView(): Int {
        return R.layout.activity_disaster_details
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }

        tvTitle.text = getString(R.string.disaster_details)
        val singleDisaster: GetDisasterResponse.SingleDisaster =
            intent.getBundleExtra(Keys.DATA_BUNDLE.name).getSerializable(Keys.single_disaster.name) as GetDisasterResponse.SingleDisaster

        PicassoImageLoader.shared.loadImage(
            getContext(),
            singleDisaster.image,
            imgDisaster
        )
        tvDisasterDate.text = DateFormatUtils.shared.getDate(singleDisaster.createdAt)
        tvDisasterTitle.text = singleDisaster.title

        tvDisasterDetails.text = singleDisaster.description
        Linkify.addLinks(tvDisasterDetails, Linkify.WEB_URLS)
    }
}
