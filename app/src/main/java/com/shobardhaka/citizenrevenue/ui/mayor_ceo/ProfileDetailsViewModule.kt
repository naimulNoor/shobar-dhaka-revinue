package com.shobardhaka.citizenrevenue.ui.mayor_ceo

import dagger.Binds
import dagger.Module

@Module
abstract class ProfileDetailsViewModule {
    @Binds
    abstract fun provideProfileDetailsView(activity: ProfileDetailsActivity): ProfileDetailsContract.View
}