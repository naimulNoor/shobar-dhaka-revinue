package com.shobardhaka.citizenrevenue.ui.councilors

import dagger.Binds
import dagger.Module

@Module
abstract class CouncilorViewModule {
    @Binds
    abstract fun provideCouncilrosHeadsViewModule(activity: CouncilorsActivity): CouncilorContract.View
}