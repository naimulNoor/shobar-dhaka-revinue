package com.shobardhaka.citizenrevenue.ui.dncc_office

import com.shobardhaka.citizenrevenue.base.BaseContract
import com.shobardhaka.citizenrevenue.data.network.api_response.BariKoiResponse

interface NearbyDnccContract{
    interface View: BaseContract.View{
        fun nearByPlacesDidReceived(nearByPlaces: ArrayList<BariKoiResponse.SinglePlace?>?)
        fun noNearbyPlaceFound(message: String)
    }

    interface Presenter: BaseContract.Presenter{
        fun getNearByPlace(url: String)
    }
}