package com.shobardhaka.citizenrevenue.ui.fragments.events

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.adapters.EventsAdapter
import com.shobardhaka.citizenrevenue.adapters.RecyclerTouchListener
import com.shobardhaka.citizenrevenue.base.BaseFragment
import com.shobardhaka.citizenrevenue.data.network.api_response.GetEventResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.ui.event_detials.EventDetailsActivity
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import com.shobardhaka.citizenrevenue.utils.Navigator
import kotlinx.android.synthetic.main.fragment_events.view.*

class EventsFragment : BaseFragment<EventsFrgPresenter>(), EventsFrgContract.View {

    private lateinit var eventsAdapter: EventsAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var animationController: LayoutAnimationController

    companion object {
        fun newInstance(): EventsFragment {
            return EventsFragment()
        }
    }

    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, saveInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_events, container, false)

    }

    override fun onViewReady(getArguments: Bundle?) {

        animationController = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_slide_from_right);
        linearLayoutManager = LinearLayoutManager(
            context,
            RecyclerView.VERTICAL,
            false
        )

        eventsAdapter = EventsAdapter(context)

        rootView.recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = eventsAdapter
            layoutAnimation = animationController
        }
        rootView.recyclerView.addOnItemTouchListener(
            RecyclerTouchListener(
                context,
                rootView.recyclerView,
                object : RecyclerTouchListener.ClickListener {
                    override fun onClick(view: View, position: Int) {
                        val singleEvent: GetEventResponse.SingleEvent =
                            eventsAdapter.getItem(position) as GetEventResponse.SingleEvent
                        val dataBundle = Bundle()
                        dataBundle.putSerializable(Keys.single_event.name, singleEvent)
                        Navigator.sharedInstance.navigateWithBundle(
                            context,
                            EventDetailsActivity::class.java,
                            Keys.DATA_BUNDLE.name,
                            dataBundle
                        )
                    }

                    override fun onLongClick(view: View?, position: Int) {
                        // Nothing to do.......
                    }
                })
        )

        mPresenter.getEvents()
        rootView.imgNoEventFound.visibility = View.GONE
    }

    override fun onNetworkCallStarted(loadingMessage: String) {
        rootView.progressBar.visibility = View.VISIBLE
    }

    override fun onNetworkCallEnded() {
        rootView.progressBar.visibility = View.GONE
    }

    override fun eventsDidReceived(events: ArrayList<GetEventResponse.SingleEvent?>) {
        if (events.isNotEmpty()) {
            eventsAdapter.addAll(events)
            rootView.recyclerView.scheduleLayoutAnimation()
        } else {
            if (mAppLanguage == AppLanguage.BENGALI.name) {
                rootView.imgNoEventFound.setImageResource(R.drawable.no_event_bn)
            } else {
                rootView.imgNoEventFound.setImageResource(R.drawable.no_event_en)
            }
            rootView.imgNoEventFound.visibility = View.VISIBLE
        }
    }
}
