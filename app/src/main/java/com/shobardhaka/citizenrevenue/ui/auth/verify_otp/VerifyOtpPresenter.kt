package com.shobardhaka.citizenrevenue.ui.auth.verify_otp

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.RetrofitHelperService
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.RequestOtpResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.VerifyOtpResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.UserAuthApiService
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.data.prefs.PrefKeys
import com.shobardhaka.citizenrevenue.data.prefs.PreferenceManager
import javax.inject.Inject

class VerifyOtpPresenter @Inject constructor(view: VerifyOtpContract.View) :
    BasePresenter<VerifyOtpContract.View>(view), VerifyOtpContract.Presenter {

    @Inject
    lateinit var userAuthApiService: UserAuthApiService

    @Inject
    lateinit var mPref: PreferenceManager

    @Inject
    lateinit var retrofitHelperService: RetrofitHelperService


    override fun requestOTP(phoneNumber: String, signingKey: String) {
        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            userAuthApiService.getOtp(phoneNumber, signingKey)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<RequestOtpResponse, VerifyOtpContract.View>(mView) {
                    override fun onRequestSuccess(response: RequestOtpResponse) {
                        mView?.requestOTPDidSucceed()
                    }
                })
        )
    }

    override fun verifyOtp(mobileNumber: String, otp: String) {
        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            userAuthApiService.verifyOtp(mobileNumber, otp)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<VerifyOtpResponse, VerifyOtpContract.View>(mView) {

                    override fun onRequestSuccess(response: VerifyOtpResponse) {
                        if (response.status != null && response.status == 423) {
                            mView?.verifyOtpDidFailed(response.error!!)
                        } else {
                            response.registered?.let {
                                if (response.registered) {
                                    mPref.putBoolean(PrefKeys.IS_REGISTERED, true)
                                    mPref.putString(Keys.ACCESS_TOKEN.name, response.token!!)

                                    retrofitHelperService.loadProfile(
                                        compositeDisposable!!,
                                        object : RetrofitHelperService.OnProfileLoadListener {
                                            override fun onProfileLoaded() {
                                                mView?.verifyOtpDidSucceed(response)
                                            }
                                        })
                                } else {
                                    mPref.putBoolean(PrefKeys.IS_REGISTERED, false)
                                    mView?.verifyOtpDidSucceed(response)
                                }
                            }
                        }
                    }
                })
        )
    }
}