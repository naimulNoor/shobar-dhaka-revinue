package com.shobardhaka.citizenrevenue.ui.settings

import dagger.Binds
import dagger.Module

@Module
abstract class SettingsViewModule {
    @Binds
    abstract fun provideSettingsView(activity: SettingsActivity): SettingsContract.View
}