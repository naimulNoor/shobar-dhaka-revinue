package com.shobardhaka.citizenrevenue.ui.auth.get_otp

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.RequestOtpResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.UserAuthApiService
import javax.inject.Inject

class GetOtpPresenter @Inject constructor(view: GetOtpContract.View) : BasePresenter<GetOtpContract.View>(view),
    GetOtpContract.Presenter {

    @Inject
    lateinit var userAuthApiService: UserAuthApiService

    override fun requestOTP(phoneNumber: String, signingKey: String) {
        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            userAuthApiService.getOtp(phoneNumber, signingKey)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<RequestOtpResponse, GetOtpContract.View>(mView) {
                    override fun onRequestSuccess(response: RequestOtpResponse) {
                        mView?.requestOTPDidSucceed()
                    }
                })
        )
    }
}