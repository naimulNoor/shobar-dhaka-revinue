package com.shobardhaka.citizenrevenue.ui.complaints.add_mosquitio_complaint

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import butterknife.OnCheckedChanged
import butterknife.OnClick
import com.bumptech.glide.load.resource.bitmap.TransformationUtils
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.data.network.api_response.DnccAddressResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.IssueTypeResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.file.FileUploader
import com.shobardhaka.citizenrevenue.location.SSFusedLocationProviderClient
import com.shobardhaka.citizenrevenue.ui.home.HomeActivity
import com.shobardhaka.citizenrevenue.utils.*
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import kotlinx.android.synthetic.main.activity_add_complaints.*
import kotlinx.android.synthetic.main.activity_add_mosquitio_complaint.*
import kotlinx.android.synthetic.main.activity_add_mosquitio_complaint.etDescription
import kotlinx.android.synthetic.main.activity_add_mosquitio_complaint.etLocation
import kotlinx.android.synthetic.main.activity_add_mosquitio_complaint.headingIssue
import kotlinx.android.synthetic.main.activity_add_mosquitio_complaint.headingIssueType
import kotlinx.android.synthetic.main.activity_add_mosquitio_complaint.imgComplaint
import kotlinx.android.synthetic.main.activity_add_mosquitio_complaint.spinner1sttLayer
import kotlinx.android.synthetic.main.activity_add_mosquitio_complaint.spinner2ndLayer
import kotlinx.android.synthetic.main.activity_add_mosquitio_complaint.tvAddComplainTitle
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.toolbar_with_text.*
import okhttp3.RequestBody
import java.io.File
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class AddMosquitioComplaintActivity : MvpBaseActivity<AddMosquitoComplaintPresenter>(),
    AddMosquitoComplainContract.View,
    AdapterView.OnItemSelectedListener {

    companion object {
        private var imageUri: Uri? = null
        private var imagePath: String? = null
        private var mosquitoComplainType: Int = 0 // 0 = Mosha, 1 = Patient
        private var gpsLocation = ""
        private var mLatitude = 0.0
        private var mLongitude = 0.0

        private var complaintType = ""
        private var complainId = ""
    }

    private val CODE_PICK_PHOTO = 1994
    private val CODE_CAMERA_PERMISSION = 13
    private var fileToUpload: File? = null

    private lateinit var fileUploader: FileUploader


    private var diseaseId = ""
    private var patientType = "0"
    private var relationWithPatient = ""
    private var place = "0"
    private var instituteName = ""
    private var holdingNmbr = ""
    private var roadNmbr = ""
    private var areaName = ""
    private var sector = ""
    private var date = ""


    /** Capture Image with Provider Variable */
    private val REQUEST_IMAGE_CAPTURE = 101
    private val REQUEST_WRITE_PERMISSION = 20

    @Inject
    lateinit var permissionUtils: PermissionUtils

    private lateinit var firstLayerAdapter: ArrayAdapter<String>
    private lateinit var secondLayerAdapter: ArrayAdapter<String>
    private lateinit var dnccAreaAdapter: ArrayAdapter<String>

    private var issueTypeResponse: IssueTypeResponse? = null
    private var layer2nd: ArrayList<IssueTypeResponse.SingleSubIssueType>? = null


    private var dnccAreas: ArrayList<DnccAddressResponse.SingleArea?>? = null
    private var ssFusedLocationApiClient: SSFusedLocationProviderClient? = null


    override fun getContentView(): Int {
        return R.layout.activity_add_mosquitio_complaint
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }

        if (mAppLanguage == AppLanguage.BENGALI.name) {
            imgComplaint.setImageResource(R.drawable.img_capture_photo_bn)
        }

        val bundle = intent.getBundleExtra(Keys.DATA_BUNDLE.name)
        complaintType = bundle.getString(Keys.COMPLAINT_TYPE.name)!!


        btnAddComplaintForMosquito.isSelected = true
        radioMosquito.isChecked = true


        // Setting adapter for diseases spinner
        val diseaseAdapter = object : ArrayAdapter<String>(
            getContext(), // Context
            R.layout.item_view_spinner,
            resources.getStringArray(R.array.diseases)
        ) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }

            override fun getView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val v = super.getView(position, convertView, parent)
                if (position == 0) {
                    (v as TextView).setTextColor(Color.GRAY)
                } else {
                    (v as TextView).setTextColor(
                        ContextCompat.getColor(
                            context,
                            R.color.colorPrimary
                        )
                    )
                }
                return v
            }

            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val view =
                    super.getDropDownView(position, convertView, parent)
                val tv: TextView = view as TextView
                if (position == 0) {
                    tv.setTextColor(Color.GRAY)
                } else {
                    tv.setTextColor(
                        ContextCompat.getColor(
                            context,
                            R.color.colorPrimary
                        )
                    )
                }
                return view
            }
        }
        diseaseSpinner.adapter = diseaseAdapter
        diseaseSpinner.onItemSelectedListener = this


        complainId = "${bundle.getInt(Keys.COMPLAINT_ID.name)}"
        tvTitle.text = complaintType
        imgTitle.setImageResource(bundle.getInt(Keys.COMPLAINT_ICON.name))
        imgTitle.visibility = View.VISIBLE

        // File Provider...
        fileUploader = FileUploader(getContext())
        startLocationService()

        etAreaName.threshold = 2
        etAreaName.setOnItemClickListener { parent, view, position, id ->
            val area = parent.getItemAtPosition(position).toString()
            //Log.e("AREA NAME: ", area)
            for (item in this.dnccAreas!!) {
                if (item!!.nameEn == area || item.nameBn == area) {
                    areaName = item.id.toString()
                    break
                }
            }
        }

        mPresenter.getDnccAreas()
    }


    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        diseaseId = "$position"
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }


    @OnClick(R.id.btnAddComplaintForMosquito, R.id.btnAddComplaintForPatient)
    fun onBtnClicked(view: View) {
        when (view.id) {
            R.id.btnAddComplaintForMosquito -> {
                mosquitoComplainType = 0
                btnAddComplaintForPatient.isSelected = false
                btnAddComplaintForMosquito.isSelected = true
                radioPatient.isChecked = false
                radioMosquito.isChecked = true
                tvAddComplainTitle.text = getString(R.string.msg_mosha)
                sectionMosha.visibility = View.GONE
                etLocation.visibility = View.VISIBLE

                // Sub Issue Type >>>>
                if (issueTypeResponse != null && issueTypeResponse!!.data != null) {
                    headingIssueType.visibility = View.VISIBLE
                    headingIssue.visibility = View.VISIBLE
                    spinner1sttLayer.visibility = View.VISIBLE
                    spinner2ndLayer.visibility = View.VISIBLE
                }
            }
            R.id.btnAddComplaintForPatient -> {
                mosquitoComplainType = 1
                btnAddComplaintForMosquito.isSelected = false
                btnAddComplaintForPatient.isSelected = true
                radioMosquito.isChecked = false
                radioPatient.isChecked = true
                tvAddComplainTitle.text = getString(R.string.msg_report)
                sectionMosha.visibility = View.VISIBLE
                etLocation.visibility = View.GONE

                // Sub Issue Type >>>>>
                headingIssueType.visibility = View.GONE
                headingIssue.visibility = View.GONE
                spinner1sttLayer.visibility = View.GONE
                spinner2ndLayer.visibility = View.GONE
            }
        }
    }


    @OnClick(R.id.imgComplaint, R.id.btnSubmit)
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgComplaint -> {
                if (hasCameraAndStoragePermission()) {
                    takePicture()
                } else {
                    manageCameraAndStoragePermission()
                }
            }

            R.id.btnSubmit -> {

                val description = etDescription.text.toString().trim()
                val userLocation = etLocation.text.toString().trim()

                when {

                    mosquitoComplainType == 0 && issueTypeResponse?.data != null && spinner1sttLayer.selectedItem.toString() == getString(R.string.h_select_issue_type) -> { // Has Issue Type
                        mAlertService.showAlert(
                            getContext(),
                            null,
                            getString(R.string.m_select_issue_type)
                        )
                    }

                    mosquitoComplainType  == 0 && layer2nd != null && layer2nd!!.isNotEmpty() && spinner2ndLayer.selectedItem.toString() == getString(R.string.h_select_issue) -> { // Has Issue
                        mAlertService.showAlert(
                            getContext(),
                            null,
                            getString(R.string.m_select_issue)
                        )
                    }

                    fileToUpload == null -> {
                        mAlertService.showAlert(
                            getContext(),
                            null,
                            getString(R.string.give_photo)
                        )
                    }

                    description.isEmpty() -> mAlertService.showAlert(
                        getContext(),
                        null,
                        getString(R.string.write_detials)
                    )

                    description.length < 3 -> {
                        mAlertService.showAlert(
                            getContext(),
                            null,
                            getString(R.string.write_details_at_least_3)
                        )
                    }

                    else -> {
                        val dataMap: HashMap<String, RequestBody> = HashMap()
                        dataMap["description"] = fileUploader.createPartFromString(description)
                        dataMap["location"] = fileUploader.createPartFromString(gpsLocation)
                        dataMap["user_location"] = fileUploader.createPartFromString(userLocation)
                        dataMap["latitude"] = fileUploader.createPartFromString("$mLatitude")
                        dataMap["longitude"] = fileUploader.createPartFromString("$mLongitude")
                        dataMap["category_id"] = fileUploader.createPartFromString(complainId)

                        if (mosquitoComplainType == 1) {

                            if (areaName.isNullOrEmpty()) {
                                mAlertService.showAlert(
                                    getContext(),
                                    null,
                                    getString(R.string.select_area_msg)
                                )
                            } else {
                                dataMap["category_id"] = fileUploader.createPartFromString("2") // 2 for mosquito >>>
                                relationWithPatient = etRelationWithPatient.text.toString().trim()
                                holdingNmbr = etHouseNumber.text.toString()
                                roadNmbr = etRoadNumber.text.toString()
                                sector = etSector.text.toString()

                                dataMap["complain_type"] =
                                    fileUploader.createPartFromString(mosquitoComplainType.toString())

                                dataMap["disease"] = fileUploader.createPartFromString(diseaseId)

                                dataMap["patient"] = fileUploader.createPartFromString(patientType)
                                dataMap["relation"] =
                                    fileUploader.createPartFromString(relationWithPatient)

                                dataMap["place"] = fileUploader.createPartFromString(place)
                                dataMap["place_name"] =
                                    fileUploader.createPartFromString(instituteName)

                                dataMap["holding_no"] =
                                    fileUploader.createPartFromString(holdingNmbr)
                                dataMap["road_no"] = fileUploader.createPartFromString(roadNmbr)
                                dataMap["area"] = fileUploader.createPartFromString(areaName)
                                dataMap["section"] = fileUploader.createPartFromString(sector)
                                dataMap["assuming_date"] = fileUploader.createPartFromString(date)

                                mPresenter.addComplain(
                                    dataMap,
                                    fileUploader.getMultipartBodyPartFromFile("image", fileToUpload)
                                )
                            }
                        } else {
                            mPresenter.addComplain(
                                dataMap,
                                fileUploader.getMultipartBodyPartFromFile("image", fileToUpload)
                            )
                        }
                    }
                }
            }
        }
    }

    private fun manageCameraAndStoragePermission() {
        val storagePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE
        val cameraPermission = Manifest.permission.CAMERA
        val permissions = arrayOf(cameraPermission, storagePermission)
        val listOfPermissionNeeded = ArrayList<String>()
        for (permission in permissions) {
            if (permissionUtils.shouldAskPermission(getContext(), permission)) {
                listOfPermissionNeeded.add(permission)
            }
        }
        mAlertService.showConfirmationAlert(getContext(),
            getString(R.string.permission_required),
            getString(R.string.permission_msg_for_camera),
            getString(R.string.button_not_now),
            getString(R.string.buttoin_continue),
            object : AlertService.AlertListener {

                override fun negativeBtnDidTapped() {}

                override fun positiveBtnDidTapped() {
                    ActivityCompat.requestPermissions(
                        this@AddMosquitioComplaintActivity,
                        listOfPermissionNeeded.toTypedArray(),
                        CODE_CAMERA_PERMISSION
                    )
                }
            })

    }


    @OnCheckedChanged(
        R.id.radioMe,
        R.id.radioMyRelative
    )
    fun onChanged(button: CompoundButton, checked: Boolean) {
        if (checked) {
            when (button.id) {
                R.id.radioMe -> {
                    etRelationWithPatient.visibility = View.GONE
                    patientType = "0"
                }
                R.id.radioMyRelative -> {
                    etRelationWithPatient.visibility = View.VISIBLE
                    patientType = "1"
                }
            }
        }
    }

    @OnCheckedChanged(
        R.id.radioHome,
        R.id.radioOffice,
        R.id.radioEducationalInstitute,
        R.id.radioOtherLocation
    )
    fun onPossibleLocationChanged(button: CompoundButton, checked: Boolean) {
        if (checked) {
            when (button.id) {
                R.id.radioHome -> {
                    etOtherLocationName.visibility = View.GONE
                    place = "0"
                }

                R.id.radioOffice -> {
                    etOtherLocationName.visibility = View.VISIBLE
                    etOtherLocationName.hint = getString(R.string.hing_institute_name)
                    place = "1"
                }

                R.id.radioEducationalInstitute -> {
                    etOtherLocationName.visibility = View.VISIBLE
                    etOtherLocationName.hint = getString(R.string.hing_institute_name)
                    place = "2"
                }

                R.id.radioOtherLocation -> {
                    etOtherLocationName.visibility = View.VISIBLE
                    etOtherLocationName.hint = getString(R.string.hint_others_loc_name_name)
                    place = "3"
                }
            }
        }
    }

    @OnClick(R.id.etPossibleDate)
    fun onDateClicked() {
        val calendar = Calendar.getInstance()
        val dateOfBirthPickerDialog = DatePickerDialog(
            getContext(),
            dateOfBirthPickerListener,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        dateOfBirthPickerDialog.datePicker.maxDate = calendar.timeInMillis
        dateOfBirthPickerDialog.show()
    }

    private val dateOfBirthPickerListener =
        DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
            //            dateOfBirth = String.format(Locale.getDefault(), "%02d", year) +
//                    "-" + String.format(Locale.getDefault(), "%02d", month + 1) +
//                    "-" + String.format(Locale.getDefault(), "%02d", dayOfMonth)
            date = "$year-${month + 1}-$dayOfMonth"
            etPossibleDate.setText(getDate(date))
        }


    fun getDate(dateStr: String): String {
        val preFormat = SimpleDateFormat("yyyy-MM-dd")
        val newFormatDate =
            SimpleDateFormat("dd MMM, yyyy", Locale.getDefault()) // Format 31 Jan, 2018
        var date = ""
        try {
            date = newFormatDate.format(preFormat.parse(dateStr))
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return date
    }

    override fun addComplainDidSucceed() {
        fileToUpload?.let {
            if (fileToUpload!!.exists()) {
                fileToUpload!!.delete()
            }
        }
        if ((complaintType == getString(R.string.mosha)) && mosquitoComplainType == 1) {
            mAlertService.showConfirmationAlert(getContext(),
                getString(R.string.add_complain_success_title),
                getString(R.string.add_complain_success_msg_for_mosquito_report),
                null,
                getString(R.string.okay),
                object : AlertService.AlertListener {

                    override fun negativeBtnDidTapped() {
                        // Nothing to do....
                    }

                    override fun positiveBtnDidTapped() {
                        Navigator.sharedInstance.back(
                            getContext(), HomeActivity::class.java
                        )
                        finish()
                    }
                })

        } else {
            mAlertService.showConfirmationAlert(getContext(),
                getString(R.string.add_complain_success_title),
                getString(R.string.add_complain_success_msg),
                null,
                getString(R.string.okay),
                object : AlertService.AlertListener {

                    override fun negativeBtnDidTapped() {
                        // Nothing to do....
                    }

                    override fun positiveBtnDidTapped() {
                        Navigator.sharedInstance.back(
                            getContext(), HomeActivity::class.java
                        )
                        finish()
                    }
                })
        }
    }

    override fun addComplainDidFailed(failedMsg: String) {
        mAlertService.showAlert(getContext(), null, failedMsg)
    }


    override fun geoFencingDidChecked(foundInDncc: Boolean) {
        if (!foundInDncc)
            showOutOfDnccMsg()
    }


    private fun showOutOfDnccMsg() {
        mAlertService.showConfirmationAlert(
            getContext(),
            null,
            getString(R.string.service_not_available_msg),
            null,
            getString(R.string.okay),
            object : AlertService.AlertListener {
                override fun negativeBtnDidTapped() {}

                override fun positiveBtnDidTapped() {
                    onBackPressed()
                }
            }
        )
    }

    override fun onNetworkCallStarted(loadingMessage: String) {
        showProgressDialog(getString(R.string.please_wait))
    }


    /**        Capturing Image with Provider  ----------------  */

    /** Get Bitmap from File >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    private fun getBitmapFromImagePath(imagePath: String?): Bitmap? {
        imagePath?.let {
            val imageFile = File(imagePath)
            if (imageFile.exists()) {
                val myBitmap = BitmapFactory.decodeFile(imageFile.absolutePath)
                val ei = androidx.exifinterface.media.ExifInterface(imageFile.absolutePath)
                val orientation = ei.getAttributeInt(
                    androidx.exifinterface.media.ExifInterface.TAG_ORIENTATION,
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_UNDEFINED
                )
                val rotatedBitmap: Bitmap?
                rotatedBitmap = when (orientation) {
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_ROTATE_90 -> TransformationUtils.rotateImage(
                        myBitmap,
                        90
                    )
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_ROTATE_180 -> TransformationUtils.rotateImage(
                        myBitmap,
                        180
                    )
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_ROTATE_270 -> TransformationUtils.rotateImage(
                        myBitmap,
                        270
                    )
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_NORMAL -> myBitmap
                    else -> myBitmap
                }
                return rotatedBitmap
            }
            return null
        }
        return null
    }


    /** Checking Camera Permission  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    private fun hasCameraPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            getContext(),
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
    }

    /** Checking Storage Permission  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    private fun hasStoragePermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            getContext(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    /** Checking Camera and Storage Permission  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    private fun hasCameraAndStoragePermission(): Boolean {
        return hasCameraPermission() && hasStoragePermission()
    }

    /** Handling Permission Result  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            CODE_CAMERA_PERMISSION -> if (grantResults.isNotEmpty()) {
                var isBothPermissionGranted = true
                for (i in 0 until grantResults.size) {
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        isBothPermissionGranted = false
                        break
                    }
                }
                if (isBothPermissionGranted) {
                    takePicture()
                } else {
                    manageCameraAndStoragePermission()
                }
            }

        }
    }


    /** Creating Image File  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "IMG_$timeStamp"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
            imageFileName,
            /** prefix */
            ".jpg",
            /** suffix */
            storageDir
            /** directory */
        )
        imagePath = image.absolutePath
        return image
    }


    /** Dispatching Camera Intent  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    private fun takePicture() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(packageManager) != null) {
            var imageFile: File? = null
            try {
                imageFile = createImageFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            imageFile?.let {
                val authorities: String = applicationContext.packageName + ".provider"
                /** Authorities That we provided in Manifest.xml*/
                imageUri = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                    Uri.fromFile(imageFile)
                } else {
                    FileProvider.getUriForFile(getContext(), authorities, imageFile)
                }
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }


    /** Handling image captured by Camera   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_IMAGE_CAPTURE -> {
                if (resultCode == Activity.RESULT_OK) {
                    imagePath?.let {
                        try {
                            imgComplaint.setImageBitmap(getBitmapFromImagePath(it))
                            fileToUpload = ImageCompresser.instance.getCompressedImageFile(it, getContext())
                            if (imagePath != null) {
                                val file = File(imagePath)
                                if (file.exists())
                                    file.delete()
                            }
                            mLatitude = SSFusedLocationProviderClient.mLatitude
                            mLongitude = SSFusedLocationProviderClient.mLongitude
                            val getAddressUrl =
                                "https://barikoi.xyz/v1/api/search/reverse/${getString(R.string.B_API_KEY)}/geocode?longitude=$mLongitude&latitude=$mLatitude"
                            val checkGeoFencingUrl =
                                "https://barikoi.xyz/v1/api/search/dncc/${getString(R.string.B_API_KEY)}/$mLongitude/$mLatitude"
                            mPresenter.getAddressFromBarikoiByLatLong(getAddressUrl)
                            if (mosquitoComplainType == 0) { // 0 for Mosquito or Larva related issue. so need to check geo fencing.
                                mPresenter.checkGeoFencing(checkGeoFencingUrl)
                            }
                        } catch (e: Exception) {
                        }
                    }
                }
            }
        }
    }

    override fun addressDidReceived(address: String) {
        if (address.isNotEmpty()) {
            gpsLocation = address
            //  if (mosquitoComplainType == 0) { // 0 means Mosquito or Larva related issue. So need location
            etLocation.setText(gpsLocation)
            //  }
        }
    }

    override fun onDestroy() {
        stopLocationService()
        fileToUpload?.let {
            fileToUpload!!.delete()
        }
        super.onDestroy()
    }


    private fun startLocationService() {
        if(ssFusedLocationApiClient == null) ssFusedLocationApiClient = SSFusedLocationProviderClient(getContext())
        ssFusedLocationApiClient?.startLocationUpdates()
//        Intent(getContext().applicationContext, LocationService::class.java).also { intent ->
//            startService(intent)
//        }
    }

    private fun stopLocationService() {
        ssFusedLocationApiClient?.stopLocationUpdates()
        ssFusedLocationApiClient?.onDestroy()
        ssFusedLocationApiClient = null
//        Intent(getContext().applicationContext, LocationService::class.java).also { intent ->
//            stopService(intent)
//        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            if (it.itemId == android.R.id.home) {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        Navigator.sharedInstance.back(getContext(), HomeActivity::class.java)
        finish()
    }

    override fun issueSubTypeDidReceived(response: IssueTypeResponse) {
        issueTypeResponse = response
        if (response.status == 200) {
            headingIssueType.visibility = View.VISIBLE
            // headingIssue.visibility = View.VISIBLE
            spinner1sttLayer.visibility = View.VISIBLE
            //  spinner2ndLayer.visibility = View.VISIBLE

            if (response.data != null) {
                val firstLayerArray = arrayOfNulls<String>(response.data.size + 1)
                complainId = "${response.data[0].id}"
                firstLayerArray[0] = getString(R.string.h_select_issue_type)
                for (i in response.data.indices) {
                    firstLayerArray[i + 1] = response.data[i].name
                }
                firstLayerAdapter = object : ArrayAdapter<String>(
                    getContext(), // Context
                    R.layout.item_view_spinner, // Layout
                    firstLayerArray // Array
                ) {
                    override fun isEnabled(position: Int): Boolean {
                        return position != 0
                    }

                    override fun getView(
                        position: Int,
                        convertView: View?,
                        parent: ViewGroup
                    ): View {
                        val v = super.getView(position, convertView, parent)
                        if (position == 0) {
                            (v as TextView).setTextColor(Color.GRAY)
                        } else {
                            (v as TextView).setTextColor(
                                ContextCompat.getColor(
                                    context,
                                    R.color.colorPrimary
                                )
                            )
                        }
                        return v
                    }

                    override fun getDropDownView(
                        position: Int,
                        convertView: View?,
                        parent: ViewGroup
                    ): View {
                        val view =
                            super.getDropDownView(position, convertView, parent)
                        val tv: TextView = view as TextView
                        if (position == 0) {
                            tv.setTextColor(Color.GRAY)
                        } else {
                            tv.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                        }
                        return view
                    }
                }
                spinner1sttLayer.adapter = firstLayerAdapter
                spinner1sttLayer.onItemSelectedListener = spinner1stItemSelectListener

//                if (response.data[0].secondCategory.isNotEmpty()) {
//                    spinner2ndLayer.visibility = View.VISIBLE
//
//                    val layer2ndArray =
//                        arrayOfNulls<String>(response.data[0].secondCategory.size + 1)
//
//                    layer2nd = response.data[0].secondCategory
//                    complainId = "${response.data[0].secondCategory[0].id}"
//
//                    layer2ndArray[0] = "--Select Issue--"
//                    for (i in response.data[0].secondCategory.indices) {
//                        layer2ndArray[i + 1] = response.data[0].secondCategory[i].name
//                    }
//                    secondLayerAdapter = object: ArrayAdapter<String>(
//                        getContext(), // Context
//                        R.layout.item_view_spinner, // Layout
//                        layer2ndArray // Array
//                    ){
//                        override fun isEnabled(position: Int): Boolean {
//                            return position != 0
//                        }
//
//                        override fun getView(
//                            position: Int,
//                            convertView: View?,
//                            parent: ViewGroup
//                        ): View {
//                            val v = super.getView(position, convertView, parent)
//                            if (position == 0) {
//                                (v as TextView).setTextColor(Color.GRAY)
//                            } else {
//                                (v as TextView).setTextColor(
//                                    ContextCompat.getColor(
//                                        context,
//                                        R.color.colorPrimary
//                                    )
//                                )
//                            }
//                            return v
//                        }
//
//                        override fun getDropDownView(
//                            position: Int,
//                            convertView: View?,
//                            parent: ViewGroup
//                        ): View {
//                            val view =
//                                super.getDropDownView(position, convertView, parent)
//                            val tv: TextView = view as TextView
//                            if (position == 0) {
//                                tv.setTextColor(Color.GRAY)
//                            } else {
//                                tv.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
//                            }
//                            return view
//                        }
//                    }
//                    spinner2ndLayer.adapter = secondLayerAdapter
//                    spinner2ndLayer.onItemSelectedListener = spinner2ndItemSelectListener
//                } else {
//                    spinner1sttLayer.visibility = View.GONE
//                }
            }
        } else {
            headingIssueType.visibility = View.GONE
            headingIssue.visibility = View.GONE
            spinner1sttLayer.visibility = View.GONE
            spinner2ndLayer.visibility = View.GONE
        }
    }


    private var spinner1stItemSelectListener: AdapterView.OnItemSelectedListener? =
        object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                for (firstLayerItem in issueTypeResponse!!.data!!) {
                    if (firstLayerItem.name == parent!!.getItemAtPosition(position).toString()) {
                        complainId = "${firstLayerItem.id}"

                        Log.e(">>>>>>>>>>>>>>", "${firstLayerItem.name}: ${firstLayerItem.id}")

                        if (firstLayerItem.secondCategory.isEmpty()) {
                            headingIssue.visibility = View.GONE
                            spinner2ndLayer.visibility = View.GONE

                        } else {
                            headingIssue.visibility = View.VISIBLE
                            spinner2ndLayer.visibility = View.VISIBLE

                            val layer2ndArray =
                                arrayOfNulls<String>(firstLayerItem.secondCategory.size + 1)
                            layer2nd = firstLayerItem.secondCategory

                            layer2ndArray[0] = getString(R.string.h_select_issue)
                            for (i in firstLayerItem.secondCategory.indices) {
                                layer2ndArray[i + 1] = firstLayerItem.secondCategory[i].name
                            }
                            secondLayerAdapter = object : ArrayAdapter<String>(
                                getContext(), // Context
                                R.layout.item_view_spinner, // Layout
                                layer2ndArray // Array
                            ) {
                                override fun isEnabled(position: Int): Boolean {
                                    return position != 0
                                }

                                override fun getView(
                                    position: Int,
                                    convertView: View?,
                                    parent: ViewGroup
                                ): View {
                                    val v = super.getView(position, convertView, parent)
                                    if (position == 0) {
                                        (v as TextView).setTextColor(Color.GRAY)
                                    } else {
                                        (v as TextView).setTextColor(
                                            ContextCompat.getColor(
                                                context,
                                                R.color.colorPrimary
                                            )
                                        )
                                    }
                                    return v
                                }

                                override fun getDropDownView(
                                    position: Int,
                                    convertView: View?,
                                    parent: ViewGroup
                                ): View {
                                    val view =
                                        super.getDropDownView(position, convertView, parent)
                                    val tv: TextView = view as TextView
                                    if (position == 0) {
                                        tv.setTextColor(Color.GRAY)
                                    } else {
                                        tv.setTextColor(
                                            ContextCompat.getColor(
                                                context,
                                                R.color.colorPrimary
                                            )
                                        )
                                    }
                                    return view
                                }
                            }
                            spinner2ndLayer.adapter = secondLayerAdapter
                            spinner2ndLayer.onItemSelectedListener = spinner2ndItemSelectListener
                        }
                        break
                    }
                }
            }
        }


    private var spinner2ndItemSelectListener: AdapterView.OnItemSelectedListener? =
        object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                layer2nd?.let {
                    for (item in it) {
                        if (item.name == parent!!.getItemAtPosition(position).toString()) {
                            complainId = "${item.id}"
                            break
                        }
                    }
                }
            }
        }

    override fun dnccAreasDidReceived(response: DnccAddressResponse) {
        this.dnccAreas = response.data
        val areas = ArrayList<String>()
        for (singleArea in response.data){
            areas.add(singleArea!!.nameBn)
            areas.add(singleArea.nameEn)
        }
        dnccAreaAdapter = ArrayAdapter<String>(
            getContext(), // Context
            R.layout.item_view_spinner,
            areas
        )
        etAreaName.setAdapter(dnccAreaAdapter)
    }
}


/**
 * 1. No need to check geofencing area for for submitting Patient Report */