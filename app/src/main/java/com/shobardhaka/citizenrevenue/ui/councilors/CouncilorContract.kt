package com.shobardhaka.citizenrevenue.ui.councilors

import com.shobardhaka.citizenrevenue.base.BaseContract
import com.shobardhaka.citizenrevenue.data.network.api_response.GetCouncilorResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.GetWardsResponse

interface CouncilorContract {

    interface View : BaseContract.View {
        fun councilorsDidReceived(councilors: ArrayList<GetCouncilorResponse.SingleCouncilor?>)
        fun onFailedToFetchData(failedMsg: String)
        fun zoneWardsDidReceived(response: GetWardsResponse)
    }

    interface Presenter : BaseContract.Presenter {
        fun getCouncilors(zone: String, ward: String)
        fun getZoneWards()
    }
}