package com.shobardhaka.citizenrevenue.ui.event_detials

import com.shobardhaka.citizenrevenue.base.BaseContract

interface EventDetialsContract {

    interface View : BaseContract.View {

        fun joinDidSucceed(message: String?)
        fun joinDidFailed(failedMsg: String?)
    }

    interface Presenter : BaseContract.Presenter {

        fun joinEvent(eventData: HashMap<String, String>)
    }
}