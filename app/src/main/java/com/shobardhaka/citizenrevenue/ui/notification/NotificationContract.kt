package com.shobardhaka.citizenrevenue.ui.notification

import com.shobardhaka.citizenrevenue.base.BaseContract

interface NotificationContract{

    interface View: BaseContract.View{
    }

    interface Presenter: BaseContract.Presenter{
    }

}