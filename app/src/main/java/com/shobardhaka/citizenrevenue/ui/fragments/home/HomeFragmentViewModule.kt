package com.shobardhaka.citizenrevenue.ui.fragments.home

import dagger.Binds
import dagger.Module

@Module
abstract class HomeFragmentViewModule(){
    @Binds
    abstract fun provideHomeFragmentViewView(fragment: HomeFragment): HomeFragmentContract.View
}
