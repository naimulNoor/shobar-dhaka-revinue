package com.shobardhaka.citizenrevenue.ui.auth.verify_otp

import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.view.View
import butterknife.OnClick
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.data.network.api_response.VerifyOtpResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.sms_retriver.AppSignatureHashHelper
import com.shobardhaka.citizenrevenue.sms_retriver.SMSReceiver
import com.shobardhaka.citizenrevenue.ui.auth.get_otp.GetOtpActivity
import com.shobardhaka.citizenrevenue.ui.home.HomeActivity
import com.shobardhaka.citizenrevenue.ui.profile_info.ProfileInfoActivity
import com.shobardhaka.citizenrevenue.utils.Keys.IntentKeys
import com.shobardhaka.citizenrevenue.utils.Navigator
import com.shobardhaka.citizenrevenue.utils.showToast
import kotlinx.android.synthetic.main.activity_verify_otp.*

class VerifyOtpActivity : MvpBaseActivity<VerifyOtpPresenter>(), VerifyOtpContract.View {

    private var mobileNumber = ""
    private var intentFilter: IntentFilter? = null
    private var smsReceiver: SMSReceiver? = null
    private var isSmsReceiverRegistered: Boolean = false

    private var countDownTimer: CountDownTimer? = null
    private var OTP_RESEND_TIME: Long = 2000

    override fun getContentView(): Int {
        return R.layout.activity_verify_otp
    }

    private fun startOtpTimer() {
        btnResendOtp.isEnabled = false
        countDownTimer = object : CountDownTimer(OTP_RESEND_TIME * 60, 1000) {
            override fun onTick(secondsRemaining: Long) {
                var seconds = (secondsRemaining / 1000)
                val minutes = seconds / 60
                seconds %= 60
                tvOtpTimer.text =
                    String.format("%02d:%02d", minutes, seconds)
            }

            override fun onFinish() {
                tvOtpTimer.text = ""
                btnResendOtp.isEnabled = true
            }
        }
        countDownTimer?.start()
    }

    private fun endOtpTimer() {
        countDownTimer?.cancel()
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        val bundle = intent.getBundleExtra(IntentKeys.DATA_BUNDLE)
        if (bundle != null) {
            mobileNumber = bundle.getString(IntentKeys.MOBILE_NUMBER) ?: ""
        }
        // Init Sms Retriever >>>>
        initSmsListener()
        initBroadCast()

        // Starting OTP Timer >>>>>>
        startOtpTimer()

        if(!isSmsReceiverRegistered){
            isSmsReceiverRegistered = true
            registerReceiver(smsReceiver, intentFilter)
        }
    }

    private fun initBroadCast() {
        intentFilter = IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION)
        smsReceiver = SMSReceiver()
        smsReceiver?.setOTPListener(object : SMSReceiver.OTPReceiveListener {
            override fun onOTPReceived(otp: String?) {
                otp?.let {
                    otp1.text = otp[0].toString()
                    otp2.text = otp[1].toString()
                    otp3.text = otp[2].toString()
                    otp4.text = otp[3].toString()
                    otp5.text = otp[4].toString()
                    otp6.text = otp[5].toString()
                    btnVerify.isEnabled = true
                }

                Handler().postDelayed({
                    btnVerifyDidTapped()
                }, 1000)
            }
        })
    }


    private fun initSmsListener() {
        val client = SmsRetriever.getClient(this)
        client.startSmsRetriever()
    }

    @OnClick(
        R.id.btn0,
        R.id.btn1,
        R.id.btn2,
        R.id.btn3,
        R.id.btn4,
        R.id.btn5,
        R.id.btn6,
        R.id.btn7,
        R.id.btn8,
        R.id.btn9,
        R.id.btnVerify,
        R.id.btnDel,
        R.id.btnResendOtp
    )
    fun onClicked(view: View) {
        when (view.id) {
            R.id.btn0 -> input("0")
            R.id.btn1 -> input("1")
            R.id.btn2 -> input("2")
            R.id.btn3 -> input("3")
            R.id.btn4 -> input("4")
            R.id.btn5 -> input("5")
            R.id.btn6 -> input("6")
            R.id.btn7 -> input("7")
            R.id.btn8 -> input("8")
            R.id.btn9 -> input("9")
            R.id.btnDel -> btnDelDidTapped()
            R.id.btnVerify -> btnVerifyDidTapped()
            R.id.btnResendOtp -> btnResendOtpDidTapped()
        }
    }

    private fun btnResendOtpDidTapped(){
        val appSignatureHashHelper = AppSignatureHashHelper(this)
        mPresenter.requestOTP(mobileNumber, appSignatureHashHelper.appSignatures[0] )    }

    private fun btnVerifyDidTapped() {
        val otp =
            otp1.text.toString() + otp2.text.toString() + otp3.text.toString() + otp4.text.toString() + otp5.text.toString() + otp6.text.toString()
        mPresenter.verifyOtp(mobileNumber, otp)
    }

    private fun btnDelDidTapped() {

        val otp1Str = otp1.text.toString()
        val otp2Str = otp2.text.toString()
        val otp3Str = otp3.text.toString()
        val otp4Str = otp4.text.toString()
        val otp5Str = otp5.text.toString()
        val otp6Str = otp6.text.toString()

        btnVerify.isEnabled = false

        when {
            !otp6Str.isEmpty() -> otp6.text = ""
            !otp5Str.isEmpty() -> otp5.text = ""
            !otp4Str.isEmpty() -> otp4.text = ""
            !otp3Str.isEmpty() -> otp3.text = ""
            !otp2Str.isEmpty() -> otp2.text = ""
            !otp1Str.isEmpty() -> otp1.text = ""
        }
    }

    private fun input(number: String) {

        val otp1Str = otp1.text.toString().trim()
        val otp2Str = otp2.text.toString().trim()
        val otp3Str = otp3.text.toString().trim()
        val otp4Str = otp4.text.toString().trim()
        val otp5Str = otp5.text.toString().trim()
        val otp6Str = otp6.text.toString().trim()

        when {
            otp1Str.isEmpty() -> otp1.text = number
            otp2Str.isEmpty() -> otp2.text = number
            otp3Str.isEmpty() -> otp3.text = number
            otp4Str.isEmpty() -> otp4.text = number
            otp5Str.isEmpty() -> otp5.text = number
            otp6Str.isEmpty() -> {
                otp6.text = number
                btnVerify.isEnabled = true
            }
        }
    }

    override fun verifyOtpDidFailed(failedMsg: String) {
        mAlertService.showAlert(getContext(), null, failedMsg)
    }

    override fun onNetworkCallEnded() {
        //
    }

    override fun verifyOtpDidSucceed(response: VerifyOtpResponse) {
        super.onNetworkCallEnded()
        mPrefManager.putString(Keys.mobile_number.name, mobileNumber)
        if (response.registered != null) {
            if (response.registered) {
                mPrefManager.putBoolean(Keys.KEEP_ME_LOGGED_IN.name, true)
                Navigator.sharedInstance.navigate(getContext(), HomeActivity::class.java)
            } else
                Navigator.sharedInstance.navigate(getContext(), ProfileInfoActivity::class.java)

            finish()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if(isSmsReceiverRegistered){
            isSmsReceiverRegistered = false
            unregisterReceiver(smsReceiver)
        }
        smsReceiver = null
    }

    override fun onBackPressed() {
        Navigator.sharedInstance.back(getContext(), GetOtpActivity::class.java)
        finish()
    }

    override fun requestOTPDidSucceed() {
        super.onNetworkCallEnded()
        initSmsListener()
        initBroadCast()
        startOtpTimer()
        showToast(getString(R.string.resend_otp_success_msg))
    }

    override fun requestOTPDidFailed(failedMsg: String) {
        super.onNetworkCallEnded()
    }
}

