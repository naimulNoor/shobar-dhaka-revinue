package com.shobardhaka.citizenrevenue.ui.complaints.complaint_details

import com.shobardhaka.citizenrevenue.base.BaseContract
import com.shobardhaka.citizenrevenue.data.network.api_response.ComplainDetailsResponse

interface ComplaintDetailsContract {
    interface View : BaseContract.View {
        fun recomplaintDidsSucceed()
        fun recomplaintDidFailed()

        fun complainDetailsDidReceived(complainDetailsResponse: ComplainDetailsResponse)

        fun submitRatingDidSucceed();
        fun submitRatingDidFailed(failedMsg: String);
    }

    interface Presenter : BaseContract.Presenter {
        fun getComplainDetails(complainId: String, showLoadingDialog: Boolean)
        fun recomplain(url: String)
        fun submitRating(dataMap: HashMap<String, String>)
    }
}