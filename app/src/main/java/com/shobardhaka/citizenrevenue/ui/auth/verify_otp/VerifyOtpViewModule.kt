package com.shobardhaka.citizenrevenue.ui.auth.verify_otp

import dagger.Binds
import dagger.Module

@Module
abstract class VerifyOtpViewModule {
    @Binds
    abstract fun provideVerifyOtpViewModule(activity: VerifyOtpActivity): VerifyOtpContract.View
}