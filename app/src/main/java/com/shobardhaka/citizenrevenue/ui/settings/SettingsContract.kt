package com.shobardhaka.citizenrevenue.ui.settings
import com.shobardhaka.citizenrevenue.base.BaseContract

interface SettingsContract {

    interface View : BaseContract.View {

    }

    interface Presenter : BaseContract.Presenter {
        fun storeDeviceToken(deviceToken: String)
        fun removeDeviceToken()
    }

}