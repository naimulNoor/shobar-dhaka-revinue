package com.shobardhaka.citizenrevenue.ui.complaints.add_mosquitio_complaint

import dagger.Binds
import dagger.Module

@Module
abstract class AddMosquitoComplainViewModule {
    @Binds
    abstract fun provideAddMosquitoComplaintView(activity: AddMosquitioComplaintActivity): AddMosquitoComplainContract.View
}