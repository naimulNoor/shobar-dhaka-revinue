package com.shobardhaka.citizenrevenue.ui.complaints.add_complaint

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.BarikoiAddressFromLatLongResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.CheckGeoFencingResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.IssueTypeResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.PostDataResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.BariKoiApiService
import com.shobardhaka.citizenrevenue.data.network.api_service.ComplainApiService
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.HashMap
import java.util.LinkedHashMap
import javax.inject.Inject

class AddComplaintPresenter @Inject constructor(view: AddComplaintContract.View) :
    BasePresenter<AddComplaintContract.View>(view), AddComplaintContract.Presenter {


    @Inject
    lateinit var complainApiService: ComplainApiService

    @Inject
    lateinit var userApiService: UserApiService

    @Inject
    lateinit var barikoiApiService: BariKoiApiService

    override fun addComplain(
        dataMap: HashMap<String, RequestBody>,
        attachment: MultipartBody.Part?
    ) {

        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            complainApiService.addComplain(dataMap, attachment)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<PostDataResponse, AddComplaintContract.View>(mView) {

                    override fun onRequestSuccess(response: PostDataResponse) {
                        if (response.status == 200) {
                            mView?.addComplainDidSucceed()
                        } else if (response.status == 423) {
                            mView?.addComplainDidFailed(response.error!!)
                        }
                    }

                    override fun onFormDataNotValid(errorMap: LinkedHashMap<String, String>) {
                        for ((_, value) in errorMap) {
                            mView?.addComplainDidFailed(value)
                            break
                        }
                    }
                })
        )
    }

    override fun checkGeoFencing(url: String) {
        compositeDisposable?.add(
            userApiService.checkGeoFencing(url)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<CheckGeoFencingResponse, AddComplaintContract.View>(
                        mView
                    ) {
                    override fun onRequestSuccess(response: CheckGeoFencingResponse) {
                        mView?.geoFencingDidChecked(response.message != "not found in DNCC")
                    }
                })
        )
    }

    override fun getAddressFromBarikoiByLatLong(url: String) {
        compositeDisposable?.add(
            barikoiApiService.getAddressByLatLong(url)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<BarikoiAddressFromLatLongResponse, AddComplaintContract.View>(
                        mView
                    ) {

                    override fun onRequestSuccess(response: BarikoiAddressFromLatLongResponse) {
                        if (response.status == 200) {
                            val address = response.place!!.address
                            val area = response.place.area
                            val city = response.place.city
                            val fullAddress = "$address, $area"
                            mView?.addressDidReceived(fullAddress)
                        }
                    }

                })
        )
    }

    override fun getSubIssueType(categoryId: String) {
        compositeDisposable?.add(
            complainApiService.getSubIssueType(categoryId)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<IssueTypeResponse, AddComplaintContract.View>(mView) {
                    override fun onRequestSuccess(response: IssueTypeResponse) {
                        mView?.issueSubTypeDidReceived(response)
                    }
                })
        )
    }
}