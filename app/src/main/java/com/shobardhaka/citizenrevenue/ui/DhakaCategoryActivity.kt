package com.shobardhaka.citizenrevenue.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.OnClick
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BaseActivity
import com.shobardhaka.citizenrevenue.ui.events.EventListActivity
import com.shobardhaka.citizenrevenue.utils.AlertService
import com.shobardhaka.citizenrevenue.utils.Navigator
import kotlinx.android.synthetic.main.toolbar_with_text.*

class DhakaCategoryActivity : BaseActivity() {

    lateinit var alertService: AlertService

    override fun getContentView(): Int {
        return R.layout.activity_dhaka_category
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }
        tvTitle.text = getString(R.string.dhaka_today)
        alertService = AlertService()
    }

    @OnClick(
        R.id.cardTraining,
        R.id.cardBusiness,
        R.id.cardExibition,
        R.id.cardHealth,
        R.id.cardCulture,
        R.id.cardEntertainment
    )
    fun onEventCardClicked(view: View) {
        when (view.id) {
            R.id.cardTraining -> {
                Navigator.sharedInstance.navigate(getContext(), EventListActivity::class.java)
            }
            R.id.cardBusiness, R.id.cardExibition,R.id.cardHealth, R.id.cardCulture, R.id.cardEntertainment -> {
                alertService.showAlert(getContext(), null, "ইভেন্টটি উপলভ্য নয়।")
            }
        }
    }

}
