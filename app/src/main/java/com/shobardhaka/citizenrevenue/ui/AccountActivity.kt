package com.shobardhaka.citizenrevenue.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.OnClick
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BaseActivity
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.data.prefs.PreferenceManager
import com.shobardhaka.citizenrevenue.file.PicassoImageLoader
import com.shobardhaka.citizenrevenue.ui.auth.login.LoginActivity
import com.shobardhaka.citizenrevenue.ui.home.HomeActivity
import com.shobardhaka.citizenrevenue.ui.profile.ProfileActivity
import com.shobardhaka.citizenrevenue.ui.settings.SettingsActivity
import com.shobardhaka.citizenrevenue.utils.AlertService
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import com.shobardhaka.citizenrevenue.utils.Navigator
import kotlinx.android.synthetic.main.activity_account.*
import kotlinx.android.synthetic.main.toolbar_with_text.*

class AccountActivity : BaseActivity() {

    private lateinit var preferenceManager: PreferenceManager

    private var mAppLanguage = ""

    override fun getContentView(): Int {
        return R.layout.activity_account
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        preferenceManager = PreferenceManager(getContext());
        mAppLanguage = preferenceManager.getString(Keys.APP_LANGUAGE.name)
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }

        tvTitle.text = null

        // App Version...
        var version = AppUtils.shared.getVersionName(getContext())
        if (mAppLanguage == AppLanguage.BENGALI.name) {
            version = AppUtils.shared.getInBangla(version)
        }
        val versionStr = "${getString(R.string.version)} $version"
        tvVersion.text = versionStr


        val profilePic = preferenceManager.getString(Keys.ProfileKey.PROFILE_PIC.name)
        val name = preferenceManager.getString(Keys.ProfileKey.NAME.name)
        var mobileNumber = preferenceManager.getString(Keys.ProfileKey.MOBILE_NUMBER.name)

        // Mobile Number
        if (mAppLanguage == AppLanguage.BENGALI.name) {
            mobileNumber = AppUtils.shared.getInBangla(mobileNumber)
        }

        if (profilePic.isNotEmpty()) {
            PicassoImageLoader.shared.loadImage(
                getContext(),
                profilePic,
                imgProfile,
                R.drawable.profile
            )
        }
        tvName.text = name
        tvMobileNumber.text = mobileNumber
    }

    @OnClick(
        R.id.tvProfile,
        R.id.tvSettings,
        R.id.tvRatings,
        R.id.tvLogout
    )
    fun onClicked(view: View) {
        when (view.id) {
            R.id.tvProfile -> {
                Navigator.sharedInstance.navigate(getContext(), ProfileActivity::class.java)
            }
            R.id.tvSettings -> {
                Navigator.sharedInstance.navigate(getContext(), SettingsActivity::class.java)
            }
            R.id.tvRatings -> {
                AppUtils.shared.openAppOnPlayStore(getContext(), applicationContext.packageName)
            }
            R.id.tvLogout -> {
                AlertService().showConfirmationAlert(
                    getContext(),
                    getString(R.string.logout),
                    getString(R.string.logout_message),
                    getString(R.string.no),
                    getString(R.string.yes),
                    object : AlertService.AlertListener {
                        override fun negativeBtnDidTapped() {
                        }

                        override fun positiveBtnDidTapped() {
                            preferenceManager.clearPreference()
//                            preferenceManager.putBoolean(Keys.KEEP_ME_LOGGED_IN.name, false)
                            Navigator.sharedInstance.back(
                                getContext(),
                                LoginActivity::class.java
                            )

                        }
                    })
            }
        }
    }

    override fun onBackPressed() {
        Navigator.sharedInstance.back(getContext(), HomeActivity::class.java)
        finish()
    }
}