package com.shobardhaka.citizenrevenue.ui.auth.login

import android.content.Intent
import android.os.Bundle
import butterknife.OnClick
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.ui.auth.get_otp.GetOtpActivity
import com.shobardhaka.citizenrevenue.utils.Navigator


class LoginActivity : MvpBaseActivity<LoginPresenter>(), LoginContract.View {

    override fun getContentView(): Int {
        return R.layout.activity_login_new
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
    }


    @OnClick(R.id.btnLoginWithMobileNumber)
    fun onLoginBtnClicked() {
        Navigator.sharedInstance.navigate(getContext(), GetOtpActivity::class.java)
        finish()
    }
}
