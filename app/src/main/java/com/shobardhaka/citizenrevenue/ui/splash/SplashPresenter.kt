package com.shobardhaka.citizenrevenue.ui.splash

import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.RetrofitHelperService
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.BarikoiAddressFromLatLongResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.GetAppVersionResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.BariKoiApiService
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.data.prefs.PrefKeys
import com.shobardhaka.citizenrevenue.data.prefs.PreferenceManager
import javax.inject.Inject

class SplashPresenter @Inject constructor(view: SplashContract.View) :
    BasePresenter<SplashContract.View>(view),
    SplashContract.Presenter {

    @Inject
    lateinit var userApiService: UserApiService

    @Inject
    lateinit var barikoiApiService: BariKoiApiService

    @Inject
    lateinit var mPrefManager: PreferenceManager

    @Inject
    lateinit var retrofitHelperService: RetrofitHelperService

    override fun getAppVersion() {
        compositeDisposable?.add(
            userApiService.getAppVersion()
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<GetAppVersionResponse, SplashContract.View>(mView) {

                    override fun onRequestSuccess(response: GetAppVersionResponse) {
                        if (mPrefManager.getBoolean(
                                PrefKeys.IS_REGISTERED,
                                false
                            ) && mPrefManager.getBoolean(
                                Keys.KEEP_ME_LOGGED_IN.name,
                                false
                            )
                        ) {
                            retrofitHelperService.loadProfile(
                                compositeDisposable!!,
                                object : RetrofitHelperService.OnProfileLoadListener {
                                    override fun onProfileLoaded() {
                                        // Nothing to do ...
                                    }
                                })
                        }
                        mView?.appVersionDidReceived(response)
                    }

                })
        )
    }

    override fun getAddressFromBarikoiByLatLong(url: String) {
        compositeDisposable?.add(
            barikoiApiService.getAddressByLatLong(url)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<BarikoiAddressFromLatLongResponse, SplashContract.View>(
                        mView
                    ) {

                    override fun onRequestSuccess(response: BarikoiAddressFromLatLongResponse) {
                        if (response.status == 200) {
                            val address = response.place!!.address
                            val area = response.place.area
                            val city = response.place.city
                            val fullAddress = "$address, $area"
                            mView?.addressDidReceived(fullAddress)
                        }
                    }

                })
        )
    }
}
