package com.shobardhaka.citizenrevenue.ui.fragments.others


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.data.prefs.PreferenceManager
import com.shobardhaka.citizenrevenue.file.PicassoImageLoader
import com.shobardhaka.citizenrevenue.ui.LanguagePickerActivity
import com.shobardhaka.citizenrevenue.ui.settings.SettingsActivity
import com.shobardhaka.citizenrevenue.ui.profile.ProfileActivity
import com.shobardhaka.citizenrevenue.utils.AlertService
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import com.shobardhaka.citizenrevenue.utils.Navigator
import com.shobardhaka.citizenrevenue.utils.NetworkUtils
import kotlinx.android.synthetic.main.fragment_others.view.*

class OthersFragment : Fragment() {

    companion object {
        fun newInstance(): OthersFragment {
            return OthersFragment()
        }
    }

    private var viewUnbinder: Unbinder? = null
    private lateinit var mNetworkUtils: NetworkUtils
    private lateinit var alertService: AlertService
    private lateinit var preferenceManager: PreferenceManager
    private lateinit var rootView: View

    private var mAppLanguage = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_others, container, false)
        viewUnbinder = ButterKnife.bind(this, view)
        mNetworkUtils = NetworkUtils()
        alertService = AlertService()
        preferenceManager = PreferenceManager(getActivityContext())
        rootView = view
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        mAppLanguage = preferenceManager.getString(Keys.APP_LANGUAGE.name)

        // App Version...
        var version = AppUtils.shared.getVersionName(activity!!)
        if (mAppLanguage == AppLanguage.BENGALI.name) {
            version = AppUtils.shared.getInBangla(version)
        }
        val versionStr = "${getString(R.string.version)} $version"
        rootView.tvVersion.text = versionStr


        val profilePic = preferenceManager.getString(Keys.ProfileKey.PROFILE_PIC.name)
        val name = preferenceManager.getString(Keys.ProfileKey.NAME.name)
        var mobileNumber = preferenceManager.getString(Keys.ProfileKey.MOBILE_NUMBER.name)

        // Mobile Number
        if (mAppLanguage == AppLanguage.BENGALI.name) {
            mobileNumber = AppUtils.shared.getInBangla(mobileNumber)
        }

        if (profilePic.isNotEmpty()) {
            PicassoImageLoader.shared.loadImage(
                activity!!,
                profilePic,
                rootView.imgProfile,
                R.drawable.profile
            )
        }
        rootView.tvName.text = name
        rootView.tvMobileNumber.text = mobileNumber
    }

    @OnClick(
        R.id.tvProfile,
        R.id.tvSettings,
        R.id.tvRatings,
        R.id.tvLogout
    )
    fun onClicked(view: View) {
        when (view.id) {
            R.id.tvProfile -> {
                Navigator.sharedInstance.navigate(getActivityContext(), ProfileActivity::class.java)
            }
            R.id.tvSettings -> {
                Navigator.sharedInstance.navigate(getActivityContext(), SettingsActivity::class.java)
                activity!!.finish()
            }
            R.id.tvRatings -> {
                AppUtils.shared.openAppOnPlayStore(context!!, context!!.applicationContext.packageName)
            }
            R.id.tvLogout -> {
                alertService.showConfirmationAlert(
                    getActivityContext(),
                    getString(R.string.logout),
                    getString(R.string.logout_message),
                    getString(R.string.no),
                    getString(R.string.yes),
                    object : AlertService.AlertListener {
                        override fun negativeBtnDidTapped() {
                            // Noting to do...
                        }

                        override fun positiveBtnDidTapped() {
                            preferenceManager.clearPreference()
//                            preferenceManager.putBoolean(Keys.KEEP_ME_LOGGED_IN.name, false)
                            Navigator.sharedInstance.back(getActivityContext(), LanguagePickerActivity::class.java)
                            activity!!.finish()
                        }
                    })
            }
        }
    }


    private fun getActivityContext(): Context {
        return activity!!
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewUnbinder?.let {
            viewUnbinder!!.unbind()
        }
    }
}
