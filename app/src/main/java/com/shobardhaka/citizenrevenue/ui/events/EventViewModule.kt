package com.shobardhaka.citizenrevenue.ui.events

import dagger.Binds
import dagger.Module

@Module
abstract class EventViewModule {
    @Binds
    abstract fun provideEventView(activity: EventListActivity): EventContract.View
}