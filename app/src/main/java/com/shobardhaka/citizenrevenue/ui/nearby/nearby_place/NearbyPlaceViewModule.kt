package com.shobardhaka.citizenrevenue.ui.nearby.nearby_place

import dagger.Binds
import dagger.Module

@Module
abstract class NearbyPlaceViewModule{
    @Binds
    abstract fun provideNearbPlaceView(activity: NearbyPlaceActivity): NearbyPlaceContract.View
}