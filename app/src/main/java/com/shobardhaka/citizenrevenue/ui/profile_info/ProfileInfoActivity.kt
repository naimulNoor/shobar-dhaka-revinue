package com.shobardhaka.citizenrevenue.ui.profile_info

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.CompoundButton
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import butterknife.OnCheckedChanged
import butterknife.OnClick
import com.bumptech.glide.load.resource.bitmap.TransformationUtils
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.data.prefs.PrefKeys
import com.shobardhaka.citizenrevenue.file.FileUploader
import com.shobardhaka.citizenrevenue.ui.home.HomeActivity
import com.shobardhaka.citizenrevenue.utils.*
import kotlinx.android.synthetic.main.activity_profile_info.*
import okhttp3.RequestBody
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class ProfileInfoActivity : MvpBaseActivity<ProfileInfoPresenter>(), ProfileInfoContract.View {

    val PICK_PHOTO_FROM_CAMERA_CODE = 1994
    val PICK_PHOTO_FROM_GALLERY_CODE = 1958
    val CAMERA_REQUEST_CODE = 13
    val GALLERY_REQUEST_CODE = 15
    lateinit var fileUploader: FileUploader
    var attachmentUri: Uri? = null
    var fileToUpload: File? = null

    // gender >>>>
    var gender: String = "-1"  // 0 = Male, 1 = Female
    val MALE = "0"
    val FEMALE = "1"
    val OTHER = "2"

    @Inject
    lateinit var permissionUtils: PermissionUtils

    companion object {
        private var imageUri: Uri? = null
        private var imagePath: String? = null
    }

    override fun getContentView(): Int {
        return R.layout.activity_profile_info
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        tvPrivacyPolicy.text = AppUtils.shared.fromHtml(getString(R.string.privacy_msg))
        tvPrivacyPolicy.movementMethod = LinkMovementMethod.getInstance()

        fileUploader = FileUploader(getContext())
    }


    @OnCheckedChanged(R.id.radioMale, R.id.radioFemale, R.id.radioOther)
    fun onGenderChanged(button: CompoundButton, isChecked: Boolean) {
        if (isChecked) {
            when (button.id) {
                R.id.radioMale -> {
                    gender = MALE
                }

                R.id.radioFemale -> {
                    gender = FEMALE
                }

                R.id.radioOther -> {
                    gender = OTHER
                }
            }
        }
    }


    @OnClick(R.id.imgProfile, R.id.btnAgreeAndContinue)
    fun onClicked(view: View) {
        when (view.id) {
            R.id.imgProfile -> {
                if (hasCameraAndStoragePermission()) {
                    takePicture()
                } else {
                    manageCameraAndStoragePermission()
                }
//                val alertBuilder = AlertDialog.Builder(this)
//                alertBuilder.setCancelable(true)
//                alertBuilder.setItems(
//                    arrayOf<CharSequence>(
//                        getString(R.string.camera),
//                        getString(R.string.gallery)
//                    )
//                ) { _, option ->
//                    when (option) {
//                        0 -> {
//
//                        }
//
//                        1 -> manageGallery()
//                    }
//                }
//                alertBuilder.create().show()
            }

            R.id.btnAgreeAndContinue -> {
                val name = etName.text.toString().trim()
                when{
                    name.isEmpty() ->{
                        mAlertService.showAlert(getContext(), null, getString(R.string.name_empty_msg))
                    }
                    gender == "-1" -> {
                        mAlertService.showAlert(getContext(), null, getString(R.string.gender_empty_msg))
                    }
                    else -> {
                        val dataMap: HashMap<String, RequestBody> = HashMap()
                        dataMap["name"] = fileUploader.createPartFromString(name)
                        dataMap["gender"] = fileUploader.createPartFromString(gender)
                        dataMap["phone"] =
                            fileUploader.createPartFromString(mPrefManager.getString(Keys.mobile_number.name))
                        dataMap["password"] =
                            fileUploader.createPartFromString(mPrefManager.getString(Keys.mobile_number.name))
                        mPresenter.updateProfileInfo(
                            dataMap,
                            fileUploader.getMultipartBodyPartFromFile("image", fileToUpload)
                        )
                    }
                }
            }
        }
    }

    private fun manageGallery() {
        if (!permissionUtils.shouldAskPermission(
                getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        ) {
            AppUtils.shared.openGallery(getContext(), PICK_PHOTO_FROM_GALLERY_CODE)
        } else {
            mAlertService.showConfirmationAlert(this@ProfileInfoActivity,
                getString(R.string.permission_required),
                getString(R.string.permission_storage_denied_one),
                getString(R.string.button_not_now),
                getString(R.string.okay),
                object : AlertService.AlertListener {

                    override fun negativeBtnDidTapped() {

                    }

                    override fun positiveBtnDidTapped() {
                        ActivityCompat.requestPermissions(
                            this@ProfileInfoActivity,
                            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                            GALLERY_REQUEST_CODE
                        )

                    }

                })

        }
    }


    private fun managePermissionAndOpenCamera() {
        val storagePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE
        val cameraPermission = Manifest.permission.CAMERA
        val permissions = arrayOf(cameraPermission, storagePermission)
        val listOfPermissionNeeded = ArrayList<String>()
        for (permission in permissions) {
            if (permissionUtils.shouldAskPermission(getContext(), permission)) {
                listOfPermissionNeeded.add(permission)
            }
        }
        if (listOfPermissionNeeded.isEmpty()) {
            AppUtils.shared.openCamera(getContext(), attachmentUri!!, PICK_PHOTO_FROM_CAMERA_CODE)
        } else {
            mAlertService.showConfirmationAlert(getContext(),
                getString(R.string.permission_required),
                getString(R.string.permission_msg_for_camera),
                getString(R.string.button_not_now),
                getString(R.string.okay),
                object : AlertService.AlertListener {

                    override fun negativeBtnDidTapped() {}

                    override fun positiveBtnDidTapped() {
                        ActivityCompat.requestPermissions(
                            this@ProfileInfoActivity,
                            listOfPermissionNeeded.toTypedArray(),
                            CAMERA_REQUEST_CODE
                        )
                    }
                })
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, getIntent: Intent?) {
        super.onActivityResult(requestCode, resultCode, getIntent)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                PICK_PHOTO_FROM_CAMERA_CODE -> {
                    try {
                        imagePath?.let {
                            imgProfile.setImageBitmap(getBitmapFromImagePath(it))
                            fileToUpload =
                                ImageCompresser.instance.getCompressedImageFile(it, getContext())
                            if (imagePath != null) {
                                val file = File(imagePath)
                                if (file.exists())
                                    file.delete()
                            }
                        }
                        // val fileSizeInKB = (fileToUpload!!.length()) / 1024
                        // val fileSizeInMB = fileSizeInKB / 1024
                        // showToast("$fileSizeInKB")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                PICK_PHOTO_FROM_GALLERY_CODE -> {
                    if (getIntent != null && getIntent.data != null) {
                        attachmentUri = getIntent.data
                        try {
                            //profileImage.setImageBitmap(getCapturedImage(attachmentUri!!))
                            val realImagePath = fileUploader.getRealPathFromUri(attachmentUri!!)
                            if (realImagePath != null) {
                                imgProfile.setImageBitmap(getBitmapFromImagePath(realImagePath))
                                fileToUpload =
                                    ImageCompresser.instance.getCompressedImageFile(
                                        realImagePath,
                                        getContext()
                                    )
//                                val bitmap = BitmapFactory.decodeFile(fileToUpload!!.absolutePath)
//                                imgProfile.setImageBitmap(bitmap)
                                // val fileSizeInKB = (fileToUpload!!.length()) / 1024
                                // val fileSizeInMB = fileSizeInKB / 1024
                                // showToast("$fileSizeInKB")
                            }
                        } catch (e: Exception) {
                            //
                        }
                    }
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            CAMERA_REQUEST_CODE ->
                if (grantResults.isNotEmpty()) {
                    var isBothPermissionGranted = true
                    for (i in permissions.indices) {
                        if (permissionUtils.shouldAskPermission(getContext(), permissions[i])) {
                            isBothPermissionGranted = false
                            break
                        }
                    }
                    if (isBothPermissionGranted) {
                        takePicture()
                    } else {
                        manageCameraAndStoragePermission()
                    }
                }

            GALLERY_REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                AppUtils.shared.openGallery(getContext(), PICK_PHOTO_FROM_GALLERY_CODE)
            }
        }
    }

    override fun profileInfoUpdateDidSucceed() {
        fileToUpload?.let {
            if (fileToUpload!!.exists()) {
                fileToUpload!!.delete()
            }
        }
        mPrefManager.putBoolean(Keys.KEEP_ME_LOGGED_IN.name, true)
        mPrefManager.putBoolean(PrefKeys.IS_REGISTERED, true)
        mPrefManager.putBoolean(PrefKeys.IS_VERIFIED, true)
        Navigator.sharedInstance.navigate(getContext(), HomeActivity::class.java)
        finish()
    }

    override fun profileInfoUpdateDidFailed(failedMsg: String) {
        mAlertService.showAlert(getContext(), null, failedMsg)
    }


    private fun manageCameraAndStoragePermission() {
        val storagePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE
        val cameraPermission = Manifest.permission.CAMERA
        val permissions = arrayOf(cameraPermission, storagePermission)
        val listOfPermissionNeeded = ArrayList<String>()
        for (permission in permissions) {
            if (permissionUtils.shouldAskPermission(getContext(), permission)) {
                listOfPermissionNeeded.add(permission)
            }
        }
        mAlertService.showConfirmationAlert(getContext(),
            getString(R.string.permission_required),
            getString(R.string.permission_msg_for_camera),
            getString(R.string.button_not_now),
            getString(R.string.buttoin_continue),
            object : AlertService.AlertListener {

                override fun negativeBtnDidTapped() {}

                override fun positiveBtnDidTapped() {
                    ActivityCompat.requestPermissions(
                        this@ProfileInfoActivity,
                        listOfPermissionNeeded.toTypedArray(),
                        CAMERA_REQUEST_CODE
                    )
                }
            })

    }

    /** Checking Camera Permission  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    private fun hasCameraPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            getContext(),
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
    }

    /** Checking Storage Permission  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    private fun hasStoragePermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            getContext(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    /** Checking Camera and Storage Permission  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    private fun hasCameraAndStoragePermission(): Boolean {
        return hasCameraPermission() && hasStoragePermission()
    }


    /** Creating Image File  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "IMG_$timeStamp"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            imageFileName,
            /** prefix */
            ".jpg",
            /** suffix */
            storageDir
            /** directory */
        ).apply {
            imagePath = absolutePath
        }
    }


    /** Dispatching Camera Intent  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    private fun takePicture() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(packageManager) != null) {
            var imageFile: File? = null
            try {
                imageFile = createImageFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            imageFile?.let {
                val authorities: String = applicationContext.packageName + ".provider"
                /** Authorities That we provided in Manifest.xml*/
                imageUri = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                    Uri.fromFile(imageFile)
                } else {
                    FileProvider.getUriForFile(getContext(), authorities, imageFile)
                }
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                startActivityForResult(intent, PICK_PHOTO_FROM_CAMERA_CODE)
            }
        }
    }

    private fun getBitmapFromImagePath(imagePath: String?): Bitmap? {
        imagePath?.let {
            val imageFile = File(imagePath)
            if (imageFile.exists()) {
                val myBitmap = BitmapFactory.decodeFile(imageFile.absolutePath)
                val ei = androidx.exifinterface.media.ExifInterface(imageFile.absolutePath)
                val orientation = ei.getAttributeInt(
                    androidx.exifinterface.media.ExifInterface.TAG_ORIENTATION,
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_UNDEFINED
                )
                val rotatedBitmap: Bitmap?
                rotatedBitmap = when (orientation) {
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_ROTATE_90 -> TransformationUtils.rotateImage(
                        myBitmap,
                        90
                    )
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_ROTATE_180 -> TransformationUtils.rotateImage(
                        myBitmap,
                        180
                    )
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_ROTATE_270 -> TransformationUtils.rotateImage(
                        myBitmap,
                        270
                    )
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_NORMAL -> myBitmap
                    else -> myBitmap
                }
                return rotatedBitmap
            }
            return null
        }
        return null
    }

}
