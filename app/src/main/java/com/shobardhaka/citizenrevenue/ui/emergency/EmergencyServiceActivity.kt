package com.shobardhaka.citizenrevenue.ui.emergency

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.RecyclerView
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.adapters.EmergencyServicesAdapter
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.callbacks.CallButtonClickListener
import com.shobardhaka.citizenrevenue.data.network.api_response.GetEmergencyServiceResponse
import com.shobardhaka.citizenrevenue.utils.AlertService
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.PermissionUtils
import kotlinx.android.synthetic.main.activity_emmergency_service.*
import kotlinx.android.synthetic.main.activity_my_complaints.*
import kotlinx.android.synthetic.main.activity_my_complaints.recyclerView
import kotlinx.android.synthetic.main.toolbar_with_text.*
import javax.inject.Inject

class EmergencyServiceActivity : MvpBaseActivity<EmergencyServicePresenter>(),
    EmergencyServiceContract.View {

    private lateinit var emergencyServiceAdapter: EmergencyServicesAdapter
    private lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
    private var animationController: LayoutAnimationController? = null


    internal val REQUEST_CALL_PHONE_PERMISSION_CODE = 15
    internal var CALL_PHONE_PERMISSION = arrayOf(Manifest.permission.CALL_PHONE)

    private var emergencyNumber = ""

    @Inject
    lateinit var permissionUtils: PermissionUtils

    override fun getContentView(): Int {
        return R.layout.activity_emmergency_service
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {

        setSupportActionBar(toolbar)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        tvTitle.text = getString(R.string.emergency_numbers)


        linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            getContext(), RecyclerView.VERTICAL,
            false
        )
        emergencyServiceAdapter =
            EmergencyServicesAdapter(getContext(), object : CallButtonClickListener {
                override fun onCallButtonClicked(number: String) {
                    emergencyNumber = number
                    nowCall()
                    //managePermissionAndCall()
                }
            })

        animationController =
            AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_slide_from_right)
        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = emergencyServiceAdapter
            layoutAnimation = animationController
        }

        mPresenter.getEmergencyServices()
    }

    override fun emergencyServicesDidReceived(services: ArrayList<GetEmergencyServiceResponse.SingleEmergencyService?>) {
        emergencyServiceAdapter.setData(services)
        recyclerView.scheduleLayoutAnimation()
    }

    private fun managePermissionAndCall() {
        permissionUtils.checkPermission(
            this,
            Manifest.permission.CALL_PHONE,
            object : PermissionUtils.OnPermissionAskListener {

                override fun onNeedPermission() {
                    mAlertService.showConfirmationAlert(this@EmergencyServiceActivity,
                        getString(R.string.permission_required),
                        getString(R.string.permission_call_phone_one),
                        getString(R.string.button_not_now),
                        getString(R.string.okay),
                        object : AlertService.AlertListener {

                            override fun negativeBtnDidTapped() {}

                            override fun positiveBtnDidTapped() {
                                ActivityCompat.requestPermissions(
                                    this@EmergencyServiceActivity,
                                    CALL_PHONE_PERMISSION,
                                    REQUEST_CALL_PHONE_PERMISSION_CODE
                                )

                            }
                        })
                }

                override fun onPermissionPreviouslyDenied() {
                    mAlertService.showConfirmationAlert(this@EmergencyServiceActivity,
                        getString(R.string.permission_required),
                        getString(R.string.permission_call_phone_one),
                        getString(R.string.button_not_now),
                        getString(R.string.okay),
                        object : AlertService.AlertListener {

                            override fun negativeBtnDidTapped() {}

                            override fun positiveBtnDidTapped() {
                                ActivityCompat.requestPermissions(
                                    this@EmergencyServiceActivity,
                                    CALL_PHONE_PERMISSION,
                                    REQUEST_CALL_PHONE_PERMISSION_CODE
                                )

                            }
                        })
                }

                override fun onPermissionPreviouslyDeniedWithNeverAskAgain() {
                    mAlertService.showConfirmationAlert(this@EmergencyServiceActivity,
                        getString(R.string.permission_required),
                        getString(R.string.permission_call_phone_two),
                        getString(R.string.button_not_now),
                        getString(R.string.settings),
                        object : AlertService.AlertListener {

                            override fun negativeBtnDidTapped() {

                            }

                            override fun positiveBtnDidTapped() {
                                AppUtils.shared.goToSettings(getContext())
                            }
                        })
                }

                override fun onPermissionGranted() {
                    nowCall()
                }
            })
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_CALL_PHONE_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                nowCall()
            }
        }
    }

    private fun nowCall() {
        AppUtils.shared.dial(getContext(), emergencyNumber)
    }

    override fun onNetworkCallStarted(loadingMessage: String) {
        showProgressDialog(getString(R.string.please_wait))
    }
}
