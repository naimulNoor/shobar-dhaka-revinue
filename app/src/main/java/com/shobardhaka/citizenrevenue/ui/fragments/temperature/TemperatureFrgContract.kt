package com.shobardhaka.citizenrevenue.ui.fragments.temperature

import com.shobardhaka.citizenrevenue.base.BaseContract
import com.shobardhaka.citizenrevenue.data.network.api_response.GetCurrentWeatherResponse

interface TemperatureFrgContract{
    interface View: BaseContract.View{
        fun currentWeatherDidReceived(currentWeatherResponse: GetCurrentWeatherResponse)

    }

    interface Presenter: BaseContract.Presenter{
        fun getCurrentWeatherOfDhaka()

    }
}