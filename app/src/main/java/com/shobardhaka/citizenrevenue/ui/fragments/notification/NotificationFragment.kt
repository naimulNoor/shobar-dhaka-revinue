package com.shobardhaka.citizenrevenue.ui.fragments.notification


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.adapters.NotificationsAdapter
import com.shobardhaka.citizenrevenue.adapters.RecyclerTouchListener
import com.shobardhaka.citizenrevenue.base.BaseFragment
import com.shobardhaka.citizenrevenue.callbacks.NotificationSeenListener
import com.shobardhaka.citizenrevenue.data.network.APIs
import com.shobardhaka.citizenrevenue.data.network.api_response.NotificationResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.ui.NotificationDetailsActivity
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import com.shobardhaka.citizenrevenue.utils.Navigator
import kotlinx.android.synthetic.main.fragment_notificaiton.view.*
import java.lang.Exception


class NotificationFragment : BaseFragment<NotificationFrgPresenter>(), NotificationFrgContract.View {

    companion object {
        fun newInstance(): NotificationFragment {
            return NotificationFragment()
        }
    }

    private lateinit var notificationAdapter: NotificationsAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var animationController: LayoutAnimationController

    private var notificationSeenListener: NotificationSeenListener? = null


    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, saveInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_notificaiton, container, false)

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            notificationSeenListener = context as NotificationSeenListener
        } catch (e: Exception) {
        }
    }

    override fun onDetach() {
        super.onDetach()
        notificationSeenListener = null
    }

    override fun onViewReady(getArguments: Bundle?) {

        animationController = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_slide_from_right);

        linearLayoutManager = LinearLayoutManager(
            context,
            RecyclerView.VERTICAL,
            false
        )
        notificationAdapter = NotificationsAdapter(context)

        rootView.recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = notificationAdapter
            layoutAnimation = animationController
        }

        rootView.recyclerView.addOnItemTouchListener(
            RecyclerTouchListener(
                context,
                rootView.recyclerView,
                object : RecyclerTouchListener.ClickListener {
                    override fun onClick(view: View, position: Int) {
                        val singleNotification: NotificationResponse.SingleNotification =
                            notificationAdapter.getItem(position) as NotificationResponse.SingleNotification
                        if (singleNotification.isRead == 0) {
                            singleNotification.isRead = 1
                            notificationSeenListener?.onSeenNotification()
                            notificationAdapter.notifyItemChanged(position)
                            mPresenter.setNotificationAsRead("${APIs.GET_NOTIFICATION_DETAILS}${singleNotification.id}")
                        }
                        val dataBundle = Bundle()
                        dataBundle.putSerializable(Keys.single_notification.name, singleNotification)
                        Navigator.sharedInstance.navigateWithBundle(
                            context,
                            NotificationDetailsActivity::class.java,
                            Keys.DATA_BUNDLE.name,
                            dataBundle
                        )
                    }

                    override fun onLongClick(view: View?, position: Int) {
                        // Nothing to do.......
                    }
                })
        )
        rootView.imgStatus.visibility = View.GONE
        mPresenter.getNotifications()
    }

    override fun notificationsDidReceived(notifications: ArrayList<NotificationResponse.SingleNotification?>) {
        if (notifications.isNotEmpty()) {
            notificationAdapter.addAll(notifications)
            rootView.recyclerView.scheduleLayoutAnimation()
        } else {
            rootView.imgStatus.visibility = View.VISIBLE
            if (mAppLanguage == AppLanguage.BENGALI.name)
                rootView.imgStatus.setImageResource(R.drawable.ic_empty_notification_ben)
            else
                rootView.imgStatus.setImageResource(R.drawable.ic_empty_notification_eng)
        }
    }

    override fun onNetworkCallStarted(loadingMessage: String) {
        rootView.progress_circular.visibility = View.VISIBLE
    }

    override fun onNetworkCallEnded() {
        rootView.progress_circular.visibility = View.GONE
    }

}
