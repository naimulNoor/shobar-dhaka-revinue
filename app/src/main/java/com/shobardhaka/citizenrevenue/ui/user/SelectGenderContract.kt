package com.shobardhaka.citizenrevenue.ui.user
import com.shobardhaka.citizenrevenue.base.BaseContract

interface SelectGenderContract {

    interface View : BaseContract.View {
        fun genderUpdateDidSucceed()
    }

    interface Presenter : BaseContract.Presenter {
        fun updateGender(gender: String)
    }
}