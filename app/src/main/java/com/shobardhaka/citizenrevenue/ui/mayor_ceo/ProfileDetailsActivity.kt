package com.shobardhaka.citizenrevenue.ui.mayor_ceo

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.data.network.api_response.MayorCeoResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.file.PicassoImageLoader
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import kotlinx.android.synthetic.main.activity_profile_details.*
import kotlinx.android.synthetic.main.toolbar_with_text.*

class ProfileDetailsActivity : MvpBaseActivity<ProfileDetailsPresenter>(),
    ProfileDetailsContract.View {

    var profileType: String? = ""
    override fun getContentView(): Int {
        return R.layout.activity_profile_details
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }
        profileType =
            intent.getBundleExtra(Keys.DATA_BUNDLE.name).getString(Keys.PROFILE_TYPE.name)

        supportActionBar?.let {
            supportActionBar!!.title = title
        }

        mPresenter.getProfileDetails()
    }

    override fun profileDetailsDidReceived(response: MayorCeoResponse) {
        when (profileType) {
            Keys.MEYOR.name -> {
                tvTitle.text = getString(R.string.meyor)
                imgProfilePic.setImageResource(R.drawable.pic_meyor)
                PicassoImageLoader.shared.loadImage(getContext(), response.data.mayorImage, imgProfilePic, R.drawable.profile)
                tvProfileName.text =
                    if (mAppLanguage == AppLanguage.BENGALI.name) response.data.mayorNameBn else response.data.mayorNameEn
                tvProfileDetails.text =
                    if (mAppLanguage == AppLanguage.BENGALI.name) response.data.mayorDescriptionBn else response.data.mayorDescriptionEn

            }
            Keys.CEO.name -> {
                tvTitle.text = getString(R.string.ceo)
                PicassoImageLoader.shared.loadImage(getContext(), response.data.ceoImage, imgProfilePic, R.drawable.profile)
                tvProfileName.text =
                    if (mAppLanguage == AppLanguage.BENGALI.name) response.data.ceoNameBn else response.data.ceoNameEn

                tvProfileDetails.text =
                    if (mAppLanguage == AppLanguage.BENGALI.name) response.data.ceiDescriptionBn else response.data.ceiDescriptionEn

            }
        }

        imgProfilePic.visibility = View.VISIBLE
        tvProfileName.visibility = View.VISIBLE
        tvProfileDetails.visibility = View.VISIBLE
    }
}
