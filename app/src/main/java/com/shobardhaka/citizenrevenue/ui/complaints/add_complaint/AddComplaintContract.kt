package com.shobardhaka.citizenrevenue.ui.complaints.add_complaint

import com.shobardhaka.citizenrevenue.base.BaseContract
import com.shobardhaka.citizenrevenue.data.network.api_response.IssueTypeResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.HashMap

interface AddComplaintContract {

    interface View : BaseContract.View {

        fun addComplainDidSucceed()

        fun addComplainDidFailed(failedMsg: String)

        fun geoFencingDidChecked(foundInDncc: Boolean)
        fun addressDidReceived(address: String)

        fun issueSubTypeDidReceived(response: IssueTypeResponse)
    }

    interface Presenter : BaseContract.Presenter {

        fun addComplain(
            dataMap: HashMap<String, RequestBody>,
            attachment: MultipartBody.Part?
        )

        fun checkGeoFencing(url: String)
        fun getAddressFromBarikoiByLatLong(url: String)

        fun getSubIssueType(categoryId: String)
    }
}