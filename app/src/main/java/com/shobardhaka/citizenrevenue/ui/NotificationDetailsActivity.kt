package com.shobardhaka.citizenrevenue.ui

import android.content.Intent
import android.os.Bundle
import android.text.util.Linkify
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BaseActivity
import com.shobardhaka.citizenrevenue.data.network.api_response.NotificationResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.utils.DateFormatUtils
import kotlinx.android.synthetic.main.activity_notification_details.*
import kotlinx.android.synthetic.main.toolbar_with_text.*

class NotificationDetailsActivity : BaseActivity() {

    override fun getContentView(): Int {
        return R.layout.activity_notification_details
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }
        tvTitle.text = getString(R.string.notification_details)
        val bundle = intent.getBundleExtra(Keys.DATA_BUNDLE.name)
        val singleNotification: NotificationResponse.SingleNotification =
            bundle.getSerializable(Keys.single_notification.name) as NotificationResponse.SingleNotification
        tvNotDate.text = DateFormatUtils.shared.getDate(singleNotification.createdAt)
        tvNotTitle.text = singleNotification.title

        // Details
        tvNotDetails.text = singleNotification.description
        Linkify.addLinks(tvNotDetails, Linkify.WEB_URLS)
    }

}