package com.shobardhaka.citizenrevenue.ui.events

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.GetBannerResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.GetCurrentWeatherResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.GetEventResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.OpenWeatherMapApiService
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import javax.inject.Inject

class EventPresenter @Inject constructor(view: EventContract.View) : BasePresenter<EventContract.View>(view),
    EventContract.Presenter {

    @Inject
    lateinit var userApiService: UserApiService

    @Inject
    lateinit var openWeatherMapApiService: OpenWeatherMapApiService

    override fun getEvents() {
        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            userApiService.getEvents()
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<GetEventResponse, EventContract.View>(mView) {
                    override fun onRequestSuccess(response: GetEventResponse) {
                        mView?.eventsDidReceived(response.events)
                    }

                })
        )
    }

    override fun getBanner() {
        compositeDisposable?.add(
            userApiService.getBanner()
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<GetBannerResponse, EventContract.View>(mView) {
                    override fun onRequestSuccess(response: GetBannerResponse) {
                        mView?.bannderDiDReceived(response.banner)
                    }

                })
        )
    }

    override fun getCurrentWeatherOfDhaka() {
        compositeDisposable?.add(
            openWeatherMapApiService.getCurrentWeatherOfDhakaCity()
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<GetCurrentWeatherResponse, EventContract.View>(mView) {
                    override fun onRequestSuccess(response: GetCurrentWeatherResponse) {
                        mView?.currentWeatherDidReceived(response)
                    }

                })
        )

    }
}