package com.shobardhaka.citizenrevenue.ui.fragments.events

import dagger.Binds
import dagger.Module

@Module
abstract class EventsFrgViewModule {
    @Binds
    abstract fun provideEventFrgViewModule(fragment: EventsFragment): EventsFrgContract.View
}