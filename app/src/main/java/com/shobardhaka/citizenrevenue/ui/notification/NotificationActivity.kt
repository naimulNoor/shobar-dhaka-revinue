package com.shobardhaka.citizenrevenue.ui.notification

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.FragmentManager
import com.google.android.material.tabs.TabLayout
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.callbacks.DisasterSeenListener
import com.shobardhaka.citizenrevenue.callbacks.NotificationSeenListener
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.ui.fragments.notification.NotificationFragment
import com.shobardhaka.citizenrevenue.ui.fragments.shotortobarta.ShotorkobartaFragment
import com.shobardhaka.citizenrevenue.ui.home.HomeActivity
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import com.shobardhaka.citizenrevenue.utils.Keys.NotificationType
import com.shobardhaka.citizenrevenue.utils.Navigator
import kotlinx.android.synthetic.main.activity_notification.*
import kotlinx.android.synthetic.main.tab_notification.view.*

class NotificationActivity : MvpBaseActivity<NotificationPresenter>(), NotificationContract.View,
    NotificationSeenListener, DisasterSeenListener {

    lateinit var mFragmentManager: FragmentManager

    private var unSeenNot = 0
    private var unSeenDisaster = 0
    private var tabNotification: View? = null
    private var tabDisaster: View? = null


    override fun getContentView(): Int {
        return R.layout.activity_notification
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {

        val bundle = intent.getBundleExtra(Keys.DATA_BUNDLE.name)
        bundle?.let {
            unSeenNot = bundle.getInt(Keys.UNSEEN_NOT_COUNT.name)
            unSeenDisaster = bundle.getInt(Keys.UNSEEN_DISASTER_COUNT.name)
        }

        tabNotification = LayoutInflater.from(getContext()).inflate(R.layout.tab_notification, null)
        tabDisaster = LayoutInflater.from(getContext()).inflate(R.layout.tab_notification, null)

        tabNotification!!.tabTitle.text = getString(R.string.tab_notification)
        tabDisaster!!.tabTitle.text = getString(R.string.tab_warning)

        bundle?.let {
            if (unSeenNot > 0) {
                tabNotification!!.count.text =
                        if (mAppLanguage == AppLanguage.BENGALI.name)
                            AppUtils.shared.getInBangla("$unSeenNot")
                        else "$unSeenNot"
            } else
                tabNotification!!.count.visibility = View.GONE

            if (unSeenDisaster > 0) {
                tabDisaster!!.count.text =
                        if (mAppLanguage == AppLanguage.BENGALI.name)
                            AppUtils.shared.getInBangla("$unSeenDisaster")
                        else "$unSeenDisaster"
            } else
                tabDisaster!!.count.visibility = View.GONE
        } ?: run {
            /** Bundle Null = Hiding Notification And Disaster Count as Intent Came from Notification Utils */
            tabNotification!!.count.visibility = View.GONE
            tabDisaster!!.count.visibility = View.GONE
        }

        tabLayout.addTab(tabLayout.newTab().setCustomView(tabNotification))
        tabLayout.addTab(tabLayout.newTab().setCustomView(tabDisaster))


        //  tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.notification)))
        //  tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.shotorkobarta)))
        mFragmentManager = supportFragmentManager
        if (bundle != null) {
            /** Bundle Not Null = Opening this activity from Home Activity */
            if (unSeenDisaster > 0) {
                mFragmentManager.beginTransaction().add(R.id.fragment_frame, ShotorkobartaFragment.newInstance())
                    .commit()
                tabLayout.getTabAt(1)?.select()
            } else {
                mFragmentManager.beginTransaction().add(R.id.fragment_frame, NotificationFragment.newInstance())
                    .commit()
            }
        } else {
            /** Bundle Null = Opening activity from Notification Utils ( That's why checking intent data)*/
            val notificationType = intent.getStringExtra(Keys.NOTIFCIATION_TYPE.name)
            if (notificationType == NotificationType.disaster.name) {
                mFragmentManager.beginTransaction().add(R.id.fragment_frame, ShotorkobartaFragment.newInstance())
                    .commit()
                tabLayout.getTabAt(1)?.select()
            } else {
                mFragmentManager.beginTransaction().add(R.id.fragment_frame, NotificationFragment.newInstance())
                    .commit()
            }
        }


        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                val position = tab?.position
                when (position) {
                    0 -> mFragmentManager.beginTransaction()
                        .replace(R.id.fragment_frame, NotificationFragment.newInstance())
                        .commit()

                    1 -> mFragmentManager.beginTransaction()
                        .replace(R.id.fragment_frame, ShotorkobartaFragment.newInstance())
                        .commit()
                }

            }

            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {

            }
        })


    }

    override fun onSeenDisaster() {
        if (unSeenDisaster != 0) {
            unSeenDisaster -= 1
            if (unSeenDisaster > 0) {
                tabDisaster!!.count.text =
                        if (mAppLanguage == AppLanguage.BENGALI.name)
                            AppUtils.shared.getInBangla("$unSeenDisaster")
                        else "$unSeenDisaster"
            } else
                tabDisaster!!.count.visibility = View.GONE
        }

    }

    override fun onSeenNotification() {
        if(unSeenNot != 0){
            unSeenNot -= 1
            if (unSeenNot > 0) {
                tabNotification!!.count.text =
                        if (mAppLanguage == AppLanguage.BENGALI.name)
                            AppUtils.shared.getInBangla("$unSeenNot")
                        else "$unSeenNot"
            } else
                tabNotification!!.count.visibility = View.GONE
        }
    }

    override fun onBackPressed() {
        Navigator.sharedInstance.back(getContext(), HomeActivity::class.java)
        finish()
    }
}
