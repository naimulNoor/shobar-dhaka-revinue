package com.shobardhaka.citizenrevenue.ui.maps

import dagger.Binds
import dagger.Module

@Module
abstract class MapsViewModule{
    @Binds
    abstract fun provideMapsViewView(activity: MapsActivity): MapsContract.View
}