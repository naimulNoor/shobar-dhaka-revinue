package com.shobardhaka.citizenrevenue.ui.home
import dagger.Binds
import dagger.Module

@Module
abstract class HomeViewModule {
    @Binds
    abstract fun provideHomeViewView(activity: HomeActivity): HomeContract.View
}