package com.shobardhaka.citizenrevenue.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.iid.FirebaseInstanceId
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.callbacks.OnComplainStatusClickListener
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.ui.fragments.complain.MyComplaintsFragment
import com.shobardhaka.citizenrevenue.ui.fragments.dncc.DnccFragment
import com.shobardhaka.citizenrevenue.ui.fragments.home.HomeFragment
import com.shobardhaka.citizenrevenue.ui.fragments.others.OthersFragment
import com.shobardhaka.citizenrevenue.ui.user.SelectGenderActivity
import com.shobardhaka.citizenrevenue.utils.Keys.ComplainType
import com.shobardhaka.citizenrevenue.utils.Navigator
import com.shobardhaka.citizenrevenue.utils.PermissionUtils
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject


class HomeActivity : MvpBaseActivity<HomePresenter>(), HomeContract.View, OnComplainStatusClickListener {

    @Inject
    lateinit var permissionUtils: PermissionUtils

    lateinit var mFragmentManager: FragmentManager

    private var currentFragmentName = Keys.HOME.name

    override fun getContentView(): Int {
        return R.layout.activity_home
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        mFragmentManager = supportFragmentManager
        mFragmentManager.beginTransaction().add(R.id.fragment_frame, HomeFragment.newInstance()).commit()
        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        // Storing Device Token to Remote
        if (mPrefManager.getBoolean(Keys.IS_NOTIFICATION_ENABLED.name, true)) {
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
                val deviceToken = it.token
                Log.e("DEVICE TOKEN: ", deviceToken)
                mPresenter.storeDeviceToken(deviceToken)
                mPrefManager.putString(Keys.DEVICE_TOKEN.name, deviceToken)
            }
        }
    }


    override fun onResume() {
        super.onResume()
        // force taking gender >>>>
        val gender =  mPrefManager.getStringHashMap(Keys.PROFILE.name)!![Keys.ProfileKey.GENDER.name] ?: ""
        if (gender.isEmpty()){
            Navigator.sharedInstance.navigate(getContext(), SelectGenderActivity::class.java)
            finish()
        }
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.bottom_nav_home -> {
                if (currentFragmentName != Keys.HOME.name)
                    loadFragment(HomeFragment.newInstance(), Keys.HOME.name)
            }
            R.id.bottom_nav_dncc -> {
                if (currentFragmentName != Keys.DNCC.name)
                    loadFragment(DnccFragment.newInstance(), Keys.DNCC.name)
            }
            R.id.bottom_nav_complain -> {
                if (currentFragmentName != Keys.COMPLAIN.name)
                    loadFragment(MyComplaintsFragment.newInstance(), Keys.COMPLAIN.name)
            }
            R.id.bottom_nav_others -> {
                if (currentFragmentName != Keys.OTHERS.name)
                    loadFragment(OthersFragment.newInstance(), Keys.OTHERS.name)
            }
        }
        true
    }

    private fun loadFragment(fragment: Fragment, fragmentName: String) {
        currentFragmentName = fragmentName
        supportFragmentManager.beginTransaction()
            //.setCustomAnimations(R.anim.slide_from_left, R.anim.slide_to_right)
            .replace(R.id.fragment_frame, fragment)
            .commit()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onComplainStatusClicked(position: Int) {
        val myComplainsFragment = MyComplaintsFragment.newInstance()
        val bundle = Bundle()
        when (position) {
            0 -> bundle.putSerializable(Keys.COMPLAINT_TYPE.name, ComplainType.ALL)
            1 -> bundle.putSerializable(Keys.COMPLAINT_TYPE.name, ComplainType.SOLVED)
            2 -> bundle.putSerializable(Keys.COMPLAINT_TYPE.name, ComplainType.UNSOLVED)
        }
        myComplainsFragment.arguments = bundle
        loadFragment(myComplainsFragment, Keys.COMPLAIN.name)
        bottom_navigation.menu.findItem(R.id.bottom_nav_complain).isChecked = true
    }

    override fun onBackPressed() {
        if (currentFragmentName == Keys.HOME.name) {
          //  stopLocationService()
            super.onBackPressed()
        } else {
            bottom_navigation.selectedItemId = R.id.bottom_nav_home
        }
    }

//    private fun stopLocationService() {
//        Intent(getContext().applicationContext, LocationService::class.java).also { intent ->
//            stopService(intent)
//        }
//    }
}
