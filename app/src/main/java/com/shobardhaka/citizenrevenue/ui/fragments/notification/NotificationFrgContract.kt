package com.shobardhaka.citizenrevenue.ui.fragments.notification

import com.shobardhaka.citizenrevenue.base.BaseContract
import com.shobardhaka.citizenrevenue.data.network.api_response.NotificationResponse

interface NotificationFrgContract {

    interface View : BaseContract.View {

        fun notificationsDidReceived(notifications: ArrayList<NotificationResponse.SingleNotification?>)
    }

    interface Presenter : BaseContract.Presenter {

        fun getNotifications()
        fun setNotificationAsRead(url: String)
    }
}