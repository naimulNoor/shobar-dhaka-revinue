package com.shobardhaka.citizenrevenue.ui.fragments.complain

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.GetComplainResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.ComplainApiService
import javax.inject.Inject

class MyComplaintFrgPresenter@Inject constructor(view: MyComplainFrgContract.View): BasePresenter<MyComplainFrgContract.View>(view), MyComplainFrgContract.Presenter{

    @Inject
    lateinit var complainApiService: ComplainApiService

    override fun getMyComplains(url: String) {
        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            complainApiService.getComplains(url)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<GetComplainResponse, MyComplainFrgContract.View>(mView) {

                    override fun onRequestSuccess(response: GetComplainResponse) {
                        mView?.myComplainsDidReceived(response)
                    }

                })
        )
    }

}