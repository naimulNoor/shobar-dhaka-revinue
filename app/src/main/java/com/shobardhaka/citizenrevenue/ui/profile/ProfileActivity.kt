package com.shobardhaka.citizenrevenue.ui.profile

import android.Manifest
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.transition.Fade
import android.transition.TransitionManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import butterknife.OnClick
import com.bumptech.glide.load.resource.bitmap.TransformationUtils
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.file.FileUploader
import com.shobardhaka.citizenrevenue.file.PicassoImageLoader
import com.shobardhaka.citizenrevenue.utils.*
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.toolbar_profile.*
import okhttp3.RequestBody
import java.io.File
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashMap


class ProfileActivity : MvpBaseActivity<ProfilePresenter>(), ProfileContract.View {

    private var overflowMenu: Menu? = null
    private var profileEditable = false

    private val PICK_PHOTO_FROM_CAMERA_CODE = 1994
    private val PICK_PHOTO_FROM_GALLERY_CODE = 1958
    private val CAMERA_REQUEST_CODE = 13
    private val GALLERY_REQUEST_CODE = 15
    private lateinit var fileUploader: FileUploader
    private var attachmentUri: Uri? = null
    private var fileToUpload: File? = null
    private var GALLERY_PERMISSION = arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    private var dateOfBirthForNetwork = ""
    private var userMap: HashMap<String, String>? = null

    // Gender
    var gender: String = ""

    @Inject
    lateinit var permissionUtils: PermissionUtils

    companion object {
        private var imageUri: Uri? = null
        private var imagePath: String? = null
    }

    override fun getContentView(): Int {
        return R.layout.activity_profile
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        setSupportActionBar(toolbar)
        tvTitle.text = getString(R.string.profile)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }
        fileUploader = FileUploader(getContext())
        setProfileData()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.edit_profile_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        overflowMenu = menu
        overflowMenu!!.findItem(R.id.menu_edit_profile_cancel).isVisible = false
        overflowMenu!!.findItem(R.id.menu_edit_profile_done).isVisible = false
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_edit_profile -> {
                makeProfileEditable()
            }
            R.id.menu_edit_profile_cancel -> {
                makeProfileNotEditable()
                setProfileData()
            }

            R.id.menu_edit_profile_done -> {
                when {
                    gender.isEmpty() -> {
                        mAlertService.showAlert(getContext(), null, getString(R.string.gender_empty_msg))
                    }
                    else -> {
                        val name = etName.text.toString().trim()
                        val emergencyContact = etEmergencyContact.text.toString().trim()
                        val emergencyContactName = etEmerencyContactName.text.toString().trim()
                        val dataMap: HashMap<String, RequestBody> = HashMap()
                        dataMap["name"] = fileUploader.createPartFromString(name)
                        dataMap["gender"] = fileUploader.createPartFromString(gender)
                        dataMap["dob"] = fileUploader.createPartFromString(dateOfBirthForNetwork)
                        dataMap["emergency_contact_number"] =
                            fileUploader.createPartFromString(emergencyContact)
                        dataMap["emergency_contact_name"] =
                            fileUploader.createPartFromString(emergencyContactName)
                        mPresenter.updateProfile(
                            dataMap,
                            fileUploader.getMultipartBodyPartFromFile("image", fileToUpload)
                        )
                    }
                }
            }
        }// Set fields with previous data..........
        return super.onOptionsItemSelected(item)
    }

    private fun makeProfileEditable() {
        profileEditable = true
        overflowMenu?.findItem(R.id.menu_edit_profile)?.isVisible = false
        overflowMenu?.findItem(R.id.menu_edit_profile_cancel)?.isVisible = true
        overflowMenu?.findItem(R.id.menu_edit_profile_done)?.isVisible = true

        etName.isFocusableInTouchMode = true
        // etPhoneNumber.isFocusableInTouchMode = true
        etEmergencyContact.isFocusableInTouchMode = true
        etEmerencyContactName.isFocusableInTouchMode = true


        etName.isCursorVisible = true
        //  etPhoneNumber.isCursorVisible = true
        etEmergencyContact.isCursorVisible = true
        etEmerencyContactName.isCursorVisible = true


        etName.setSelection(etName.text.length)
        //  etPhoneNumber.setSelection(etPhoneNumber.text.length)
        etEmergencyContact.setSelection(etEmergencyContact.text.length)
        etEmerencyContactName.setSelection(etEmerencyContactName.text.length)
        toggleUpdateProfileImageBtn()

    }

    private fun makeProfileNotEditable() {

        profileEditable = false

        KeyboardUtils.sharedInstance.hideSoftInput(this)
        overflowMenu?.findItem(R.id.menu_edit_profile)?.isVisible = true
        overflowMenu?.findItem(R.id.menu_edit_profile_cancel)?.isVisible = false
        overflowMenu?.findItem(R.id.menu_edit_profile_done)?.isVisible = false

        etName.isFocusable = false
        etPhoneNumber.isFocusable = false
        etEmergencyContact.isFocusable = false
        etEmerencyContactName.isFocusable = false

        etName.isFocusableInTouchMode = false
        etPhoneNumber.isFocusableInTouchMode = false
        etEmergencyContact.isFocusableInTouchMode = false
        etEmerencyContactName.isFocusableInTouchMode = false

        etName.isCursorVisible = false
        etPhoneNumber.isCursorVisible = false
        etEmergencyContact.isCursorVisible = false
        etEmerencyContactName.isCursorVisible = false

        toggleUpdateProfileImageBtn()


    }

    private fun toggleUpdateProfileImageBtn() {
        val transition = Fade()
        transition.duration = 450
        transition.addTarget(R.id.updateProfileImgBtn)
        TransitionManager.beginDelayedTransition(rr, transition)
        updateProfileImgBtn.visibility =
            if (updateProfileImgBtn.visibility == View.GONE) View.VISIBLE else View.GONE

    }

    @OnClick(R.id.updateProfileImgBtn)
    fun onClicked(view: View) {
        when (view.id) {
            R.id.updateProfileImgBtn -> if (profileEditable) {
                if (hasCameraAndStoragePermission()) {
                    takePicture()
                } else {
                    manageCameraAndStoragePermission()
                }
//                val alertBuilder = AlertDialog.Builder(this)
//                alertBuilder.setCancelable(true)
//                alertBuilder.setItems(
//                    arrayOf<CharSequence>(
//                        getString(R.string.camera),
//                        getString(R.string.gallery)
//                    )
//                ) { _, option ->
//                    when (option) {
//                        0 -> {
//                            if (hasCameraAndStoragePermission()) {
//                                takePicture()
//                            } else {
//                                manageCameraAndStoragePermission()
//                            }
//                        }
//
//                        1 -> manageGallery()
//                    }
//                }
//                alertBuilder.create().show()
            }
        }
    }

    @OnClick(R.id.layoutDateOfBirth, R.id.etDateOfBirth)
    fun onDateOfBirthClicked() {
        if (profileEditable) {
            val calendar = Calendar.getInstance()
            val dateOfBirthPickerDialog = DatePickerDialog(
                getContext(),
                dateOfBirthPickerListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            )
            dateOfBirthPickerDialog.datePicker.maxDate = calendar.timeInMillis
            dateOfBirthPickerDialog.show()
        }
    }

    @OnClick(R.id.layoutGender, R.id.etGender)
    fun onGenderClicked() {
        if (profileEditable) {
            val genders: Array<CharSequence> = arrayOf(
                getString(R.string.gender_male),
                getString(R.string.gender_female),
                getString(R.string.gender_other)
            )
            val dialogBuilder = AlertDialog.Builder(getContext())
            dialogBuilder.setCancelable(true)
            var checkedItem = when {
                etGender.text.toString() == getString(R.string.gender_male) -> 0
                etGender.text.toString() == getString(R.string.gender_female) -> 1
                etGender.text.toString() == getString(R.string.gender_other) -> 2
                else -> -1
            }
            dialogBuilder.setSingleChoiceItems(
                genders, checkedItem
            ) { _, position ->
                checkedItem = position
            }
            dialogBuilder.setPositiveButton(getString(R.string.okay)) { dialog, _ ->
                if (checkedItem != -1) {
                    gender = "$checkedItem"
                    etGender.setText(genders[checkedItem])
                }
                dialog.dismiss()
            }
            val dialog = dialogBuilder.create()
          //  dialog.getButton(AlertDialog.BUTTON_POSITIVE).
            dialog.show()
        }
    }

    private fun managePermissionAndOpenCamera() {
        val storagePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE
        val cameraPermission = Manifest.permission.CAMERA
        val permissions = arrayOf(cameraPermission, storagePermission)
        val listOfPermissionNeeded = ArrayList<String>()
        for (permission in permissions) {
            if (permissionUtils.shouldAskPermission(getContext(), permission)) {
                listOfPermissionNeeded.add(permission)
            }
        }
        if (listOfPermissionNeeded.isEmpty()) {
            AppUtils.shared.openCamera(getContext(), attachmentUri!!, PICK_PHOTO_FROM_CAMERA_CODE)
        } else {
            mAlertService.showConfirmationAlert(getContext(),
                getString(R.string.permission_required),
                getString(R.string.permission_msg_for_camera),
                getString(R.string.button_not_now),
                getString(R.string.okay),
                object : AlertService.AlertListener {

                    override fun negativeBtnDidTapped() {}

                    override fun positiveBtnDidTapped() {
                        ActivityCompat.requestPermissions(
                            this@ProfileActivity,
                            listOfPermissionNeeded.toTypedArray(),
                            CAMERA_REQUEST_CODE
                        )
                    }
                })
        }
    }


    private fun manageGallery() {
        if (!permissionUtils.shouldAskPermission(
                getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        ) {
            openGallery(PICK_PHOTO_FROM_GALLERY_CODE)
        } else {
            mAlertService.showConfirmationAlert(this@ProfileActivity,
                getString(R.string.permission_required),
                getString(R.string.permission_storage_denied_one),
                getString(R.string.button_not_now),
                getString(R.string.okay),
                object : AlertService.AlertListener {

                    override fun negativeBtnDidTapped() {

                    }

                    override fun positiveBtnDidTapped() {
                        ActivityCompat.requestPermissions(
                            this@ProfileActivity,
                            GALLERY_PERMISSION,
                            GALLERY_REQUEST_CODE
                        )

                    }

                })

        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            CAMERA_REQUEST_CODE ->
                if (grantResults.isNotEmpty()) {
                    var isBothPermissionGranted = true
                    for (i in permissions.indices) {
                        if (permissionUtils.shouldAskPermission(getContext(), permissions[i])) {
                            isBothPermissionGranted = false
                            break
                        }
                    }
                    if (isBothPermissionGranted) {
                        takePicture()
                    } else {
                        manageCameraAndStoragePermission()
                    }
                }

            GALLERY_REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openGallery(PICK_PHOTO_FROM_GALLERY_CODE)
            }
        }

    }

    private fun checkAndRequestPermission(): Boolean {
        var res = true
        val storagePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE
        val cameraPermission = Manifest.permission.CAMERA
        val permissions = arrayOf(storagePermission, cameraPermission)
        val listOfPermissionNeeded = ArrayList<String>()
        for (permission in permissions) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                listOfPermissionNeeded.add(permission)
                res = false
            }
        }
        if (!listOfPermissionNeeded.isEmpty()) {
            val params = listOfPermissionNeeded.toTypedArray()
            ActivityCompat.requestPermissions(this@ProfileActivity, params, CAMERA_REQUEST_CODE)
        }
        return res
    }

    fun openCamera(REQUEST_CODE: Int) {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val packageManager = packageManager
        if (cameraIntent.resolveActivity(packageManager) != null) {
            startActivityForResult(cameraIntent, REQUEST_CODE)
        }
    }

    fun openGallery(REQUEST_CODE: Int) {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(galleryIntent, REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, getIntent: Intent?) {
        super.onActivityResult(requestCode, resultCode, getIntent)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                PICK_PHOTO_FROM_CAMERA_CODE -> {
                    try {
                        imagePath?.let {
                            profileImage.setImageBitmap(getBitmapFromImagePath(it))
                            fileToUpload =
                                ImageCompresser.instance.getCompressedImageFile(it, getContext())
                            if (imagePath != null) {
                                val file = File(imagePath)
                                if (file.exists())
                                    file.delete()
                            }
                        }

                        // val fileSizeInKB = (fileToUpload!!.length()) / 1024
                        // val fileSizeInMB = fileSizeInKB / 1024
                        // showToast("$fileSizeInKB")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                PICK_PHOTO_FROM_GALLERY_CODE -> {
                    if (getIntent != null && getIntent.data != null) {
                        attachmentUri = getIntent.data
                        try {
                            //profileImage.setImageBitmap(getCapturedImage(attachmentUri!!))
                            val realImagePath = fileUploader.getRealPathFromUri(attachmentUri!!)
                            if (realImagePath != null) {
                                fileToUpload =
                                    ImageCompresser.instance.getCompressedImageFile(
                                        realImagePath,
                                        getContext()
                                    )
                                val bitmap = BitmapFactory.decodeFile(fileToUpload!!.absolutePath)
                                profileImage.setImageBitmap(bitmap)
                                // val fileSizeInKB = (fileToUpload!!.length()) / 1024
                                // val fileSizeInMB = fileSizeInKB / 1024
                                // showToast("$fileSizeInKB")
                            }
                        } catch (e: Exception) {
                            //
                        }
                    }
                }
            }
        }
    }

    private fun getBitmapFromImagePath(imagePath: String?): Bitmap? {
        imagePath?.let {
            val imageFile = File(imagePath)
            if (imageFile.exists()) {
                val myBitmap = BitmapFactory.decodeFile(imageFile.absolutePath)
                val ei = androidx.exifinterface.media.ExifInterface(imageFile.absolutePath)
                val orientation = ei.getAttributeInt(
                    androidx.exifinterface.media.ExifInterface.TAG_ORIENTATION,
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_UNDEFINED
                )
                val rotatedBitmap: Bitmap?
                rotatedBitmap = when (orientation) {
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_ROTATE_90 -> TransformationUtils.rotateImage(
                        myBitmap,
                        90
                    )
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_ROTATE_180 -> TransformationUtils.rotateImage(
                        myBitmap,
                        180
                    )
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_ROTATE_270 -> TransformationUtils.rotateImage(
                        myBitmap,
                        270
                    )
                    androidx.exifinterface.media.ExifInterface.ORIENTATION_NORMAL -> myBitmap
                    else -> myBitmap
                }
                return rotatedBitmap
            }
            return null
        }
        return null
    }

    private fun setProfileData() {

        userMap = mPrefManager.getStringHashMap(Keys.PROFILE.name)
        userMap?.let {
            val name = userMap!![Keys.ProfileKey.NAME.name]
            val userId = userMap!![Keys.ProfileKey.USER_ID.name]
            var mobileNumber = userMap!![Keys.ProfileKey.MOBILE_NUMBER.name]
            var emergencyContact = userMap!![Keys.ProfileKey.EMERGENCY_CONTACT.name]
            val emergencyContactName = userMap!![Keys.ProfileKey.EMERGENCY_CONTACT_NAME.name]

            gender = userMap!![Keys.ProfileKey.GENDER.name] ?: ""
            if(gender.isNotEmpty()){
                when(gender){
                    "0" -> etGender.setText(getString(R.string.gender_male))
                    "1" -> etGender.setText(getString(R.string.gender_female))
                    "2" -> etGender.setText(getString(R.string.gender_other))
                }
            }

            dateOfBirthForNetwork = userMap!![Keys.ProfileKey.DATE_OF_BIRTH.name] ?: ""
            if (dateOfBirthForNetwork.isNotEmpty()) {
                etDateOfBirth.setText(DateFormatUtils.shared.getDate(dateOfBirthForNetwork))
            }
            val profilePic = userMap!![Keys.ProfileKey.PROFILE_PIC.name]
            etName.setText(name)

            if (mAppLanguage == AppLanguage.BENGALI.name && emergencyContact != "") {
                emergencyContact = AppUtils.shared.getInBangla(emergencyContact!!)
            }

            if (mAppLanguage == AppLanguage.BENGALI.name) {
                mobileNumber = AppUtils.shared.getInBangla(mobileNumber!!)
            }

            etUserId.setText(userId)
            etPhoneNumber.setText(mobileNumber)
            etEmergencyContact.setText(emergencyContact)
            etEmerencyContactName.setText(emergencyContactName)
            if (profilePic!!.isNotEmpty()) {
                PicassoImageLoader.shared.loadImage(
                    getContext(),
                    profilePic,
                    profileImage,
                    R.drawable.profile
                )
            }
        }
    }

    private val dateOfBirthPickerListener =
        DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
            //            dateOfBirth = String.format(Locale.getDefault(), "%02d", year) +
//                    "-" + String.format(Locale.getDefault(), "%02d", month + 1) +
//                    "-" + String.format(Locale.getDefault(), "%02d", dayOfMonth)
            dateOfBirthForNetwork = "$year-${month + 1}-$dayOfMonth"
            etDateOfBirth.setText(getDate(dateOfBirthForNetwork))
        }

    override fun profileUpdateDidSucceed() {
        fileToUpload?.let {
            if (fileToUpload!!.exists()) {
                fileToUpload!!.delete()
            }
        }
        makeProfileNotEditable()
        showToast(getString(R.string.profile_update_success_msg))
    }

    override fun profileUpdateDidFailed(failedMsg: String) {
        mAlertService.showAlert(getContext(), null, failedMsg)
    }


    fun getDate(dateStr: String): String {
        val preFormat = SimpleDateFormat("yyyy-MM-dd")
        val newFormatDate =
            SimpleDateFormat("dd MMM, yyyy", Locale.getDefault()) // Format 31 Jan, 2018
        var date = ""
        try {
            date = newFormatDate.format(preFormat.parse(dateStr))
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return date
    }

    override fun onNetworkCallStarted(loadingMessage: String) {
        showProgressDialog(getString(R.string.please_wait))
    }


    private fun manageCameraAndStoragePermission() {
        val storagePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE
        val cameraPermission = Manifest.permission.CAMERA
        val permissions = arrayOf(cameraPermission, storagePermission)
        val listOfPermissionNeeded = ArrayList<String>()
        for (permission in permissions) {
            if (permissionUtils.shouldAskPermission(getContext(), permission)) {
                listOfPermissionNeeded.add(permission)
            }
        }
        mAlertService.showConfirmationAlert(getContext(),
            getString(R.string.permission_required),
            getString(R.string.permission_msg_for_camera),
            getString(R.string.button_not_now),
            getString(R.string.buttoin_continue),
            object : AlertService.AlertListener {

                override fun negativeBtnDidTapped() {}

                override fun positiveBtnDidTapped() {
                    ActivityCompat.requestPermissions(
                        this@ProfileActivity,
                        listOfPermissionNeeded.toTypedArray(),
                        CAMERA_REQUEST_CODE
                    )
                }
            })

    }

    /** Checking Camera Permission  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    private fun hasCameraPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            getContext(),
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
    }

    /** Checking Storage Permission  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    private fun hasStoragePermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            getContext(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    /** Checking Camera and Storage Permission  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    private fun hasCameraAndStoragePermission(): Boolean {
        return hasCameraPermission() && hasStoragePermission()
    }


    /** Creating Image File  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "IMG_$timeStamp"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            imageFileName,
            /** prefix */
            ".jpg",
            /** suffix */
            storageDir
            /** directory */
        ).apply {
            imagePath = absolutePath
        }
    }


    /** Dispatching Camera Intent  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
    private fun takePicture() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(packageManager) != null) {
            var imageFile: File? = null
            try {
                imageFile = createImageFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            imageFile?.let {
                val authorities: String = applicationContext.packageName + ".provider"
                /** Authorities That we provided in Manifest.xml*/
                imageUri = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                    Uri.fromFile(imageFile)
                } else {
                    FileProvider.getUriForFile(getContext(), authorities, imageFile)
                }
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                startActivityForResult(intent, PICK_PHOTO_FROM_CAMERA_CODE)
            }
        }
    }


}

