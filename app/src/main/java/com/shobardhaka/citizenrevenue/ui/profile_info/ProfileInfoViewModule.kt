package com.shobardhaka.citizenrevenue.ui.profile_info

import dagger.Binds
import dagger.Module

@Module
abstract class ProfileInfoViewModule {

    @Binds
    abstract fun provideProfileInfoViewModule(activity: ProfileInfoActivity): ProfileInfoContract.View
}