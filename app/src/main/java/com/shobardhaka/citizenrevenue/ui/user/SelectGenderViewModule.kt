package com.shobardhaka.citizenrevenue.ui.user
import dagger.Binds
import dagger.Module

@Module
abstract class SelectGenderViewModule {
    @Binds
    abstract fun provideSelectGenderView(activity: SelectGenderActivity): SelectGenderContract.View
}