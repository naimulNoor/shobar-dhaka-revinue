package com.shobardhaka.citizenrevenue.ui.profile_info

import com.shobardhaka.citizenrevenue.base.BaseContract
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.HashMap

interface ProfileInfoContract {

    interface View : BaseContract.View {

        fun profileInfoUpdateDidSucceed()
        fun profileInfoUpdateDidFailed(failedMsg: String)
    }

    interface Presenter : BaseContract.Presenter {

        fun updateProfileInfo(
            dataMap: HashMap<String, RequestBody>,
            attachment: MultipartBody.Part?
        )
    }
}