package com.shobardhaka.citizenrevenue.ui.events

import com.shobardhaka.citizenrevenue.base.BaseContract
import com.shobardhaka.citizenrevenue.data.network.api_response.GetBannerResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.GetCurrentWeatherResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.GetEventResponse

interface EventContract {
    interface View : BaseContract.View {

        fun eventsDidReceived(events: ArrayList<GetEventResponse.SingleEvent?>)
        fun bannderDiDReceived(banner: GetBannerResponse.Banner?)
        fun currentWeatherDidReceived(currentWeatherResponse: GetCurrentWeatherResponse)
    }

    interface Presenter : BaseContract.Presenter {
        fun getEvents()
        fun getBanner()

        // Weather
        fun getCurrentWeatherOfDhaka()
    }
}