package com.shobardhaka.citizenrevenue.ui.event_detials

import dagger.Binds
import dagger.Module

@Module
abstract class EventDetialsViewModule {

    @Binds
    abstract fun provideEventDetialsView(activity: EventDetailsActivity): EventDetialsContract.View
}