package com.shobardhaka.citizenrevenue.ui.maps

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.data.network.api_response.BariKoiResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import kotlinx.android.synthetic.main.toolbar_with_text.*

class MapsActivity : MvpBaseActivity<MapsPresenter>(), OnMapReadyCallback, MapsContract.View,
    GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener {


//    var bitmapBusStand: BitmapDescriptor? = null
//    var bitmapHospital: BitmapDescriptor? = null
//    var bitmapFireService: BitmapDescriptor? = null
//    var bitmapPublicToilet: BitmapDescriptor? = null
//    var bitmapPoliceStation: BitmapDescriptor? = null
//
//    var currentBitmap: BitmapDescriptor? = null

//    var locType: String? = null

    override fun onMyLocationClick(p0: Location) {
        // showToast("My Location Clicked")
    }

    override fun onMyLocationButtonClick(): Boolean {
        //showToast("My location button clicked")
        return false
    }

    private lateinit var mMap: GoogleMap
    var singlePlace: BariKoiResponse.SinglePlace? = null


    override fun getContentView(): Int {
        return R.layout.activity_maps
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }

        // bitmapBusStand = BitmapDescriptorFactory.fromBitmap(AppUtils.shared.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_map_bus_stand))
        //bitmapHospital =  BitmapDescriptorFactory.fromBitmap(AppUtils.shared.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_map_hospital))
        // bitmapFireService =  BitmapDescriptorFactory.fromBitmap(AppUtils.shared.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_map_fire_service))
        // bitmapPublicToilet =  BitmapDescriptorFactory.fromBitmap(AppUtils.shared.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_map_public_toilet))
        // bitmapPoliceStation =  BitmapDescriptorFactory.fromBitmap(AppUtils.shared.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_map_police_station))

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        val bundle = intent.getBundleExtra(Keys.DATA_BUNDLE.name)
        val locType = bundle.getString("loc_type")
        tvTitle.text = locType
        singlePlace = bundle.getSerializable(Keys.SINGLE_PLACE.name) as BariKoiResponse.SinglePlace
//        locType = bundle.getString(Keys.LOCATION_TYPE.name)
//        var title = ""

        // Barikoi Map Api......................

//        val apiKey = "NjcwOjRPTUQ1OVRDV1A"
//        var distance = 2.0
//        val limit = 80
//        var pType = ""


//        when (locType) {
//            Keys.NEARBY_BUS_STAND.name -> {
//                title = getString(R.string.nearby) + " " + getString(R.string.bus_stand)
//                pType = "Bus Stand"
//            }
//            Keys.NEARBY_HOSPITAL.name -> {
//                title = getString(R.string.nearby) + " " + getString(R.string.hospital)
//                pType = "Hospital"
//            }
//            Keys.NEARBY_POLICE_STATION.name -> {
//                title = getString(R.string.nearby) + " " + getString(R.string.thana)
//                pType = "Police Station"
//            }
//            Keys.NEARBY_FIRE_SERVICE.name -> {
//                title = getString(R.string.nearby) + " " + getString(R.string.domkol_bahini)
//                pType = "Fire Service"
//                distance = 5.0
//            }
//            Keys.NEARBY_PUBLIC_TOILET.name -> {
//                title = getString(R.string.nearby) + " " + getString(R.string.public_toilet)
//                pType = "Public Toilet"
//                distance = 5.0
//            }
//            Keys.DNCC_OFFICE.name -> {
//                title = getString(R.string.dncc_office)
//                pType = "Dncc"
//                distance = 5.0
//            }
//            Keys.NEARBY_ATM.name -> {
//                title = getString(R.string.nearby) + " " + getString(R.string.nearby_atm)
//                pType = "Atm"
//            }
//            Keys.NEARBY_PARKING.name -> {
//                title = getString(R.string.nearby) + " " + getString(R.string.nearby_parking)
//                pType = "Parking"
//            }
//            Keys.NEARBY_PHARMACY.name -> {
//                title = getString(R.string.nearby) + " " + getString(R.string.nearby_farmacy)
//                pType = "Pharmacy"
//            }
//            Keys.NEARBY_COUNCILOR_OFC.name -> {
//                title = getString(R.string.nearby) + " " + getString(R.string.nearby_councilor_ofc)
//                pType = "Public Toilet"
//            }
//            Keys.NEARBY_PARK.name -> {
//                title = getString(R.string.nearby) + " " + getString(R.string.nearby_park)
//                pType = "Park"
//            }
//            Keys.NEARBY_COMMUNITY_CENTER.name -> {
//                title = getString(R.string.nearby) + " " + getString(R.string.nearby_community_center)
//                pType = "Community Center"
//            }
//            Keys.NEARBY_CNG_PUMP.name -> {
//                title = getString(R.string.nearby) + " " + getString(R.string.nearby_cng_pumps)
//                pType = "Fuel"
//            }
//        }
//        val nearByWithCategory = "https://barikoi.xyz/v1/api/search/nearby/category/$apiKey/$distance/$limit?longitude=$currentLocLong&latitude=$currentLocLat&ptype=$pType"
//        mPresenter.getNearByPlace(nearByWithCategory)
//        supportActionBar?.let {
//            supportActionBar!!.title = title
//        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.isMyLocationEnabled = true
            mMap.uiSettings.isMyLocationButtonEnabled = true
            mMap.uiSettings.isZoomControlsEnabled = true
            mMap.uiSettings.isZoomGesturesEnabled = true
            mMap.uiSettings.isMapToolbarEnabled = true
        }
        //  mMap.setMinZoomPreference(12.2.toFloat())

        mMap.setOnMyLocationButtonClickListener(this)
        mMap.setOnMyLocationClickListener(this)

        val placeLoc = LatLng(singlePlace?.latitude!!.toDouble(), singlePlace?.longitude!!.toDouble())
        val markerOption = MarkerOptions()
        markerOption.position(placeLoc)
            .title(singlePlace?.name)
            .snippet(singlePlace?.address)
            .icon(BitmapDescriptorFactory.defaultMarker())
        val mapMarker: Marker = mMap.addMarker(markerOption)
        mMap.moveCamera(CameraUpdateFactory.newLatLng(placeLoc))

        mapMarker.showInfoWindow() // Showing info window automatically when map will open


      //  mMap.moveCamera(CameraUpdateFactory.newLatLng(placeLoc))

        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(placeLoc, 14.6.toFloat()))
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(placeLoc, 12.6.toFloat()))
    }

    override fun nearByPlacesDidReceived(nearByPlaces: ArrayList<BariKoiResponse.SinglePlace>) {
//        if (nearByPlaces.isNotEmpty()) {
//            for (place in nearByPlaces) {
//
//                val placeLoc = LatLng(place.latitude.toDouble(), place.longitude.toDouble())
//
//                val markerOption = MarkerOptions()
//                markerOption.position(placeLoc)
//                    .title(place.name)
//                    .snippet(place.address)
//                    .icon(currentBitmap)
//                mMap.addMarker(markerOption)
//                mMap.moveCamera(CameraUpdateFactory.newLatLng(placeLoc))
//            }
//            val currentPlace = LatLng(currentLocLat, currentLocLong)
//            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentPlace, 12.2.toFloat()))
//        }
    }

    override fun noNearbyPlaceFound(message: String) {
        //  showToast(message)
    }

    override fun onNetworkCallStarted(loadingMessage: String) {
        // showToast(loadingMessage)
    }

}
