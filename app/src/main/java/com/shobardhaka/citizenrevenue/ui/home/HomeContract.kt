package com.shobardhaka.citizenrevenue.ui.home

import com.shobardhaka.citizenrevenue.base.BaseContract

interface HomeContract {

    interface View : BaseContract.View {
    }

    interface Presenter : BaseContract.Presenter {

        fun storeDeviceToken(deviceToken: String)

    }
}
