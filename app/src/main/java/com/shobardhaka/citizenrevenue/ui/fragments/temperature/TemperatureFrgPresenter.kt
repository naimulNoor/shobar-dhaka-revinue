package com.shobardhaka.citizenrevenue.ui.fragments.temperature

import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.GetCurrentWeatherResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.OpenWeatherMapApiService
import javax.inject.Inject

class TemperatureFrgPresenter @Inject constructor(view: TemperatureFrgContract.View) :
    BasePresenter<TemperatureFrgContract.View>(view), TemperatureFrgContract.Presenter {

    @Inject
    lateinit var openWeatherMapApiService: OpenWeatherMapApiService

    override fun getCurrentWeatherOfDhaka() {
        compositeDisposable?.add(
            openWeatherMapApiService.getCurrentWeatherOfDhakaCity()
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<GetCurrentWeatherResponse, TemperatureFrgContract.View>(mView) {
                    override fun onRequestSuccess(response: GetCurrentWeatherResponse) {
                        mView?.currentWeatherDidReceived(response)
                    }

                })
        )

    }

}