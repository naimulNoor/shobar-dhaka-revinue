package com.shobardhaka.citizenrevenue.ui.profile

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.RetrofitHelperService
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.PostDataResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.HashMap
import java.util.LinkedHashMap
import javax.inject.Inject

/**
 * Created on 19/08/2019.
 */
class ProfilePresenter @Inject constructor(view: ProfileContract.View): BasePresenter<ProfileContract.View>(view), ProfileContract.Presenter{

    @Inject
    lateinit var userApiService: UserApiService

    @Inject
    lateinit var retrofitHelperService: RetrofitHelperService

    override fun updateProfile(dataMap: HashMap<String, RequestBody>, attachment: MultipartBody.Part?) {
        mView?.onNetworkCallStarted(context.getString(R.string.updating_profile))
        compositeDisposable?.add(
            userApiService.updateProfile(dataMap, attachment)
                .subscribeOn(appSchedulerProvider.io())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<PostDataResponse, ProfileContract.View>(mView) {

                    override fun onRequestSuccess(response: PostDataResponse) {
                        retrofitHelperService.loadProfile(compositeDisposable!!, object : RetrofitHelperService.OnProfileLoadListener{
                            override fun onProfileLoaded() {
                                mView?.profileUpdateDidSucceed()
                            }
                        })
                    }

                    override fun onFormDataNotValid(errorMap: LinkedHashMap<String, String>) {
                        for ((_, value) in errorMap) {
                            mView?.profileUpdateDidFailed(value)
                            break
                        }
                    }
                })
        )
    }
}