package com.shobardhaka.citizenrevenue.ui.fragments.shotortobarta
import dagger.Binds
import dagger.Module

@Module
abstract class ShotorkobartaFrgViewModule {

    @Binds
    abstract fun provideShotorkobartaFrgViewModule(fragment: ShotorkobartaFragment): ShotorkobartaFrgContract.View
}