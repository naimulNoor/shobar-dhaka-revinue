package com.shobardhaka.citizenrevenue.ui.department_heads

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.adapters.DepartmentHeadsAdapter
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.callbacks.CallButtonClickListener
import com.shobardhaka.citizenrevenue.data.network.api_response.GetDepartmentalHeadsResponse
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.PermissionUtils
import com.shobardhaka.citizenrevenue.utils.showToast
import kotlinx.android.synthetic.main.activity_department_heads.*
import kotlinx.android.synthetic.main.toolbar_with_text.*
import javax.inject.Inject

class DepartmentHeadsActivity : MvpBaseActivity<DepartmentHeadsPresenter>(), DepartmentHeadsContract.View {

    private lateinit var departmentHeadsAdapter: DepartmentHeadsAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var animationController: LayoutAnimationController

    internal val REQUEST_CALL_PHONE_PERMISSION_CODE = 15
    internal var CALL_PHONE_PERMISSION = arrayOf(Manifest.permission.CALL_PHONE)

    @Inject
    lateinit var permissionUtils: PermissionUtils

    var currentSelectedPhone = ""

    override fun getContentView(): Int {
        return R.layout.activity_department_heads
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }
        tvTitle.text = getString(R.string.departmental_heads)

        animationController = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_slide_from_right);
        linearLayoutManager = LinearLayoutManager(
            getContext(),
            RecyclerView.VERTICAL,
            false
        )
        departmentHeadsAdapter = DepartmentHeadsAdapter(getContext(), object : CallButtonClickListener {
            override fun onCallButtonClicked(number: String) {
                AppUtils.shared.dial(getContext(), number)

//                if (!permissionUtils.shouldAskPermission(getContext(), Manifest.permission.CALL_PHONE)) {
//                    AppUtils.shared.dial(getContext(), number)
//                } else {
//                    currentSelectedPhone = number
//                    mAlertService.showConfirmationAlert(this@DepartmentHeadsActivity,
//                        getString(R.string.permission_required),
//                        getString(R.string.permission_call_phone_one),
//                        getString(R.string.button_not_now),
//                        getString(R.string.okay),
//                        object : AlertService.AlertListener {
//
//                            override fun negativeBtnDidTapped() {}
//
//                            override fun positiveBtnDidTapped() {
//                                ActivityCompat.requestPermissions(
//                                    this@DepartmentHeadsActivity,
//                                    CALL_PHONE_PERMISSION,
//                                    REQUEST_CALL_PHONE_PERMISSION_CODE
//                                )
//
//                            }
//                        })
//                }
            }
        })

        recyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = departmentHeadsAdapter
            layoutAnimation = animationController
        }

        mPresenter.getDepartmentalHeads()
    }

    override fun onNetworkCallStarted(loadingMessage: String) {
        progressBar.visibility = View.VISIBLE
    }

    override fun onNetworkCallEnded() {
        progressBar.visibility = View.GONE
    }

    override fun onFailedToFetchData(failedMsg: String) {
        showToast(failedMsg)
    }

    override fun departmentalHeadsDidReceived(councilors: ArrayList<GetDepartmentalHeadsResponse.SingleHeads?>) {
        if (councilors.isNotEmpty()) {
            departmentHeadsAdapter.addAll(councilors)
            recyclerView.scheduleLayoutAnimation()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CALL_PHONE_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                AppUtils.shared.dial(getContext(), currentSelectedPhone)
            }
        }
    }
}
