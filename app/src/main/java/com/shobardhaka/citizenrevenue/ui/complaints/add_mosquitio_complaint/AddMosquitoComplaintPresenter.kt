package com.shobardhaka.citizenrevenue.ui.complaints.add_mosquitio_complaint

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.*
import com.shobardhaka.citizenrevenue.data.network.api_service.BariKoiApiService
import com.shobardhaka.citizenrevenue.data.network.api_service.ComplainApiService
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.*
import javax.inject.Inject

class AddMosquitoComplaintPresenter @Inject constructor(view: AddMosquitoComplainContract.View) :
    BasePresenter<AddMosquitoComplainContract.View>(view), AddMosquitoComplainContract.Presenter {


    @Inject
    lateinit var complainApiService: ComplainApiService

    @Inject
    lateinit var userApiService: UserApiService

    @Inject
    lateinit var barikoiApiService: BariKoiApiService

    override fun addComplain(dataMap: HashMap<String, RequestBody>, attachment: MultipartBody.Part?) {

        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            complainApiService.addComplain(dataMap, attachment)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<PostDataResponse, AddMosquitoComplainContract.View>(mView) {

                    override fun onRequestSuccess(response: PostDataResponse) {
                        if (response.status == 200){
                            mView?.addComplainDidSucceed()
                        }else{
                            mView?.addComplainDidFailed(response.error!!)
                        }
                    }

                    override fun onFormDataNotValid(errorMap: LinkedHashMap<String, String>) {
                        for ((_, value) in errorMap) {
                            mView?.addComplainDidFailed(value)
                            break
                        }
                    }
                })
        )
    }

    override fun checkGeoFencing(url: String) {
        compositeDisposable?.add(
            userApiService.checkGeoFencing(url)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<CheckGeoFencingResponse, AddMosquitoComplainContract.View>(mView) {
                    override fun onRequestSuccess(response: CheckGeoFencingResponse) {
                        mView?.geoFencingDidChecked(response.message != "not found in DNCC")
                    }
                })
        )
    }

    override fun getAddressFromBarikoiByLatLong(url: String) {
        compositeDisposable?.add(
            barikoiApiService.getAddressByLatLong(url)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<BarikoiAddressFromLatLongResponse, AddMosquitoComplainContract.View>(mView) {

                    override fun onRequestSuccess(response: BarikoiAddressFromLatLongResponse) {
                        if(response.status == 200){
                            val address = response.place!!.address
                            val area = response.place.area
                            val city = response.place.city
                            val fullAddress = "$address, $area"
                            mView?.addressDidReceived(fullAddress)
                        }
                    }

                })
        )
    }

    override fun getSubIssueType(categoryId: String) {
        compositeDisposable?.add(
            complainApiService.getSubIssueType(categoryId)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<IssueTypeResponse, AddMosquitoComplainContract.View>(mView) {
                    override fun onRequestSuccess(response: IssueTypeResponse) {
                        mView?.issueSubTypeDidReceived(response)
                    }
                })
        )
    }


    override fun getDnccAreas() {
        compositeDisposable?.add(
            complainApiService.getDnccAreas()
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<DnccAddressResponse, AddMosquitoComplainContract.View>(mView) {
                    override fun onRequestSuccess(response: DnccAddressResponse) {
                        mView?.dnccAreasDidReceived(response)
                    }
                })
        )
    }
}