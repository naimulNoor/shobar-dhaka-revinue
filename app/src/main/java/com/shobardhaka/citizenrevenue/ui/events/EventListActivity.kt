package com.shobardhaka.citizenrevenue.ui.events

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentManager
import butterknife.OnClick
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.adapters.AjkerDhakaPagerAdapter
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.data.network.api_response.GetBannerResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.GetCurrentWeatherResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.GetEventResponse
import com.shobardhaka.citizenrevenue.file.PicassoImageLoader
import com.shobardhaka.citizenrevenue.ui.fragments.dncc_fb_page.DnccFbFragment
import com.shobardhaka.citizenrevenue.ui.fragments.events.EventsFragment
import com.shobardhaka.citizenrevenue.ui.fragments.temperature.TemperatureFragment
import com.shobardhaka.citizenrevenue.utils.AppUtils
import kotlinx.android.synthetic.main.activity_event_list.*
import kotlinx.android.synthetic.main.toolbar_with_text.*


class EventListActivity : MvpBaseActivity<EventPresenter>(), EventContract.View {

    private var banner: GetBannerResponse.Banner? = null
    private lateinit var ajkerDhakaPagerAdapter: AjkerDhakaPagerAdapter
    private lateinit var fragmentManager: FragmentManager

    override fun getContentView(): Int {
        return R.layout.activity_event_list
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }

        tvTitle.text = getString(R.string.dhaka_today)

        fragmentManager = supportFragmentManager
        ajkerDhakaPagerAdapter = AjkerDhakaPagerAdapter(fragmentManager)
        ajkerDhakaPagerAdapter.addFragment(EventsFragment.newInstance())
        ajkerDhakaPagerAdapter.addFragment(TemperatureFragment.newInstance())
        ajkerDhakaPagerAdapter.addFragment(DnccFbFragment.newInstance())
        tabDots.setupWithViewPager(view_pager, true)
        view_pager.apply {
            adapter = ajkerDhakaPagerAdapter
        }
        mPresenter.getBanner()
    }

    override fun onNetworkCallStarted(loadingMessage: String) {
        //  progressBar.visibility = View.VISIBLE
    }

    override fun eventsDidReceived(events: ArrayList<GetEventResponse.SingleEvent?>) {
//        if (events.isNotEmpty()) {
//            eventsAdapter.addAll(events)
//            recyclerView.scheduleLayoutAnimation()
//        } else {
//            showToast(getString(R.string.no_event_found))
//        }
    }

    override fun bannderDiDReceived(banner: GetBannerResponse.Banner?) {
        this.banner = banner
        banner?.let {
            PicassoImageLoader.shared
                .loadImage(
                    getContext(),
                    banner.bannerImage,
                    imgBanner
                )
            imgBanner.visibility = View.VISIBLE
            sectionBanner.visibility = View.VISIBLE
        }
    }

    @OnClick(R.id.imgBanner)
    fun onClicked() {
        banner?.let {
            if (mNetworkUtils.isConnectedToNetwork(getContext())) {
                AppUtils.shared.openLinkOnExternalBrowser(getContext(), banner!!.link)
            }
        }
    }

    override fun currentWeatherDidReceived(currentWeatherResponse: GetCurrentWeatherResponse) {
        //val currentTempInCelsius = "${currentWeatherResponse.main.temp}$celsiusSign"
        // tvTemperature.text = currentTempInCelsius
    }

}

