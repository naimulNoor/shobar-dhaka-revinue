package com.shobardhaka.citizenrevenue.ui.fragments.home

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.*
import com.shobardhaka.citizenrevenue.data.network.api_service.BariKoiApiService
import com.shobardhaka.citizenrevenue.data.network.api_service.EmergencyApiService
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import javax.inject.Inject

class HomeFragmentPresenter @Inject constructor(view: HomeFragmentContract.View) : BasePresenter<HomeFragmentContract.View>(view),
    HomeFragmentContract.Presenter {

    @Inject
    lateinit var emergencyApiService: EmergencyApiService

    @Inject
    lateinit var userApiService: UserApiService

    @Inject
    lateinit var barikoiApiService: BariKoiApiService


    override fun sendEmergencyAlert(dataMap: HashMap<String, String>) {
        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            emergencyApiService.sendEmergencyAlert(dataMap)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<PostDataResponse, HomeFragmentContract.View>(mView) {

                    override fun onRequestSuccess(response: PostDataResponse) {
                        mView?.sendEmergencyAlertDidSucceed()
                    }

                    override fun onFormDataNotValid(errorMap: LinkedHashMap<String, String>) {
                        for ((_, value) in errorMap) {
                            mView?.sendEmergencyAlertDidFailed(value)
                            break
                        }
                    }
                })
        )
    }

    override fun getComplainStatus() {
        compositeDisposable?.add(
            userApiService.getComplainStatus()
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<ComplainStatusResponse, HomeFragmentContract.View>(mView) {
                    override fun onRequestSuccess(response: ComplainStatusResponse) {
                        mView?.complainStatusDidReceived(response)
                    }
                })
        )
    }

    override fun getNotice() {
        compositeDisposable?.add(
            userApiService.getNotice()
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<GetNoticeResponse, HomeFragmentContract.View>(mView) {
                    override fun onRequestSuccess(response: GetNoticeResponse) {
                        mView?.noticeDidReceived(response.notices)
                    }
                })
        )
    }

    override fun checkGeoFencing(url: String) {
        compositeDisposable?.add(
            userApiService.checkGeoFencing(url)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<CheckGeoFencingResponse, HomeFragmentContract.View>(mView) {
                    override fun onRequestSuccess(response: CheckGeoFencingResponse) {
                        mView?.geoFencingDidChecked(response.message != "not found in DNCC")
                    }
                })
        )
    }

    override fun getUnreadNotificationCount() {
        compositeDisposable?.add(
            userApiService.getUnReadNotificationCount()
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<GetUnReadNotificaitonResponse, HomeFragmentContract.View>(mView) {
                    override fun onRequestSuccess(response: GetUnReadNotificaitonResponse) {
                        mView?.unReadNotificationDidReceived(response)
                    }
                })
        )
    }

    override fun getAddressFromBarikoiByLatLong(url: String) {
        compositeDisposable?.add(
            barikoiApiService.getAddressByLatLong(url)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<BarikoiAddressFromLatLongResponse, HomeFragmentContract.View>(mView) {

                    override fun onRequestSuccess(response: BarikoiAddressFromLatLongResponse) {
                        if(response.status == 200){
                            val address = response.place!!.address
                            val area = response.place.area
                            val city = response.place.city
                            val fullAddress = "$address, $area"
                            mView?.addressDidReceived(fullAddress)
                        }
                    }

                })
        )
    }
}