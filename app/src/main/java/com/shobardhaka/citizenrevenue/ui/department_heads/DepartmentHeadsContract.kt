package com.shobardhaka.citizenrevenue.ui.department_heads

import com.shobardhaka.citizenrevenue.base.BaseContract
import com.shobardhaka.citizenrevenue.data.network.api_response.GetDepartmentalHeadsResponse

interface DepartmentHeadsContract{
    interface View : BaseContract.View {
        fun departmentalHeadsDidReceived(councilors: ArrayList<GetDepartmentalHeadsResponse.SingleHeads?>)
        fun onFailedToFetchData(failedMsg: String)
    }

    interface Presenter : BaseContract.Presenter {
        fun getDepartmentalHeads()
    }
}