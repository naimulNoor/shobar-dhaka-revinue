package com.shobardhaka.citizenrevenue.ui.mayor_ceo

import com.shobardhaka.citizenrevenue.base.BaseContract
import com.shobardhaka.citizenrevenue.data.network.api_response.MayorCeoResponse

interface ProfileDetailsContract {

    interface View : BaseContract.View {
        fun profileDetailsDidReceived(response: MayorCeoResponse)
    }

    interface Presenter : BaseContract.Presenter {
        fun getProfileDetails()
    }
}