package com.shobardhaka.citizenrevenue.ui.user

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.OnClick
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.ui.home.HomeActivity
import com.shobardhaka.citizenrevenue.utils.Navigator
import kotlinx.android.synthetic.main.activity_select_gender.*

class SelectGenderActivity : MvpBaseActivity<SelectGenderPresenter>(), SelectGenderContract.View {

    var selectedGender = ""

    override fun getContentView(): Int {
        return R.layout.activity_select_gender
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
    }

    @OnClick(R.id.relMale, R.id.relFemale)
    fun onGenderSelected(v: View) {
        btnNext.isEnabled = true

        when (v.id) {
            R.id.relMale -> {
                selectedGender = "0"
                relMale.isSelected = true
                relFemale.isSelected = false
            }

            R.id.relFemale -> {
                selectedGender = "1"
                relFemale.isSelected = true
                relMale.isSelected = false
            }
        }
    }


    @SuppressLint("NonConstantResourceId")
    @OnClick(R.id.btnNext)
    fun nextBtnDidTapped(){
        mPresenter.updateGender(selectedGender)
    }

    override fun onNetworkCallEnded() {
        // Nothing to do >>>
    }

    override fun genderUpdateDidSucceed() {
        super.onNetworkCallEnded()
        Navigator.sharedInstance.navigate(getContext(), HomeActivity::class.java)
        finish()
    }

}