package com.shobardhaka.citizenrevenue.ui.fragments.home

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import androidx.appcompat.app.AlertDialog
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.SSHorizontalScrollView
import com.shobardhaka.citizenrevenue.ScrollViewListener
import com.shobardhaka.citizenrevenue.base.BaseFragment
import com.shobardhaka.citizenrevenue.callbacks.OnComplainStatusClickListener
import com.shobardhaka.citizenrevenue.data.network.RetrofitHelperService
import com.shobardhaka.citizenrevenue.data.network.api_response.ComplainStatusResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.GetNoticeResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.GetUnReadNotificaitonResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.file.PicassoImageLoader
import com.shobardhaka.citizenrevenue.location.SSFusedLocationProviderClient
import com.shobardhaka.citizenrevenue.ui.AccountActivity
import com.shobardhaka.citizenrevenue.ui.complaints.add_complaint.AddComplaintsActivity
import com.shobardhaka.citizenrevenue.ui.emergency.EmergencyServiceActivity
import com.shobardhaka.citizenrevenue.ui.events.EventListActivity
import com.shobardhaka.citizenrevenue.ui.nearby.nearby_place.NearbyPlaceActivity
import com.shobardhaka.citizenrevenue.ui.notification.NotificationActivity
import com.shobardhaka.citizenrevenue.utils.*
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.section_complain.view.*
import kotlinx.android.synthetic.main.section_complain_status.view.*
import kotlinx.android.synthetic.main.section_nearby_places.view.*
import kotlinx.android.synthetic.main.section_necessary_app.view.*
import java.util.*
import javax.inject.Inject

class HomeFragment : BaseFragment<HomeFragmentPresenter>(), HomeFragmentContract.View {

    companion object {
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    private val APP_MUJIB_100 = "bd.gov.mujib100app"
    private val APP_MUKTIJUDDHO = "com.mcc.muktijuddho"
    private val APP_MUKTO_PATH = "orangebd.newaspaper.mymuktopathapp"
    private val APP_SUROKKHA = "com.codersbucket.surokkha_app"

    @Inject
    lateinit var retrofitHelperService: RetrofitHelperService

    @Inject
    lateinit var permissionUtils: PermissionUtils

    private var smsUtils: SmsUtils? = null

    val compositeDisposable = CompositeDisposable()

    private val CALL_PHONE_PERMISSION_CODE = 154
    private val SEND_SMS_PERMISSION_CODE = 153
    private val ACCESS_LOCATIN_PERMISSION_CODE = 152
    private var unSeenNotificationCount = 0
    private var unSeenDisasterCount = 0

    private var complainStatusClickListener: OnComplainStatusClickListener? = null
    private var gpsAlertDialog: AlertDialog? = null
    private var isGpsStateReceiverRegistered = false


    private var ssFusedLocationApiClient: SSFusedLocationProviderClient? = null


    override fun getFragmentView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        saveInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            complainStatusClickListener = context as OnComplainStatusClickListener
        } catch (e: Exception) {
        }
    }

    override fun onViewReady(getArguments: Bundle?) {

        smsUtils = SmsUtils(context)

        if (!isPlayServiceAvailable()) {
            mAlertService.showAlert(
                context,
                null,
                getString(R.string.google_play_service_msg)
            )
        }

        if (mAppLanguage == AppLanguage.BENGALI.name) {
            Glide.with(context).load(R.drawable.ajker_dhaka_bn).into(rootView.imgAjkerDhaka)
           // Glide.with(context).load(R.drawable.app_bongobondhu_bn).into(rootView.appBongobondhu)
            Glide.with(context).load(R.drawable.app_muktijuddho).into(rootView.appMuktijuddho)
        } else {
            Glide.with(context).load(R.drawable.ajker_dhaka_eng).into(rootView.imgAjkerDhaka)
          //  Glide.with(context).load(R.drawable.app_bongobondhu_eng).into(rootView.appBongobondhu)
            Glide.with(context).load(R.drawable.app_muktijuddho).into(rootView.appMuktijuddho)
        }

        rootView.swipe_refreshLayout.setColorSchemeResources(R.color.colorPrimary)
        rootView.swipe_refreshLayout.setOnRefreshListener {
            refreshContent()
        }
        mPresenter.getNotice()
        mPresenter.getComplainStatus()

        val hv: SSHorizontalScrollView = rootView.nearbyScrollView
        rootView.dotOne.isSelected = true
        hv.setScrollViewListener(object : ScrollViewListener {
            override fun onScrollChanged(
                scrollView: SSHorizontalScrollView,
                x: Int,
                y: Int,
                oldx: Int,
                oldy: Int
            ) {
                if (x < 360) {
                    rootView.dotOne.isSelected = true
                    rootView.dotTwo.isSelected = false
                } else {
                    rootView.dotOne.isSelected = false
                    rootView.dotTwo.isSelected = true
                }
            }
        })

    }

    private fun refreshContent() {
        rootView.swipe_refreshLayout.isRefreshing = true
        mPresenter.getComplainStatus()
    }

    private fun showUnseenNotificationCounts() {

        val unSeenNotCount = mPrefManager.getUnSeenNotificationsCount()
        if (unSeenNotCount > 0) {
            val banglaCount = AppUtils.shared.getInBangla("$unSeenNotCount")
            rootView.tvNotificaitoncount.text = banglaCount
            rootView.tvNotificaitoncount.visibility = View.VISIBLE
        } else {
            rootView.tvNotificaitoncount.visibility = View.GONE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionNotification -> {
                Navigator.sharedInstance.navigate(context, NotificationActivity::class.java)
            }
        }
        return false
    }


    @OnClick(R.id.imgNotification, R.id.toolbar_profileImg)
    fun notificationBtnClicked(view: View) {

        when (view.id) {
            R.id.imgNotification -> {
                val bundle = Bundle()
                bundle.putInt(Keys.UNSEEN_NOT_COUNT.name, unSeenNotificationCount)
                bundle.putInt(Keys.UNSEEN_DISASTER_COUNT.name, unSeenDisasterCount)
                Navigator.sharedInstance.navigateWithBundle(
                    context,
                    NotificationActivity::class.java,
                    Keys.DATA_BUNDLE.name,
                    bundle
                )
            }
            R.id.toolbar_profileImg -> Navigator.sharedInstance.navigate(
                context,
                AccountActivity::class.java
            )
        }
    }


    override fun onStart() {
        super.onStart()
        // Show Saved Complain Status Before getting data from api....
        var total = mPrefManager.getString("total", "")
        var solved = mPrefManager.getString("solved", "")
        var unsolved = mPrefManager.getString("unsolved", "")


        if (total!!.isNotEmpty() && solved!!.isNotEmpty() && unsolved!!.isNotEmpty()) {
            // Total
            if (mAppLanguage == AppLanguage.BENGALI.name)
                total = AppUtils.shared.getInBangla(total)

            // Solved
            if (mAppLanguage == AppLanguage.BENGALI.name)
                solved = AppUtils.shared.getInBangla(solved)

            // UnSolved
            if (mAppLanguage == AppLanguage.BENGALI.name)
                unsolved = AppUtils.shared.getInBangla(unsolved)

            rootView.tvTotalCounter.text = total
            rootView.tvSolvedCounter.text = solved
            rootView.tvUnsolvedCounter.text = unsolved
        }

        registerGpsStateReceiver()

        if (!permissionUtils.shouldAskPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            checkGpsAndStartLocationService()
        } else {
            mAlertService.showConfirmationAlert(
                context,
                getString(R.string.permission_required),
                getString(R.string.permission_msg_for_location),
                null,
                getString(R.string.buttoin_continue),
                object : AlertService.AlertListener {
                    override fun negativeBtnDidTapped() {

                    }

                    override fun positiveBtnDidTapped() {
                        requestPermissions(
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            ACCESS_LOCATIN_PERMISSION_CODE
                        )
                    }

                }
            )
        }

        mPresenter.getComplainStatus()
        mPresenter.getUnreadNotificationCount()
        smsUtils?.registerSentDeliveredReceiver()
    }

    private fun checkGpsAndStartLocationService() {
        if (isLocationProviderEnabled()) {
            startLocationService()
        } else {
            val gpsAlertDialogBuilder = AlertDialog.Builder(context)
            gpsAlertDialogBuilder.setCancelable(true)
            gpsAlertDialogBuilder.setMessage(R.string.enable_location_msg)
            gpsAlertDialogBuilder.setPositiveButton(getString(R.string.settings)) { dialog, _ ->
                dialog.dismiss()
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }
            gpsAlertDialog = gpsAlertDialogBuilder.create()
            gpsAlertDialog?.show()
        }
    }


    private fun registerGpsStateReceiver() {
        isGpsStateReceiverRegistered = true
        activity?.registerReceiver(
            gpsSwitchStateReceiver,
            IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION)
        )
    }

    override fun onStop() {
        super.onStop()
        smsUtils?.unRegisterSentDeliveredReceiver()
        if (isGpsStateReceiverRegistered) {
            activity?.unregisterReceiver(gpsSwitchStateReceiver)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            ACCESS_LOCATIN_PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkGpsAndStartLocationService()
                } else {
                    mAlertService.showConfirmationAlert(
                        context,
                        getString(R.string.permission_required),
                        getString(R.string.permission_msg_for_location),
                        null,
                        getString(R.string.buttoin_continue),
                        object : AlertService.AlertListener {
                            override fun negativeBtnDidTapped() {

                            }

                            override fun positiveBtnDidTapped() {
                                requestPermissions(
                                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                    ACCESS_LOCATIN_PERMISSION_CODE
                                )
                            }

                        }
                    )
                }
            }

            CALL_PHONE_PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    AppUtils.shared.dial(context, "999")
                }
            }

        }
    }

    override fun onResume() {
        super.onResume()
        val profilePic = mPrefManager.getString(Keys.ProfileKey.PROFILE_PIC.name)
        if (profilePic.isNotEmpty()) {
            PicassoImageLoader.shared.loadImage(
                context,
                profilePic,
                rootView.toolbar_profileImg,
                R.drawable.profile
            )
        }
        showUnseenNotificationCounts()
    }

    // Section Nearby...
    @OnClick(
        R.id.cardNearByBusStand,
        R.id.cardNearByHospital,
        R.id.cardNearByMaternityClinic,
        R.id.cardNearByPoliceStation,
        R.id.cardNearbyFireService,
        R.id.cardNearByPublicToilet,
        R.id.cardNearByAtms,
        R.id.cardNearByParking,
        R.id.cardNearbyFuelStation,
        R.id.cardNearByFarmacy,
        R.id.cardNearbyPark,
        R.id.cardNearbyCommunityCenter,
        R.id.cardNearbySts

    )
    fun onNearbyLocationClicked(view: View) {
        val mLat = SSFusedLocationProviderClient.mLatitude
        val mLong = SSFusedLocationProviderClient.mLongitude
        if (mLat != 0.0 && mLong != 0.0) {
            val bundle = Bundle()
            when (view.id) {
                //Section 03
                R.id.cardNearByBusStand -> {
                    bundle.putDouble(Keys.CURRENT_LOC_LAT.name, mLat)
                    bundle.putDouble(Keys.CURRENT_LOC_LONG.name, mLong)
                    bundle.putString(Keys.LOCATION_TYPE.name, Keys.NEARBY_BUS_STAND.name)
                    bundle.putInt(Keys.NEARBY_ICON.name, R.drawable.ic_bus_stand)
                    Navigator.sharedInstance.navigateWithBundle(
                        context,
                        NearbyPlaceActivity::class.java,
                        Keys.DATA_BUNDLE.name,
                        bundle
                    )
                }
                R.id.cardNearByHospital -> {
                    bundle.putDouble(Keys.CURRENT_LOC_LAT.name, mLat)
                    bundle.putDouble(Keys.CURRENT_LOC_LONG.name, mLong)
                    bundle.putString(Keys.LOCATION_TYPE.name, Keys.NEARBY_HOSPITAL.name)
                    bundle.putInt(Keys.NEARBY_ICON.name, R.drawable.ic_hospital)
                    Navigator.sharedInstance.navigateWithBundle(
                        context,
                        NearbyPlaceActivity::class.java,
                        Keys.DATA_BUNDLE.name,
                        bundle
                    )
                }
                R.id.cardNearByMaternityClinic -> {
                    bundle.putDouble(Keys.CURRENT_LOC_LAT.name, mLat)
                    bundle.putDouble(Keys.CURRENT_LOC_LONG.name, mLong)
                    bundle.putString(Keys.LOCATION_TYPE.name, Keys.NEARBY_MATERNITY_CLINIC.name)
                    bundle.putInt(Keys.NEARBY_ICON.name, R.drawable.ic_nearby_maternity_clinic)
                    Navigator.sharedInstance.navigateWithBundle(
                        context,
                        NearbyPlaceActivity::class.java,
                        Keys.DATA_BUNDLE.name,
                        bundle
                    )
                }
                R.id.cardNearByPoliceStation -> {
                    bundle.putDouble(Keys.CURRENT_LOC_LAT.name, mLat)
                    bundle.putDouble(Keys.CURRENT_LOC_LONG.name, mLong)
                    bundle.putString(Keys.LOCATION_TYPE.name, Keys.NEARBY_POLICE_STATION.name)
                    bundle.putInt(Keys.NEARBY_ICON.name, R.drawable.ic_thana)
                    Navigator.sharedInstance.navigateWithBundle(
                        context,
                        NearbyPlaceActivity::class.java,
                        Keys.DATA_BUNDLE.name,
                        bundle
                    )
                }
                R.id.cardNearbyFireService -> {
                    bundle.putDouble(Keys.CURRENT_LOC_LAT.name, mLat)
                    bundle.putDouble(Keys.CURRENT_LOC_LONG.name, mLong)
                    bundle.putString(Keys.LOCATION_TYPE.name, Keys.NEARBY_FIRE_SERVICE.name)
                    bundle.putInt(Keys.NEARBY_ICON.name, R.drawable.ic_fire_service)
                    Navigator.sharedInstance.navigateWithBundle(
                        context,
                        NearbyPlaceActivity::class.java,
                        Keys.DATA_BUNDLE.name,
                        bundle
                    )
                }
                R.id.cardNearByPublicToilet -> {
                    bundle.putDouble(Keys.CURRENT_LOC_LAT.name, mLat)
                    bundle.putDouble(Keys.CURRENT_LOC_LONG.name, mLong)
                    bundle.putString(Keys.LOCATION_TYPE.name, Keys.NEARBY_PUBLIC_TOILET.name)
                    bundle.putInt(Keys.NEARBY_ICON.name, R.drawable.ic_public_toilet)
                    Navigator.sharedInstance.navigateWithBundle(
                        context,
                        NearbyPlaceActivity::class.java,
                        Keys.DATA_BUNDLE.name,
                        bundle
                    )
                }
                R.id.cardNearByAtms -> {
                    bundle.putDouble(Keys.CURRENT_LOC_LAT.name, mLat)
                    bundle.putDouble(Keys.CURRENT_LOC_LONG.name, mLong)
                    bundle.putString(Keys.LOCATION_TYPE.name, Keys.NEARBY_ATM.name)
                    bundle.putInt(Keys.NEARBY_ICON.name, R.drawable.ic_nearby_atm)
                    Navigator.sharedInstance.navigateWithBundle(
                        context,
                        NearbyPlaceActivity::class.java,
                        Keys.DATA_BUNDLE.name,
                        bundle
                    )
                }
                R.id.cardNearByParking -> {
                    bundle.putDouble(Keys.CURRENT_LOC_LAT.name, mLat)
                    bundle.putDouble(Keys.CURRENT_LOC_LONG.name, mLong)
                    bundle.putString(Keys.LOCATION_TYPE.name, Keys.NEARBY_PARKING.name)
                    bundle.putInt(Keys.NEARBY_ICON.name, R.drawable.ic_nearby_parking)
                    Navigator.sharedInstance.navigateWithBundle(
                        context,
                        NearbyPlaceActivity::class.java,
                        Keys.DATA_BUNDLE.name,
                        bundle
                    )
                }
                R.id.cardNearByFarmacy -> {
                    bundle.putDouble(Keys.CURRENT_LOC_LAT.name, mLat)
                    bundle.putDouble(Keys.CURRENT_LOC_LONG.name, mLong)
                    bundle.putString(Keys.LOCATION_TYPE.name, Keys.NEARBY_PHARMACY.name)
                    bundle.putInt(Keys.NEARBY_ICON.name, R.drawable.ic_nearby_farmacy)
                    Navigator.sharedInstance.navigateWithBundle(
                        context,
                        NearbyPlaceActivity::class.java,
                        Keys.DATA_BUNDLE.name,
                        bundle
                    )
                }

                R.id.cardNearbyPark -> {
                    bundle.putDouble(Keys.CURRENT_LOC_LAT.name, mLat)
                    bundle.putDouble(Keys.CURRENT_LOC_LONG.name, mLong)
                    bundle.putString(Keys.LOCATION_TYPE.name, Keys.NEARBY_PARK.name)
                    bundle.putInt(Keys.NEARBY_ICON.name, R.drawable.img_nearby_park)
                    Navigator.sharedInstance.navigateWithBundle(
                        context,
                        NearbyPlaceActivity::class.java,
                        Keys.DATA_BUNDLE.name,
                        bundle
                    )
                }
                R.id.cardNearbyCommunityCenter -> {
                    bundle.putDouble(Keys.CURRENT_LOC_LAT.name, mLat)
                    bundle.putDouble(Keys.CURRENT_LOC_LONG.name, mLong)
                    bundle.putString(Keys.LOCATION_TYPE.name, Keys.NEARBY_COMMUNITY_CENTER.name)
                    bundle.putInt(Keys.NEARBY_ICON.name, R.drawable.ic_nearby_community_center)
                    Navigator.sharedInstance.navigateWithBundle(
                        context,
                        NearbyPlaceActivity::class.java,
                        Keys.DATA_BUNDLE.name,
                        bundle
                    )
                }
                R.id.cardNearbyFuelStation -> {
                    bundle.putDouble(Keys.CURRENT_LOC_LAT.name, mLat)
                    bundle.putDouble(Keys.CURRENT_LOC_LONG.name, mLong)
                    bundle.putString(Keys.LOCATION_TYPE.name, Keys.NEARBY_CNG_PUMP.name)
                    bundle.putInt(Keys.NEARBY_ICON.name, R.drawable.ic_nearby_fuel)
                    Navigator.sharedInstance.navigateWithBundle(
                        context,
                        NearbyPlaceActivity::class.java,
                        Keys.DATA_BUNDLE.name,
                        bundle
                    )
                }

                R.id.cardNearbySts -> {
                    bundle.putDouble(Keys.CURRENT_LOC_LAT.name, mLat)
                    bundle.putDouble(Keys.CURRENT_LOC_LONG.name, mLong)
                    bundle.putString(Keys.LOCATION_TYPE.name, Keys.NEARBY_STS.name)
                    Navigator.sharedInstance.navigateWithBundle(
                        context,
                        NearbyPlaceActivity::class.java,
                        Keys.DATA_BUNDLE.name,
                        bundle
                    )
                }
            }
        } else {
            mAlertService.showAlert(
                context,
                null,
                getString(R.string.no_current_loc_found_msg)
            )
        }
    }

    // Section Add Complain...
    @OnClick(

        //revinue
        R.id.card_holding,
        R.id.card_signboard,
        R.id.card_trade

        )
    fun onAddComplainBtnClicked(view: View) {
        val bundle = Bundle()
        if (!permissionUtils.shouldAskPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            when (view.id) {


                R.id.card_holding -> {
                    bundle.putString(Keys.COMPLAINT_TYPE.name, getString(R.string.holding))
                    bundle.putInt(Keys.COMPLAINT_ID.name, 9)
                    bundle.putInt(Keys.COMPLAINT_ICON.name, R.drawable.ic_holding_tax)
                    bundle.putString(Keys.COMPLAINT_MSG.name, getString(R.string.msg_holding))
                    Navigator.sharedInstance.navigateWithBundle(
                        context,
                        AddComplaintsActivity::class.java,
                        Keys.DATA_BUNDLE.name,
                        bundle
                    )
                }
                R.id.card_signboard -> {
                    bundle.putString(Keys.COMPLAINT_TYPE.name, getString(R.string.signboard))
                    bundle.putInt(Keys.COMPLAINT_ID.name, 42)
                    bundle.putInt(Keys.COMPLAINT_ICON.name, R.drawable.ic_signboard_tax)
                    bundle.putString(
                        Keys.COMPLAINT_MSG.name,
                        getString(R.string.msg_signboard)
                    )
                    Navigator.sharedInstance.navigateWithBundle(
                        context,
                        AddComplaintsActivity::class.java,
                        Keys.DATA_BUNDLE.name,
                        bundle
                    )
                }
                R.id.card_trade -> {
                    bundle.putString(Keys.COMPLAINT_TYPE.name, getString(R.string.trade))
                    bundle.putInt(Keys.COMPLAINT_ID.name, 41)
                    bundle.putInt(Keys.COMPLAINT_ICON.name, R.drawable.ic_trade_license)
                    bundle.putString(Keys.COMPLAINT_MSG.name, getString(R.string.msg_trade))
                    Navigator.sharedInstance.navigateWithBundle(
                        context,
                        AddComplaintsActivity::class.java,
                        Keys.DATA_BUNDLE.name,
                        bundle
                    )
                }

            }
        } else {
            mAlertService.showConfirmationAlert(context,
                getString(R.string.permission_required),
                getString(R.string.permission_msg_for_location),
                getString(R.string.button_not_now),
                getString(R.string.buttoin_continue),
                object : AlertService.AlertListener {

                    override fun negativeBtnDidTapped() {}

                    override fun positiveBtnDidTapped() {
                        requestPermissions(
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            ACCESS_LOCATIN_PERMISSION_CODE
                        )
                    }
                })

        }
    }


    @OnClick(
        // Section Complain Status
        R.id.cardTotalComplain,
        R.id.cardSolvedComplain,
        R.id.cardUnSolvedComplain,

        // Section Emergency service
        R.id.cardEmergencyService,
        R.id.cardEmergencyAlert,
        R.id.cardEmergencyCall,


        // Section Card Dhaka today
        R.id.cardDhakaToday,

        // Section Necessary App
        R.id.cardAppMujib100,
        R.id.cardAppMuktiJuddho,
        R.id.card_app_mukto_path,
        R.id.cardAppSurokkha
    )
    fun onCardClicked(view: View) {
        val bundle = Bundle()
        when (view.id) {
            // Section Complain Status,

            R.id.cardTotalComplain -> {
                complainStatusClickListener?.let {
                    complainStatusClickListener!!.onComplainStatusClicked(0)
                }
            }
            R.id.cardSolvedComplain -> {
                complainStatusClickListener?.let {
                    complainStatusClickListener!!.onComplainStatusClicked(1)
                }
            }

            R.id.cardUnSolvedComplain -> {
                complainStatusClickListener?.let {
                    complainStatusClickListener!!.onComplainStatusClicked(2)
                }
            }

            // Section 02

            R.id.cardEmergencyService -> {
                Navigator.sharedInstance.navigate(context, EmergencyServiceActivity::class.java)
            }

            R.id.cardEmergencyAlert -> {
                val emergencyContact =
                    mPrefManager.getString(Keys.ProfileKey.EMERGENCY_CONTACT.name)
                val mLat = SSFusedLocationProviderClient.mLatitude
                val mLong = SSFusedLocationProviderClient.mLongitude
                if (mLat != 0.0 && mLong != 0.0) {
                    val message =
                        if (mAppLanguage == AppLanguage.BENGALI.name) {
                            "আমি ${mPrefManager.getString(Keys.ProfileKey.NAME.name)}, আমার সাহায্য প্রয়োজন! আমার অবস্থান: https://www.google.com/maps/place/$mLat,$mLong"

                        } else {
                            "I am ${mPrefManager.getString(Keys.ProfileKey.NAME.name)}.I need your help! My Location: https://www.google.com/maps/place/$mLat,$mLong"
                        }
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse("smsto:$emergencyContact"))
                    intent.addCategory(Intent.CATEGORY_DEFAULT)
                    //  intent.type = "vnd.android-dir/mms-sms";
                    intent.putExtra("sms_body", message)
                    startActivity(intent)
                    // smsUtils?.sendSMS(context, emergencyContact, message)
                }
//                if (!permissionUtils.shouldAskPermission(context, Manifest.permission.SEND_SMS)) {
//                    val emergencyContact =
//                        mPrefManager.getString(Keys.ProfileKey.EMERGENCY_CONTACT.name)
//                    if (emergencyContact.isNotEmpty()) {
//                        val mLat = SSFusedLocationProviderClient.mLatitude
//                        val mLong = SSFusedLocationProviderClient.mLongitude
//                        if (mLat != 0.0 && mLong != 0.0) {
//                            val message =
//                                if (mAppLanguage == AppLanguage.BENGALI.name) {
//                                    "আমি ${mPrefManager.getString(Keys.ProfileKey.NAME.name)}, আমার সাহায্য প্রয়োজন! আমার অবস্থান: https://www.google.com/maps/place/$mLat,$mLong"
//
//                                } else {
//                                    "I am ${mPrefManager.getString(Keys.ProfileKey.NAME.name)}.I need your help! My Location: https://www.google.com/maps/place/$mLat,$mLong"
//                                }
//                            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("sms:$emergencyContact"))
//                            intent.putExtra("sms_body", message)
//                            startActivity(intent)
//                           // smsUtils?.sendSMS(context, emergencyContact, message)
//                        }
//                    } else {
//                        mAlertService.showConfirmationAlert(
//                            context,
//                            null,
//                            getString(R.string.set_emergency_nmbr_msg),
//                            null,
//                            getString(R.string.okay),
//                            object : AlertService.AlertListener {
//                                override fun positiveBtnDidTapped() {
//                                    Navigator.sharedInstance.navigate(
//                                        context,
//                                        ProfileActivity::class.java
//                                    )
//                                }
//
//                                override fun negativeBtnDidTapped() {
//                                    // nothing to do
//                                }
//                            }
//                        )
//                    }
//                } else {
//                    mAlertService.showConfirmationAlert(context,
//                        getString(R.string.permission_required),
//                        getString(R.string.permission_msg_sms),
//                        getString(R.string.button_not_now),
//                        getString(R.string.okay),
//                        object : AlertService.AlertListener {
//
//                            override fun negativeBtnDidTapped() {}
//
//                            override fun positiveBtnDidTapped() {
//                                requestPermissions(
//                                    arrayOf(Manifest.permission.SEND_SMS),
//                                    SEND_SMS_PERMISSION_CODE
//                                )
//                            }
//                        })
//                }
            }

            R.id.cardEmergencyCall -> {
                AppUtils.shared.dial(context, "999")

//                if (!permissionUtils.shouldAskPermission(context, Manifest.permission.CALL_PHONE)) {
//                    AppUtils.shared.dial(context, "999")
//                } else {
//                    mAlertService.showConfirmationAlert(context,
//                        getString(R.string.permission_required),
//                        getString(R.string.permission_call_phone_one),
//                        getString(R.string.button_not_now),
//                        getString(R.string.okay),
//                        object : AlertService.AlertListener {
//
//                            override fun negativeBtnDidTapped() {}
//
//                            override fun positiveBtnDidTapped() {
//                                requestPermissions(
//                                    arrayOf(Manifest.permission.CALL_PHONE),
//                                    CALL_PHONE_PERMISSION_CODE
//                                )
//
//                            }
//                        })
//
//                }
            }


            R.id.cardDhakaToday -> {
                Navigator.sharedInstance.navigate(context, EventListActivity::class.java)
            }

            // Section 04
            R.id.cardAppMujib100 -> {
                AppUtils.shared.openAppOnPlayStore(context, APP_MUJIB_100)
            }
            R.id.cardAppMuktiJuddho -> {
                AppUtils.shared.openAppOnPlayStore(context, APP_MUKTIJUDDHO)
            }
            R.id.card_app_mukto_path -> {
                AppUtils.shared.openAppOnPlayStore(context, APP_MUKTO_PATH)
            }
            R.id.cardAppSurokkha -> {
                AppUtils.shared.openAppOnPlayStore(context, APP_SUROKKHA)
            }
        }
    }

    override fun sendEmergencyAlertDidSucceed() {
        context.showToast("Emergency Alert Sent Successfully.")
    }

    override fun sendEmergencyAlertDidFailed(failedMsg: String) {
        mAlertService.showAlert(context, null, failedMsg)
    }

    override fun complainStatusDidReceived(response: ComplainStatusResponse) {

        mPrefManager.putString("total", response.total.toString())
        mPrefManager.putString("solved", response.solved.toString())
        mPrefManager.putString("unsolved", response.unsolved.toString())

        // Total
        val total = if (mAppLanguage == AppLanguage.BENGALI.name)
            AppUtils.shared.getInBangla("${response.total}")
        else
            "${response.total}"

        // Solved
        val solved = if (mAppLanguage == AppLanguage.BENGALI.name)
            AppUtils.shared.getInBangla("${response.solved}")
        else
            "${response.solved}"

        // UnSolved
        val unSolved = if (mAppLanguage == AppLanguage.BENGALI.name)
            AppUtils.shared.getInBangla("${response.unsolved}")
        else
            "${response.unsolved}"
        rootView.tvTotalCounter.text = total
        rootView.tvSolvedCounter.text = solved
        rootView.tvUnsolvedCounter.text = unSolved
    }

    override fun noticeDidReceived(notices: ArrayList<GetNoticeResponse.SingleNotice?>?) {
        if (notices != null && notices.isNotEmpty()) {
            var noticeTxt = ""
            for (notice in notices) {
                if (notice!!.status == "1") {
                    noticeTxt += " ** ${notice.title}"
                }
            }
            rootView.tvNotice.text = noticeTxt
            //tvNotice.startAnimation(animFadeIn)
            rootView.tvNotice.isSelected = true
        }
    }


    override fun geoFencingDidChecked(isUnderDNCC: Boolean) {
        // Nothing to do with this data now
    }


    override fun onNetworkCallEnded() {
        if (rootView.swipe_refreshLayout.isRefreshing) {
            rootView.swipe_refreshLayout.isRefreshing = false
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
        smsUtils?.let {
            smsUtils = null
        }
    }

    override fun unReadNotificationDidReceived(response: GetUnReadNotificaitonResponse) {
        unSeenNotificationCount = response.notification
        unSeenDisasterCount = response.alert
        if (response.total > 0) {
            rootView.tvNotificaitoncount.text =
                if (mAppLanguage == AppLanguage.BENGALI.name)
                    AppUtils.shared.getInBangla("${response.total}")
                else
                    "${response.total}"
            rootView.tvNotificaitoncount.visibility = View.VISIBLE

            if (unSeenDisasterCount > 0) {
                rootView.imgNotification.setImageResource(R.drawable.ic_notifications_yellow)
                val animation = AlphaAnimation(
                    1.0.toFloat(),
                    0f
                ) // Change alpha from fully visible to invisible
                animation.duration = 1000 //1 second duration for each animation cycle
                animation.interpolator = LinearInterpolator()
                animation.repeatCount = Animation.INFINITE //repeating indefinitely
                animation.repeatMode =
                    Animation.REVERSE //animation will start from end point once ended.
                rootView.sectionNotification.startAnimation(animation)
            } else {
                rootView.imgNotification.setImageResource(R.drawable.ic_notifications)
                rootView.sectionNotification.clearAnimation()
            }
        } else {
            rootView.tvNotificaitoncount.visibility = View.GONE
            rootView.imgNotification.setImageResource(R.drawable.ic_notifications)
            rootView.sectionNotification.clearAnimation()
        }
    }

    //  No need this if we use Picasso/Glide library to load images
    override fun onDestroyView() {

        //  rootView.imgAjkerDhaka.setImageDrawable(null)

        // Complain Images
        rootView.imgComplainRoad.setImageDrawable(null)
        rootView.imgComplainMosquito.setImageDrawable(null)
        rootView.imgComplainGarbage.setImageDrawable(null)
        rootView.imgComplainStreetLamp.setImageDrawable(null)
        rootView.imgComplainPublicToilet.setImageDrawable(null)
        rootView.imgComplainDranage.setImageDrawable(null)
        rootView.imgComplainIllegalStructure.setImageDrawable(null)
        rootView.imgComplainWaterLogging.setImageDrawable(null)

        // Nearby Images
        rootView.imgNearbyBusStand.setImageDrawable(null)
        rootView.imgNearbyHospital.setImageDrawable(null)
        rootView.imgNearbyPharmacy.setImageDrawable(null)
        rootView.imgNearbyPoliceStation.setImageDrawable(null)
        rootView.imgNearbyFirBrigade.setImageDrawable(null)
        rootView.imgNearbyParking.setImageDrawable(null)
        rootView.imgNearbyFuelStation.setImageDrawable(null)
        rootView.imgNearbyPublicToilet.setImageDrawable(null)
        rootView.imgNearbyParks.setImageDrawable(null)
        rootView.imgNearbyCommunityCenter.setImageDrawable(null)
        rootView.imgNearbySts.setImageDrawable(null)

        // Necessary App Image
       // rootView.appBongobondhu.setImageDrawable(null)
        rootView.appMuktijuddho.setImageDrawable(null)
        super.onDestroyView()

        stopLocationService()
    }


    private fun isLocationProviderEnabled(): Boolean {
        val lm = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val isGpsProviderEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
        val isNetworkProviderEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        return isGpsProviderEnabled || isNetworkProviderEnabled
    }

    private fun isPlayServiceAvailable(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val resultCode = apiAvailability.isGooglePlayServicesAvailable(context)
        return resultCode == ConnectionResult.SUCCESS
    }

    private fun startLocationService() {
        if (ssFusedLocationApiClient == null)
            ssFusedLocationApiClient = SSFusedLocationProviderClient(context)
        ssFusedLocationApiClient?.startLocationUpdates()

//        if(!AppUtils.shared.isServiceRunning(context, LocationService::class.java)){
//            Intent(context.applicationContext, LocationService::class.java).also { intent ->
//                  context.startService(intent)
//            }
//        }
    }

    private fun stopLocationService() {
        ssFusedLocationApiClient?.stopLocationUpdates()
        ssFusedLocationApiClient?.onDestroy()
        ssFusedLocationApiClient = null
//        if(AppUtils.shared.isServiceRunning(context, LocationService::class.java)){
//            Intent(context.applicationContext, LocationService::class.java).also { intent ->
//                context.stopService(intent)
//            }
//        }
    }

    override fun addressDidReceived(address: String) {
        // Nothing to do with current location now
    }

    private val gpsSwitchStateReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {

            if (intent.action == LocationManager.PROVIDERS_CHANGED_ACTION) {
                if (isLocationProviderEnabled()) {
                    if (gpsAlertDialog != null && gpsAlertDialog!!.isShowing) {
                        gpsAlertDialog!!.dismiss()
                    }
                    startLocationService()
                } else {
                    stopLocationService()
                    val gpsAlertDialogBuilder = AlertDialog.Builder(context)
                    gpsAlertDialogBuilder.setCancelable(true)
                    gpsAlertDialogBuilder.setMessage(R.string.enable_location_msg)
                    gpsAlertDialogBuilder.setPositiveButton(getString(R.string.settings)) { dialog, _ ->
                        dialog.dismiss()
                        startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                    }
                    gpsAlertDialog = gpsAlertDialogBuilder.create()
                    gpsAlertDialog?.show()
                }
            }
        }
    }
}

/**
 * 0. Notification icon will be blinking if we have emergency alert with emergency alert count (If we dnt have
 *      emergency alert it will show unseen notification count..
 *
 * 1. We are sending flag for we have emergency alert or not. If we have, we will open Shotorkobarta tab from Notification
 *      Otherwise we will open Notifications tab*/
