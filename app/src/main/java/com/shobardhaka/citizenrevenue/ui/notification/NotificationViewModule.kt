package com.shobardhaka.citizenrevenue.ui.notification

import dagger.Binds
import dagger.Module

@Module
abstract class NotificationViewModule {

    @Binds
    abstract fun provideNotificationViewModule(activity: NotificationActivity): NotificationContract.View
}