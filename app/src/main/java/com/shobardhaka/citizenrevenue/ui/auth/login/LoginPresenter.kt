package com.shobardhaka.citizenrevenue.ui.auth.login

import com.shobardhaka.citizenrevenue.base.BasePresenter
import javax.inject.Inject

class LoginPresenter @Inject constructor(view: LoginContract.View) : BasePresenter<LoginContract.View>(view),
    LoginContract.Presenter {


}