package com.shobardhaka.citizenrevenue.ui.auth.get_otp

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import butterknife.OnClick
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.MvpBaseActivity
import com.shobardhaka.citizenrevenue.sms_retriver.AppSignatureHashHelper
import com.shobardhaka.citizenrevenue.ui.auth.login.LoginActivity
import com.shobardhaka.citizenrevenue.ui.auth.verify_otp.VerifyOtpActivity
import com.shobardhaka.citizenrevenue.utils.Keys.IntentKeys
import com.shobardhaka.citizenrevenue.utils.Navigator
import com.shobardhaka.citizenrevenue.utils.showToast
import kotlinx.android.synthetic.main.activity_get_otp.*

class GetOtpActivity : MvpBaseActivity<GetOtpPresenter>(), GetOtpContract.View {

    private var mobileNumber: String = ""

    override fun getContentView(): Int {
        return R.layout.activity_get_otp
    }

    override fun onViewReady(savedInstanceState: Bundle?, intent: Intent) {
        etMobileNumber.addTextChangedListener(mobileNumberWatcher)
    }

    @OnClick(
        R.id.btn0,
        R.id.btn1,
        R.id.btn2,
        R.id.btn3,
        R.id.btn4,
        R.id.btn5,
        R.id.btn6,
        R.id.btn7,
        R.id.btn8,
        R.id.btn9,
        R.id.btnGetOtp,
        R.id.btnDel
    )
    fun onClicked(view: View) {
        when (view.id) {
            R.id.btn0 -> input("0")
            R.id.btn1 -> input("1")
            R.id.btn2 -> input("2")
            R.id.btn3 -> input("3")
            R.id.btn4 -> input("4")
            R.id.btn5 -> input("5")
            R.id.btn6 -> input("6")
            R.id.btn7 -> input("7")
            R.id.btn8 -> input("8")
            R.id.btn9 -> input("9")
            R.id.btnDel -> delBtnDidTapped()
            R.id.btnGetOtp -> getOtpBtnDidTapped()

        }
    }

    private fun getOtpBtnDidTapped() {
        mobileNumber = etMobileNumber.text.toString()
        val appSignatureHashHelper = AppSignatureHashHelper(this)
        mPresenter.requestOTP(mobileNumber, appSignatureHashHelper.appSignatures[0] )
    }

    private fun delBtnDidTapped() {
        val length = etMobileNumber.text!!.length
        if (length > 0) {
            etMobileNumber.text!!.delete(length - 1, length)
        }
    }

    private fun input(number: String) {
        val editedNumber = etMobileNumber.text.toString() + number
        etMobileNumber.setText(editedNumber)
    }


    ///////////// VIEW...........................
    override fun requestOTPDidFailed(failedMsg: String) {
        showToast(failedMsg)
    }

    override fun requestOTPDidSucceed() {
        val bundle = Bundle()
        bundle.putString(IntentKeys.MOBILE_NUMBER, mobileNumber)
        Navigator.sharedInstance.navigateWithBundle(
            getContext(),
            VerifyOtpActivity::class.java,
            IntentKeys.DATA_BUNDLE,
            bundle
        )
        finish()
    }

    override fun onBackPressed() {
        Navigator.sharedInstance.back(getContext(), LoginActivity::class.java)
        finish()
    }

    private var mobileNumberWatcher: TextWatcher? = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            btnGetOtp.isEnabled = (!s.isNullOrEmpty() && s?.length == 11 && s.startsWith("01"))
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    }

    override fun onDestroy() {
        mobileNumberWatcher = null
        super.onDestroy()
    }
}