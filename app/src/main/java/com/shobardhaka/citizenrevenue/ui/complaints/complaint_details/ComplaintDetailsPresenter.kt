package com.shobardhaka.citizenrevenue.ui.complaints.complaint_details

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.ComplainDetailsResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.PostDataResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.ComplainApiService
import java.util.LinkedHashMap
import javax.inject.Inject

class ComplaintDetailsPresenter @Inject constructor(view: ComplaintDetailsContract.View) :
    BasePresenter<ComplaintDetailsContract.View>(view), ComplaintDetailsContract.Presenter {


    @Inject
    lateinit var complainApiService: ComplainApiService

    override fun getComplainDetails(complainId: String, showLoadingDialog: Boolean) {
        if (showLoadingDialog)
            mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            complainApiService.getComplainDetails(complainId)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<ComplainDetailsResponse, ComplaintDetailsContract.View>(
                        mView
                    ) {

                    override fun onRequestSuccess(response: ComplainDetailsResponse) {
                        mView?.complainDetailsDidReceived(response)
                    }
                })
        )
    }


    override fun recomplain(url: String) {
        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            complainApiService.reComplaint(url)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<PostDataResponse, ComplaintDetailsContract.View>(
                        mView
                    ) {

                    override fun onRequestSuccess(response: PostDataResponse) {
                        if (response.status == 200) {
                            mView?.recomplaintDidsSucceed()
                        } else if (response.status == 423) {
                            mView?.recomplaintDidFailed()
                        }
                    }
                })
        )
    }

    override fun submitRating(dataMap: HashMap<String, String>) {
        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            complainApiService.submitRating(dataMap)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<PostDataResponse, ComplaintDetailsContract.View>(
                        mView
                    ) {

                    override fun onRequestSuccess(response: PostDataResponse) {
                        if (response.status == 200) {
                            mView?.submitRatingDidSucceed()
                        }
                    }

                    override fun onFormDataNotValid(errorMap: LinkedHashMap<String, String>) {
                        for ((_, value) in errorMap) {
                            mView?.submitRatingDidFailed(value)
                            break
                        }
                    }
                })
        )
    }
}