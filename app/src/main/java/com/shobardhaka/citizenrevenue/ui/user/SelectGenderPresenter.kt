package com.shobardhaka.citizenrevenue.ui.user

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.RetrofitHelperService
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.PostDataResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import javax.inject.Inject

class SelectGenderPresenter @Inject constructor(view: SelectGenderContract.View) :
    BasePresenter<SelectGenderContract.View>(view),
    SelectGenderContract.Presenter {

    @Inject
    lateinit var userApiService: UserApiService

    @Inject
    lateinit var retrofitHelperService: RetrofitHelperService

    override fun updateGender(gender: String) {
        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            userApiService.updateGender(gender)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object :
                    SSDisposableSingleObserver<PostDataResponse, SelectGenderContract.View>(mView) {

                    override fun onRequestSuccess(response: PostDataResponse) {
                        retrofitHelperService.loadProfile(compositeDisposable!!, object : RetrofitHelperService.OnProfileLoadListener{
                            override fun onProfileLoaded() {
                                mView?.genderUpdateDidSucceed()
                            }
                        })
                    }
                })
        )
    }
}