package com.shobardhaka.citizenrevenue.ui.councilors

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.GetCouncilorResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.GetWardsResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import javax.inject.Inject

class CouncilorPresenter @Inject constructor(view: CouncilorContract.View): BasePresenter<CouncilorContract.View>(view),
        CouncilorContract.Presenter{

    @Inject
    lateinit var userApiService: UserApiService

    override fun getCouncilors(zone: String, ward: String) {
        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
            userApiService.getCouncilors(zone, ward)
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<GetCouncilorResponse, CouncilorContract.View>(mView) {

                    override fun onRequestSuccess(response: GetCouncilorResponse) {
                        mView?.councilorsDidReceived(response.councilors)
                    }

                })
        )
    }

    override fun getZoneWards() {
        compositeDisposable?.add(
            userApiService.getZoneWards()
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : SSDisposableSingleObserver<GetWardsResponse, CouncilorContract.View>(mView) {

                    override fun onRequestSuccess(response: GetWardsResponse) {
                        mView?.zoneWardsDidReceived(response)
                    }

                })
        )
    }
}