package com.shobardhaka.citizenrevenue.ui.emergency

import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BasePresenter
import com.shobardhaka.citizenrevenue.data.network.SSDisposableSingleObserver
import com.shobardhaka.citizenrevenue.data.network.api_response.GetEmergencyServiceResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import javax.inject.Inject

class EmergencyServicePresenter @Inject constructor(view: EmergencyServiceContract.View) : BasePresenter<EmergencyServiceContract.View>(view), EmergencyServiceContract.Presenter {

    @Inject
    lateinit var userApiService: UserApiService

    override fun getEmergencyServices() {


        mView?.onNetworkCallStarted(context.getString(R.string.please_wait))
        compositeDisposable?.add(
                userApiService.getEmergencyServices()
                        .subscribeOn(appSchedulerProvider.io())
                        .unsubscribeOn(appSchedulerProvider.computation())
                        .observeOn(appSchedulerProvider.ui())
                        .subscribeWith(object : SSDisposableSingleObserver<GetEmergencyServiceResponse, EmergencyServiceContract.View>(mView) {

                            override fun onRequestSuccess(response: GetEmergencyServiceResponse) {
                                mView?.emergencyServicesDidReceived(response.emergencyServices)
                            }

                        })
        )
    }

}