package com.shobardhaka.citizenrevenue.callbacks

interface DisasterSeenListener {
    fun onSeenDisaster()
}