package com.shobardhaka.citizenrevenue.callbacks

interface OnComplainStatusClickListener {
    fun onComplainStatusClicked(position: Int)
}