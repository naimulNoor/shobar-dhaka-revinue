package com.shobardhaka.citizenrevenue.callbacks

interface CallButtonClickListener {
    fun onCallButtonClicked(number: String)
}