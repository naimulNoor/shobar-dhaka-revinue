package com.shobardhaka.citizenrevenue.callbacks

interface NotificationSeenListener{

    fun onSeenNotification()
}