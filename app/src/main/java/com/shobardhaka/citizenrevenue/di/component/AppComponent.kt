package com.shobardhaka.citizenrevenue.di.component
import com.shobardhaka.citizenrevenue.base.BaseApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import com.shobardhaka.citizenrevenue.di.module.ActivityBindingModule
import com.shobardhaka.citizenrevenue.di.module.ApplicationModule
import com.shobardhaka.citizenrevenue.di.module.NetworkModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    NetworkModule::class,
    ApplicationModule::class,
    ActivityBindingModule::class])

interface AppComponent {
    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: BaseApplication): Builder

        fun build(): AppComponent
    }

    fun inject(app: BaseApplication)
}