package com.shobardhaka.citizenrevenue.di.module

import com.shobardhaka.citizenrevenue.di.scope.ActivityScope
import com.shobardhaka.citizenrevenue.ui.auth.get_otp.GetOtpActivity
import com.shobardhaka.citizenrevenue.ui.auth.get_otp.GetOtpViewModule
import com.shobardhaka.citizenrevenue.ui.auth.login.LoginActivity
import com.shobardhaka.citizenrevenue.ui.auth.login.LoginViewModule
import com.shobardhaka.citizenrevenue.ui.auth.verify_otp.VerifyOtpActivity
import com.shobardhaka.citizenrevenue.ui.auth.verify_otp.VerifyOtpViewModule
import com.shobardhaka.citizenrevenue.ui.complaints.add_complaint.AddComplaintViewModule
import com.shobardhaka.citizenrevenue.ui.complaints.add_complaint.AddComplaintsActivity
import com.shobardhaka.citizenrevenue.ui.complaints.add_mosquitio_complaint.AddMosquitioComplaintActivity
import com.shobardhaka.citizenrevenue.ui.complaints.add_mosquitio_complaint.AddMosquitoComplainViewModule
import com.shobardhaka.citizenrevenue.ui.complaints.complaint_details.ComplaintDetailsActivity
import com.shobardhaka.citizenrevenue.ui.complaints.complaint_details.ComplaintDetailsViewModule
import com.shobardhaka.citizenrevenue.ui.councilors.CouncilorViewModule
import com.shobardhaka.citizenrevenue.ui.councilors.CouncilorsActivity
import com.shobardhaka.citizenrevenue.ui.department_heads.DepartmentHeadsActivity
import com.shobardhaka.citizenrevenue.ui.department_heads.DepartmentHeadsViewModule
import com.shobardhaka.citizenrevenue.ui.dncc_office.NearbyDnccOfficeActivity
import com.shobardhaka.citizenrevenue.ui.dncc_office.NearbyDnccViewModule
import com.shobardhaka.citizenrevenue.ui.emergency.EmergencyServiceActivity
import com.shobardhaka.citizenrevenue.ui.emergency.EmergencyServiceViewModule
import com.shobardhaka.citizenrevenue.ui.event_detials.EventDetailsActivity
import com.shobardhaka.citizenrevenue.ui.event_detials.EventDetialsViewModule
import com.shobardhaka.citizenrevenue.ui.events.EventListActivity
import com.shobardhaka.citizenrevenue.ui.events.EventViewModule
import com.shobardhaka.citizenrevenue.ui.home.HomeActivity
import com.shobardhaka.citizenrevenue.ui.home.HomeViewModule
import com.shobardhaka.citizenrevenue.ui.maps.MapsActivity
import com.shobardhaka.citizenrevenue.ui.maps.MapsViewModule
import com.shobardhaka.citizenrevenue.ui.mayor_ceo.ProfileDetailsActivity
import com.shobardhaka.citizenrevenue.ui.mayor_ceo.ProfileDetailsViewModule
import com.shobardhaka.citizenrevenue.ui.nearby.nearby_place.NearbyPlaceActivity
import com.shobardhaka.citizenrevenue.ui.nearby.nearby_place.NearbyPlaceViewModule
import com.shobardhaka.citizenrevenue.ui.notification.NotificationActivity
import com.shobardhaka.citizenrevenue.ui.notification.NotificationViewModule
import com.shobardhaka.citizenrevenue.ui.profile.ProfileActivity
import com.shobardhaka.citizenrevenue.ui.profile.ProfileViewModule
import com.shobardhaka.citizenrevenue.ui.profile_info.ProfileInfoActivity
import com.shobardhaka.citizenrevenue.ui.profile_info.ProfileInfoViewModule
import com.shobardhaka.citizenrevenue.ui.settings.SettingsActivity
import com.shobardhaka.citizenrevenue.ui.settings.SettingsViewModule
import com.shobardhaka.citizenrevenue.ui.splash.SplashScreenActivity
import com.shobardhaka.citizenrevenue.ui.splash.SplashViewModule
import com.shobardhaka.citizenrevenue.ui.user.SelectGenderActivity
import com.shobardhaka.citizenrevenue.ui.user.SelectGenderViewModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module(includes = [FragmentBindingModule::class])
abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [LoginViewModule::class])
    abstract fun bindLoginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [HomeViewModule::class])
    abstract fun bindHomeActivity(): HomeActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [AddComplaintViewModule::class])
    abstract fun bindAddComplaintsActivity(): AddComplaintsActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [ComplaintDetailsViewModule::class])
    abstract fun bindComplaintDetialsActivity(): ComplaintDetailsActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [EmergencyServiceViewModule::class])
    abstract fun bindEmgergencySericeActivity(): EmergencyServiceActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [ProfileViewModule::class])
    abstract fun bindProfileActivity(): ProfileActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [MapsViewModule::class])
    abstract fun bindMapsActivity(): MapsActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [NearbyPlaceViewModule::class])
    abstract fun bindNearbyPlaceActivity(): NearbyPlaceActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [ProfileInfoViewModule::class])
    abstract fun bindProfileInfoActivity(): ProfileInfoActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [NotificationViewModule::class])
    abstract fun bindNotificationActivity(): NotificationActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [CouncilorViewModule::class])
    abstract fun bindCouncilorsHeadsActivity(): CouncilorsActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [DepartmentHeadsViewModule::class])
    abstract fun bindDepartHeadsActivity(): DepartmentHeadsActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [EventViewModule::class])
    abstract fun bindEventsActivity(): EventListActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [NearbyDnccViewModule::class])
    abstract fun bindNearbyDnccActivity(): NearbyDnccOfficeActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [EventDetialsViewModule::class])
    abstract fun bindEventDetailsActivity(): EventDetailsActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [SplashViewModule::class])
    abstract fun bindEventSplashActivity(): SplashScreenActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [GetOtpViewModule::class])
    abstract fun bindGetOtpActivity(): GetOtpActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [VerifyOtpViewModule::class])
    abstract fun bindVerifyOtpActivity(): VerifyOtpActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [AddMosquitoComplainViewModule::class])
    abstract fun bindAddMosquitoComplainActivity(): AddMosquitioComplaintActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [ProfileDetailsViewModule::class])
    abstract fun bindProfileDetailsActivity(): ProfileDetailsActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [SettingsViewModule::class])
    abstract fun bindSettingsActivity(): SettingsActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [SelectGenderViewModule::class])
    abstract fun bindSelectGenderActivity(): SelectGenderActivity

}