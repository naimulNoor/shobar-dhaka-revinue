package com.shobardhaka.citizenrevenue.di.module

import android.content.Context
import com.shobardhaka.citizenrevenue.base.BaseApplication
import com.shobardhaka.citizenrevenue.data.prefs.PreferenceManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton
import com.shobardhaka.citizenrevenue.rx.AppSchedulerProvider
import com.shobardhaka.citizenrevenue.utils.*

@Module
class ApplicationModule {

    @Provides
    fun provideContext(baseApp: BaseApplication): Context {
        return baseApp
    }

    @Provides
    @Singleton
    fun providePreferenceManager(context: Context): PreferenceManager {
        return PreferenceManager(context)
    }

    @Provides
    @Singleton
    fun providePermissionUtils(preferenceManager: PreferenceManager): PermissionUtils {
        return PermissionUtils(preferenceManager)
    }

    @Provides
    @Singleton
    fun provideAlertService(): AlertService {
        return AlertService()
    }

    @Provides
    @Singleton
    fun provideAppSchedule(): AppSchedulerProvider {
        return AppSchedulerProvider()
    }

    @Provides
    @Singleton
    fun provideNavigator(): Navigator {
        return Navigator()
    }

    @Provides
    @Singleton
    fun provideAppLogger(): AppLogger {
        return AppLogger()
    }

    @Provides
    @Singleton
    fun provideNetworkUtils(): NetworkUtils {
        return NetworkUtils()
    }
}