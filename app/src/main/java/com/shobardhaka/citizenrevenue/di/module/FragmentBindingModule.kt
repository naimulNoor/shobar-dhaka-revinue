package com.shobardhaka.citizenrevenue.di.module
import com.shobardhaka.citizenrevenue.di.scope.FragmentScope
import com.shobardhaka.citizenrevenue.ui.fragments.complain.MyComplainFrgViewModule
import com.shobardhaka.citizenrevenue.ui.fragments.complain.MyComplaintsFragment
import com.shobardhaka.citizenrevenue.ui.fragments.events.EventsFragment
import com.shobardhaka.citizenrevenue.ui.fragments.events.EventsFrgViewModule
import com.shobardhaka.citizenrevenue.ui.fragments.home.HomeFragment
import com.shobardhaka.citizenrevenue.ui.fragments.home.HomeFragmentViewModule
import com.shobardhaka.citizenrevenue.ui.fragments.notification.NotificationFragment
import com.shobardhaka.citizenrevenue.ui.fragments.notification.NotificationFrgViewModule
import com.shobardhaka.citizenrevenue.ui.fragments.shotortobarta.ShotorkobartaFragment
import com.shobardhaka.citizenrevenue.ui.fragments.shotortobarta.ShotorkobartaFrgViewModule
import com.shobardhaka.citizenrevenue.ui.fragments.temperature.TemperatureFragment
import com.shobardhaka.citizenrevenue.ui.fragments.temperature.TemperatureFrgViewModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class FragmentBindingModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = [HomeFragmentViewModule::class])
    abstract fun bindHomeFragment(): HomeFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [NotificationFrgViewModule::class])
    abstract fun bindNotificaitonFragment(): NotificationFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [ShotorkobartaFrgViewModule::class])
    abstract fun bindShotorkobartaFragment(): ShotorkobartaFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [EventsFrgViewModule::class])
    abstract fun bindEventFrgView(): EventsFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [TemperatureFrgViewModule::class])
    abstract fun bindTemperatureFragment(): TemperatureFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [MyComplainFrgViewModule::class])
    abstract fun bindMyComplainFrgFragment(): MyComplaintsFragment

}