package com.shobardhaka.citizenrevenue.di.module

import android.content.Context
import com.shobardhaka.citizenrevenue.data.network.ApiServiceBuilder
import com.shobardhaka.citizenrevenue.data.network.RetrofitApiClient
import com.shobardhaka.citizenrevenue.data.network.api_service.*
import com.shobardhaka.citizenrevenue.data.prefs.PreferenceManager
import com.shobardhaka.citizenrevenue.utils.NetworkUtils
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton


@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideRetrofitApiClient(context: Context, networkUtils: NetworkUtils, preferenceManager: PreferenceManager): Retrofit {
        return RetrofitApiClient.getRetrofit(context, networkUtils, preferenceManager)
    }

    @Provides
    @Singleton
    fun provideApiServiceBuilder(retrofit: Retrofit): ApiServiceBuilder {
        return ApiServiceBuilder(retrofit)
    }

    //////////////////////////////////////////////////////////////////

    @Provides
    @Singleton
    fun provideUserApiService(apiServiceBuilder: ApiServiceBuilder): UserApiService {
        return apiServiceBuilder.buildService(UserApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideComplainApiService(apiServiceBuilder: ApiServiceBuilder): ComplainApiService {
        return apiServiceBuilder.buildService(ComplainApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideEmergencyApiService(apiServiceBuilder: ApiServiceBuilder):EmergencyApiService {
        return apiServiceBuilder.buildService(EmergencyApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideBarikoiApiService(apiServiceBuilder: ApiServiceBuilder):BariKoiApiService {
        return apiServiceBuilder.buildService(BariKoiApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideOpenWeatherMapApiService(apiServiceBuilder: ApiServiceBuilder):OpenWeatherMapApiService {
        return apiServiceBuilder.buildService(OpenWeatherMapApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideUserAuthApiService(apiServiceBuilder: ApiServiceBuilder):UserAuthApiService {
        return apiServiceBuilder.buildService(UserAuthApiService::class.java)
    }

}