package com.shobardhaka.citizenrevenue

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.widget.TextView

class ScrollingTextView : TextView {

    constructor(mContext: Context) : super(mContext) {}

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        if (focused)
            super.onFocusChanged(focused, direction, previouslyFocusedRect)
      //  Log.v("SCROLLING_TEXT", "On focus change: $focused")
    }

    override fun onWindowFocusChanged(hasWindowFocus: Boolean) {
        if (hasWindowFocus)
            super.onWindowFocusChanged(hasWindowFocus)
       // Log.v("SCROLLING_TEXT", "Window Focused: $hasWindowFocus")
    }

    override fun isFocused(): Boolean {
        return true
    }
}