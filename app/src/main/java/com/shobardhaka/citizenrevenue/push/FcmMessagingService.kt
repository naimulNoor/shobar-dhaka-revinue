package com.shobardhaka.citizenrevenue.push

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.data.prefs.PreferenceManager
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import com.shobardhaka.citizenrevenue.utils.NotificationUtils
import org.json.JSONObject

class FcmMessagingService : FirebaseMessagingService() {

    private var mNotificationUtils: NotificationUtils? = null
    private var mPrefs: PreferenceManager? = null


    override fun onCreate() {
        super.onCreate()
        mNotificationUtils = NotificationUtils(this)
        mPrefs = PreferenceManager(this)
    }

    override fun onNewToken(newToken: String) {
        mPrefs?.let {
            mPrefs!!.putString(Keys.DEVICE_TOKEN.name, newToken)
        }
    }

    //your app is in background or foreground all time calling
    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        remoteMessage.notification?.let {
            val title = remoteMessage.notification!!.title
            val body = remoteMessage.notification!!.body
            handleNotificationMessage(title, body)
        }

        /*...................................................
            Handle Data Payload
         ..................................................*/

        if (remoteMessage.data.isNotEmpty()) {
            try {
                val json = JSONObject(remoteMessage.data as Map<*, *>)
                handleDataMessage(json)
            } catch (e: Exception) {
                // Log.d(TAG, e.message)
            }
        }



    }

    private fun handleNotificationMessage(title: String?, message: String?) {
        /* If app is in background, notification will be handled by fireBase itself.
        We have to show notification manually if app is in foreground. */
        mNotificationUtils?.let {
            if (mNotificationUtils!!.isAppIsInBackground(applicationContext)) {
                if (mPrefs != null) {
                    if (mPrefs!!.getBoolean(Keys.IS_NOTIFICATION_ENABLED.name, true)) {
                        mNotificationUtils!!.showNotification("", R.drawable.notificaiton_blue, title, message, null, "")
                    }
                }

            }
        }
    }

    private fun handleDataMessage(notificationJson: JSONObject) {
        val appLanguage = mPrefs?.getString(Keys.APP_LANGUAGE.name, AppLanguage.BENGALI.name)
        var title = ""
        var message = ""
        val imageUrl = notificationJson.optString("image")
        val complainId = notificationJson.optString("complain_id")
        val notificationType = notificationJson.optString("type")
        if (appLanguage == AppLanguage.BENGALI.name) {
            title = notificationJson.optString("title_bn")
            message = notificationJson.optString("message_bn")
        } else {
            title = notificationJson.optString("title_en")
            message = notificationJson.optString("message_en")
        }
        mNotificationUtils?.let {
            if (mPrefs != null) {
                if (mPrefs!!.getBoolean(Keys.IS_NOTIFICATION_ENABLED.name, true)) {
                    mNotificationUtils!!.showNotification(
                        notificationType,
                        R.mipmap.ic_not_blue,
                        title,
                        message,
                        imageUrl,
                        complainId
                    )
                }
            }
        }
    }
}
