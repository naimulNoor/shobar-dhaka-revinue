package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DnccAddressResponse(

    @Expose
    @SerializedName("status")
    val status: Int,
    @Expose
    @SerializedName("data")
    val data: ArrayList<SingleArea?>
) {
    data class SingleArea(
        @Expose
        @SerializedName("id")
        val id: Int,
        @Expose
        @SerializedName("name_en")
        val nameEn: String,
        @Expose
        @SerializedName("name_bn")
        val nameBn: String
    )
}

