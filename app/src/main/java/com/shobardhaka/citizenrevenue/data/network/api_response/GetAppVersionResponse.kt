package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GetAppVersionResponse(

    @Expose
    @SerializedName("app_details")
    var appDetails: AppDetails
) {
    data class AppDetails(
        @Expose
        @SerializedName("app_version")
        var appVersion: Int,
        @Expose
        @SerializedName("is_force_update_enable")
        var isForceUpdateEnable: Int?,
        @Expose
        @SerializedName("update_title")
        val updateTitle: String,
        @Expose
        @SerializedName("update_msg")
        val updateMsg: String
    )
}

