package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class NotificationResponse(

    @Expose
    @SerializedName("notifications")
    var notifications: ArrayList<SingleNotification?>
) {
    data class SingleNotification(
        @Expose
        @SerializedName("id")
        var id: Int,
        @Expose
        @SerializedName("title")
        val title: String,
        @Expose
        @SerializedName("description")
        val description: String,
        @Expose
        @SerializedName("created_at")
        val createdAt: String,
        @Expose
        @SerializedName("updated_at")
        val updatedAt: String,
        @Expose
        @SerializedName("read")
        var isRead: Int
    ) : Serializable
}
