package com.shobardhaka.citizenrevenue.data.network

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.shobardhaka.citizenrevenue.base.BaseContract
import io.reactivex.observers.DisposableSingleObserver
import org.json.JSONObject
import retrofit2.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.LinkedHashMap
import javax.net.ssl.SSLHandshakeException

/**
 * Created on 09/07/2019.
 */

abstract class SSDisposableSingleObserver<R, out V : BaseContract.View>(private val mView: V?) :
    DisposableSingleObserver<R>() {

    abstract fun onRequestSuccess(response: R)
    protected open fun onFormDataNotValid(errorMap: LinkedHashMap<String, String>) {}
    protected open fun onExpectationFailed() {}

    override fun onSuccess(response: R) {
        mView?.onNetworkCallEnded()
        onRequestSuccess(response)
    }

    override fun onError(throwable: Throwable) {
        mView?.onNetworkCallEnded()
        when (throwable) {
            is NoConnectivityException -> mView?.onNetworkUnavailable()
            is HttpException -> {
                val errorCode = throwable.code()
                val res = throwable.response()
                when (errorCode) {
                    401 -> {
                        try {
                            val rootObj = JSONObject(res.errorBody()?.string())
                            val errorMsg = rootObj.getString("error")
                            mView?.onUserUnauthorized(errorMsg)
                        } catch (exception: Exception) {
                            mView?.onUserUnauthorized("User is Unauthorized.")
                        }
                    }       // 401 = USER UNAUTHORIZED
                    403 -> {   // 402 for user disabled status
                        try {
                            val rootObj = JSONObject(res.errorBody()?.string())
                            val errorMsg = rootObj.getString("error")
                            mView?.onUserDisabled(errorMsg)
                        } catch (exception: Exception) {
                            mView?.onUserDisabled("Your are banned.")
                        }
                    }
                    404 -> mView?.onUserUnauthorized("Not Found")                    // 404 = NOT FOUND
                    417 -> onExpectationFailed()                                                 // 417 = Expectation Failed
                    422 -> {                                                                     // 422 = FORM DATA NOT VALID
                        try {
                            val rootObj = JSONObject(res.errorBody()?.string())
                            val errors = rootObj.getJSONObject("errors")
                            val errorMap = Gson().fromJson<LinkedHashMap<String, String>>(
                                errors.toString(),
                                object : TypeToken<LinkedHashMap<String, String>>() {}.type
                            )
                            onFormDataNotValid(errorMap)
                        } catch (exception: Exception) {
                            exception.printStackTrace()
                        }
                    }
                    429 -> mView?.onUserDidTooManyAttempts("Too many Requests")
                    500 -> mView?.onServerError()
                    503 -> mView?.onSystemUpgrading()
                }
            }
            is SocketTimeoutException, is UnknownHostException -> mView?.onServerError()
            is SSLHandshakeException, is ConnectException -> mView?.onTimeOutError()
        }
    }
}


/**
 *  .........................................................................
 *  SocketTimeoutException  -> Might be internet connection is okay but could not connect with server. It is basically SERVER ISSUE.
 *  SSLHandshakeException   -> Might be the internet connection is slow or your app cannot connect to the server. It is basically low CONNECTIVITY ISSUE.
 *  Connect Exception       -> This is SERVER ISSUE
 */