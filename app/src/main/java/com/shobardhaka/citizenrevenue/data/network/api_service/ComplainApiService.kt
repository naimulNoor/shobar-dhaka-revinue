package com.shobardhaka.citizenrevenue.data.network.api_service

import com.shobardhaka.citizenrevenue.data.network.APIs
import com.shobardhaka.citizenrevenue.data.network.api_response.*
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ComplainApiService {

    @Multipart
    @POST(APIs.ADD_COMPLAIN)
    fun addComplain(@PartMap partMap: HashMap<String, RequestBody>,
                    @Part attachment: MultipartBody.Part?): Single<PostDataResponse>

    @GET
    fun getComplains(@Url url: String): Single<GetComplainResponse>

    @GET(APIs.GET_COMPLAIN_DETAILS)
    fun getComplainDetails(@Path("complain_id") complainId: String): Single<ComplainDetailsResponse>

    @GET
    fun reComplaint(@Url url: String): Single<PostDataResponse>

    @GET(APIs.GET_DNCC_AREAS)
    fun getDnccAreas(): Single<DnccAddressResponse>

    @FormUrlEncoded
    @POST(APIs.SUBMIT_RATING)
    fun submitRating(@FieldMap dataMap: HashMap<String, String>): Single<PostDataResponse>

    @GET(APIs.ISSUE_SUB_TYPE + "/{category_id}")
    fun getSubIssueType(@Path("category_id") categoryId: String): Single<IssueTypeResponse>

}