package com.shobardhaka.citizenrevenue.data.network

import android.content.Context
import com.shobardhaka.citizenrevenue.data.network.api_response.NotificationResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.ProfileResponse
import com.shobardhaka.citizenrevenue.data.network.api_service.UserApiService
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.data.prefs.PreferenceManager
import com.shobardhaka.citizenrevenue.rx.AppSchedulerProvider
import com.shobardhaka.citizenrevenue.utils.AppUtils
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.ArrayList

/**
 * Created on 19/08/2019.
 */
@Singleton
class RetrofitHelperService @Inject constructor() {

    @Inject
    lateinit var appSchedulerProvider: AppSchedulerProvider

    @Inject
    lateinit var userApiService: UserApiService

    @Inject
    lateinit var mPrefs: PreferenceManager

    @Inject
    lateinit var context: Context


    fun loadProfile(compositeDisposable: CompositeDisposable, profileLoadListener: OnProfileLoadListener) {
        val installedAppVersion = AppUtils.shared.getVersionName(context)
        compositeDisposable.add(
            userApiService.getProfile(installedAppVersion)
                .subscribeOn(appSchedulerProvider.io())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : DisposableSingleObserver<ProfileResponse>() {
 
                    override fun onSuccess(response: ProfileResponse) {
                        val userMap = HashMap<String, String>()
                        var emergencyContact = ""
                        var emergencyContactName = ""
                        var profilePic = ""
                        var dob = ""
                        response.emergencyContactNumber?.let { emergencyContact = response.emergencyContactNumber!! }
                        response.emergencyContactName?.let { emergencyContactName = response.emergencyContactName!! }
                        response.dob?.let { dob = response.dob!! }
                        response.image?.let {
                            profilePic = "${APIs.BASE_URL}/assets/upload/users/${response.image}"
                        }
                        userMap[Keys.ProfileKey.NAME.name] = response.name
                        userMap[Keys.ProfileKey.USER_ID.name] = response.userId
                        userMap[Keys.ProfileKey.MOBILE_NUMBER.name] = response.phone
                        userMap[Keys.ProfileKey.GENDER.name] = response.gender ?: ""
                        userMap[Keys.ProfileKey.DATE_OF_BIRTH.name] = dob
                        userMap[Keys.ProfileKey.EMERGENCY_CONTACT.name] = emergencyContact
                        userMap[Keys.ProfileKey.EMERGENCY_CONTACT_NAME.name] = emergencyContactName
                        userMap[Keys.ProfileKey.PROFILE_PIC.name] = profilePic

                        mPrefs.putStringHashMap(Keys.PROFILE.name, userMap)

                        mPrefs.putString(Keys.ProfileKey.EMERGENCY_CONTACT.name, emergencyContact)
                        mPrefs.putString(Keys.ProfileKey.NAME.name, response.name)
                        mPrefs.putString(Keys.ProfileKey.MOBILE_NUMBER.name, response.phone)
                        mPrefs.putString(Keys.ProfileKey.PROFILE_PIC.name, profilePic)

                        profileLoadListener.onProfileLoaded()
                    }

                    override fun onError(e: Throwable) {

                    }

                })
        )

    }

    fun loadNotifications(compositeDisposable: CompositeDisposable, completionListener: OnCompletionListener) {
        compositeDisposable.add(
            userApiService.getNotifications()
                .subscribeOn(appSchedulerProvider.io())
                .unsubscribeOn(appSchedulerProvider.computation())
                .observeOn(appSchedulerProvider.ui())
                .subscribeWith(object : DisposableSingleObserver<NotificationResponse>() {
                    override fun onSuccess(response: NotificationResponse) {

                        val notifications = response.notifications
                        val seenNotifications: ArrayList<String> = mPrefs.getSeenNotificationList()
                        var unSeenNotificationCount = 0

                        if (notifications.isEmpty()) {
                            mPrefs.putUnseenNotificationCount(0)
                            mPrefs.putStringArrayList(Keys.seen_notification_list.name, ArrayList<String>())
                        } else if (notifications.isNotEmpty() && seenNotifications.isEmpty()) {
                            mPrefs.putUnseenNotificationCount(notifications.size)
                        } else if (notifications.isNotEmpty() && seenNotifications.isNotEmpty()) {
                            for (notification in notifications) {
                                if (!seenNotifications.contains("${notification!!.id}")) {
                                    unSeenNotificationCount++
                                }
                            }
                            mPrefs.putUnseenNotificationCount(unSeenNotificationCount)
                        }
                        completionListener.onComplete()
                    }

                    override fun onError(e: Throwable) {

                    }
                })
        )
    }


    interface OnProfileLoadListener {

        fun onProfileLoaded()
    }

    interface OnCompletionListener {
        fun onComplete()
    }
}
