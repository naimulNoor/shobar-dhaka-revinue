package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GetEmergencyServiceResponse(
        @Expose
        @SerializedName("services")
        var emergencyServices: ArrayList<SingleEmergencyService?>) {

    data class SingleEmergencyService(
            @Expose
            @SerializedName("id")
            var id: Int,
            @Expose
            @SerializedName("name")
            val name: String,
            @Expose
            @SerializedName("number")
            val number: String,
            @Expose
            @SerializedName("created_at")
            val createdAt: String,
            @Expose
            @SerializedName("updated_at")
            val updatedAt: String)
}

