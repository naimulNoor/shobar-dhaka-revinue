package com.shobardhaka.citizenrevenue.data.prefs

enum class Keys {

    ACCESS_TOKEN,
    KEEP_ME_LOGGED_IN,
    LANGUAGE,
    PROFILE,

    DATA_BUNDLE,
    COMPLAINT_TYPE,
    COMPLAINT_ICON,
    COMPLAINT_MSG,
    COMPLAINT_ID,
    mobile_number,

    is_phone_number_verified,
    DEVICE_TOKEN,

    cat_name,
    cat_slug,

    single_complain,
    SINGLE_PLACE,
    single_notification,
    single_disaster,
    single_event,

    // ABOUT CITY CORPORATION
    PROFILE_TYPE,
    MEYOR,
    CEO,

    // PDF
    PDF_LINK,
    TITLE,

    // NEARBY
    CURRENT_LOC_LAT,
    CURRENT_LOC_LONG,
    LOCATION_TYPE,
    NEARBY_ICON,
    NEARBY_ALL,
    NEARBY_BUS_STAND,
    NEARBY_HOSPITAL,
    NEARBY_MATERNITY_CLINIC,
    NEARBY_POLICE_STATION,
    NEARBY_FIRE_SERVICE,
    NEARBY_PUBLIC_TOILET,
    NEARBY_ATM,
    NEARBY_PARKING,
    NEARBY_PHARMACY,
    NEARBY_COUNCILOR_OFC,
    NEARBY_CNG_PUMP,
    NEARBY_PARK,
    NEARBY_COMMUNITY_CENTER,
    NEARBY_STS,

    DNCC_OFFICE,
    UNDER_DNCC,

    // COMPLAIN
    SOLVED,
    UNSOLVED,
    ALL,

    UNSEEN_NOT_COUNT,
    UNSEEN_DISASTER_COUNT,


    //NOTIFICAITON
    un_seen_notification_count,
    seen_notification_list,

    // Fragment
    HOME,
    DNCC,
    COMPLAIN,
    OTHERS,

    // SETTINS
    IS_NOTIFICATION_ENABLED,
    APP_LANGUAGE,
    locatoin,
    NOTIFCIATION_TYPE;


   enum class ProfileKey{
       NAME,
       USER_ID,
       EMAIL,
       GENDER,
       PROFILE_PIC,
       MOBILE_NUMBER,
       DATE_OF_BIRTH,
       EMERGENCY_CONTACT,
       EMERGENCY_CONTACT_NAME,
   }
}