package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AuthenticateNumberResponse(
    @Expose
    @SerializedName("status")
    var statusCode: Int,
    @Expose
    @SerializedName("token")
    var token: String? = null
)