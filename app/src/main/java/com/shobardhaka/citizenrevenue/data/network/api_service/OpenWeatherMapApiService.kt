package com.shobardhaka.citizenrevenue.data.network.api_service

import com.shobardhaka.citizenrevenue.data.network.APIs
import com.shobardhaka.citizenrevenue.data.network.api_response.GetCurrentWeatherResponse
import io.reactivex.Single
import retrofit2.http.GET

interface OpenWeatherMapApiService{

    @GET(APIs.GET_CURRENT_WEATHER)
    fun getCurrentWeatherOfDhakaCity(): Single<GetCurrentWeatherResponse>
}