package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GetNoticeResponse(

    @Expose
    @SerializedName("notices")
    var notices: ArrayList<SingleNotice?>?
) {
    data class SingleNotice(
        @Expose
        @SerializedName("id")
        var id: Int,
        @Expose
        @SerializedName("title")
        val title: String,
        @Expose
        @SerializedName("decription")
        val decription: String,
        @Expose
        @SerializedName("status")
        val status: String,
        @Expose
        @SerializedName("created_at")
        val createdAt: String,
        @Expose
        @SerializedName("updated_at")
        val updatedAt: String
    )
}

