package com.shobardhaka.citizenrevenue.data.network.api_service

import com.shobardhaka.citizenrevenue.data.network.APIs
import com.shobardhaka.citizenrevenue.data.network.api_response.RequestOtpResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.VerifyOtpResponse
import io.reactivex.Single
import retrofit2.http.*

interface UserAuthApiService {

    @FormUrlEncoded
    @POST(APIs.VERIFY_OTP)
    fun verifyOtp(
        @Field("phone") phone: String,
        @Field("otp") otp: String
    ): Single<VerifyOtpResponse>

    @FormUrlEncoded
    @POST(APIs.GET_OTP)
    fun getOtp(@Field("phone") phone: String, @Field("signing_key") signingKey: String): Single<RequestOtpResponse>
}