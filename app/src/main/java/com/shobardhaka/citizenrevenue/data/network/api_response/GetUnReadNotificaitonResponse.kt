package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GetUnReadNotificaitonResponse(
    @Expose
    @SerializedName("notification")
    var notification: Int,
    @Expose
    @SerializedName("alert")
    var alert: Int,
    @Expose
    @SerializedName("total")
    var total: Int
)