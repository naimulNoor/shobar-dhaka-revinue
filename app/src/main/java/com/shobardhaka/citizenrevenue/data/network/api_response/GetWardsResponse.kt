package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GetWardsResponse(
    @Expose
    @SerializedName("zone")
    var zone: Map<String, ArrayList<String>>
)
