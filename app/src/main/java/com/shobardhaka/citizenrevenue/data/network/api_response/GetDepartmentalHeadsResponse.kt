package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GetDepartmentalHeadsResponse(

    @Expose
    @SerializedName("heads")
    var heads: ArrayList<SingleHeads?>
) {
    data class SingleHeads(
        @Expose
        @SerializedName("id")
        var id: Int,
        @Expose
        @SerializedName("name")
        val name: String,
        @Expose
        @SerializedName("designation")
        val designation: String,
        @Expose
        @SerializedName("office_address")
        val officeAddress: String,
        @Expose
        @SerializedName("intercom_no")
        val intercomNo: String,
        @Expose
        @SerializedName("room_no")
        val roomNo: String,
        @Expose
        @SerializedName("phone")
        val phone: String?,
        @Expose
        @SerializedName("created_at")
        val createdAt: String,
        @Expose
        @SerializedName("updated_at")
        val updatedAt: String,
        @Expose
        @SerializedName("departmental_head_image")
        val departmentalHeadImage: String?
    )
}

