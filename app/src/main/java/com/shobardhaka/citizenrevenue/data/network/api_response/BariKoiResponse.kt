package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class BariKoiResponse(
    @Expose
    @SerializedName("status")
    var statusCode: String? = null,

    @Expose
    @SerializedName("message")
    var message: String? = null,

    @Expose
    @SerializedName("Place")
    var nearByPlaces: ArrayList<SinglePlace?>? = ArrayList()
) {
    data class SinglePlace(
        @Expose
        @SerializedName("id")
        var id: Int,
        @Expose
        @SerializedName("name")
        val name: String,
        @Expose
        @SerializedName("distance_in_meters")
        val distanceInMeters: Double,
        @Expose
        @SerializedName("longitude")
        val longitude: String,
        @Expose
        @SerializedName("latitude")
        val latitude: String,
        @Expose
        @SerializedName("Address")
        val address: String,
        @Expose
        @SerializedName("city")
        val city: String,
        @Expose
        @SerializedName("area")
        val area: String,
        @Expose
        @SerializedName("pType")
        val ptype: String,
        @Expose
        @SerializedName("subType")
        val subtype: String,
        @Expose
        @SerializedName("uCode")
        val ucode: String
    ): Serializable
}