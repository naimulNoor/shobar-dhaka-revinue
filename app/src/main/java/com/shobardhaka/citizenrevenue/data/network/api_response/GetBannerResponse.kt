package com.shobardhaka.citizenrevenue.data.network.api_response
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GetBannerResponse(

    @Expose
    @SerializedName("banner")
    var banner: Banner?
) {
    data class Banner(
        @Expose
        @SerializedName("id")
        var id: Int,
        @Expose
        @SerializedName("company_name")
        val companyName: String?,
        @Expose
        @SerializedName("link")
        val link: String,
        @Expose
        @SerializedName("image")
        val image: String,
        @Expose
        @SerializedName("start_date")
        val startDate: String,
        @Expose
        @SerializedName("end_date")
        val endDate: String,
        @Expose
        @SerializedName("created_at")
        val createdAt: String,
        @Expose
        @SerializedName("updated_at")
        val updatedAt: String,
        @Expose
        @SerializedName("banner_image")
        val bannerImage: String
    )
}
