package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created  on 19/08/2019.
 */
data class ProfileResponse(

        @Expose
        @SerializedName("id")
        var id: Int,
        @Expose
        @SerializedName("user_id")
        var userId: String,
        @Expose
        @SerializedName("name")
        val name: String,
        @Expose
        @SerializedName("phone")
        val phone: String,
        @Expose
        @SerializedName("gender")
        val gender: String?,
        @Expose
        @SerializedName("dob")
        val dob: String?,
        @Expose
        @SerializedName("image")
        val image: String?,
        @Expose
        @SerializedName("email_verified_at")
        val emailVerifiedAt: String?,
        @Expose
        @SerializedName("status")
        var status: Boolean?,
        @Expose
        @SerializedName("emergency_contact_number")
        val emergencyContactNumber: String?,
        @Expose
        @SerializedName("emergency_contact_name")
        val emergencyContactName: String,
        @Expose
        @SerializedName("created_at")
        val createdAt: String,
        @Expose
        @SerializedName("updated_at")
        val updatedAt: String
)