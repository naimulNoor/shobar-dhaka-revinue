package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class GetEventResponse(

    @Expose
    @SerializedName("events")
    var events: ArrayList<SingleEvent?>
) {
    data class SingleEvent(
        @Expose
        @SerializedName("id")
        var id: Int,
        @Expose
        @SerializedName("category_id")
        var categoryId: Int,
        @Expose
        @SerializedName("title")
        val title: String,
        @Expose
        @SerializedName("description")
        val description: String,
        @Expose
        @SerializedName("image")
        val image: String,
        @Expose
        @SerializedName("venue")
        val venue: String,
        @Expose
        @SerializedName("event_date")
        val eventDate: String,
        @Expose
        @SerializedName("join_yn")
        val joinYn: String,
        @Expose
        @SerializedName("interested_yn")
        val interestedYn: String,
        @Expose
        @SerializedName("created_at")
        val createdAt: String,
        @Expose
        @SerializedName("updated_at")
        val updatedAt: String,
        @Expose
        @SerializedName("event_month")
        val eventMonth: String,
        @Expose
        @SerializedName("event_day")
        val eventDay: String,
        @Expose
        @SerializedName("event_time")
        val eventTime: String,
        @Expose
        @SerializedName("event_image")
        val eventImage: String,
        @Expose
        @SerializedName("join")
        var join: Join?,
        @Expose
        @SerializedName("category")
        var category: Category
    ) : Serializable

    data class Category(
        @Expose
        @SerializedName("id")
        var id: Int,
        @Expose
        @SerializedName("name")
        val name: String,
        @Expose
        @SerializedName("created_at")
        val createdAt: String,
        @Expose
        @SerializedName("updated_at")
        val updatedAt: String
    ) : Serializable

    data class Join(
        @Expose
        @SerializedName("id")
        var id: Int,
        @Expose
        @SerializedName("user_id")
        val userId: String,
        @Expose
        @SerializedName("event_id")
        val eventId: String,
        @Expose
        @SerializedName("join_yn")
        val joinYn: String,
        @Expose
        @SerializedName("interested_yn")
        val interestedYn: String,
        @Expose
        @SerializedName("created_at")
        val createdAt: String,
        @Expose
        @SerializedName("updated_at")
        val updatedAt: String
    ) : Serializable
}


//
//    @Expose
//    @SerializedName("events")
//    var events: ArrayList<SingleEvent?>
//) {
//    data class SingleEvent(
//        @Expose
//        @SerializedName("id")
//        var id: Int,
//        @Expose
//        @SerializedName("category_id")
//        var categoryId: Int,
//        @Expose
//        @SerializedName("title")
//        val title: String,
//        @Expose
//        @SerializedName("description")
//        val description: String,
//        @Expose
//        @SerializedName("image")
//        val image: String,
//        @Expose
//        @SerializedName("venue")
//        val venue: String,
//        @Expose
//        @SerializedName("event_date")
//        val eventDate: String,
//        @Expose
//        @SerializedName("join_yn")
//        val joinYn: String,
//        @Expose
//        @SerializedName("interested_yn")
//        val interestedYn: String,
//        @Expose
//        @SerializedName("created_at")
//        val createdAt: String,
//        @Expose
//        @SerializedName("updated_at")
//        val updatedAt: String,
//        @Expose
//        @SerializedName("event_month")
//        val eventMonth: String,
//        @Expose
//        @SerializedName("event_day")
//        val eventDay: String,
//        @Expose
//        @SerializedName("event_time")
//        val eventTime: String,
//        @Expose
//        @SerializedName("event_image")
//        val eventImage: String,
//        @Expose
//        @SerializedName("category")
//        var category: Category,
//        @Expose
//        @SerializedName("join")
//        val join: ArrayList<Join>?
//
//    ) : Serializable
//
//    data class Category(
//        @Expose
//        @SerializedName("id")
//        var id: Int,
//        @Expose
//        @SerializedName("name")
//        val name: String,
//        @Expose
//        @SerializedName("created_at")
//        val createdAt: String,
//        @Expose
//        @SerializedName("updated_at")
//        val updatedAt: String
//    ) : Serializable
//
//    data class Join(
//        @Expose
//        @SerializedName("id")
//        var id: Int,
//        @Expose
//        @SerializedName("user_id")
//        val userId: String,
//        @Expose
//        @SerializedName("event_id")
//        val eventId: String,
//        @Expose
//        @SerializedName("join_yn")
//        val joinYn: String,
//        @Expose
//        @SerializedName("interested_yn")
//        val interestedYn: String,
//        @Expose
//        @SerializedName("created_at")
//        val createdAt: String,
//        @Expose
//        @SerializedName("updated_at")
//        val updatedAt: String
//    ):Serializable
//}
