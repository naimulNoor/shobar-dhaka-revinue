package com.shobardhaka.citizenrevenue.data.network.api_service

import com.shobardhaka.citizenrevenue.data.network.APIs
import com.shobardhaka.citizenrevenue.data.network.api_response.GetEmergencyServiceResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.PostDataResponse
import io.reactivex.Single
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface EmergencyApiService{

    @GET(APIs.GET_EMERGENCY_SERVICES)
    fun getEmergencyServices(): Single<GetEmergencyServiceResponse>

    @FormUrlEncoded
    @POST(APIs.SEND_EMERGENCY_ALERT)
    fun sendEmergencyAlert(@FieldMap dataMap: HashMap<String, String>): Single<PostDataResponse>

}