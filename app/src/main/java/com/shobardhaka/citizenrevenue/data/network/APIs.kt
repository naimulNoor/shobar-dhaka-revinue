package com.shobardhaka.citizenrevenue.data.network

object APIs {

    //const val BASE_URL = "https://dev.shobardhaka.com"
    const val BASE_URL = "https://shobardhaka.com"
    private const val SUB_URL = "/api"

    // AUTH
    const val LOGIN = "$SUB_URL/user/login"
    const val REGISTRATION = "$SUB_URL/user/register"
    const val LOGOUT = "$SUB_URL/user/logout"
    const val AUTHENTICATE_NUMBER = "$SUB_URL/user/authenticate-number"

    // PROFILE
    const val GET_PROFILE = "$SUB_URL/user/get-profile"
    const val UPDATE_PROFILE = "$SUB_URL/user/update-profile"

    // COMPLAINTS
    const val ADD_COMPLAIN = "$SUB_URL/user/add-complain"
    const val GET_COMPLAIN = "$SUB_URL/user/get-complain"
    const val GET_COMPLAIN_DETAILS = "$SUB_URL/user/get-complain-details/{complain_id}"
    const val COMPLAIN_STATUS = "$SUB_URL/user/customer-complain-status"
    const val RE_COMPLAIN = "$BASE_URL$SUB_URL/user/can-recomplain"

    // EMERGENCY SERVICES
    const val GET_EMERGENCY_SERVICES = "$SUB_URL/user/get-emergency"
    const val SEND_EMERGENCY_ALERT = "$SUB_URL/user/send-emergency-alert"

    const val ASSET_PREFIX = "$BASE_URL/assets/upload/complains/"

    // NOTIFICATION
    const val NOTIFICATION = "$SUB_URL/user/get-user-notification"
    const val STORE_DEVICE_TOKEN = "$SUB_URL/user/store-device-token"
    const val REMOVE_DEVICE_TOKEN = "$SUB_URL/user/remove-device-token"

    // GET COUNCILOR
    const val GET_COUNCILOR = "$SUB_URL/user/get-councilor-zone-ward/{zone}/{ward}"
    const val GET_DEPARTMENTAL_HEADS = "$SUB_URL/user/get-departmental-head"
    const val GET_NOTICE = "$SUB_URL/user/get-notice"
    const val GET_WARDS = "$SUB_URL/user/get-wards"

    // EVENT
    const val GET_EVENTS = "$SUB_URL/user/get-events/0"
    const val GET_ALERT = "$SUB_URL/user/get-alert"
    const val GET_BANNER = "$SUB_URL/user/get-banner"
    const val JOIN_EVENT = "$SUB_URL/user/join-event"

    // SEEN Alert + Notification
    const val UNSEEN_NOTIFICATION = "$SUB_URL/user/total-notifications"
    const val GET_APP_VERSION = "$SUB_URL/user/get-app-version"
    const val GET_DISASTER_DETAILS = "$BASE_URL$SUB_URL/user/get-detail-alert/"
    const val GET_NOTIFICATION_DETAILS = "$BASE_URL$SUB_URL/user/get-notification-detail/"
    const val GET_CURRENT_WEATHER = "$SUB_URL/user/current-weather"


    const val GET_OTP = "$SUB_URL/user/send-otp"
    const val VERIFY_OTP ="$SUB_URL/user/authenticate-otp"

    const val GET_MAYOR_CEO_INFO ="$SUB_URL/user/get-ceo-mayor"
    const val ISSUE_SUB_TYPE ="$SUB_URL/user/category"
    const val SUBMIT_RATING ="$SUB_URL/user/submit-rating"
    const val UPDATE_GENDER = "$SUB_URL/user/gender"


    const val GET_DNCC_AREAS ="$SUB_URL/user/get-dncc-areas"

    val NO_AUTH = arrayOf(
        "$BASE_URL$LOGIN",
        "$BASE_URL$REGISTRATION",
        "$BASE_URL$AUTHENTICATE_NUMBER",
        "$BASE_URL$GET_OTP",
        "$BASE_URL$VERIFY_OTP"
    )
}