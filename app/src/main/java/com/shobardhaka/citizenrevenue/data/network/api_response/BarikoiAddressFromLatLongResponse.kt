package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class BarikoiAddressFromLatLongResponse(

    @Expose
    @SerializedName("place")
    val place: Place?,
    @Expose
    @SerializedName("status")
    val status: Int
) {
    data class Place(
        @Expose
        @SerializedName("id")
        val id: Int,
        @Expose
        @SerializedName("distance_within_meters")
        val distanceWithinMeters: Double,
        @Expose
        @SerializedName("address")
        val address: String,
        @Expose
        @SerializedName("area")
        val area: String,
        @Expose
        @SerializedName("city")
        val city: String
    )
}

