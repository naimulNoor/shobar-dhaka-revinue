package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ProfileInfoResponse(
    @Expose
    @SerializedName("current_page")
    var currentPage: Int
)