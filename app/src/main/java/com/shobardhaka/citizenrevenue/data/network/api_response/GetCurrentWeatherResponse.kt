package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GetCurrentWeatherResponse(

    @Expose
    @SerializedName("coord")
    var coord: Coord,
    @Expose
    @SerializedName("weather")
    var weather: List<Weather>,
    @Expose
    @SerializedName("base")
    val base: String,
    @Expose
    @SerializedName("main")
    var main: Main,
    @Expose
    @SerializedName("visibility")
    var visibility: Int,
    @Expose
    @SerializedName("wind")
    var wind: Wind,
    @Expose
    @SerializedName("clouds")
    var clouds: Clouds,
    @Expose
    @SerializedName("dt")
    var dt: Int,
    @Expose
    @SerializedName("sys")
    var sys: Sys,
    @Expose
    @SerializedName("timezone")
    var timezone: Int,
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("name")
    val name: String,
    @Expose
    @SerializedName("cod")
    var cod: Int
) {

    data class Sys(
        @Expose
        @SerializedName("type")
        var type: Int,
        @Expose
        @SerializedName("id")
        var id: Int,
        @Expose
        @SerializedName("country")
        val country: String,
        @Expose
        @SerializedName("sunrise")
        var sunrise: Int,
        @Expose
        @SerializedName("sunset")
        var sunset: Int
    )

    data class Clouds(
        @Expose
        @SerializedName("all")
        var all: Int
    )

    data class Wind(
        @Expose
        @SerializedName("speed")
        val speed: Double,
        @Expose
        @SerializedName("deg")
        var deg: Int
    )

    data class Main(
        @Expose
        @SerializedName("temp")
        var temp: Int,
        @Expose
        @SerializedName("pressure")
        var pressure: Int,
        @Expose
        @SerializedName("humidity")
        var humidity: Int,
        @Expose
        @SerializedName("temp_min")
        var tempMin: Int,
        @Expose
        @SerializedName("temp_max")
        var tempMax: Int
    )

    data class Weather(
        @Expose
        @SerializedName("id")
        var id: Int,
        @Expose
        @SerializedName("main")
        val main: String,
        @Expose
        @SerializedName("description")
        val description: String,
        @Expose
        @SerializedName("icon")
        val icon: String
    )

    data class Coord(
        @Expose
        @SerializedName("lon")
        val lon: Double,
        @Expose
        @SerializedName("lat")
        val lat: Double
    )
}
