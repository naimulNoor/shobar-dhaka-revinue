package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

data class IssueTypeResponse(
    @Expose
    @SerializedName("status")
    val status: Int,
    @Expose
    @SerializedName("data")
    val data: ArrayList<SingleSubIssueType>?
) {
    data class SingleSubIssueType(
        @Expose
        @SerializedName("id")
        val id: Int,
        @Expose
        @SerializedName("name")
        val name: String,
        @Expose
        @SerializedName("name_bn")
        val nameBn: String?,
        @Expose
        @SerializedName("created_at")
        val createdAt: String,
        @Expose
        @SerializedName("updated_at")
        val updatedAt: String,
        @Expose
        @SerializedName("parent_id")
        val parentId: Int,
        @Expose
        @SerializedName("status")
        val status: Int,
        @Expose
        @SerializedName("second_category")
        val secondCategory: ArrayList<SingleSubIssueType>
    )
}

