package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class GetDisasterResponse(

    @Expose
    @SerializedName("disaster")
    var disaster: ArrayList<SingleDisaster?>
) {
    data class SingleDisaster(
        @Expose
        @SerializedName("id")
        var id: Int,
        @Expose
        @SerializedName("title")
        val title: String,
        @Expose
        @SerializedName("description")
        val description: String,
        @Expose
        @SerializedName("disaster_image")
        val image: String,
        @Expose
        @SerializedName("created_at")
        val createdAt: String,
        @Expose
        @SerializedName("updated_at")
        val updatedAt: String,
        @Expose
        @SerializedName("disaster_month")
        val disasterMonth: String,
        @Expose
        @SerializedName("disaster_day")
        val disasterDay: String,
        @Expose
        @SerializedName("disaster_time")
        val disasterTime: String,
        @Expose
        @SerializedName("read")
        var isRead: Int
    ): Serializable
}

