package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class JoinEventResponse(

    @Expose
    @SerializedName("status")
    var status: Int,

    @Expose
    @SerializedName("error")
    val error: String,

    @Expose
    @SerializedName("message")
    var message: String? = null
)