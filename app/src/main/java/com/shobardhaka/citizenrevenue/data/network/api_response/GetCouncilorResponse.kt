package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GetCouncilorResponse(
    @Expose
    @SerializedName("councilors")
    var councilors: ArrayList<SingleCouncilor?>
) {
    data class SingleCouncilor(
        @Expose
        @SerializedName("id")
        var id: Int,
        @Expose
        @SerializedName("name")
        val name: String,
        @Expose
        @SerializedName("designation")
        val designation: String,
        @Expose
        @SerializedName("zone")
        val zone: String,
        @Expose
        @SerializedName("ward")
        val word: String,
        @Expose
        @SerializedName("phone")
        val phone: String?,
        @Expose
        @SerializedName("mp")
        val mp: String?,
        @Expose
        @SerializedName("election_area_name")
        val electionAreaName: String,
        @Expose
        @SerializedName("created_at")
        val createdAt: String,
        @Expose
        @SerializedName("updated_at")
        val updatedAt: String,
        @Expose
        @SerializedName("councilor_image")
        val councilorImage: String?
    )
}

