package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ComplainDetailsResponse(

    @Expose
    @SerializedName("complain")
    val complain: Complain
) {
    data class Complain(
        @Expose
        @SerializedName("id")
        val id: Int,
        @Expose
        @SerializedName("complain_id")
        val complainId: String,
        @Expose
        @SerializedName("user_id")
        val userId: String,
        @Expose
        @SerializedName("category_id")
        val categoryId: String,
        @Expose
        @SerializedName("image")
        val image: String,
        @Expose
        @SerializedName("description")
        val description: String,
        @Expose
        @SerializedName("location")
        val location: String,
        @Expose
        @SerializedName("user_location")
        val userLocation: String,
        @Expose
        @SerializedName("latitude")
        val latitude: String,
        @Expose
        @SerializedName("longitude")
        val longitude: String,
        @Expose
        @SerializedName("zone")
        val zone: String,
        @Expose
        @SerializedName("ward")
        val ward: String,
        @Expose
        @SerializedName("re_complain")
        val reComplain: String,
        @Expose
        @SerializedName("complain_status")
        val complainStatus: Int,
        @Expose
        @SerializedName("complain_type")
        val complainType: Int?,
        @Expose
        @SerializedName("last_role")
        val lastRole: Int,
        @Expose
        @SerializedName("created_at")
        val createdAt: String,
        @Expose
        @SerializedName("updated_at")
        val updatedAt: String,
        @Expose
        @SerializedName("read_status")
        val readStatus: Int,
        @Expose
        @SerializedName("can_recomplain")
        val canRecomplain: Int?,
        @Expose
        @SerializedName("can_rating")
        val canRating: Int?,
        @Expose
        @SerializedName("solution")
        val solution: Solution,
        @Expose
        @SerializedName("category")
        val category: Category?,
        @Expose
        @SerializedName("sub_category")
        val subCategory: Category?,
        @Expose
        @SerializedName("issue")
        val issue: Category?,
        @Expose
        @SerializedName("actions")
        val actions: List<SingleComplainAction>,
        @Expose
        @SerializedName("get_rating")
        val getRating: GetRating?
    )

    data class Category(
        @Expose
        @SerializedName("id")
        val id: Int,
        @Expose
        @SerializedName("name")
        val name: String,
        @Expose
        @SerializedName("name_bn")
        val nameBn: String
    )

    data class Solution(
        @Expose
        @SerializedName("message")
        val message: String?
    )

    data class SingleComplainAction(
        @Expose
        @SerializedName("complain_id")
        val complainId: Int,
        @Expose
        @SerializedName("action")
        val action: String,
        @Expose
        @SerializedName("created_at")
        val createdAt: String
    )
    data class GetRating(
        @Expose
        @SerializedName("rating_mark")
        val ratingMark: Int
    )
}

