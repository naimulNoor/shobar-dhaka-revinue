package com.shobardhaka.citizenrevenue.data.network.api_service

import com.shobardhaka.citizenrevenue.data.network.APIs
import com.shobardhaka.citizenrevenue.data.network.api_response.*
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface UserApiService {

    @FormUrlEncoded
    @POST(APIs.LOGIN)
    fun login(@FieldMap authData: HashMap<String, String>): Single<LoginResponse>

    @FormUrlEncoded
    @POST(APIs.AUTHENTICATE_NUMBER)
    fun authenticateNumber(@FieldMap dataMap: HashMap<String, String>): Single<AuthenticateNumberResponse>

    @FormUrlEncoded
    @POST(APIs.REGISTRATION)
    fun registration(@FieldMap registerData: HashMap<String, String>): Single<RegistrationResponse>

    @GET(APIs.GET_EMERGENCY_SERVICES)
    fun getEmergencyServices(): Single<GetEmergencyServiceResponse>

    @GET(APIs.GET_PROFILE)
    fun getProfile(@Query("installed_app_version") installedAppVersion: String,
                   @Query("device") device: String = "android"): Single<ProfileResponse>

    @Multipart
    @POST(APIs.UPDATE_PROFILE)
    fun updateProfile(
        @PartMap partMap: HashMap<String, RequestBody>,
        @Part attachment: MultipartBody.Part?
    ): Single<PostDataResponse>

    @Multipart
    @POST(APIs.REGISTRATION)
    fun updateProfileInfo(
        @PartMap partMap: HashMap<String, RequestBody>,
        @Part attachment: MultipartBody.Part?
    ): Single<RegistrationResponse>

    // GET COMPLAIN STATUS
    @GET(APIs.COMPLAIN_STATUS)
    fun getComplainStatus(): Single<ComplainStatusResponse>

    // Notification List
    @GET(APIs.NOTIFICATION)
    fun getNotifications(): Single<NotificationResponse>

    // STORE DEVICE TOKEN
    @FormUrlEncoded
    @POST(APIs.STORE_DEVICE_TOKEN)
    fun storeDeviceToken(@Field("device_token") deviceToken: String,
                         @Field("device") device: String = "android"): Single<PostDataResponse>

    @GET(APIs.REMOVE_DEVICE_TOKEN)
    fun removeDeviceToken(): Single<PostDataResponse>

    @GET(APIs.GET_COUNCILOR)
    fun getCouncilors(
        @Path("zone") zone: String,
        @Path("ward") ward: String
    ): Single<GetCouncilorResponse>

    @GET(APIs.GET_DEPARTMENTAL_HEADS)
    fun getDepartmentHeads(): Single<GetDepartmentalHeadsResponse>

    @GET(APIs.GET_NOTICE)
    fun getNotice(): Single<GetNoticeResponse>

    @GET(APIs.GET_WARDS)
    fun getZoneWards(): Single<GetWardsResponse>

    @GET
    fun checkGeoFencing(@Url url: String): Single<CheckGeoFencingResponse>

    @GET(APIs.GET_EVENTS)
    fun getEvents(): Single<GetEventResponse>


    @GET(APIs.GET_ALERT)
    fun getDisasters(): Single<GetDisasterResponse>


    @GET(APIs.GET_BANNER)
    fun getBanner(): Single<GetBannerResponse>

    // STORE DEVICE TOKEN

    @FormUrlEncoded
    @POST(APIs.JOIN_EVENT)
    fun joinEvent(@FieldMap eventData: HashMap<String, String>): Single<JoinEventResponse>

    // Read / Unread
    @GET
    fun setDisasterAsSeen(@Url url: String): Single<GetDisasterResponse.SingleDisaster>

    @GET
    fun setNotificationAsSeen(@Url url: String): Single<NotificationResponse.SingleNotification>

    @GET(APIs.UNSEEN_NOTIFICATION)
    fun getUnReadNotificationCount(): Single<GetUnReadNotificaitonResponse>

    @GET(APIs.GET_APP_VERSION)
    fun getAppVersion(): Single<GetAppVersionResponse>

    @GET(APIs.GET_MAYOR_CEO_INFO)
    fun getMayorCeoInfo(): Single<MayorCeoResponse>

    @FormUrlEncoded
    @POST(APIs.UPDATE_GENDER)
    fun updateGender(@Field("gender") gender: String): Single<PostDataResponse>
}