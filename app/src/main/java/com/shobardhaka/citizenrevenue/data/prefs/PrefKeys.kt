package com.shobardhaka.citizenrevenue.data.prefs

object PrefKeys {

    // Auth
    const val IS_VERIFIED = "is_verified"
    const val IS_REGISTERED = "is_registered"
    const val APP_SIGNING_KEY = "app_signing_key"
}