package com.shobardhaka.citizenrevenue.data.network.api_response

import java.io.Serializable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GetComplainResponse(

    @Expose
    @SerializedName("current_page")
    var currentPage: Int,
    @Expose
    @SerializedName("data")
    var complaintList: ArrayList<SingleComplain?>,
    @Expose
    @SerializedName("first_page_url")
    val firstPageUrl: String,
    @Expose
    @SerializedName("from")
    var from: Int,
    @Expose
    @SerializedName("last_page")
    var lastPage: Int,
    @Expose
    @SerializedName("last_page_url")
    val lastPageUrl: String,
    @Expose
    @SerializedName("next_page_url")
    val nextPageUrl: String?,
    @Expose
    @SerializedName("path")
    val path: String,
    @Expose
    @SerializedName("per_page")
    var perPage: Int,
    @Expose
    @SerializedName("to")
    var to: Int,
    @Expose
    @SerializedName("total")
    var total: Int
) {

    data class SingleComplain(
        @Expose
        @SerializedName("id")
        var id: Int,
        @Expose
        @SerializedName("user_id")
        val userId: String,
        @Expose
        @SerializedName("category_id")
        val categoryId: String,
        @Expose
        @SerializedName("image")
        val image: String,
        @Expose
        @SerializedName("description")
        val description: String,
        @Expose
        @SerializedName("location")
        val location: String,
        @Expose
        @SerializedName("latitude")
        val latitude: String,
        @Expose
        @SerializedName("longitude")
        val longitude: String,
        @Expose
        @SerializedName("zone")
        val zone: String,
        @Expose
        @SerializedName("ward")
        val ward: String,
        @Expose
        @SerializedName("re_complain")
        var reComplain: Int,
        @Expose
        @SerializedName("complain_status")
        val status: String,
        @Expose
        @SerializedName("last_role")
        val lastRole: String,
        @Expose
        @SerializedName("created_at")
        val createdAt: String,
        @Expose
        @SerializedName("updated_at")
        val updatedAt: String,
        @Expose
        @SerializedName("can_recomplain")
        var canRecomplain: Int,
        @Expose
        @SerializedName("solution")
        var solution: Solution?,
        @Expose
        @SerializedName("category")
        var complainCategory: Category,
        @Expose
        @SerializedName("get_rating")
        var Rating: GetRating?
    ) : Serializable

    data class Category(
        @Expose
        @SerializedName("id")
        var categoryId: Int,
        @Expose
        @SerializedName("name")
        val name: String,
        @Expose
        @SerializedName("created_at")
        val createdAt: String,
        @Expose
        @SerializedName("updated_at")
        val updatedAt: String
    ) : Serializable

    data class Solution(
        @Expose
        @SerializedName("message")
        val message: String,
        @Expose
        @SerializedName("image")
        val image: String?
    ) : Serializable

    data class GetRating(
        @Expose
        @SerializedName("rating_mark")
        val message: Int
    ) : Serializable
}