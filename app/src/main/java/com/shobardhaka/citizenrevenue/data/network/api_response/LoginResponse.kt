package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LoginResponse(

    @Expose
    @SerializedName("status")
    var status: Int,

    @Expose
    @SerializedName("token")
    var token: String,

    @Expose
    @SerializedName("error")
    var errorMsg: String?

)