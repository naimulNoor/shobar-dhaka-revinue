package com.shobardhaka.citizenrevenue.data.network.api_response
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RegistrationResponse(

    @Expose
    @SerializedName("status")
    var status: Int,

    @Expose
    @SerializedName("token")
    var token: String,


    @Expose
    @SerializedName("errors")
    var errorsMap: Map<String, String>?

)