package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class MayorCeoResponse(
    @Expose
    @SerializedName("data")
    val data: Data
) {
    data class Data(
        @Expose
        @SerializedName("mayor_image")
        val mayorImage: String,
        @Expose
        @SerializedName("ceo_image")
        val ceoImage: String,
        @Expose
        @SerializedName("mayor_name_en")
        val mayorNameEn: String,
        @Expose
        @SerializedName("mayor_description_en")
        val mayorDescriptionEn: String,
        @Expose
        @SerializedName("ceo_name_en")
        val ceoNameEn: String,
        @Expose
        @SerializedName("cei_description_en")
        val ceiDescriptionEn: String,
        @Expose
        @SerializedName("mayor_name_bn")
        val mayorNameBn: String,
        @Expose
        @SerializedName("mayor_description_bn")
        val mayorDescriptionBn: String,
        @Expose
        @SerializedName("ceo_name_bn")
        val ceoNameBn: String,
        @Expose
        @SerializedName("cei_description_bn")
        val ceiDescriptionBn: String
    )
}
