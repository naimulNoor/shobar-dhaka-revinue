package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ComplainStatusResponse(
    @Expose
    @SerializedName("total")
    var total: Int,
    @Expose
    @SerializedName("solved")
    var solved: Int,
    @Expose
    @SerializedName("unsolved")
    var unsolved: Int
)