package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RequestOtpResponse(
    @Expose
    @SerializedName("stats")
    val status: Int,
    @Expose
    @SerializedName("message")
    val verified: String
)
