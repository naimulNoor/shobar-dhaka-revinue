package com.shobardhaka.citizenrevenue.data.network.api_response
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PostDataResponse(

    @Expose
    @SerializedName("status")
    var status: Int,

    @Expose
    @SerializedName("error")
    var error: String?,

    @Expose
    @SerializedName("errors")
    var errorsMap: Map<String, String>?

)