package com.shobardhaka.citizenrevenue.data.network

import android.content.Context
import com.shobardhaka.citizenrevenue.BuildConfig
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.data.prefs.PreferenceManager
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import com.shobardhaka.citizenrevenue.utils.NetworkUtils
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object RetrofitApiClient {

    fun getRetrofit(
        context: Context,
        networkUtils: NetworkUtils,
        preferenceManager: PreferenceManager
    ): Retrofit {

        val okHttpClientBuilder = OkHttpClient.Builder()
        okHttpClientBuilder.readTimeout(60, TimeUnit.SECONDS)
        okHttpClientBuilder.connectTimeout(60, TimeUnit.SECONDS)
        okHttpClientBuilder.writeTimeout(60, TimeUnit.SECONDS)

        okHttpClientBuilder.addInterceptor { chain ->
            if (networkUtils.isConnectedToNetwork(context)) {
                val requestBuilder = chain.request().newBuilder()
                requestBuilder.addHeader("Accept", Headers.ACCEPT)
                if (preferenceManager.getString(Keys.APP_LANGUAGE.name) == AppLanguage.ENGLISH.name) {
                    requestBuilder.addHeader("Accept-Language", "en")
                } else {
                    requestBuilder.addHeader("Accept-Language", "bn")
                }
                requestBuilder.addHeader("content-type", Headers.MULTIPART_FORM_DATA)
                if (!APIs.NO_AUTH.contains(chain.request().url().toString())) {
                    requestBuilder.addHeader(
                        "Authorization",
                        "${Headers.BEARER}${preferenceManager.getString(Keys.ACCESS_TOKEN.name)}"
                    )
                }
                chain.proceed(requestBuilder.build())
            } else {
                throw NoConnectivityException()
            }
        }

        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            okHttpClientBuilder.addInterceptor(logging)
        }

        return Retrofit.Builder()
            .baseUrl(APIs.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClientBuilder.build())
            .build()
    }
}