package com.shobardhaka.citizenrevenue.data.network.api_service

import com.shobardhaka.citizenrevenue.data.network.api_response.BariKoiResponse
import com.shobardhaka.citizenrevenue.data.network.api_response.BarikoiAddressFromLatLongResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Url

interface BariKoiApiService {

    @GET
    fun getNearByPlaces(@Url url: String): Single<BariKoiResponse>

    @GET
    fun getAddressByLatLong(@Url url: String): Single<BarikoiAddressFromLatLongResponse>

}