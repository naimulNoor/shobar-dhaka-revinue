package com.shobardhaka.citizenrevenue.data.network.api_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class VerifyOtpResponse(

    @Expose
    @SerializedName("status")
    val status: Int?,
    @Expose
    @SerializedName("verified")
    val verified: Boolean?,
    @Expose
    @SerializedName("registered")
    val registered: Boolean?,
    @Expose
    @SerializedName("token")
    val token: String?,
    @Expose
    @SerializedName("error")
    val error: String?
)
