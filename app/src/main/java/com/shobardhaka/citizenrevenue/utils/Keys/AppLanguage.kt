package com.shobardhaka.citizenrevenue.utils.Keys

enum class AppLanguage{
    ENGLISH,
    BENGALI
}