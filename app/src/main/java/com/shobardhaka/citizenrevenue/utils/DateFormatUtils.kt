package com.shobardhaka.citizenrevenue.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class DateFormatUtils {

    companion object {
        val shared = DateFormatUtils()
    }

    private val preFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    private var dateFormat = "dd MMM, yyyy hh:mm a"
    private val dateTimeFormatter = SimpleDateFormat(dateFormat)

    fun getFormattedDateTime(dateStr: String): Array<String> {
        val newFormatTime = SimpleDateFormat("hh:mm", Locale.getDefault())  // 12:00:00 pm
        val newFormatDate = SimpleDateFormat("dd MMM, yyyy", Locale.getDefault()) // Format 31 Jan, 2018
        val DATE: Date
        var date = ""
        var time = ""
        try {
            DATE = preFormat.parse(dateStr)
            date = newFormatDate.format(DATE)
            time = newFormatTime.format(DATE)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return arrayOf(date, time)
    }

    fun getTime(dateStr: String): String {
        val newFormatTime = SimpleDateFormat("hh:mm a", Locale.getDefault())  // 12:00:00 pm
        var time = ""
        try {
            time = newFormatTime.format(preFormat.parse(dateStr))
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return time
    }

    fun getDate(dateStr: String): String {
        val newFormatDate = SimpleDateFormat("dd MMM, yyyy", Locale.getDefault()) // Format 31 Jan, 2018
        var date = ""
        try {
            date = newFormatDate.format(preFormat.parse(dateStr))
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return date
    }

    fun convertMilliSecondsToTime(milliSeconds:Long): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        val ssFormatter = SimpleDateFormat("hh:mm")  // 12:00:00 pm
        ssFormatter.timeZone = TimeZone.getTimeZone("UTC")
        return ssFormatter.format(calendar.timeInMillis)
    }
    fun convertUnixToTime(unixSeconds:Long): String {
        val date = Date(unixSeconds * 1000)
        val sdf = SimpleDateFormat("hh:mm a", Locale.getDefault())  // 12:00:00 pm
       // sdf.timeZone = TimeZone.getTimeZone("GMT+06:00")
        return sdf.format(date)
    }
}