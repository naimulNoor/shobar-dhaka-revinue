package com.shobardhaka.citizenrevenue.utils
import android.content.Context
import android.net.ConnectivityManager
import androidx.annotation.NonNull

class NetworkUtils {

    companion object {
        val sharedInstance = NetworkUtils()
    }

    fun isConnectedToNetwork(@NonNull context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = connectivityManager.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnected
    }
}