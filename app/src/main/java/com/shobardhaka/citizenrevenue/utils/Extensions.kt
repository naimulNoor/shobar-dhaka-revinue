package com.shobardhaka.citizenrevenue.utils
import android.content.Context
import android.widget.Toast
/**
 * Created  on 18/07/2019.
 */

fun Context.showToast(message: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this.applicationContext, message, duration).show()
}