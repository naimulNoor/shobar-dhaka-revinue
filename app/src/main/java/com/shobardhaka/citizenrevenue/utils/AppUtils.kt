package com.shobardhaka.citizenrevenue.utils

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.provider.Settings
import androidx.annotation.NonNull
import androidx.core.content.pm.PackageInfoCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import android.telephony.SmsManager
import android.text.Html
import android.text.Spanned
import android.widget.TextView
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.shobardhaka.citizenrevenue.BuildConfig
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import java.util.*

class AppUtils private constructor() {

    companion object {
        val shared = AppUtils()
    }

    val isDebug: Boolean
        get() = BuildConfig.DEBUG

    fun getAppVersion(@NonNull context: Context): Int {
        var currentAppVersion = 0
        try {
            val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            val longVersionCode = PackageInfoCompat.getLongVersionCode(pInfo)
            currentAppVersion = longVersionCode.toInt()
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        return currentAppVersion
    }

    fun getVersionName(context: Context): String {
        return context.packageManager
            .getPackageInfo(context.packageName, 0).versionName
    }


    fun openAppOnPlayStore(@NonNull context: Context, packageName: String = "") {
        var appPackageName = packageName
        if (packageName == "") {
            appPackageName = context.applicationContext.packageName
        }
        try {
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
        } catch (anfe: android.content.ActivityNotFoundException) {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                )
            )
        }
    }


    @SuppressLint("HardwareIds")
    fun getDeviceId(@NonNull context: Context): String {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }


    /* ------------------------------------------
        Get Launcher Activity name
        ------------------------------------------
        This method return the name of launcher activity of the app
     */

    fun getLauncherActivity(@NonNull context: Context): String {
        var activityName = ""
        val pm = context.packageManager
        val intent = pm.getLaunchIntentForPackage(context.applicationContext.packageName)
        val activityList = pm.queryIntentActivities(intent, 0)
        if (activityList != null) {
            activityName = activityList[0].activityInfo.name
        }
        return activityName

    }

    /*-------------------------------------------
        Check a service is running or not
    ----------------------------------------------------- */
    fun <S : Service> isServiceRunning(
        context: Context,
        serviceClass: Class<S>
    ): Boolean {

        val activityManager = context.applicationContext.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val runningServices = activityManager.getRunningServices(Integer.MAX_VALUE)

        for (runningServiceInfo in runningServices) {
            if (serviceClass.name == runningServiceInfo.service.className) {
                return true
            }
        }
        return false
    }

    fun setLocale(context: Context, appLanguage: AppLanguage) {
        val locale = when (appLanguage) {
            AppLanguage.BENGALI -> Locale("bn")
            AppLanguage.ENGLISH -> Locale("en")
        }
        val config = Configuration()
        Locale.setDefault(locale)
        config.locale = locale
        context.resources.updateConfiguration(config, context.resources.displayMetrics)
    }

    fun dial(context: Context, phoneNumber: String) {
        val dialIntent = Intent(Intent.ACTION_DIAL)
        dialIntent.data = Uri.parse("tel:$phoneNumber")
        context.startActivity(dialIntent)
    }

    @SuppressLint("MissingPermission")
    fun call(context: Context, phoneNumber: String) {
        val callIntent = Intent(Intent.ACTION_CALL)
        callIntent.data = Uri.parse("tel:$phoneNumber")
        context.startActivity(callIntent)
    }

    fun openCamera(context: Context, REQUEST_CODE: Int) {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (cameraIntent.resolveActivity(context.packageManager) != null) {
            (context as AppCompatActivity).startActivityForResult(cameraIntent, REQUEST_CODE)
        }
    }

    fun openCamera(context: Context, imageUri: Uri, REQUEST_CODE: Int) {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
        if (cameraIntent.resolveActivity(context.packageManager) != null) {
            (context as AppCompatActivity).startActivityForResult(cameraIntent, REQUEST_CODE)
        }
    }

    fun openGallery(context: Context, REQUEST_CODE: Int) {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        (context as AppCompatActivity).startActivityForResult(galleryIntent, REQUEST_CODE)
    }

    fun goToSettings(context: Context) {
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.parse("package:" + context.applicationContext.packageName)
        intent.data = uri
        context.startActivity(intent)
    }

    fun openLinkOnExternalBrowser(context: Context, link: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
        context.startActivity(browserIntent)
    }

    fun openFacebookPage(context: Context, FB_PAGE_URL: String) {
        return try {
            context.packageManager.getPackageInfo("com.facebook.katana", 0)
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("fb://facewebmodal/f?href=$FB_PAGE_URL")))
        } catch (e: Exception) {
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(FB_PAGE_URL)))
        }
    }

    fun getBitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
        return BitmapDescriptorFactory.fromBitmap(getBitmapFromVectorDrawable(context, vectorResId))
    }

    fun getBitmapFromVectorDrawable(context: Context, drawableId: Int): Bitmap {
        var drawable = AppCompatResources.getDrawable(context, drawableId)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable!!)).mutate()
        }

        val bitmap = Bitmap.createBitmap(
            drawable!!.intrinsicWidth,
            drawable.intrinsicHeight, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }

    @SuppressWarnings("deprecation")
    fun fromHtml(html: String): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            // FROM_HTML_MODE_LEGACY is the behaviour that was used for versions below android N
            // we are using this flag to give a consistent behaviour
            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(html)
        }
    }

    fun showIntegerValueWithAnim(textView: TextView, value: Int) {
        val animator: ValueAnimator = ValueAnimator.ofInt(0, value)
        animator.duration = 500
        animator.addUpdateListener {
            textView.text = "${animator.animatedValue}"
        }
        animator.start()
    }

    fun sendSMS(mContext: Context,
                phoneNo: String,
                messageInfo: String) {
        val SENT = "SMS_SENT"
        val DELIVERED = "SMS_DELIVERED"
        try {
            val sentPI = PendingIntent.getBroadcast(mContext, 0, Intent(SENT), 0)
            val deliveredPI = PendingIntent.getBroadcast(mContext, 0, Intent(DELIVERED), 0)


            val smsManager: SmsManager = SmsManager.getDefault()
            smsManager.sendTextMessage(phoneNo, null, messageInfo, sentPI, deliveredPI)
            AlertService().showAlert(mContext, null, "আপনার জরুরী ম্যাসেজটি পাঠানো হয়েছে।")
        } catch (ex: Exception) {
            mContext.showToast("${ex.message}")
        }
    }

    fun getInBangla(string: String): String {
        val banglaNumber = arrayOf('১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯', '০')
        val engNumber = arrayOf('1', '2', '3', '4', '5', '6', '7', '8', '9', '0')
        var values = ""
        val character = string.toCharArray()
        for (i in character.indices) {
            var c: Char? = ' '
            for (j in engNumber.indices) {
                if (character[i] == engNumber[j]) {
                    c = banglaNumber[j]
                    break
                } else {
                    c = character[i]
                }
            }
            values += c!!
        }
        return values
    }

    fun getRealPathFromURI(context: Context, contentUri: Uri): String? {
        var res: String? = null
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = context.contentResolver.query(contentUri, proj, null, null, null)
        cursor?.let {
            if (cursor.moveToFirst()) {
                val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                res = cursor.getString(columnIndex)
            }
            cursor.close()
        }
        return res

    }

//    fun getBengaliMonth(month: String): String {
//        val bengaliMonths = arrayOf(
//            "জানুয়ারি",
//            "ফেব্রুয়ারী",
//            "মার্চ",
//            "এপ্রিল",
//            "মে",
//            "জুন",
//            "জুলাই",
//            "আগস্ট",
//            "সেপ্টেম্বর",
//            "অক্টোবর",
//            "নভেম্বর",
//            "ডিসেম্বর"
//        )
//        val englishMonth = arrayOf("JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC")
//        return ""
//    }
}

/**
 * 1.  No permission needed for ACTION_DIAL but permission needed for ACTION_CALL
 *
 * */