package com.shobardhaka.citizenrevenue.utils

import android.app.ActivityManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Build
import android.util.Patterns
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.data.prefs.PreferenceManager
import com.shobardhaka.citizenrevenue.ui.auth.login.LoginActivity
import com.shobardhaka.citizenrevenue.ui.complaints.complaint_details.ComplaintDetailsActivity
import com.shobardhaka.citizenrevenue.ui.home.HomeActivity
import com.shobardhaka.citizenrevenue.ui.notification.NotificationActivity
import com.shobardhaka.citizenrevenue.utils.Keys.NotificationType
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.*

class NotificationUtils(private val context: Context) {

    private var mNotificationManager: NotificationManager? = null
    private var mBuilder: NotificationCompat.Builder? = null
    private val notificationChannelID: String = context.applicationContext.packageName
    private val notificationChannelName: String = context.applicationContext.packageName
    private var complainId = ""

    private val notificationManager: NotificationManager?
        get() {
            if (mNotificationManager == null) {
                mNotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    val mChannel = NotificationChannel(
                        notificationChannelID,
                        notificationChannelName,
                        NotificationManager.IMPORTANCE_HIGH
                    )
                    mChannel.enableLights(true)
                    mChannel.setShowBadge(false)
                    mChannel.enableVibration(true)
                    mNotificationManager!!.createNotificationChannel(mChannel)
                }
            }
            return mNotificationManager
        }

    private val notificationId: Int
        get() = (Date().time / 1000L % Integer.MAX_VALUE).toInt()

    /*--------------------------------------------------
        BEFORE STARTED
    -------------------------------------------------------*/

    private fun buildNotification(NOTIFICATION_ID: Int) {
        notificationManager?.let {
            notificationManager!!.notify(NOTIFICATION_ID, mBuilder!!.build())
        }
    }

    private fun getLaunchIntent(notificationId: Int, notificationType: String): PendingIntent {
        val intent: Intent
        val mPrefs = PreferenceManager(context)
        if (mPrefs.getBoolean(Keys.KEEP_ME_LOGGED_IN.name, false)) {
            when (notificationType) {
                "notification" -> {
                    intent = Intent(context, NotificationActivity::class.java)
                    intent.putExtra(Keys.NOTIFCIATION_TYPE.name, NotificationType.notification.name)
                }
                "disaster" -> {
                    intent = Intent(context, NotificationActivity::class.java)
                    intent.putExtra(Keys.NOTIFCIATION_TYPE.name, NotificationType.disaster.name)
                }
                "complain"-> {
                    intent = Intent(context, ComplaintDetailsActivity::class.java)
                    intent.putExtra(Keys.COMPLAINT_ID.name, complainId )
                }
                else ->
                    intent = Intent(context, HomeActivity::class.java)
            }
        } else {
            intent = Intent(context, LoginActivity::class.java)
        }

        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra("notificationId", notificationId)
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT)
    }

    /*--------------------------------------------------
        BIG PICTURE STYLE NOTIFICATION
      -------------------------------------------------------*/

    private fun bigPictureStyleNotification(
        notificationId: Int,
        title: String?,
        bitmap: Bitmap?
    ) {
        mBuilder?.let {
            mBuilder!!.setContentTitle(title)
            mBuilder!!.setStyle(NotificationCompat.BigPictureStyle().bigPicture(bitmap))
            buildNotification(notificationId)
        }
    }


    /*--------------------------------------------------
        BIG TEXT STYLE NOTIFICATION
      -------------------------------------------------------*/

    private fun bigTextStyleNotification(
        notificationId: Int,
        title: String?,
        message: String?
    ) {

        mBuilder?.let {
            mBuilder!!.setContentTitle(title)
            mBuilder!!.setStyle(NotificationCompat.BigTextStyle().bigText(message))
            buildNotification(notificationId)
        }
    }

    /*--------------------------------------------------
        INBOX STYLE NOTIFICATION
      -------------------------------------------------------*/

    private fun inboxStyleNotification(
        notificationId: Int,
        title: String,
        summeryText: String,
        messageList: ArrayList<String>?
    ) {

        if (messageList == null || messageList.isEmpty())
            return


        val inboxStyle = NotificationCompat.InboxStyle()
        inboxStyle.setSummaryText(summeryText)
        for (singleMsg in messageList) {
            inboxStyle.addLine(singleMsg)
        }
        mBuilder!!.setContentTitle(title)
        mBuilder!!.setStyle(inboxStyle)
        buildNotification(notificationId)
    }

    /*--------------------------------------------------
        SHOW NOTIFICATION PUBLIC
     -------------------------------------------------------*/

    fun showNotification(
        notificationType: String,
        smallIcon: Int,
        title: String?,
        message: String?,
        imageUrl: String?,
        complainId: String
    ) {
        this.complainId = complainId
        if (message == null || message.isEmpty())
            return
        val NOTIFICATION_ID = notificationId
        mBuilder = NotificationCompat.Builder(context.applicationContext, notificationChannelID)
        mBuilder!!.setSmallIcon(smallIcon)
        mBuilder!!.setAutoCancel(true)
        mBuilder!!.color = ContextCompat.getColor(context, R.color.colorPrimary)
        mBuilder!!.setContentIntent(getLaunchIntent(NOTIFICATION_ID, notificationType))
        mBuilder!!.setLargeIcon(
            BitmapFactory.decodeResource(
                context.resources,
                R.drawable.notificaiton_blue
            )
        )


        if (imageUrl != null && imageUrl.length > 4 && Patterns.WEB_URL.matcher(imageUrl).matches()) {

            val imageDownloader = ImageDownloader(imageUrl, object : ImageDownloadListener {

                override fun onDownloadedImage(bitmap: Bitmap?) {
                    if (bitmap != null) {
                        bigPictureStyleNotification(NOTIFICATION_ID, title, bitmap)
                    } else {
                        bigTextStyleNotification(NOTIFICATION_ID, title, message)
                    }
                }
            })
            imageDownloader.execute()
        } else {
            bigTextStyleNotification(NOTIFICATION_ID, title, message)
        }
    }

    internal fun isAppIsInBackground(context: Context): Boolean {
        var isInBackground = true
        val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            val runningProcesses = am.runningAppProcesses
            for (processInfo in runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (activeProcess in processInfo.pkgList) {
                        if (activeProcess == context.packageName) {
                            isInBackground = false
                        }
                    }
                }
            }
        } else {
            val taskInfo = am.getRunningTasks(1)
            val componentInfo = taskInfo[0].topActivity
            if (componentInfo?.packageName == context.packageName) {
                isInBackground = false
            }
        }

        return isInBackground
    }


    internal class ImageDownloader(private val imageUrl: String, private val downloadListener: ImageDownloadListener) :
        AsyncTask<String, Void, Bitmap>() {

        override fun doInBackground(vararg params: String): Bitmap? {
            try {
                val url = URL(imageUrl)
                val inputStream: InputStream
                val connection = url.openConnection() as HttpURLConnection
                connection.doInput = true
                connection.connect()
                inputStream = connection.inputStream
                return BitmapFactory.decodeStream(inputStream)

            } catch (e: MalformedURLException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            return null
        }

        override fun onPostExecute(bitmap: Bitmap) {
            downloadListener.onDownloadedImage(bitmap)
        }
    }

    interface ImageDownloadListener {

        fun onDownloadedImage(bitmap: Bitmap?)
    }

}
