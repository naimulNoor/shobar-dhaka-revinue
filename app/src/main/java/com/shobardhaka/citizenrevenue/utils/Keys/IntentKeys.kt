package com.shobardhaka.citizenrevenue.utils.Keys

object IntentKeys{
    const val COMPLAIN_TYPE: String = "complain_type"
    const val LOCATION_ACTION = "current_location"
    const val LONGITUDE = "longitude"
    const val LATITUDE = "latitude"
    const val DATA_BUNDLE = "data_bundle"
    const val MOBILE_NUMBER = "mobile_number"
}