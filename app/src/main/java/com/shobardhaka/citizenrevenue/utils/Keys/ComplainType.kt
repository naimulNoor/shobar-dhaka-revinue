package com.shobardhaka.citizenrevenue.utils.Keys

enum class ComplainType{
    ALL,
    SOLVED,
    UNSOLVED
}