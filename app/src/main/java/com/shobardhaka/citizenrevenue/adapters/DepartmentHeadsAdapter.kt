package com.shobardhaka.citizenrevenue.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BaseRecyclerAdapter
import com.shobardhaka.citizenrevenue.base.BaseViewHolder
import com.shobardhaka.citizenrevenue.callbacks.CallButtonClickListener
import com.shobardhaka.citizenrevenue.data.network.api_response.GetDepartmentalHeadsResponse
import com.shobardhaka.citizenrevenue.file.PicassoImageLoader
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import kotlinx.android.synthetic.main.item_view_department_heads.view.*

class DepartmentHeadsAdapter(
    context: Context,
    val callButtonClickListener: CallButtonClickListener
) : BaseRecyclerAdapter<GetDepartmentalHeadsResponse.SingleHeads>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val view = layoutInflater.inflate(R.layout.item_view_department_heads, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        if (holder is ItemViewHolder) {
            holder.bind(position)
        }
    }

    inner class ItemViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun bind(position: Int) {
            val singleHeads: GetDepartmentalHeadsResponse.SingleHeads =
                dataList[position] as GetDepartmentalHeadsResponse.SingleHeads
            itemView.tvDepartmentHeadName.text = singleHeads.name
            itemView.tvDepartmentHeadsDesignation.text = singleHeads.designation
            singleHeads.departmentalHeadImage?.let {
                PicassoImageLoader.shared.loadImage(
                    context,
                    singleHeads.departmentalHeadImage,
                    itemView.imgDepartmentHead,
                    R.drawable.profile
                )
            }
            val address = "${context.getString(R.string.address)}: ${singleHeads.officeAddress}"
            itemView.tvDepartmentHeadsAddress.text = address
            if (singleHeads.phone != null) {
                val mobile =
                    if (mAppLanguage == AppLanguage.BENGALI.name) {
                        "${context.getString(R.string.mobile)}: ${AppUtils.shared.getInBangla(singleHeads.phone)}"
                    } else {
                        "${context.getString(R.string.mobile)}: ${singleHeads.phone}"
                    }
                itemView.tvDepartmentHeadsPhoneNumber.text = mobile
                itemView.btnCall.setOnClickListener {
                    callButtonClickListener.onCallButtonClicked(singleHeads.phone)
                }
            }
        }
    }
}