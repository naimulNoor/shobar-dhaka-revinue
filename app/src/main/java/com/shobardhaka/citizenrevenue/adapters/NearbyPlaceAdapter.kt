package com.shobardhaka.citizenrevenue.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BaseRecyclerAdapter
import com.shobardhaka.citizenrevenue.base.BaseViewHolder
import com.shobardhaka.citizenrevenue.data.network.api_response.BariKoiResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import kotlinx.android.synthetic.main.item_view_nearby_place.view.*
import java.text.DecimalFormat

class NearbyPlaceAdapter(context: Context, val placeTypeIcon: Int) :
    BaseRecyclerAdapter<BariKoiResponse.SinglePlace>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val view = layoutInflater.inflate(R.layout.item_view_nearby_place, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        if (holder is ItemViewHolder) {
            holder.bind(position)
        }
    }

    inner class ItemViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun bind(position: Int) {
            val singlePlace: BariKoiResponse.SinglePlace = dataList[position] as BariKoiResponse.SinglePlace
            itemView.tvNearbyPlaceName.text = singlePlace.name
            itemView.tvNearbyPlaceAddress.text = singlePlace.address
            val disInKm = singlePlace.distanceInMeters / 1000
            var distanceStr = DecimalFormat("##.##").format(disInKm)
            if (mPrefs?.getString(Keys.APP_LANGUAGE.name) == AppLanguage.BENGALI.name) {
                distanceStr = AppUtils.shared.getInBangla(distanceStr)
            }
            val distance = "${context.getString(R.string.distance)} $distanceStr ${context.getString(R.string.km)}"
            itemView.tvNearbyPlaceDistance.text = distance
        }
    }
}