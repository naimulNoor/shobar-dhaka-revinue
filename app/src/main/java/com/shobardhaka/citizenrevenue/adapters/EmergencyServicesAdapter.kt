package com.shobardhaka.citizenrevenue.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BaseRecyclerAdapter
import com.shobardhaka.citizenrevenue.base.BaseViewHolder
import com.shobardhaka.citizenrevenue.callbacks.CallButtonClickListener
import com.shobardhaka.citizenrevenue.data.network.api_response.GetEmergencyServiceResponse
import com.shobardhaka.citizenrevenue.data.prefs.Keys
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import kotlinx.android.synthetic.main.itemview_emergency_service.view.*

/**
 * Created on 19/08/2019.
 */

class EmergencyServicesAdapter(context: Context, val callButtonClickListener: CallButtonClickListener) :
    BaseRecyclerAdapter<GetEmergencyServiceResponse.SingleEmergencyService>(context) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val view = layoutInflater.inflate(R.layout.itemview_emergency_service, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        if (holder is ItemViewHolder) {
            holder.bind(position)
        }
    }

    inner class ItemViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun bind(position: Int) {
            val singleEmergencyService: GetEmergencyServiceResponse.SingleEmergencyService =
                dataList[position] as GetEmergencyServiceResponse.SingleEmergencyService

            itemView.tvEmergencyServiceName.text = singleEmergencyService.name
            itemView.tvEmergencyServiceNumber.text =
                    if (mPrefs?.getString(Keys.APP_LANGUAGE.name) == AppLanguage.BENGALI.name)
                        AppUtils.shared.getInBangla(singleEmergencyService.number)
                    else
                        singleEmergencyService.number

            itemView.btnCall.setOnClickListener {
                callButtonClickListener.onCallButtonClicked(singleEmergencyService.number)
            }
        }
    }

}

