package com.shobardhaka.citizenrevenue.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BaseRecyclerAdapter
import com.shobardhaka.citizenrevenue.base.BaseViewHolder
import com.shobardhaka.citizenrevenue.data.network.APIs
import com.shobardhaka.citizenrevenue.data.network.api_response.GetComplainResponse
import com.shobardhaka.citizenrevenue.file.PicassoImageLoader
import com.shobardhaka.citizenrevenue.utils.DateFormatUtils
import kotlinx.android.synthetic.main.item_view_my_complaint.view.*

class MyComplaintsAdapter(context: Context) : BaseRecyclerAdapter<GetComplainResponse.SingleComplain>(context) {

    override fun getItemViewType(position: Int): Int {
        dataList[position]?.let {
            return VIEW_TYPE_ITEM
        } ?: return VIEW_TYPE_FOOTER
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return if (viewType == VIEW_TYPE_FOOTER) {
            super.getFooterViewHolder(parent)
        } else {
            ItemViewHolder(layoutInflater.inflate(R.layout.item_view_my_complaint, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        if (holder is ItemViewHolder) {
            holder.bind(position)
        }
    }

    inner class ItemViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun bind(position: Int) {
            val complain: GetComplainResponse.SingleComplain = dataList[position] as GetComplainResponse.SingleComplain
            val categoryId = complain.complainCategory.categoryId
            when (categoryId) {
                1 -> {
                    itemView.tvComplaintType.text = context.getString(R.string.rasta)
                }
                2 -> {
                    itemView.tvComplaintType.text = context.getString(R.string.mosha)
                }
                3 -> {
                    itemView.tvComplaintType.text = context.getString(R.string.aborjona)
                }
                4 -> {
                    itemView.tvComplaintType.text = context.getString(R.string.bati)
                }
                5 -> {
                    itemView.tvComplaintType.text = context.getString(R.string.public_toilet)
                }
                6 -> {
                    itemView.tvComplaintType.text = context.getString(R.string.nordoma)
                }
                7 -> {
                    itemView.tvComplaintType.text = context.getString(R.string.oboidho_sthapona)
                }
                8 -> {
                    itemView.tvComplaintType.text = context.getString(R.string.pani)
                }
                9 -> {
                    itemView.tvComplaintType.text = context.getString(R.string.holding)
                }
                41 -> {
                    itemView.tvComplaintType.text = context.getString(R.string.trade)
                }
                42 -> {
                    itemView.tvComplaintType.text = context.getString(R.string.signboard)
                }
            }
            itemView.tvComplaintDetails.text = complain.description
            itemView.tvComplaintLocation.text = complain.location

            if (complain.status == "3" || complain.status == "-1") {
                itemView.imgComplainStatus.setImageResource(R.drawable.ic_complain_accept)
            } else {
                itemView.imgComplainStatus.setImageResource(R.drawable.ic_complain_ongoing)
            }

            val date = DateFormatUtils.shared.getDate(complain.createdAt)
            itemView.tvComplainDate.text = date
            val imageUrl = "${APIs.ASSET_PREFIX}${complain.image}"
            PicassoImageLoader.shared.loadImage(context, imageUrl, itemView.imgComplaint)
        }
    }
}