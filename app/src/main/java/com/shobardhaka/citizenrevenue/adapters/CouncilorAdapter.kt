package com.shobardhaka.citizenrevenue.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BaseRecyclerAdapter
import com.shobardhaka.citizenrevenue.base.BaseViewHolder
import com.shobardhaka.citizenrevenue.callbacks.CallButtonClickListener
import com.shobardhaka.citizenrevenue.data.network.api_response.GetCouncilorResponse
import com.shobardhaka.citizenrevenue.file.PicassoImageLoader
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import kotlinx.android.synthetic.main.item_view_councilor.view.*

class CouncilorAdapter(
    context: Context,
    val callButtonClickListener: CallButtonClickListener
) : BaseRecyclerAdapter<GetCouncilorResponse.SingleCouncilor>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val view = layoutInflater.inflate(R.layout.item_view_councilor, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        if (holder is ItemViewHolder) {
            holder.bind(position)
        }
    }

    inner class ItemViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun bind(position: Int) {
            val singleCouncilor: GetCouncilorResponse.SingleCouncilor =
                dataList[position] as GetCouncilorResponse.SingleCouncilor
            itemView.tvCouncilorName.text = singleCouncilor.name
            itemView.tvCouncilorDesignation.text = singleCouncilor.designation
            singleCouncilor.councilorImage?.let {
                PicassoImageLoader.shared.loadImage(
                    context,
                    singleCouncilor.councilorImage,
                    itemView.imgCouncilor,
                    R.drawable.profile
                )
            }
            val ward =
                if (mAppLanguage == AppLanguage.BENGALI.name) {
                    "${context.getString(R.string.ward_no)} - ${AppUtils.shared.getInBangla(singleCouncilor.word)}"
                } else {
                    "${context.getString(R.string.ward_no)} - ${singleCouncilor.word}"
                }
            itemView.tvCouncilorWard.text = ward
            if (singleCouncilor.phone != null) {
                itemView.btnCall.visibility = View.VISIBLE
                val mobile =
                    if (mAppLanguage == AppLanguage.BENGALI.name) {
                        "${context.getString(R.string.mobile)}: ${AppUtils.shared.getInBangla(singleCouncilor.phone)}"
                    } else {
                        "${context.getString(R.string.mobile)}: ${singleCouncilor.phone}"
                    }
                itemView.tvCouncilorPhoneNumber.text = mobile
                itemView.btnCall.setOnClickListener {
                    callButtonClickListener.onCallButtonClicked(singleCouncilor.phone)
                }
            } else {
                itemView.btnCall.visibility = View.GONE
            }
            val mpName = singleCouncilor.mp
            if (mpName != null) {
                val mp = "${context.getString(R.string.honourable_member_of_perlament)} - $mpName"
                itemView.tvCouncilorMp.text = mp
            } else {
                itemView.tvCouncilorMp.text = null
            }
        }
    }
}