package com.shobardhaka.citizenrevenue.adapters


import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter


class AjkerDhakaPagerAdapter(fragmentManager: androidx.fragment.app.FragmentManager): FragmentStatePagerAdapter(fragmentManager){

    private var fragments = ArrayList<Fragment>()

    fun addFragment(fragment: Fragment){
        fragments.add(fragment)
    }

    override fun getItem(position: Int): Fragment {
       return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }
}