package com.shobardhaka.citizenrevenue.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BaseRecyclerAdapter
import com.shobardhaka.citizenrevenue.base.BaseViewHolder
import com.shobardhaka.citizenrevenue.data.network.api_response.GetEventResponse
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import kotlinx.android.synthetic.main.item_view_event.view.*

class EventsAdapter(context: Context) : BaseRecyclerAdapter<GetEventResponse.SingleEvent>(context) {

    override fun getItemViewType(position: Int): Int {
        dataList[position]?.let {
            return VIEW_TYPE_ITEM
        } ?: return VIEW_TYPE_FOOTER
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return if (viewType == VIEW_TYPE_FOOTER) {
            super.getFooterViewHolder(parent)
        } else {
            ItemViewHolder(layoutInflater.inflate(R.layout.item_view_event, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        if (holder is ItemViewHolder) {
            holder.bind(position)
        }
    }

    inner class ItemViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun bind(position: Int) {
            val singleEvent: GetEventResponse.SingleEvent = dataList[position] as GetEventResponse.SingleEvent
            itemView.tvEventMonth.text = singleEvent.eventMonth
            itemView.tvEventDay.text =
                    if (mAppLanguage == AppLanguage.BENGALI.name) AppUtils.shared.getInBangla(singleEvent.eventDay)
                    else singleEvent.eventDay
            itemView.tvEventTitle.text = singleEvent.title
            itemView.tvEventDetails.text = singleEvent.description

            val category = "${context.getString(R.string.category)}: ${singleEvent.category.name}"
            itemView.tvCategory.text = category
        }
    }
}
