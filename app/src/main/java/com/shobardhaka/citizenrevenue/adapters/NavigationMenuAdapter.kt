package com.shobardhaka.citizenrevenue.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.TextView
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.model.NavigationMenu

/**
 * Created  on 21/08/2019.
 */
class NavigationMenuAdapter : BaseExpandableListAdapter() {

    private var navigationMenus: ArrayList<NavigationMenu> = ArrayList()

    fun setData(navigationMenus: ArrayList<NavigationMenu>) {
        this.navigationMenus = navigationMenus
        notifyDataSetChanged()
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getChild(groupPosition: Int, childPosition: Int): String {
        return navigationMenus[groupPosition].submenus[childPosition]
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        if (view == null) {
            view = LayoutInflater.from(parent.context).inflate(R.layout.list_group_child, parent, false)
        }
        val tvSubMenu = view!!.findViewById<TextView>(R.id.lblListItem)
        tvSubMenu.text = getChild(groupPosition, childPosition)
        return view
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return navigationMenus[groupPosition].submenus.size
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getGroup(groupPosition: Int): Any {
        return navigationMenus[groupPosition]
    }

    override fun getGroupCount(): Int {
        return navigationMenus.size
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        val menu = getGroup(groupPosition) as NavigationMenu
        if (view == null) {
            view = LayoutInflater.from(parent.context).inflate(R.layout.list_group_header, parent, false)
        }
        val tvMenu = view!!.findViewById<TextView>(R.id.lblListHeader)
        val imgIcon = view.findViewById<ImageView>(R.id.imgIcon)
        val imgGroupIndicator = view.findViewById<ImageView>(R.id.imgGroupIndicator)
        tvMenu.text = menu.menuName
        imgIcon.setImageResource(menu.menuIcon)
        if (menu.submenus.isEmpty()) {
            imgGroupIndicator.visibility = View.GONE
        } else {
            if (isExpanded) {
                imgGroupIndicator.setImageResource(R.drawable.ic_arrow_up)
            } else {
                imgGroupIndicator.setImageResource(R.drawable.ic_arrow_down)
            }
            imgGroupIndicator.visibility = View.VISIBLE
        }
        return view
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }
}