package com.shobardhaka.citizenrevenue.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BaseRecyclerAdapter
import com.shobardhaka.citizenrevenue.base.BaseViewHolder
import com.shobardhaka.citizenrevenue.data.network.api_response.NotificationResponse
import com.shobardhaka.citizenrevenue.data.prefs.PreferenceManager
import com.shobardhaka.citizenrevenue.utils.DateFormatUtils
import kotlinx.android.synthetic.main.item_view_notification.view.*

class NotificationsAdapter(context: Context) : BaseRecyclerAdapter<NotificationResponse.SingleNotification>(context) {

    private var mPres: PreferenceManager = PreferenceManager(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val view = layoutInflater.inflate(R.layout.item_view_notification, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        if (holder is ItemViewHolder) {
            holder.bind(position)
        }
    }

    inner class ItemViewHolder(itemView: View) : BaseViewHolder(itemView) {

        override fun bind(position: Int) {
            val singleNotification: NotificationResponse.SingleNotification =
                dataList[position] as NotificationResponse.SingleNotification
            val notTitle = singleNotification.title
            val notDetails = singleNotification.description
            val date = DateFormatUtils.shared.getDate(singleNotification.createdAt)

            itemView.tvNotificationTitle.text = notTitle
            if (singleNotification.isRead == 0) {
                itemView.tvNotificationTitle.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                itemView.imgNotificationSeenUnseen.setImageResource(R.drawable.icon_notification_unseen)
            } else {
                itemView.tvNotificationTitle.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray))
                itemView.imgNotificationSeenUnseen.setImageResource(R.drawable.icon_notification_seen)
            }
            itemView.tvNotificationDetails.text = notDetails
            itemView.tvNotificationDate.text = date
        }
    }
}