package com.shobardhaka.citizenrevenue.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.shobardhaka.citizenrevenue.R
import com.shobardhaka.citizenrevenue.base.BaseRecyclerAdapter
import com.shobardhaka.citizenrevenue.base.BaseViewHolder
import com.shobardhaka.citizenrevenue.data.network.api_response.GetDisasterResponse
import com.shobardhaka.citizenrevenue.file.PicassoImageLoader
import com.shobardhaka.citizenrevenue.utils.AppUtils
import com.shobardhaka.citizenrevenue.utils.DateFormatUtils
import com.shobardhaka.citizenrevenue.utils.Keys.AppLanguage
import kotlinx.android.synthetic.main.item_view_disaster.view.*

class DisasterAdapter(context: Context, private val detailsBtnClickListener: OnDetailsBtnClickListener) :
    BaseRecyclerAdapter<GetDisasterResponse.SingleDisaster>(context) {

    override fun getItemViewType(position: Int): Int {
        dataList[position]?.let {
            return VIEW_TYPE_ITEM
        } ?: return VIEW_TYPE_FOOTER
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return if (viewType == VIEW_TYPE_FOOTER) {
            super.getFooterViewHolder(parent)
        } else {
            ItemViewHolder(layoutInflater.inflate(R.layout.item_view_disaster, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        if (holder is ItemViewHolder) {
            holder.bind(position)
        }
    }

    inner class ItemViewHolder(itemView: View) : BaseViewHolder(itemView), View.OnClickListener {

        override fun bind(position: Int) {
            val singleDisaster: GetDisasterResponse.SingleDisaster =
                dataList[position] as GetDisasterResponse.SingleDisaster

            val date = if (mAppLanguage == AppLanguage.BENGALI.name)
                "${AppUtils.shared.getInBangla(singleDisaster.disasterDay)}, ${singleDisaster.disasterMonth}"
            else
                "${singleDisaster.disasterDay}, ${singleDisaster.disasterMonth}"
            itemView.tvDisasterDate.text = DateFormatUtils.shared.getDate(singleDisaster.createdAt)
            itemView.tvDisasterTitle.text = singleDisaster.title
            if (singleDisaster.isRead == 0) {
                itemView.tvDisasterTitle.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
            } else {
                itemView.tvDisasterTitle.setTextColor(ContextCompat.getColor(context, android.R.color.darker_gray))
            }
            itemView.tvDisasterDetails.text = singleDisaster.description
            PicassoImageLoader.shared.loadImage(
                context,
                singleDisaster.image,
                itemView.imgDisaster
            )
            itemView.btnDisasterDetails.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            detailsBtnClickListener.detailsBtnDidClicked(adapterPosition)
        }
    }

    interface OnDetailsBtnClickListener {
        fun detailsBtnDidClicked(position: Int)
    }
}
