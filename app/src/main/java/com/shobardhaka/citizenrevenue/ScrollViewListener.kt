package com.shobardhaka.citizenrevenue

interface ScrollViewListener {

    fun onScrollChanged(scrollView: SSHorizontalScrollView, x: Int, y: Int, oldx: Int, oldy: Int)

}